package sdt.osgi.springboot.dvcqgdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGThuTucHanhChinhDTO {

    private String maTTHC;
    private String tenTTHC;
    private String maCoQuanCongBo;
    private List<DVCQGNhomTTHCChiTietDTO> linhVucThucHien;
    private List<DVCQGCapCoQuanThucHienDTO> capThucHien;
    private List<DVCQGTrinhTuThucHienDTO> trinhTuThucHien;
    private List<DVCQGCachThucThucHienDTO> cachThucThucHien;
    private List<DVCQGThanhPhanHoSoDTO> thanhPhanHoSo;
    private List<DVCQGDoiTuongThucHienDTO> doiTuongThucHien;
    private String moTaDoiTuongThucHien;
    private List<DVCQGCoQuanThucHienDTO> coQuanThucHien;
    private List<DVCQGCoQuanThucHienDTO> coQuanCoThamQuyen;
    private List<DVCQGCoQuanThucHienDTO> coQuanDuocUyQuyen;
    private String diaChiTiepNhan;
    private List<DVCQGCoQuanThucHienDTO> coQuanPhoiHop;
    private List<DVCQGKetQuaThucHienDTO> ketQuaThucHien;
    private List<DVCQGCanCuPhapLyDTO> canCuPhapLy;
    private String yeuCau;
    private String idQuyetDinhCongBo;
    private Byte trangThai;
}
