package sdt.osgi.springboot.dvcqgdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCDongBoDonViDTO {

    private int id;
    private String maDonVi;
    private String tenDonVi;
    private int capDonVi;
    private int idCoQuanChuQuan;
    private String diaChi;
}
