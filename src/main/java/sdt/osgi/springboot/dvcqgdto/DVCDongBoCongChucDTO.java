package sdt.osgi.springboot.dvcqgdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCDongBoCongChucDTO {

    private long id;
    private String hoTen;
    private String dienThoai;
    private String email;
    private String gioiTinh;
    private String ngaySinh;
    private String soCmnd;
    private String ngayCapCmnd;
    private String noiCapCmnd;
    private int idChucVu;
    private String chucVu;
    private int idDonVi;
    private String donVi;
    private int idPhongBan;
    private String phongBan;
    private String anhThe;

}
