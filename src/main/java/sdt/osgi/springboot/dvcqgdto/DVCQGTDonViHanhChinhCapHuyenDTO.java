package sdt.osgi.springboot.dvcqgdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGTDonViHanhChinhCapHuyenDTO {

    private String maSoCapTinh;
    private String maSoCapQuanHuyenThiXa;
    private String tenDonViHanhChinh;
    private long capDonViHanhChinhId = 2;
}
