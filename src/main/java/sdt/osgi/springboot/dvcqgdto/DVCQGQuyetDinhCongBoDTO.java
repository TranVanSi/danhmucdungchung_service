package sdt.osgi.springboot.dvcqgdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGQuyetDinhCongBoDTO {
    private String quyetDinhCongBoId;
    private String soQuyetDinh;
    private String ngayQuyetDinh;

}
