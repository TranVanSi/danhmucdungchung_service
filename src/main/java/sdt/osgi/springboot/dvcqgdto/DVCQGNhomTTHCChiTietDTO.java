package sdt.osgi.springboot.dvcqgdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGNhomTTHCChiTietDTO {
    private String maLinhVuc;
    public String tenLinhVuc;
    public String maNganh;
    private int loaiNhomTTHC = 2;
}
