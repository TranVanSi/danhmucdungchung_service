package sdt.osgi.springboot.dvcqgdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DongBoChucVuDTO {

    private int id;
    private String tenChucVu;
    private String heSo;
}
