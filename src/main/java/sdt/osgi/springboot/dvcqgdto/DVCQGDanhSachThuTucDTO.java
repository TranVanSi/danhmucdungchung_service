package sdt.osgi.springboot.dvcqgdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGDanhSachThuTucDTO {
    private String maTTHC;
    private String tenTTHC;
    private String maCoQuanCongBo;

}
