package sdt.osgi.springboot.dvcqgdto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGTDonViHanhChinhCapTinhDTO {

    private String maSoCapTinh;
    private String tenDonViHanhChinh;
    private long capDonViHanhChinhId = 1;

}
