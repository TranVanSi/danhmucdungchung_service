package sdt.osgi.springboot.dvcqgdto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGDanTocDTO {

    private String maDanToc;
    private String tenDanToc;
    private String qdBanHanhQdSuaDoi;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date ngayBanHanhQd;
    private String coQuanBanHanhQd;
}
