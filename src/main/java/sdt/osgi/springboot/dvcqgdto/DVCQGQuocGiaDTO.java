package sdt.osgi.springboot.dvcqgdto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGQuocGiaDTO {

    private String maQuocGia;
    private String tenQuocGiaDayDuBangTiengViet;
    private String maAlpha_3;
    private String tenQuocGiaDayDuBangTiengAnh;
    private String tenQuocGiaVietGonBangTiengViet;
    private String tenQuocGiaVietGonBangTiengAnh;
    private String cacNgonNguHanhChinhAlpha_2;
    private String cacNgonNguHanhChinhAlpha_3;
    private String cacTenDiaPhuongVietGon;
    private String nuocDocLap;
    private String qdBanHanhQdSuaDoi;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date ngayBanHanhQd;
    private String coQuanBanHanhQd;
}
