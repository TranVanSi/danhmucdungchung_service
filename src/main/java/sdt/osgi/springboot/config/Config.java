package sdt.osgi.springboot.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.DefaultUriTemplateHandler;
import sdt.osgi.springboot.mapper.QuocTichMapper;
import sdt.osgi.springboot.mapper.TinhTrangHonNhanMapper;

@Configuration
@RequiredArgsConstructor
public class Config {

    private final Environment env;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean("danhMucDungChungRest")
    public RestTemplate danhMucDungChungRestTemplate() {
        String rootURL = env.getProperty("api.v1.url.danhmucdungchung");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("dichVuCongQuocGiaRest")
    public RestTemplate dichVuCongQuocGiaRestTemplate() {
        String rootURL = env.getProperty("dvcqg.adapterUrl");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("tinhTrangHonNhanMapper")
    public TinhTrangHonNhanMapper getTinhTrangHonNhanMapper() {
        return Mappers.getMapper(TinhTrangHonNhanMapper.class);
    }

    @Bean("objectMapper")
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


        return objectMapper;
    }

    @Bean("quocTichMapper")
    public QuocTichMapper getQuocTichMapper() {
        return Mappers.getMapper(QuocTichMapper.class);
    }

    @Bean("dmdcRest")
    public RestTemplate templateLGSPRest() {
        String rootURL = env.getProperty("api.ngsp.url");

        RestTemplate restClient = new RestTemplate();
        restClient.setUriTemplateHandler(new DefaultUriBuilderFactory(rootURL));

        return restClient;
    }

    @Bean("canBoCongChucRest")
    public RestTemplate canBoCongChucRestTemplate() {
        String rootURL = env.getProperty("cbcc.url");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }

    @Bean("quanLyNguoiDungRest")
    public RestTemplate taiNguyenRestTemplate() {
        String rootURL = env.getProperty("api.v1.url.quanlynguoidung");

        RestTemplate restClient = new RestTemplate();
        DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
        uriTemplateHandler.setBaseUrl(rootURL);
        restClient.setUriTemplateHandler(uriTemplateHandler);

        return restClient;
    }
}
