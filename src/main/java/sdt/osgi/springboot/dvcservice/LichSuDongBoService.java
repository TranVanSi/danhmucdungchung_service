package sdt.osgi.springboot.dvcservice;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.LichSuDongBoRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.LichSuDongBoMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class LichSuDongBoService {

    private final LichSuDongBoRepository lichSuDongBoRepository;
    private final LichSuDongBoMapper lichSuDongBoMapper;

    public LichSuDongBo luuLichSuXoaDuLieu(String tenLopDongBo, Date thoiDiemXoaDuLieu) {
        LichSuDongBo lichSuDongBo = new LichSuDongBo();
        lichSuDongBo.setLopDongBo(tenLopDongBo);
        lichSuDongBo.setThoiGianXoaDuLieuCu(thoiDiemXoaDuLieu);
        lichSuDongBo.setTrangThaiDongBo(TrangThaiDongBo.DELETED);

        return lichSuDongBoRepository.save(lichSuDongBo);
    }

    public LichSuDongBo capNhatThoiGianDongBo(LichSuDongBo lichSuDongBo, Date thoiDiemBatDau, Date thoiDiemKetThuc, int soLuongDongBo, TrangThaiDongBo trangThaiDongBo, int soLuongTruocDongBo) {
        if (thoiDiemBatDau != null) {
            lichSuDongBo.setThoiGianBatDauDongBo(thoiDiemBatDau);
        }

        if (thoiDiemKetThuc != null) {
            lichSuDongBo.setThoiGianKetThucDongBo(thoiDiemKetThuc);
        }

        lichSuDongBo.setSoLuongDongBo(soLuongDongBo);
        lichSuDongBo.setTrangThaiDongBo(trangThaiDongBo);
        lichSuDongBo.setSoLuongTruocDongBo(soLuongTruocDongBo);

        return lichSuDongBoRepository.save(lichSuDongBo);
    }


    public Response findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page / size, size);
        return new Response(
                Status.SUCCESS.value(),
                lichSuDongBoMapper.toLichSuDongBoDTOList(lichSuDongBoRepository.findAll(pageable).getContent()));

    }

    public Response count() {
        return new Response(
                Status.SUCCESS.value(),
                lichSuDongBoRepository.count());

    }
}
