package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.TinhTrangHonNhanRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGTinhTrangHonNhanDTO;
import sdt.osgi.springboot.mapper.TinhTrangHonNhanMapper;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.TinhTrangHonNhan;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DichVuCongTinhTrangHonNhanService {

    private final TinhTrangHonNhanRepository tinhTrangHonNhanRepository;
    private final TinhTrangHonNhanMapper tinhTrangHonNhanMapper;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;

    public List<DVCQGTinhTrangHonNhanDTO> getListTinhTrangHonNhan() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.matinhtranghonnhan"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGTinhTrangHonNhanDTO>>() {
        });
    }

    public void dongBoDuLieuTinhTrangHonNhan() {
        try {
            List<DVCQGTinhTrangHonNhanDTO> tinhTrangHonNhanList = getListTinhTrangHonNhan();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) tinhTrangHonNhanRepository.count();


            if (tinhTrangHonNhanList != null) {
                deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(TinhTrangHonNhan.class.getName()), thoiDiemXoaDuLieu);

                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, tinhTrangHonNhanList.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);

                saveAllTinhTrangHonNhan(tinhTrangHonNhanList);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), tinhTrangHonNhanList.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll() {
        tinhTrangHonNhanRepository.deleteAll();
    }

    private void saveAllTinhTrangHonNhan(List<DVCQGTinhTrangHonNhanDTO> tinhTrangHonNhanDTOList) {
        tinhTrangHonNhanRepository.saveAll(tinhTrangHonNhanMapper.toDVCQGTinhTrangHonNhanList(tinhTrangHonNhanDTOList));
    }
}

