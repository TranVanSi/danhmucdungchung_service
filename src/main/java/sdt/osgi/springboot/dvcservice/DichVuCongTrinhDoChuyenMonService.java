package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.TrinhDoChuyenMonKyThuatRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGTrinhDoChuyenMonDTO;
import sdt.osgi.springboot.mapper.TrinhDoChuyenMonKyThuatMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.TrinhDoChuyenMonKyThuat;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DichVuCongTrinhDoChuyenMonService {

    private final TrinhDoChuyenMonKyThuatRepository trinhDoChuyenMonKyThuatRepository;
    private final TrinhDoChuyenMonKyThuatMapper trinhDoChuyenMonKyThuatMapper;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;

    public List<DVCQGTrinhDoChuyenMonDTO> getListTrinhDoChuyenMonKyThuat() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.trinhdochuyenmon"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGTrinhDoChuyenMonDTO>>() {
        });
    }

    public void dongBoDuLieuTrinhDoChuyenMonKyThuat() {
        try {
            List<DVCQGTrinhDoChuyenMonDTO> trinhDoChuyenMonKyThuatList = getListTrinhDoChuyenMonKyThuat();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) trinhDoChuyenMonKyThuatRepository.count();


            if (trinhDoChuyenMonKyThuatList != null) {
                deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(TrinhDoChuyenMonKyThuat.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, trinhDoChuyenMonKyThuatList.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllTrinhDoChuyenMonKyThuat(trinhDoChuyenMonKyThuatList);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), trinhDoChuyenMonKyThuatList.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll() {
        trinhDoChuyenMonKyThuatRepository.deleteAll();
    }

    private void saveAllTrinhDoChuyenMonKyThuat(List<DVCQGTrinhDoChuyenMonDTO> trinhDoChuyenMonKyThuatDTOList) {
        trinhDoChuyenMonKyThuatRepository.saveAll(trinhDoChuyenMonKyThuatMapper.toDVCQGTrinhDoChuyenMonList(trinhDoChuyenMonKyThuatDTOList));
    }
}

