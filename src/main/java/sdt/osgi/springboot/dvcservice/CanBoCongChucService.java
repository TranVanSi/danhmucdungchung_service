package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.CanBoCongChucClient;
import sdt.osgi.springboot.clients.ChucVuClient;
import sdt.osgi.springboot.dvcqgdto.DongBoChucVuDTO;
import sdt.osgi.springboot.mapper.ChucVuMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CanBoCongChucService {

    private final ObjectMapper objectMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final CanBoCongChucClient canBoCongChucClient;
    private final ChucVuClient chucVuClient;
    private final ChucVuMapper chucVuMapper;

    public List<DongBoChucVuDTO> getListChucVu(String task) throws IOException {
        String data = canBoCongChucClient.getDataByTask(task);
        return objectMapper.readValue(data, new TypeReference<List<DongBoChucVuDTO>>() {
        });
    }

    public void dongBoDuLieuChucVu() {
        try {
            List<DongBoChucVuDTO> dongBoChucVuDTOS = getListChucVu("dsChucvu");
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) chucVuClient.countAll();

            if (dongBoChucVuDTOS != null) {
                chucVuClient.deleteByMa("CBCC");
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty("sdt.osgi.springboot.model.ChucVu"), thoiDiemXoaDuLieu);

                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dongBoChucVuDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);

                saveAllChucVu(dongBoChucVuDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dongBoChucVuDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveAllChucVu(List<DongBoChucVuDTO> dongBoChucVuDTOS) throws IOException {
        for (int i = 0; i < dongBoChucVuDTOS.size() ; i++) {
            chucVuClient.addChucVu(
                    chucVuMapper.CHUC_VU_DTO(dongBoChucVuDTOS.get(i)));
        }
    }

}

