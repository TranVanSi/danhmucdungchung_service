package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.NhomThuTucHanhChinhRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGCapCoQuanThucHienDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGNhomTTHCChiTietDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGQuyetDinhCongBoDTO;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.*;
import sdt.osgi.springboot.model.NhomThuTucHanhChinh;
import sdt.osgi.springboot.util.Constants;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DichVuCongThuTucHanhChinhCustomService {

    @Value("${html.close.tagp}")
    private String closeTagP;
    @Value("${html.close.taga}")
    private String closeTagA;
    @Value("${html.space}")
    private String space;
    @Value("${html.space_two}")
    private String spaceTwo;
    @Value("${html.span.tagp}")
    private String spanTagP;
    @Value("${html.close.left}")
    private String closeLeft;
    @Value("${html.comma}")
    private String comma;

    @Value("${html.trinhtu}")
    private String htmlTrinhTu;

    @Value("${html.cachthucthuchien.case1}")
    private String cachThucThucHienCase1;
    @Value("${html.cachthucthuchien.case2}")
    private String cachThucThucHienCase2;
    @Value("${html.cachthucthuchien.case3}")
    private String cachThucThucHienCase3;
    @Value("${html.cachthucthuchien.default}")
    private String cachThucThucHienCaseDefault;
    @Value("${html.cachthucthuchien.thoihangiaiquyet}")
    private String cachThucThucHienThoiHanGiaiQuyet;
    @Value("${html.cachthucthuchien.phi}")
    private String cachThucThucHienPhi;
    @Value("${html.cachthucthuchien.sotien}")
    private String cachThucThucHienSoTien;
    @Value("${html.cachthucthuchien.mota}")
    private String cachThucThucHienMoTa;

    @Value("${html.thanhphanmacdinh.thanhphan}")
    private String thanhPhanMacDinhThanhPhan;
    @Value("${html.thanhphanmacdinh.tengiayto}")
    private String thanhPhanMacDinhTenGiayTo;
    @Value("${html.thanhphanmacdinh.soban}")
    private String thanhPhanMacDinhSoBan;
    @Value("${html.thanhphanmacdinh.maudon}")
    private String thanhPhanMacDinhMauDon;
    @Value("${html.thanhphanmacdinh.banchinh}")
    private String thanhPhanMacDinhBanChinh;
    @Value("${html.thanhphanmacdinh.bansao}")
    private String thanhPhanMacDinhBanSao;
    @Value("${html.thanhphanmacdinh.truonghop}")
    private String thanhPhanMacDinhTruongHop;

    @Value("${html.ngaybanhanh}")
    private String htmlNgayBanHanh;
    @Value("${html.ngayhieuluc}")
    private String htmlNgayHieuLuc;

    @Value("${html.cancuphaply}")
    private String htmlCanCuPhapLy;


    private final NhomThuTucHanhChinhRepository nhomThuTucHanhChinhRepository;
    private final DichVuCongQuocGiaClient dichVuCongQuocGiaClient;
    private final ObjectMapper objectMapper;

    private String getToken() {
        return  dichVuCongQuocGiaClient.getToken();
    }

    @Named("getNhomThuTucHanhChinhIdByMa")
    public long getNhomThuTucHanhChinhIdByMa(List<DVCQGNhomTTHCChiTietDTO> dvcqgNhomTTHCChiTietDTOS) {
        NhomThuTucHanhChinh nhomThuTucHanhChinh = nhomThuTucHanhChinhRepository.findFirstByMaAndDaXoa(dvcqgNhomTTHCChiTietDTOS.get(0).getMaLinhVuc(), Constants.DAXOA);
        return nhomThuTucHanhChinh.getId();
    }

    @Named("getCapThucHien")
    public long getCapThucHien(List<DVCQGCapCoQuanThucHienDTO> dvcqgCapCoQuanThucHienDTOS) {
        return Long.valueOf(dvcqgCapCoQuanThucHienDTOS.get(0).getCapThucHien());
    }

    @Named("trinhTuThucHien")
    public String trinhTuThucHien(List<DVCQGTrinhTuThucHienDTO> dvcqgTrinhTuThucHienDTOS) {
        StringBuilder trinhTu = new StringBuilder();
        if (dvcqgTrinhTuThucHienDTOS.size() > 0) {
            List<DVCQGTrinhTuDTO> dvcqgTrinhTuDTOS = dvcqgTrinhTuThucHienDTOS.get(0).getTrinhTu();
            if (dvcqgTrinhTuDTOS.size() > 0) {
                for (int i =0;  i < dvcqgTrinhTuDTOS.size() ; i++) {
                trinhTu.append(htmlTrinhTu)
                        .append(dvcqgTrinhTuDTOS.get(i).getTenTrinhTu())
                        .append(closeTagP);
                }
            }
        }
        return trinhTu.toString();
    }

    @Named("cachThucThucHien")
    public String cachThucThucHien(List<DVCQGCachThucThucHienDTO> dvcqgTrinhTuThucHienDTOS) {
        StringBuilder cachThucThucHien = new StringBuilder();
        if (dvcqgTrinhTuThucHienDTOS.size() > 0) {
            switch (Integer.valueOf(dvcqgTrinhTuThucHienDTOS.get(0).getKenh())) {
                case 1 : cachThucThucHien.append(cachThucThucHienCase1);
                break;
                case 2: cachThucThucHien.append(cachThucThucHienCase2);
                break;
                case 3: cachThucThucHien.append(cachThucThucHienCase3);
                break;
                default: cachThucThucHien.append(cachThucThucHienCaseDefault);
                break;
            }

            List<DVCQGThoiGianDTO> dvcqgThoiGianDTOS = dvcqgTrinhTuThucHienDTOS.get(0).getThoiGian();
            if (dvcqgThoiGianDTOS.size() > 0) {
                cachThucThucHien.append(cachThucThucHienThoiHanGiaiQuyet)
                        .append(dvcqgThoiGianDTOS.get(0).getThoiGianGiaiQuyet())
                        .append(space)
                        .append(dvcqgThoiGianDTOS.get(0).getDonViTinh());
            }

            List<DVCQGPhiLePhiDTO> dvcqgPhiLePhiDTOS = dvcqgThoiGianDTOS.get(0).getPhiLePhi();
            if (dvcqgPhiLePhiDTOS.size() > 0) {
                cachThucThucHien.append(cachThucThucHienPhi);
                DVCQGPhiLePhiDTO dvcqgPhiLePhiDTO = dvcqgPhiLePhiDTOS.get(0);
                if (dvcqgPhiLePhiDTO != null) {
                    cachThucThucHien.append(cachThucThucHienSoTien)
                            .append(dvcqgPhiLePhiDTOS.get(0).getSoTien())
                            .append(space)
                            .append(dvcqgPhiLePhiDTOS.get(0).getDonVi())
                            .append(spaceTwo)
                            .append(dvcqgPhiLePhiDTOS.get(0).getMoTa())
                            .append(spanTagP);
                }
            }

            if (dvcqgThoiGianDTOS != null && dvcqgThoiGianDTOS.size() > 0
                    && dvcqgThoiGianDTOS.get(0).getMoTa() != null && dvcqgThoiGianDTOS.get(0).getMoTa().length() > 0) {
                cachThucThucHien.append(cachThucThucHienMoTa)
                        .append(dvcqgThoiGianDTOS.get(0).getMoTa())
                        .append(closeTagP);
            }
        }
        return cachThucThucHien.toString();
    }

    @Named("soNgayXuLy")
    public Integer soNgayXuLy(List<DVCQGCachThucThucHienDTO> dvcqgTrinhTuThucHienDTOS) {
        int soNgayXuLy = 0;
        if (dvcqgTrinhTuThucHienDTOS.size() > 0) {
            List<DVCQGThoiGianDTO> dvcqgThoiGianDTOS = dvcqgTrinhTuThucHienDTOS.get(0).getThoiGian();
            if (dvcqgThoiGianDTOS.size() > 0) {
                int soNgayXuLyString = 0;
                if (dvcqgThoiGianDTOS.get(0).getThoiGianGiaiQuyet().matches("[0-9]+")) {
                    soNgayXuLyString = Integer.valueOf(dvcqgThoiGianDTOS.get(0).getThoiGianGiaiQuyet());
                }

                if (soNgayXuLyString > soNgayXuLy) {
                    soNgayXuLy = soNgayXuLyString;
                }
            }
        }

        return soNgayXuLy;
    }

    @Named("thanhPhanHoSo")
    public String thanhPhanHoSo(List<DVCQGThanhPhanHoSoDTO> dvcqgThanhPhanHoSoDTOS) {
        StringBuilder thanhPhanHoSo = new StringBuilder();
        if (dvcqgThanhPhanHoSoDTOS.size() > 0) {
            StringBuilder thanhPhanMacDinh = new StringBuilder();
            StringBuilder truongHop = new StringBuilder();
            for (int i =0;  i < dvcqgThanhPhanHoSoDTOS.size() ; i++) {
                DVCQGThanhPhanHoSoDTO dvcqgThanhPhanHoSoDTO = dvcqgThanhPhanHoSoDTOS.get(i);
                if (dvcqgThanhPhanHoSoDTO.getTruongHop().length() == 0) {
                    thanhPhanMacDinh.append(thanhPhanMacDinhThanhPhan);

                    List<DVCQGGiayToDTO> dvcqgGiayToDTOS = dvcqgThanhPhanHoSoDTO.getGiayTo();
                    for (int j = 0; j <dvcqgGiayToDTOS.size(); j++) {
                        DVCQGGiayToDTO dvcqgGiayToDTO = dvcqgGiayToDTOS.get(j);
                        thanhPhanMacDinh.append(thanhPhanMacDinhTenGiayTo)
                                .append(dvcqgGiayToDTO.getTenGiayTo())
                                .append(closeTagP);
                        thanhPhanMacDinh.append(thanhPhanMacDinhSoBan)
                                .append(dvcqgGiayToDTO.getSoBanChinh())
                                .append(space)
                                .append(thanhPhanMacDinhBanChinh)
                                .append(closeTagP);

                        thanhPhanMacDinh.append(thanhPhanMacDinhSoBan)
                                .append(dvcqgGiayToDTO.getSoBanSao())
                                .append(space)
                                .append(thanhPhanMacDinhBanSao)
                                .append(closeTagP);

                        if (dvcqgGiayToDTO.getUrl().length() > 0 && dvcqgGiayToDTO.getTenMauDon().length() >0) {
                            thanhPhanMacDinh.append(thanhPhanMacDinhMauDon)
                                    .append(dvcqgGiayToDTO.getUrl())
                                    .append(closeLeft)
                                    .append(dvcqgGiayToDTO.getTenMauDon())
                                    .append(closeTagA)
                                    .append(closeTagP);

                        }
                    }
                } else {
                    truongHop.append(thanhPhanMacDinhTruongHop)
                            .append(dvcqgThanhPhanHoSoDTO.getTruongHop())
                            .append(closeTagP);

                    List<DVCQGGiayToDTO> dvcqgGiayToDTOS = dvcqgThanhPhanHoSoDTO.getGiayTo();
                    for (int j = 0; j <dvcqgGiayToDTOS.size(); j++) {
                        DVCQGGiayToDTO dvcqgGiayToDTO = dvcqgGiayToDTOS.get(j);
                        truongHop.append(thanhPhanMacDinhTruongHop)
                                .append(dvcqgGiayToDTO.getTenGiayTo())
                                .append(closeTagP);
                        truongHop.append(thanhPhanMacDinhTruongHop)
                                .append(dvcqgGiayToDTO.getSoBanChinh())
                                .append(space)
                                .append(thanhPhanMacDinhBanChinh)
                                .append(closeTagP);
                        truongHop.append(thanhPhanMacDinhTruongHop)
                                .append(dvcqgGiayToDTO.getSoBanSao())
                                .append(space)
                                .append(thanhPhanMacDinhBanSao)
                                .append(closeTagP);
                        if (dvcqgGiayToDTO.getUrl().length() > 0 && dvcqgGiayToDTO.getTenMauDon().length() >0) {
                            truongHop.append(thanhPhanMacDinhMauDon)
                                    .append(dvcqgGiayToDTO.getUrl())
                                    .append(closeLeft)
                                    .append(dvcqgGiayToDTO.getTenMauDon())
                                    .append(closeTagA)
                                    .append(closeTagP);
                        }
                    }
                }
            }
            thanhPhanHoSo.append(thanhPhanMacDinh).append(truongHop);
        }

        return thanhPhanHoSo.toString();
    }

    @Named("doiTuongThucHien")
    public String doiTuongThucHien(List<DVCQGDoiTuongThucHienDTO> dvcqgDoiTuongThucHienDTOS) {
        StringBuilder doiTuongThucHien = new StringBuilder();
        if (dvcqgDoiTuongThucHienDTOS.size() > 0) {
            for (int i =0;  i < dvcqgDoiTuongThucHienDTOS.size() ; i++) {
                doiTuongThucHien.append(dvcqgDoiTuongThucHienDTOS.get(i).getTenDoiTuong())
                        .append(", ");
            }
        }
        if (doiTuongThucHien.length() >0){
            doiTuongThucHien.deleteCharAt(doiTuongThucHien.length() -2);
        }
        return doiTuongThucHien.toString();
    }

    @Named("mucDoDvc")
    public int mucDoDvc(List<DVCQGDoiTuongThucHienDTO> dvcqgDoiTuongThucHienDTOS) {
        int mucDoDvc;
        if (dvcqgDoiTuongThucHienDTOS.size() > 1) {
            mucDoDvc = Constants.MucDoDichVuCong.TRUC_TUYEN;
        } else {
            mucDoDvc = Constants.MucDoDichVuCong.TRUC_TIEP;
        }

        return mucDoDvc;
    }

    @Named("coQuanThucHien")
    public String coQuanThucHien(List<DVCQGCoQuanThucHienDTO> dvcqgCoQuanThucHienDTOS) {
        StringBuilder coQuanThucHien = new StringBuilder();
        if (dvcqgCoQuanThucHienDTOS != null && dvcqgCoQuanThucHienDTOS.size() > 0) {
            for (int i =0;  i < dvcqgCoQuanThucHienDTOS.size() ; i++) {
                coQuanThucHien.append(dvcqgCoQuanThucHienDTOS.get(i).getTenDonVi())
                        .append(", ");
            }
        }
        if (coQuanThucHien.length() >0){
            coQuanThucHien.deleteCharAt(coQuanThucHien.length() -2);
        }
        return coQuanThucHien.toString();
    }

    @Named("ketQuaThucHien")
    public String ketQuaThucHien(List<DVCQGKetQuaThucHienDTO> dvcqgKetQuaThucHienDTOS) {
        StringBuilder ketQuaThucHien = new StringBuilder();
        if (dvcqgKetQuaThucHienDTOS.size() > 0) {
            for (int i =0;  i < dvcqgKetQuaThucHienDTOS.size() ; i++) {
                DVCQGKetQuaThucHienDTO dvcqgKetQuaThucHienDTO = dvcqgKetQuaThucHienDTOS.get(i);
                ketQuaThucHien.append(thanhPhanMacDinhTruongHop)
                        .append(dvcqgKetQuaThucHienDTO.getTenKetQua())
                        .append(closeTagP);
            }
        }
        return ketQuaThucHien.toString();
    }

    @Named("canCuPhapLy")
    public String canCuPhapLy(List<DVCQGCanCuPhapLyDTO> dvcqgCanCuPhapLyDTOS) throws Exception {
        StringBuilder canCuPhapLy = new StringBuilder();
        if (dvcqgCanCuPhapLyDTOS.size() > 0) {
            for (int i =0;  i < dvcqgCanCuPhapLyDTOS.size() ; i++) {
                DVCQGCanCuPhapLyDTO dvcqgCanCuPhapLyDTO = dvcqgCanCuPhapLyDTOS.get(i);

                DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
                DateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");

                StringBuilder ngayBanHanh = new StringBuilder();
                StringBuilder ngayHieuLuc = new StringBuilder();

                if (dvcqgCanCuPhapLyDTO.getNgayBanHanh() != null && dvcqgCanCuPhapLyDTO.getNgayBanHanh().length() > 2 ) {
                    ngayBanHanh.append(htmlNgayBanHanh).append(" ").append(df2.format(df1.parse(dvcqgCanCuPhapLyDTO.getNgayBanHanh())));
                }
                if (dvcqgCanCuPhapLyDTO.getNgayHieuLuc() != null && dvcqgCanCuPhapLyDTO.getNgayHieuLuc().length() > 2) {
                    ngayHieuLuc.append(comma).append(" ").append(htmlNgayHieuLuc).append(df2.format(df1.parse(dvcqgCanCuPhapLyDTO.getNgayHieuLuc())));
                }
                canCuPhapLy.append(htmlCanCuPhapLy)
                        .append(dvcqgCanCuPhapLyDTO.getTenVanBan())
                        .append(closeTagA).append(" ")
                        .append(ngayBanHanh)
                        .append(ngayHieuLuc)
                        .append(closeTagP);

            }
        }
        return canCuPhapLy.toString();
    }

    @Named("ngayBanHanh")
    public Date ngayBanHanh(String quyetDinhCongBoId) throws Exception {
        Date ngayBanHanh = new Date();
        List<DVCQGQuyetDinhCongBoDTO> dvcqgQuyetDinhCongBoDTO = objectMapper.readValue(dichVuCongQuocGiaClient.getQuyetDinhCongBo(quyetDinhCongBoId, getToken()), new TypeReference<List<DVCQGQuyetDinhCongBoDTO>>() {} );
        if (dvcqgQuyetDinhCongBoDTO != null && dvcqgQuyetDinhCongBoDTO.size() > 0) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            ngayBanHanh = df.parse(dvcqgQuyetDinhCongBoDTO.get(0).getNgayQuyetDinh());
        }
        return ngayBanHanh;
    }

    @Named("soQuyetDinh")
    public String soQuyetDinh(String quyetDinhCongBoId) throws IOException {
        String soQuyetDinh = "";
        List<DVCQGQuyetDinhCongBoDTO> dvcqgQuyetDinhCongBoDTO = objectMapper.readValue(dichVuCongQuocGiaClient.getQuyetDinhCongBo(quyetDinhCongBoId, getToken()), new TypeReference<List<DVCQGQuyetDinhCongBoDTO>>() {} );
        if (dvcqgQuyetDinhCongBoDTO != null && dvcqgQuyetDinhCongBoDTO.size() > 0) {
            soQuyetDinh = dvcqgQuyetDinhCongBoDTO.get(0).getSoQuyetDinh();
        }
        return soQuyetDinh;
    }

    @Named("phiLePhi")
    public String phiLePhi(List<DVCQGCachThucThucHienDTO> dvcqgTrinhTuThucHienDTOS) {
        String phiLePhi = "";
        if (dvcqgTrinhTuThucHienDTOS.size() > 0) {
            List<DVCQGThoiGianDTO> dvcqgThoiGianDTOS = dvcqgTrinhTuThucHienDTOS.get(0).getThoiGian();
            List<DVCQGPhiLePhiDTO> dvcqgPhiLePhiDTOS = dvcqgThoiGianDTOS.get(0).getPhiLePhi();
            if (dvcqgPhiLePhiDTOS.size() > 0) {
                DVCQGPhiLePhiDTO dvcqgPhiLePhiDTO = dvcqgPhiLePhiDTOS.get(0);
                if (dvcqgPhiLePhiDTO != null) {
                    phiLePhi = dvcqgPhiLePhiDTOS.get(0).getSoTien();
                }
            }
        }
        return phiLePhi;
    }

}

