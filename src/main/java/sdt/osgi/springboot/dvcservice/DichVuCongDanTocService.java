package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.DanTocsRepository;
import sdt.osgi.springboot.dao.QuanLyDongBoDuLieuRepository;
import sdt.osgi.springboot.dao.QuocTichRepository;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGDanTocDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGQuocGiaDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapXaDTO;
import sdt.osgi.springboot.mapper.DanTocMapper;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.QuanLyDongBoDuLieu;
import sdt.osgi.springboot.model.QuocTich;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DichVuCongDanTocService {

    private final DanTocsRepository danTocRepository;
    private final DanTocMapper danTocMapper;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;

    public List<DVCQGDanTocDTO> getListDanToc() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.madantoc"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGDanTocDTO>>() {
        });
    }

    public void dongBoDuLieuDanToc() {
        try {
            List<DVCQGDanTocDTO> danTocList = getListDanToc();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) danTocRepository.count();

            if (danTocList != null) {
                deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DanToc.class.getName()), thoiDiemXoaDuLieu);

                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, danTocList.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);

                saveAllDanToc(danTocList);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), danTocList.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll() {
        danTocRepository.deleteAll();
    }

    private void saveAllDanToc(List<DVCQGDanTocDTO> dvcqgDanTocDTOS) {
        danTocRepository.saveAll(
                danTocMapper.toDVCQGDanTocDTOList(dvcqgDanTocDTOS));
    }
}

