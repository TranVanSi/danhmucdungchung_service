package sdt.osgi.springboot.dvcservice;

import lombok.RequiredArgsConstructor;
import org.mapstruct.Named;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGDoiTuongThucHienDTO;
import sdt.osgi.springboot.util.Constants;

import java.text.SimpleDateFormat;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class CanBoCongChucCustomService {
    @Named("addMaById")
    public String addMaById(String id) {
        StringBuilder ma = new StringBuilder();
        return ma.append("CBCC").append(id).toString();
    }

    @Named("addGioiTinh")
    public long addGioiTinh(String gioiTinh) {
        long gioiTinhId;
        if (gioiTinh.equals("Nam")) {
            gioiTinhId = 1;
        } else if (gioiTinh.equals("Nữ")) {
            gioiTinhId = 2;
        } else {
            gioiTinhId = 3;
        }
        return gioiTinhId;
    }

    @Named("convertEmail")
    public String convertEmail(String email) {
        String emailReplace = "";
        long countKyTu = email.chars().filter(ch -> ch == '@').count();
        if (email != null && countKyTu >= 2) {
            emailReplace = email.replace("@daklak.gov.vn", "");
        } else {
            emailReplace = email;
        }
        return emailReplace;
    }

}

