package sdt.osgi.springboot.dvcservice;

import lombok.RequiredArgsConstructor;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.osgi.springboot.util.OAuthConnectResponse;


import java.nio.charset.Charset;

@Service
@RequiredArgsConstructor
public class DanhMucDungChungNGSPService {

    private final Environment env;

    @Autowired
    @Qualifier("dmdcRest")
    public RestTemplate restTemplate;

    public String getAccessToken() {
        try {
            OAuthClientRequest tokenRequest = OAuthClientRequest.tokenLocation(env.getProperty("ngsp.token.url"))
                    .setGrantType(GrantType.CLIENT_CREDENTIALS)
                    .setClientId(env.getProperty("ngsp.token.clientId"))
                    .setClientSecret(env.getProperty("ngsp.token.clientSecret")).buildBodyMessage();

            OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
            OAuthConnectResponse oAuthResponse = oAuthClient.accessToken(tokenRequest, OAuthConnectResponse.class);

            return oAuthResponse.getAccessToken();
        } catch (OAuthSystemException e) {
            e.printStackTrace();
        } catch (OAuthProblemException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getDataWithPath(String pathURL) {
        String token = getAccessToken();
        UriComponentsBuilder restURL = UriComponentsBuilder.fromUriString(pathURL);

        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", org.springframework.http.MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + token);


        HttpEntity<?> entity = new HttpEntity(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(restURL.toUriString(), HttpMethod.GET, entity, String.class);

        return responseEntity.getBody();
    }
}
