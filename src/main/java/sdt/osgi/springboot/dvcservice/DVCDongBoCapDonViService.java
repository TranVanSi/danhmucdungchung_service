package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.CanBoCongChucClient;
import sdt.osgi.springboot.dao.DongBoCapDonViRepository;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCapDonViDTO;
import sdt.osgi.springboot.mapper.DongBoCapDonViMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DVCDongBoCapDonViService {

    private final ObjectMapper objectMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final CanBoCongChucClient canBoCongChucClient;
    private final DongBoCapDonViRepository dongBoCapDonViRepository;
    private final DongBoCapDonViMapper dongBoCapDonViMapper;

    public List<DVCDongBoCapDonViDTO> getListCapDonVi(String task) throws IOException {
        String data = canBoCongChucClient.getDataByTask(task);
        return objectMapper.readValue(data, new TypeReference<List<DVCDongBoCapDonViDTO>>() {
        });
    }

    public void dongBoDuLieuCapDonVi() {
        try {
            List<DVCDongBoCapDonViDTO> DVCDongBoCapDonViDTOS = getListCapDonVi("dsCapdonvi");
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) dongBoCapDonViRepository.count();

            if (DVCDongBoCapDonViDTOS != null) {
                dongBoCapDonViRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty("sdt.osgi.springboot.model.CapDonVi"), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, DVCDongBoCapDonViDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllCapDonVi(DVCDongBoCapDonViDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), DVCDongBoCapDonViDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveAllCapDonVi(List<DVCDongBoCapDonViDTO> DVCDongBoCapDonViDTOS) {
        for (int i = 0; i < DVCDongBoCapDonViDTOS.size() ; i++) {
            dongBoCapDonViRepository.save(
                    dongBoCapDonViMapper.DONG_BO_CAP_DON_VI(DVCDongBoCapDonViDTOS.get(i)));
        }
    }

}

