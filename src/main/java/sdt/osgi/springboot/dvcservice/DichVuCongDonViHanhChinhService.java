package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sdt.osgi.springboot.dao.CapDonViHanhChinhsRepository;
import sdt.osgi.springboot.dao.DonViHanhChinhsRepository;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapHuyenDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapTinhDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapXaDTO;
import sdt.osgi.springboot.mapper.DonViHanhChinhMapper;
import sdt.osgi.springboot.model.DonViHanhChinh;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static sdt.osgi.springboot.util.Constants.*;

@Service
@RequiredArgsConstructor
public class DichVuCongDonViHanhChinhService {

    private final DonViHanhChinhsRepository donViHanhChinhsRepository;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final DonViHanhChinhMapper donViHanhChinhMapper;

    public List<DVCQGTDonViHanhChinhCapTinhDTO> getListDVHCCapTinh() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.donvihanhchinhcap1"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGTDonViHanhChinhCapTinhDTO>>() {
        });
    }

    public List<DVCQGTDonViHanhChinhCapHuyenDTO> getListDVHCCapHuyen() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.donvihanhchinhcap2"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGTDonViHanhChinhCapHuyenDTO>>() {
        });
    }

    public List<DVCQGTDonViHanhChinhCapXaDTO> getListDVHCCapXa() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.donvihanhchinhcap3"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGTDonViHanhChinhCapXaDTO>>() {
        });
    }

    public void dongBoDuLieuDonViHanhChinhCapTinh() {
        try {
            List<DVCQGTDonViHanhChinhCapTinhDTO> dvcqgtDonViHanhChinhCapTinhDTOS = getListDVHCCapTinh();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBoCapTinh = donViHanhChinhsRepository.countByCapDonViHanhChinh_Id(CAPTINH);
            if (dvcqgtDonViHanhChinhCapTinhDTOS != null) {
                donViHanhChinhsRepository.deleteDonViHanhChinhsByCapDonViHanhChinh_Id(Long.valueOf(CAPTINH));
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DonViHanhChinh.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgtDonViHanhChinhCapTinhDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBoCapTinh);
                saveAllCapTinh(dvcqgtDonViHanhChinhCapTinhDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgtDonViHanhChinhCapTinhDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBoCapTinh);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dongBoDuLieuDonViHanhChinhCapHuyen() {
        try {
            List<DVCQGTDonViHanhChinhCapHuyenDTO> dvcqgtDonViHanhChinhCapHuyenDTOS = getListDVHCCapHuyen();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBoCapHuyen = donViHanhChinhsRepository.countByCapDonViHanhChinh_Id(CAPHUYEN);
            if (dvcqgtDonViHanhChinhCapHuyenDTOS != null) {
                donViHanhChinhsRepository.deleteDonViHanhChinhsByCapDonViHanhChinh_Id(Long.valueOf(CAPHUYEN));
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DonViHanhChinh.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgtDonViHanhChinhCapHuyenDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBoCapHuyen);
                saveAllCapHuyen(dvcqgtDonViHanhChinhCapHuyenDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgtDonViHanhChinhCapHuyenDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBoCapHuyen);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dongBoDuLieuDonViHanhChinhCapXa() {
        try {
            List<DVCQGTDonViHanhChinhCapXaDTO> dvcqgtDonViHanhChinhCapXaDTOS = getListDVHCCapXa();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBoCapXa = donViHanhChinhsRepository.countByCapDonViHanhChinh_Id(CAPXA);
            if (dvcqgtDonViHanhChinhCapXaDTOS != null) {
                donViHanhChinhsRepository.deleteDonViHanhChinhsByCapDonViHanhChinh_Id(Long.valueOf(CAPXA));
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DonViHanhChinh.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgtDonViHanhChinhCapXaDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBoCapXa);
                saveAllCapXa(dvcqgtDonViHanhChinhCapXaDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgtDonViHanhChinhCapXaDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBoCapXa);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAllCapTinh(List<DVCQGTDonViHanhChinhCapTinhDTO> dvcqgtDonViHanhChinhCapTinhDTOS) {
        donViHanhChinhsRepository.saveAll(
                donViHanhChinhMapper.toDonViHanhChinhList(
                        donViHanhChinhMapper.toDVCQGDonViHanhChinhCapTinh(
                                dvcqgtDonViHanhChinhCapTinhDTOS)));
    }

    private void saveAllCapHuyen(List<DVCQGTDonViHanhChinhCapHuyenDTO> dvcqgtDonViHanhChinhCapHuyenDTOS) {
        donViHanhChinhsRepository.saveAll(
                donViHanhChinhMapper.toDonViHanhChinhList(
                        donViHanhChinhMapper.toDVCQGDonViHanhChinhCapHuyen(
                                dvcqgtDonViHanhChinhCapHuyenDTOS)));
    }

    private void saveAllCapXa(List<DVCQGTDonViHanhChinhCapXaDTO> dvcqgtDonViHanhChinhCapXaDTOS) {
        donViHanhChinhsRepository.saveAll(
                donViHanhChinhMapper.toDonViHanhChinhList(
                        donViHanhChinhMapper.toDVCQGDonViHanhChinhCapXa(
                                dvcqgtDonViHanhChinhCapXaDTOS)));
    }
}

