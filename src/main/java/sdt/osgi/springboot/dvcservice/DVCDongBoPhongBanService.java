package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.CanBoCongChucClient;
import sdt.osgi.springboot.dao.DongBoPhongBanRepository;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoPhongBanDTO;
import sdt.osgi.springboot.mapper.DongBoPhongBanMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DVCDongBoPhongBanService {

    private final ObjectMapper objectMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final CanBoCongChucClient canBoCongChucClient;
    private final DongBoPhongBanRepository dongBoPhongBanRepository;
    private final DongBoPhongBanMapper dongBoPhongBanMapper;

    public List<DVCDongBoPhongBanDTO> getListCapDonVi(String task) throws IOException {
        String data = canBoCongChucClient.getDataByTask(task);
        return objectMapper.readValue(data, new TypeReference<List<DVCDongBoPhongBanDTO>>() {
        });
    }

    public void dongBoDuLieuPhongBan() {
        try {
            List<DVCDongBoPhongBanDTO> DVCDongBoCapDonViDTOS = getListCapDonVi("dsPhongban");
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) dongBoPhongBanRepository.count();

            if (DVCDongBoCapDonViDTOS != null) {
                dongBoPhongBanRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty("sdt.osgi.springboot.model.PhongBan"), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, DVCDongBoCapDonViDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllCapDonVi(DVCDongBoCapDonViDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), DVCDongBoCapDonViDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveAllCapDonVi(List<DVCDongBoPhongBanDTO> DVCDongBoCapDonViDTOS) {
        for (int i = 0; i < DVCDongBoCapDonViDTOS.size() ; i++) {
            dongBoPhongBanRepository.save(
                    dongBoPhongBanMapper.DONG_BO_PHONG_BAN(DVCDongBoCapDonViDTOS.get(i)));
        }
    }

}

