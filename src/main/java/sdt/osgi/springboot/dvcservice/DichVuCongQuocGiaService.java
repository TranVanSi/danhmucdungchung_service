package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.QuocTichRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGQuocGiaDTO;
import sdt.osgi.springboot.mapper.QuocTichMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.QuocTich;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DichVuCongQuocGiaService {
    private final QuocTichRepository quocGiaRepository;
    private final QuocTichMapper quocTichMapper;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;

    public List<DVCQGQuocGiaDTO> getListQuocGia() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.maquocgia"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGQuocGiaDTO>>() {
        });
    }

    public void dongBoDuLieuQuocGia() {
        try {
            List<DVCQGQuocGiaDTO> quocGiaList = getListQuocGia();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) quocGiaRepository.count();


            if (quocGiaList != null) {
                deleteAll();

                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(QuocTich.class.getName()), thoiDiemXoaDuLieu);

                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, quocGiaList.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);

                saveAllQuocGia(quocGiaList);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), quocGiaList.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll() {
        quocGiaRepository.deleteAll();
    }

    private void saveAllQuocGia(List<DVCQGQuocGiaDTO> quocGiaDTOList) {
        quocGiaRepository.saveAll(
                quocTichMapper.toDVCQGQuocGiaList(quocGiaDTOList));
    }
}

