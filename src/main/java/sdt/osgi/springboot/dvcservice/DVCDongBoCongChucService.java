package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.CanBoCongChucClient;
import sdt.osgi.springboot.clients.CongChucClient;
import sdt.osgi.springboot.dao.DongBoCongChucRepository;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCongChucDTO;
import sdt.osgi.springboot.mapper.CongChucMapper;
import sdt.osgi.springboot.mapper.DongBoCongChucMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DVCDongBoCongChucService {

    private final ObjectMapper objectMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final CanBoCongChucClient canBoCongChucClient;
    private final DongBoCongChucMapper dongBoCongChucMapper;
    private final DongBoCongChucRepository dongBoCongChucRepository;
    private final CongChucClient congChucClient;
    private final CongChucMapper congChucMapper;

    public List<DVCDongBoCongChucDTO> getListCongChuc(String task) throws IOException {
        String data = canBoCongChucClient.getDataCongChuc(task);
        return objectMapper.readValue(data, new TypeReference<List<DVCDongBoCongChucDTO>>() {
        });
    }

    public void dongBoDuLieuCongChuc() {
        try {
            List<DVCDongBoCongChucDTO> dvcDongBoCongChucDTOS = getListCongChuc("dsCongchuc");
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) dongBoCongChucRepository.count();

            if (dvcDongBoCongChucDTOS != null) {
                dongBoCongChucRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty("sdt.osgi.springboot.model.CongChuc"), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcDongBoCongChucDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllDongBoCongChuc(dvcDongBoCongChucDTOS);
                saveCongChuc(dvcDongBoCongChucDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcDongBoCongChucDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveAllDongBoCongChuc(List<DVCDongBoCongChucDTO> dvcDongBoCongChucDTOS) {
        dongBoCongChucRepository.saveAll(dongBoCongChucMapper.DONG_BO_CONG_CHUCS(dvcDongBoCongChucDTOS));
    }

    void saveCongChuc(List<DVCDongBoCongChucDTO> dvcDongBoCongChucDTOS) {
        List<CongChucDTO> congChucDTOS = new ArrayList<>();
        dvcDongBoCongChucDTOS.stream().forEach(dvcDongBoCongChucDTO -> {
            if (dvcDongBoCongChucDTO.getEmail() != null) {
                long countKyTu = dvcDongBoCongChucDTO.getEmail().chars().filter(ch -> ch == '@').count();
                String email = "";
                if (countKyTu >= 2) {
                    email = dvcDongBoCongChucDTO.getEmail().replace("@daklak.gov.vn", "");
                } else {
                    email = dvcDongBoCongChucDTO.getEmail();
                }
                CongChucDTO congChucDTO = congChucClient.findByEmail(email);
                if (congChucDTO != null) {
                    dvcDongBoCongChucDTO.setId(congChucDTO.getId());
                    congChucDTOS.add(congChucMapper.dvcDongBoCongChucDTOtoCongChucDTO(dvcDongBoCongChucDTO));
                }
            }
        });
        congChucClient.updateAllCongChuc(congChucDTOS);
    }
}

