package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.DoiTuongThucHienRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapXaDTO;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGDoiTuongThucHienDTO;
import sdt.osgi.springboot.mapper.DoiTuongThucHienMapper;
import sdt.osgi.springboot.model.DoiTuongThucHien;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
public class DichVuCongDoiTuongThucHienService {
    private final DichVuCongQuocGiaClient dichVuCongQuocGiaClient;
    private final DoiTuongThucHienRepository doiTuongThucHienRepository;
    private final DoiTuongThucHienMapper doiTuongThucHienMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final ObjectMapper objectMapper;

    public List<DVCQGDoiTuongThucHienDTO> getListDoiTuongThucHien() throws IOException {
        String data = dichVuCongQuocGiaClient.getDataDoiTuongThucHien();
        return objectMapper.readValue(data, new TypeReference<List<DVCQGDoiTuongThucHienDTO>>() {
        });
    }

    public void dongBoDuLieuDoiTuongThucHien() {
        try {
            List<DVCQGDoiTuongThucHienDTO> dvcqgDoiTuongThucHienDTOS = getListDoiTuongThucHien();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) doiTuongThucHienRepository.count();

            if (dvcqgDoiTuongThucHienDTOS != null) {
                doiTuongThucHienRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DoiTuongThucHien.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgDoiTuongThucHienDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllDoiTuongThucHien(dvcqgDoiTuongThucHienDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgDoiTuongThucHienDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAllDoiTuongThucHien(List<DVCQGDoiTuongThucHienDTO> dvcqgDoiTuongThucHienDTOS) {
        doiTuongThucHienRepository.saveAll(
                doiTuongThucHienMapper.toDVCQGDoiTuongThucHienList(dvcqgDoiTuongThucHienDTOS));
    }
}

