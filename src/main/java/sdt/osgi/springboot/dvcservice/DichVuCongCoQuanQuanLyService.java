package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.DongBoCoQuanQuanLyRepository;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGCoQuanQuanLyDTO;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGDoiTuongThucHienDTO;
import sdt.osgi.springboot.mapper.DongBoCoQuanQuanLyMapper;
import sdt.osgi.springboot.model.DoiTuongThucHien;
import sdt.osgi.springboot.model.DongBoCoQuanQuanLy;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
public class DichVuCongCoQuanQuanLyService {
    private final DichVuCongQuocGiaClient dichVuCongQuocGiaClient;
    private final DongBoCoQuanQuanLyRepository dongBoCoQuanQuanLyRepository;
    private final DongBoCoQuanQuanLyMapper dongBoCoQuanQuanLyMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final ObjectMapper objectMapper;

    public List<DVCQGCoQuanQuanLyDTO> getListCoQuan() throws IOException {
        String data = dichVuCongQuocGiaClient.getDataCoQuan();
        return objectMapper.readValue(data, new TypeReference<List<DVCQGCoQuanQuanLyDTO>>() {
        });
    }

    public void dongBoDuLieuCoQuanQuanLy() {
        try {
            List<DVCQGCoQuanQuanLyDTO> dvcqgCoQuanQuanLyDTOS = getListCoQuan();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) dongBoCoQuanQuanLyRepository.count();

            if (dvcqgCoQuanQuanLyDTOS != null) {
                dongBoCoQuanQuanLyRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DongBoCoQuanQuanLy.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgCoQuanQuanLyDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllCoQuanQuanLy(dvcqgCoQuanQuanLyDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgCoQuanQuanLyDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAllCoQuanQuanLy(List<DVCQGCoQuanQuanLyDTO> dvcqgCoQuanQuanLyDTOS) {
        dongBoCoQuanQuanLyRepository.saveAll(
                dongBoCoQuanQuanLyMapper.toDVCQGDVCCoQuanQuanLyList(dvcqgCoQuanQuanLyDTOS));
    }
}

