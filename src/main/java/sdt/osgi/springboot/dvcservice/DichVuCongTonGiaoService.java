package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.TonGiaoRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGTonGiaoDTO;
import sdt.osgi.springboot.mapper.TonGiaoMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.TinhTrangHonNhan;
import sdt.osgi.springboot.model.TonGiao;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DichVuCongTonGiaoService {

    private final TonGiaoRepository tonGiaoRepository;
    private final TonGiaoMapper tonGiaoMapper;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;

    public List<DVCQGTonGiaoDTO> getListTonGiao() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.matongiao"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGTonGiaoDTO>>() {
        });
    }

    public void dongBoDuLieuTonGiao() {
        try {
            List<DVCQGTonGiaoDTO> tonGiaoList = getListTonGiao();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) tonGiaoRepository.count();


            if (tonGiaoList != null) {
                deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(TonGiao.class.getName()), thoiDiemXoaDuLieu);

                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, tonGiaoList.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);

                saveAllTonGiao(tonGiaoList);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), tonGiaoList.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll() {
        tonGiaoRepository.deleteAll();
    }

    private void saveAllTonGiao(List<DVCQGTonGiaoDTO> tonGiaoDTOList) {
        tonGiaoRepository.saveAll(tonGiaoMapper.toDVCQGTonGiaoList(tonGiaoDTOList));
    }
}

