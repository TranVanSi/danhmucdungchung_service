package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DoKhanVanBanRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGDoKhanVanBanDTO;
import sdt.osgi.springboot.mapper.DoKhanVanBanMapper;
import sdt.osgi.springboot.model.DoKhanVanBan;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DichVuCongDoKhanVanBanService {

    private final DoKhanVanBanRepository doKhanVanBanRepository;
    private final DoKhanVanBanMapper doKhanVanBanMapper;
    private final ObjectMapper objectMapper;
    private final DanhMucDungChungNGSPService danhMucDungChungNGSPService;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;

    public List<DVCQGDoKhanVanBanDTO> getListDoKhanVanBan() throws IOException {
        String data = danhMucDungChungNGSPService.getDataWithPath(env.getProperty("ngsp.path.dokhanvanban"));
        return objectMapper.readValue(data, new TypeReference<List<DVCQGDoKhanVanBanDTO>>() {
        });
    }

    public void dongBoDuLieuDoKhanVanBan() {
        try {
            List<DVCQGDoKhanVanBanDTO> doKhanVanBanList = getListDoKhanVanBan();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) doKhanVanBanRepository.count();


            if (doKhanVanBanList != null) {
                deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(DoKhanVanBan.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, doKhanVanBanList.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllDoKhanVanBan(doKhanVanBanList);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), doKhanVanBanList.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll() {
        doKhanVanBanRepository.deleteAll();
    }

    private void saveAllDoKhanVanBan(List<DVCQGDoKhanVanBanDTO> doKhanVanBanDTOList) {
        doKhanVanBanRepository.saveAll(doKhanVanBanMapper.toDVCQGTonGiaoList(doKhanVanBanDTOList));
    }
}

