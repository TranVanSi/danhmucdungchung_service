package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.CanBoCongChucClient;
import sdt.osgi.springboot.dao.DongBoDonViRepository;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoDonViDTO;
import sdt.osgi.springboot.mapper.DongBoDonViMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DVCDongBoDonViService {

    private final ObjectMapper objectMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final CanBoCongChucClient canBoCongChucClient;
    private final DongBoDonViRepository dongBoDonViRepository;
    private final DongBoDonViMapper dongBoDonViMapper;

    public List<DVCDongBoDonViDTO> getListCapDonVi(String task) throws IOException {
        String data = canBoCongChucClient.getDataByTask(task);
        return objectMapper.readValue(data, new TypeReference<List<DVCDongBoDonViDTO>>() {
        });
    }

    public void dongBoDuLieuDonVi() {
        try {
            List<DVCDongBoDonViDTO> DVCDongBoDonViDTOS = getListCapDonVi("dsDonvi");
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) dongBoDonViRepository.count();

            if (DVCDongBoDonViDTOS != null) {
                dongBoDonViRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty("sdt.osgi.springboot.model.DonVi"), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, DVCDongBoDonViDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllDonVi(DVCDongBoDonViDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), DVCDongBoDonViDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveAllDonVi(List<DVCDongBoDonViDTO> DVCDongBoDonViDTOS) {
        for (int i = 0; i < DVCDongBoDonViDTOS.size() ; i++) {
            dongBoDonViRepository.save(
                    dongBoDonViMapper.DONG_BO_DON_VI(DVCDongBoDonViDTOS.get(i)));
        }
    }

}

