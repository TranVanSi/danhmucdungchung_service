package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.CapCoQuanThucHienRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGCapCoQuanThucHienDTO;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGDoiTuongThucHienDTO;
import sdt.osgi.springboot.mapper.CapCoQuanThucHienMapper;
import sdt.osgi.springboot.model.CapCoQuanThucHien;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
public class DichVuCongCapCoQuanThucHienService {
    private final DichVuCongQuocGiaClient dichVuCongQuocGiaClient;
    private final CapCoQuanThucHienRepository capCoQuanThucHienRepository;
    private final CapCoQuanThucHienMapper capCoQuanThucHienMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final ObjectMapper objectMapper;

    public List<DVCQGCapCoQuanThucHienDTO> getListCapCoQuanThucHien() throws IOException {
        String data = dichVuCongQuocGiaClient.getCapCoQuanThucHien();
        return objectMapper.readValue(data, new TypeReference<List<DVCQGCapCoQuanThucHienDTO>>() {
        });
    }

    public void dongBoDuLieuCapCoQuanThucHien() {
        try {
            List<DVCQGCapCoQuanThucHienDTO> dvcqgCapCoQuanThucHienDTOS = getListCapCoQuanThucHien();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) capCoQuanThucHienRepository.count();

            if (dvcqgCapCoQuanThucHienDTOS != null) {
                deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(CapCoQuanThucHien.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgCapCoQuanThucHienDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllCapCoQuanThucHien(dvcqgCapCoQuanThucHienDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgCapCoQuanThucHienDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteAll(){
        capCoQuanThucHienRepository.deleteAll();
    }

    private void saveAllCapCoQuanThucHien(List<DVCQGCapCoQuanThucHienDTO> dvcqgCapCoQuanThucHienDTOS) {
        capCoQuanThucHienRepository.saveAll(
                capCoQuanThucHienMapper.toDVCQGCapCoQuanThucHienList(dvcqgCapCoQuanThucHienDTOS));
    }
}

