package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.ThuTucHanhChinhRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGDanhSachThuTucDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGThuTucHanhChinhDTO;
import sdt.osgi.springboot.mapper.ThuTucHanhChinhMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.ThuTucHanhChinh;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
public class DichVuCongThuTucHanhChinhService {
    private final DichVuCongQuocGiaClient dichVuCongQuocGiaClient;
    private final ThuTucHanhChinhRepository thuTucHanhChinhRepository;
    private final ThuTucHanhChinhMapper thuTucHanhChinhMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final ObjectMapper objectMapper;

    public List<DVCQGDanhSachThuTucDTO> getListThuTucHanhChinh() throws IOException{
        String data = dichVuCongQuocGiaClient.getDanhSachThuTuc();
        return objectMapper.readValue(data, new TypeReference<List<DVCQGDanhSachThuTucDTO>>() {
        });
    }

    public void dongBoDuLieuThuTucHanhChinh() {
        try {
            List<DVCQGDanhSachThuTucDTO> dvcqgDanhSachThuTucDTOS = getListThuTucHanhChinh();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = (int) thuTucHanhChinhRepository.count();

            if (dvcqgDanhSachThuTucDTOS != null) {
                thuTucHanhChinhRepository.deleteAll();
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(ThuTucHanhChinh.class.getName()), thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgDanhSachThuTucDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllThuTucHanhChinh(dvcqgDanhSachThuTucDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgDanhSachThuTucDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveAllThuTucHanhChinh(List<DVCQGDanhSachThuTucDTO> dvcqgNhomTTHCVoBocDTOS) throws IOException {
        String token = dichVuCongQuocGiaClient.getToken();
        for (int i =0;  i < dvcqgNhomTTHCVoBocDTOS.size() ; i++) {
            dongBoThuTucHanhChinh(dvcqgNhomTTHCVoBocDTOS.get(i).getMaTTHC(), token);
        }
    }

    private void dongBoThuTucHanhChinh(String maThuTuc, String token) throws IOException {
        String dvcqgThuTucHanhChinhDTO = dichVuCongQuocGiaClient.getThuTuc(maThuTuc, token);

        if (dvcqgThuTucHanhChinhDTO != null) {
            List<DVCQGThuTucHanhChinhDTO> dvcqgThuTucHanhChinhDTOS =  objectMapper.readValue(dvcqgThuTucHanhChinhDTO, new TypeReference<List<DVCQGThuTucHanhChinhDTO>>() {});
            thuTucHanhChinhRepository.save(
                    thuTucHanhChinhMapper.dvcqgThuTucHanhChinhDTOToThuTucHanhChinh(dvcqgThuTucHanhChinhDTOS.get(0)));
        }
    }
}

