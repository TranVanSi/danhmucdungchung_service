package sdt.osgi.springboot.dvcservice;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.clients.DichVuCongQuocGiaClient;
import sdt.osgi.springboot.dao.NhomThuTucHanhChinhRepository;
import sdt.osgi.springboot.dvcqgdto.DVCQGNhomTTHCChiTietDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGNhomTTHCVoBocDTO;
import sdt.osgi.springboot.mapper.NhomThuTucHanhChinhMapper;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.model.NhomThuTucHanhChinh;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.io.IOException;
import java.util.Date;
import java.util.List;


@Service
@RequiredArgsConstructor
public class DichVuCongNhomTTHCService {
    private final DichVuCongQuocGiaClient dichVuCongQuocGiaClient;
    private final NhomThuTucHanhChinhRepository nhomThuTucHanhChinhRepository;
    private final NhomThuTucHanhChinhMapper nhomThuTucHanhChinhMapper;
    private final Environment env;
    private final LichSuDongBoService lichSuDongBoService;
    private final ObjectMapper objectMapper;

    public List<DVCQGNhomTTHCVoBocDTO> getListNhomTTHCVoBoc() throws IOException{
        String data = dichVuCongQuocGiaClient.getNhomVoBoc();
        return objectMapper.readValue(data, new TypeReference<List<DVCQGNhomTTHCVoBocDTO>>() {
        });
    }

    public List<DVCQGNhomTTHCChiTietDTO> getListNhomTTHCChiTiet() throws IOException {
        String data = dichVuCongQuocGiaClient.getNhomChiTiet();
        return objectMapper.readValue(data, new TypeReference<List<DVCQGNhomTTHCChiTietDTO>>() {
        });

    }

    public void dongBoDuLieuNhomTTHCVoBoc() {
        try {
            List<DVCQGNhomTTHCVoBocDTO> dvcqgNhomTTHCVoBocDTOS = getListNhomTTHCVoBoc();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = nhomThuTucHanhChinhRepository.countByLoaiNhomTTHC(Constants.LoaiNhom.VOBOC);

            if (dvcqgNhomTTHCVoBocDTOS != null) {
                nhomThuTucHanhChinhRepository.deleteNhomThuTucHanhChinhsByLoaiNhomTTHC(Constants.LoaiNhom.VOBOC);
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(NhomThuTucHanhChinh.class.getName())+Constants.LoaiNhom.NHOM_VO_BOC, thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgNhomTTHCVoBocDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllNhomTTHCVoBoc(dvcqgNhomTTHCVoBocDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgNhomTTHCVoBocDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAllNhomTTHCVoBoc(List<DVCQGNhomTTHCVoBocDTO> dvcqgNhomTTHCVoBocDTOS) {
        nhomThuTucHanhChinhRepository.saveAll(
                nhomThuTucHanhChinhMapper.toDVCQGNhomThuTucHanhChinhVoBocList(dvcqgNhomTTHCVoBocDTOS));
    }

    public void dongBoDuLieuNhomTTHCChiTiet() {
        try {
            List<DVCQGNhomTTHCChiTietDTO> dvcqgNhomTTHCChiTietDTOS = getListNhomTTHCChiTiet();
            Date thoiDiemXoaDuLieu = new Date();
            int soLuongTruocDongBo = nhomThuTucHanhChinhRepository.countByLoaiNhomTTHC(Constants.LoaiNhom.CHITIET);

            if (dvcqgNhomTTHCChiTietDTOS != null) {
                nhomThuTucHanhChinhRepository.deleteNhomThuTucHanhChinhsByLoaiNhomTTHC(Constants.LoaiNhom.CHITIET);
                LichSuDongBo lichSuDongBo = lichSuDongBoService.luuLichSuXoaDuLieu(env.getProperty(NhomThuTucHanhChinh.class.getName())+Constants.LoaiNhom.NHOM_CHI_TIET, thoiDiemXoaDuLieu);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, new Date(), null, dvcqgNhomTTHCChiTietDTOS.size(), TrangThaiDongBo.PROCESSING, soLuongTruocDongBo);
                saveAllNhomTTHCChiTiet(dvcqgNhomTTHCChiTietDTOS);
                lichSuDongBoService.capNhatThoiGianDongBo(lichSuDongBo, null, new Date(), dvcqgNhomTTHCChiTietDTOS.size(), TrangThaiDongBo.UPDATED, soLuongTruocDongBo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveAllNhomTTHCChiTiet(List<DVCQGNhomTTHCChiTietDTO> dvcqgNhomTTHCChiTietDTOS) {
        nhomThuTucHanhChinhRepository.saveAll(
                nhomThuTucHanhChinhMapper.toDVCQGNhomThuTucHanhChinhChiTietList(dvcqgNhomTTHCChiTietDTOS));
    }
}

