package sdt.osgi.springboot.dvcqgthutuchanhchinhdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGCachThucThucHienDTO {

    private String kenh;
    private List<DVCQGThoiGianDTO> thoiGian;

}
