package sdt.osgi.springboot.dvcqgthutuchanhchinhdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGThanhPhanHoSoDTO {

    private String truongHop;
    private List<DVCQGGiayToDTO> giayTo;


}
