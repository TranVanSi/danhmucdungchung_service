package sdt.osgi.springboot.dvcqgthutuchanhchinhdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGPhiLePhiDTO {

    private String maPhiLePhi;
    private String soTien;
    private String donVi;
    private String moTa;
    private String tenTep;
    private String url;

}
