package sdt.osgi.springboot.dvcqgthutuchanhchinhdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGCanCuPhapLyDTO {


    private String soVanBan;
    private String tenVanBan;
    private String diaChiTruyCap;
    private String ngayBanHanh;
    private String ngayHieuLuc;
    private String coQuanBanHanh;

}
