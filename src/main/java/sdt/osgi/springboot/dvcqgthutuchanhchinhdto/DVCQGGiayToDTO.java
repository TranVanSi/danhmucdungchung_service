package sdt.osgi.springboot.dvcqgthutuchanhchinhdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DVCQGGiayToDTO {

    private String maGiayTo;
    private String tenGiayTo;
    private String soBanChinh;
    private String soBanSao;
    private String tenMauDon;
    private String url;


}
