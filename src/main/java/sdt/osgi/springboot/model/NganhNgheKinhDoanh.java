package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class NganhNgheKinhDoanh extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ma;
    private String ten;
    private String chaMa;
    private byte cap;
    private int daXoa;
    @JsonIgnore
    @OneToMany(mappedBy = "nganhNgheKinhDoanh", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<DoanhNghiep2NganhNgheKinhDoanh> doanhNghiep2NganhNgheKinhDoanhs;
}
