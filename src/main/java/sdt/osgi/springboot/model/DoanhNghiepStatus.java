package sdt.osgi.springboot.model;

public enum DoanhNghiepStatus {
    TN(0),
    FDI(1);


    private final Integer status;

    private DoanhNghiepStatus(int value) {
        this.status = value;
    }

    public int value() {
        return this.status;
    }
    @Override
    public String toString() {
        return status.toString();
    }

}
