package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class NhaHang extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String dacSan;
    private int soLuongBep;
    private int soLuongPhucVu;
    private int soLuongBan;
    private int soLuongGhe;
    private String dichVuPhongRieng;
    private int sucChuaPhongRieng;
    private int dichVuTaxi;
    private String phongCachNhaHang;
    private String url;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;

    public NhaHang() {

    }
}
