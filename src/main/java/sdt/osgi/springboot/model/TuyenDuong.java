package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class TuyenDuong extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private Long quanHuyenId;
    private Long phuongXaId;
    private String thonPho;
    private int daXoa;
    private Long chaId;
    private String diemDau;
    private String diemCuoi;
    private Float chieuDai;
    private Float chieuRongNen;
    private Float chieuRongMat;

    public TuyenDuong() {

    }

}
