package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class DiaDiemDuLich extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String tenKhac;
    private String diaChi;
    private String netVanHoa;
    private String anh;
    private String diemNoiBat;
    private int dichVuVanChuyen;
    private String dienTich;
    private String hinhThucDuLich;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;

    public DiaDiemDuLich() {

    }
}
