package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DongBoDonVi {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String ma;
    private String maDonVi;
    private String tenDonVi;
    private String capDonVi;
    private String idCoQuanChuQuan;
    private String diaChi;
    int daXoa;
}
