package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@Builder
public class VanBanPhapLy extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String tenNguoiKy;
    private String soKyHieu;
    private Date ngayBanHanh;
    private Date ngayHetHan;
    private String tenCoQuanBanHanh;
    private String tenLinhVuc;
    private String tenLoaiVanBan;
    private String trichYeu;
    private String noiDung;
    private String vanBanDinhKem;
    private int daXoa;

    public VanBanPhapLy() {

    }

}
