package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DongBoCongChuc {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String ma;
    private String hoTen;
    private String dienThoai;
    private String email;
    private String gioiTinh;
    private String ngaySinh;
    private String soCmnd;
    private String ngayCapCmnd;
    private int idChucVu;
    private String chucVu;
    private int idDonVi;
    private String donVi;
    private int idPhongBan;
    private String phongBan;
    private String anhThe;
    int daXoa;
}
