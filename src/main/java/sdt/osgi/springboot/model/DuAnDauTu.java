package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class DuAnDauTu extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String tenDuAn;
    private String tongMucDauTu;
    private String diaDiem;
    private String quyMoDuAn;
    private String vatTuThietBi;
    private String duKienTienDo;
    private String hoanVon;
    private String dienTichChiemDung;
    private String soDienThoai;
    private String phanBoVon;
    private String uuDai;
    private String anh;
    private String url;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;

    public DuAnDauTu() {

    }
}
