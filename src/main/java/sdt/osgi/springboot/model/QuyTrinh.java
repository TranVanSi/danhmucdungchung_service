package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class QuyTrinh extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String maHinhThucVanBan;
    private Long coQuanId;
    private Long capVanBanId;
    private String urlFileMoTa;
    private int daXoa;
    private String tenCoQuan;
    private String maThuTucHanhChinh;
    private String tenThuTucHanhChinh;


    public QuyTrinh() {

    }

}
