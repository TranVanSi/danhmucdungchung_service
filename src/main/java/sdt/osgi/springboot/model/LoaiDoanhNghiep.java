package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoaiDoanhNghiep extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private int daXoa;
    private String qdBanHanhSuaDoi;

    private Date qdNgayBanHanh;

    private String qdCoQuanBanHanh;
    @OneToMany(mappedBy = "loaiDoanhNghiep", cascade=CascadeType.ALL)
    private List<DoanhNghiep> doanhNghieps;
}
