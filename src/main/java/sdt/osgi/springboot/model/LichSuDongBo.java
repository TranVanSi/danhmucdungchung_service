package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sdt.osgi.springboot.util.TrangThaiDongBo;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LichSuDongBo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lopDongBo;
    private Date thoiGianXoaDuLieuCu;
    private Date thoiGianBatDauDongBo;
    private Date thoiGianKetThucDongBo;
    private TrangThaiDongBo trangThaiDongBo;
    private int soLuongDongBo;
    private int soLuongTruocDongBo;
}
