package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@Builder
public class LichSuTruyCap  extends Auditable<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long nguoiDungId;
    private String ten;
    private String diaChiDangNhap;
    private Date thoiGianDangNhap;
    private String mayChu;
    private String tenMien;
    private long coQuanQuanLyId;
    private long congChucId;
    private int daXoa;
    private String nguoiTao;
    private String nguoiSua;

    public LichSuTruyCap() {
    }
}
