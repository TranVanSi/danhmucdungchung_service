package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Builder
public class NhomThuTucHanhChinh extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    @Column(name = "nhomThuTucChaId")
    private long chaId;
    @Column(name = "capCoQuanThucHienId")
    private byte capCQQLId;
    @Column(name = "loaiNhomThuTuc")
    private int loaiNhomTTHC;
    @OneToMany(mappedBy = "nhomThuTucHanhChinh", cascade=CascadeType.ALL)
    private List<ThuTucHanhChinh> thuTucHanhChinhs;
    private int daXoa;
    private String chaMa;

    public NhomThuTucHanhChinh() {

    }

}
