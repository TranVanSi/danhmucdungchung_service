package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TrangThaiDoanhNghiep extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ten;
    private int daXoa;
    @OneToMany(mappedBy = "trangThaiDoanhNghiep", cascade=CascadeType.ALL)
    private List<DoanhNghiep> doanhNghieps;
}
