package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class KhachSan extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private int soLuongPhong;
    private int soTang;
    private String gioNhanPhong;
    private String gioTraPhong;
    private int choDauXe;
    private int dichVuTaxi;
    private int hoTroXeLan;
    private int phongHutThuoc;
    private int wifiTrongPhong;
    private int dichVuDoiTien;
    private String url;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;

    public KhachSan() {

    }
}
