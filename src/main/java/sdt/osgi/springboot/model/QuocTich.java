package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@Builder
public class QuocTich extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String maQuocGia;
    private String tenQuocGiaDayDuBangTiengViet;
    @Column(name = "ma_alpha_3")
    private String maAlpha3;
    private int daXoa;
    private String nguoiTao;
    private String nguoiSua;
    private String tenQuocGiaDayDuBangTiengAnh;
    private String tenQuocGiaVietGonBangTiengViet;
    private String tenQuocGiaVietGonBangTiengAnh;
    @Column(name = "cac_ngon_ngu_hanh_chinh_alpha_2")
    private String cacNgonNguHanhChinhAlpha2;
    @Column(name = "cac_ngon_ngu_hanh_chinh_alpha_3")
    private String cacNgonNguHanhChinhAlpha3;
    private String cacTenDiaPhuongVietGon;
    private String nuocDocLap;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;

    public QuocTich() {

    }

}
