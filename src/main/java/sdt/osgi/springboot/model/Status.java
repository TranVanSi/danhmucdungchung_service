package sdt.osgi.springboot.model;

public enum Status {

    SUCCESS(1),
    FAIL(0);

    private final Integer status;

    private Status(int value) {
        this.status = value;
    }

    public int value() {
        return this.status;
    }

    @Override
    public String toString() {
        return status.toString();
    }

}
