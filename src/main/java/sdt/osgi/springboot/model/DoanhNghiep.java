package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DoanhNghiep extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String tenDoanhNghiep;
    private String maDoanhNghiep;
    private String maSoThue;
    private String diaChiDoanhNghiep;
    private Long diaChiDoanhNghiepTinhId;
    private Long diaChiDoanhNghiepHuyenId;
    private Long diaChiDoanhNghiepXaId;
    private String vonDieuLe;
    @ManyToOne(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "trangThaiDoanhNghiepId")
    @JsonIgnore
    private TrangThaiDoanhNghiep trangThaiDoanhNghiep;
    private String dienThoaiDoanhNghiep;
    private String email;
    private String hoTenNdd;
    private Date ngaySinhNdd;
    private String cmndNguoiDaiDien;
    private Date ngayCapCmndNdd;
    private Long noiCapCmndNddId;
    private String chuSoHuu;
    @ManyToOne(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "loaiHinhDoanhNghiepId")
    @JsonIgnore
    private LoaiDoanhNghiep loaiDoanhNghiep;
    private Integer soLaoDong;
    private String danhSachThanhVienGopVon;
    private String danhSachCoDong;
    private int loaiDoanhNghiepEnum;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;
    @JsonIgnore
    @OneToMany(mappedBy = "doanhNghiep", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<DoanhNghiep2NganhNgheKinhDoanh> doanhNghiep2NganhNgheKinhDoanhs;
}
