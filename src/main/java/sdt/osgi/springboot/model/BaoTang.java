package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class BaoTang extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String lichSuHinhThanh;
    private String lichSuPhatTrien;
    private int soLuongCoVat;
    private int soPhong;
    private int soLuongVatPhamTrungBay;
    private String chuDe;
    private int dichVuPhongChieuPhim;
    private String boSuuTap;
    private String thuVienLuuTru;
    private String giaVeThamQuan;
    private String url;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;

    public BaoTang() {

    }
}
