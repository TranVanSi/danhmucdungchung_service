package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DoanhNghiep2NganhNgheKinhDoanh{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "doanhNghiepId")
    private DoanhNghiep doanhNghiep;
    @ManyToOne
    @JoinColumn(name = "nganhNgheKinhDoanhId")
    private NganhNgheKinhDoanh nganhNgheKinhDoanh;
}
