package sdt.osgi.springboot.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private Date ngayTao;

    @LastModifiedDate
    private Date ngaySua;

    @CreatedBy
    @Column(nullable = false, updatable = false)
    private U nguoiTao;

    @LastModifiedBy
    private U nguoiSua;
}
