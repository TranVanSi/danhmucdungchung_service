package sdt.osgi.springboot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@Builder
public class DiTichLichSu extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String lichSuHinhThanh;
    private String quaTrinhPhatTrien;
    private String loTrinh;
    @Column(name="yNghiaLichSu")
    private String ynghiaLichSu;
    private String viTri;
    private String capDiTichLichSu;
    private String url;
    @Size(max = 1000)
    private String diaChiGoogleMap;
    private int daXoa;

    public DiTichLichSu() {

    }

}
