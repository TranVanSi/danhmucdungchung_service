package sdt.osgi.springboot.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@Builder
public class ThuTucHanhChinh extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;
    private String ten;
    private String maNiemYet;
    private String tenRutGon;
    private String maQuyTrinh;
    @ManyToOne(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "nhomThuTucHanhChinhId")
    @JsonIgnore
    private NhomThuTucHanhChinh nhomThuTucHanhChinh;
    private int mucDoDvc;
    private String trinhTuThucHien;
    private String cachThucHien;
    private String canCuPhapLy;
    private String yeuCauDieuKienThucHien;
    private long capCoQuanThucHienId;
    private String thanhPhanHoSo;
    private Integer soBoHoSo;
    private String bieuMauDinhKem;
    private Byte trangThai;
    private String soQdCongBo;
    private Date ngayBanHanhQd;
    private Date ngayApDung;
    private String phi;
    private String lePhi;
    private String thoiHanGiaiQuyet;
    private String coQuanThucHien;
    private Integer soNgayXuLy;
    private Integer soNgayNiemYet;
    private String ketQuaThucHien;
    private String doiTuongThucHien;
    private String maGiayToKemTheo;
    private int daXoa;

    public ThuTucHanhChinh() {

    }

}
