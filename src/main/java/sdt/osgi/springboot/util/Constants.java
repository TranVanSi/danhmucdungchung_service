package sdt.osgi.springboot.util;

public class Constants {
    public static int DAXOA = 0;
    public static int XOA = 1;
    public static int CAPTINH = 1;
    public static int CAPHUYEN = 2;
    public static int CAPXA = 3;
    public static int SUCCESS = 200;
    public static int FAIL = 400;
    public static String SPACE = " ";
    public static final long MACDINH_TINH_DAKLAK = 87252;



    public static interface LoaiNhom {
        public static int VOBOC = 1;
        public static int CHITIET = 2;
        public static String NHOM_VO_BOC = " Vỏ bọc";
        public static String NHOM_CHI_TIET = " Chi tiết";
    }

    public static class MucDoDichVuCong {
        public static final int TRUC_TIEP = 2;
        public static final int TRUC_TUYEN = 3;
        public static final int TRUC_TUYEN_THANH_TOAN_TRUC_TUYEN = 4;
    }

}
