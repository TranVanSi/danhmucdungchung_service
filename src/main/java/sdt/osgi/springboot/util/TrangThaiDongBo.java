package sdt.osgi.springboot.util;
import java.util.Arrays;

public enum TrangThaiDongBo {
    DELETED(0),
    PROCESSING(1),
    UPDATED(2);

    public Integer id;

    TrangThaiDongBo(Integer id) {
        this.id = id;
    }

    public static TrangThaiDongBo getTrangThaiDongBoById(Integer id) {
        return Arrays.stream(TrangThaiDongBo.values())
                .filter(trangThaiDongBo -> trangThaiDongBo.id == id)
                .findFirst()
                .orElse(null);
    }
}
