package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.*;
import sdt.osgi.springboot.service.*;

@Service
@RequiredArgsConstructor
public class TinhTrangHonNhanValidate {

    public boolean validateTinhTrangHonNhan(TinhTrangHonNhanDTO itemDraft, TinhTrangHonNhanDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }

        return valid;
    }


}
