package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.BaoTangDTO;

@Service
@RequiredArgsConstructor
public class BaoTangValidate {

    public boolean validateBaoTang(BaoTangDTO itemDraft, BaoTangDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }
        return valid;
    }


}
