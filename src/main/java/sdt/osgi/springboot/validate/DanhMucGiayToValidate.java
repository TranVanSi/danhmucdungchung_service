package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.DanhMucGiayToDTO;

@Service
@RequiredArgsConstructor
public class DanhMucGiayToValidate {

    public boolean validateDanhMucGiayTo(DanhMucGiayToDTO itemDraft, DanhMucGiayToDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }
        String ma = itemDraft.getMa();

        if ((ma == null && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }
        return valid;


    }


}
