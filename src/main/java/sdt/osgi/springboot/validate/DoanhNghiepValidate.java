package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.DoanhNghiepDTO;

@Service
@RequiredArgsConstructor
public class DoanhNghiepValidate {

    public boolean validateDoanhNghiep(DoanhNghiepDTO itemDraft, DoanhNghiepDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMaDoanhNghiep().equals(itemByMa.getMaDoanhNghiep())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId()))  {
            valid = false;
        }

        return valid;
    }


}
