package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.TinhTrangHonNhanDTO;
import sdt.osgi.springboot.dto.TonGiaoDTO;
import sdt.osgi.springboot.service.TinhTrangHonNhanService;

@Service
@RequiredArgsConstructor
public class TonGiaoValidate {

    public boolean validateTonGiao(TonGiaoDTO itemDraft, TonGiaoDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }

        return valid;
    }


}
