package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.DoKhanVanBanDTO;
import sdt.osgi.springboot.dto.LoaiVanBanDTO;

@Service
@RequiredArgsConstructor
public class DoKhanVanBanValidate {

    public boolean validateDoKhanVanBan(DoKhanVanBanDTO itemDraft, DoKhanVanBanDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }

        return valid;
    }


}
