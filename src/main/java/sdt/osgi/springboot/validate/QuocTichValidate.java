package sdt.osgi.springboot.validate;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dto.QuocTichDTO;
import sdt.osgi.springboot.dto.TinhTrangHonNhanDTO;

@Service
@RequiredArgsConstructor
public class QuocTichValidate {

    public boolean validateQuocTich(QuocTichDTO itemDraft, QuocTichDTO itemByMa) throws Exception {

        boolean valid = true;
        long id = itemDraft.getId();

        if ((id == 0 && itemByMa != null && itemDraft.getMaQuocGia().equals(itemByMa.getMaQuocGia())) ||
                (id > 0 && itemByMa != null && id != itemByMa.getId())) {
            valid = false;
        }

        return valid;
    }


}
