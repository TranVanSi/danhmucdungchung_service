package sdt.osgi.springboot.task;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import sdt.osgi.springboot.dvcservice.*;

import java.io.IOException;

@Component("dmdcScheduler")
@RequiredArgsConstructor
@EnableScheduling
public class DanhMucDungChungScheduler {

    private final DichVuCongQuocGiaService dichVuCongQuocGiaService;
    private final DichVuCongDanTocService dichVuCongDanTocService;
    private final DichVuCongTonGiaoService dichVuCongTonGiaoService;
    private final DichVuCongTinhTrangHonNhanService dichVuCongTinhTrangHonNhanService;
    private final DichVuCongDonViHanhChinhService dichVuCongDonViHanhChinhService;
    private final DichVuCongTrinhDoChuyenMonService dichVuCongTrinhDoChuyenMonService;
    private final DichVuCongDoKhanVanBanService dichVuCongDoKhanVanBanService;
    private final DichVuCongCapCoQuanThucHienService dichVuCongCapCoQuanThucHienService;
    private final DichVuCongNhomTTHCService dichVuCongNhomTTHCService;
    private final DichVuCongDoiTuongThucHienService dichVuCongDoiTuongThucHienService;
    private final DichVuCongThuTucHanhChinhService dichVuCongThuTucHanhChinhService;
    private final CanBoCongChucService canBoCongChucService;
    private final DVCDongBoCapDonViService dvcDongBoCapDonViService;
    private final DVCDongBoDonViService dvcDongBoDonViService;
    private final DVCDongBoPhongBanService dvcDongBoPhongBanService;
    private final DVCDongBoCongChucService dvcDongBoCongChucService;
    private final DichVuCongCoQuanQuanLyService dichVuCongCoQuanQuanLyService;

    public void dongBoHangThang() {
        dichVuCongCoQuanQuanLyService.dongBoDuLieuCoQuanQuanLy();
        dvcDongBoCongChucService.dongBoDuLieuCongChuc();
        dvcDongBoCapDonViService.dongBoDuLieuCapDonVi();
        dvcDongBoDonViService.dongBoDuLieuDonVi();
        dvcDongBoPhongBanService.dongBoDuLieuPhongBan();
        canBoCongChucService.dongBoDuLieuChucVu();
        dichVuCongDoiTuongThucHienService.dongBoDuLieuDoiTuongThucHien();
        dichVuCongCapCoQuanThucHienService.dongBoDuLieuCapCoQuanThucHien();
        dichVuCongNhomTTHCService.dongBoDuLieuNhomTTHCVoBoc();
        dichVuCongNhomTTHCService.dongBoDuLieuNhomTTHCChiTiet();
        dichVuCongThuTucHanhChinhService.dongBoDuLieuThuTucHanhChinh();
        dichVuCongDoKhanVanBanService.dongBoDuLieuDoKhanVanBan();
        dichVuCongTrinhDoChuyenMonService.dongBoDuLieuTrinhDoChuyenMonKyThuat();
        dichVuCongDonViHanhChinhService.dongBoDuLieuDonViHanhChinhCapTinh();
        dichVuCongDonViHanhChinhService.dongBoDuLieuDonViHanhChinhCapHuyen();
        dichVuCongDonViHanhChinhService.dongBoDuLieuDonViHanhChinhCapXa();
        dichVuCongQuocGiaService.dongBoDuLieuQuocGia();
        dichVuCongDanTocService.dongBoDuLieuDanToc();
        dichVuCongTonGiaoService.dongBoDuLieuTonGiao();
        dichVuCongTinhTrangHonNhanService.dongBoDuLieuTinhTrangHonNhan();
    }

    public void dongBoHangNgay() {
    }

    public void dongBoHangTuan() {
    }

    public void dongBoHangNam() {
    }
}
