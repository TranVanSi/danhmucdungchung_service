package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DoiTuongThucHienDTO;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGDoiTuongThucHienDTO;
import sdt.osgi.springboot.model.DoiTuongThucHien;
import sdt.osgi.springboot.viewdto.DoiTuongThucHienViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoiTuongThucHienMapper {
    DoiTuongThucHien doiTuongThucHienDTOToDoiTuongThucHien(DoiTuongThucHienDTO doiTuongThucHienDTO);
    DoiTuongThucHienDTO doiTuongThucHienToDoiTuongThucHienDTO(DoiTuongThucHien doiTuongThucHien);
    List<DoiTuongThucHienDTO> toDoiTuongThucHienDTOList(List<DoiTuongThucHien> doiTuongThucHiens);
    List<DoiTuongThucHien> toDoiTuongThucHienList(List<DoiTuongThucHienDTO> doiTuongThucHienDTOS);
    List<DoiTuongThucHienViewDTO> toDoiTuongThucHienViewDTOList(List<DoiTuongThucHien> doiTuongThucHiens);

    @Mapping(source = "dvcqgDoiTuongThucHienDTO.maDoiTuong",target = "ma")
    @Mapping(source = "dvcqgDoiTuongThucHienDTO.tenDoiTuong",target = "ten")
    DoiTuongThucHien toDVCQGDoiTuongThucHien(DVCQGDoiTuongThucHienDTO dvcqgDoiTuongThucHienDTO);
    List<DoiTuongThucHien> toDVCQGDoiTuongThucHienList(List<DVCQGDoiTuongThucHienDTO> dvcqgDoiTuongThucHienDTOS);
}