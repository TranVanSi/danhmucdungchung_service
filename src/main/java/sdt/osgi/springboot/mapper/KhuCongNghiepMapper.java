package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.KhuCongNghiepDTO;
import sdt.osgi.springboot.model.KhuCongNghiep;
import sdt.osgi.springboot.viewdto.KhuCongNghiepViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KhuCongNghiepMapper {
    KhuCongNghiep khuCongNghiepDTOToKhuCongNghiep(KhuCongNghiepDTO khuCongNghiepDTO);

    KhuCongNghiepDTO khuCongNghiepToKhuCongNghiepDTO(KhuCongNghiep khuCongNghiep);

    List<KhuCongNghiepDTO> toKhuCongNghiepDTOList(List<KhuCongNghiep> khuCongNghieps);

    List<KhuCongNghiep> toKhuCongNghiepList(List<KhuCongNghiepDTO> khuCongNghiepDTOS);

    List<KhuCongNghiepViewDTO> toKhuCongNghiepViewDTOList(List<KhuCongNghiep> khuCongNghieps);
}
