package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;

import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapHuyenDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapTinhDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapXaDTO;
import sdt.osgi.springboot.model.DonViHanhChinh;
import sdt.osgi.springboot.viewdto.DonViHanhChinhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DonViHanhChinhMapper {
    @Mapping(source = "donViHanhChinhDTO.capDonViHanhChinhId", target = "capDonViHanhChinh.id")
    DonViHanhChinh toDonViHanhChinh(DonViHanhChinhDTO donViHanhChinhDTO);

    @Mapping(source = "donViHanhChinh.capDonViHanhChinh.ten", target = "tenCapDonViHanhChinh")
    @Mapping(source = "donViHanhChinh.capDonViHanhChinh.id", target = "capDonViHanhChinhId")
    DonViHanhChinhDTO toDonViHanhChinhDTO(DonViHanhChinh donViHanhChinh);

    List<DonViHanhChinhDTO> toDonViHanhChinhDTOList(List<DonViHanhChinh> donViHanhChinhs);

    List<DonViHanhChinh> toDonViHanhChinhList(List<DonViHanhChinhDTO> donViHanhChinhDTOS);

    List<DonViHanhChinhViewDTO> toDonViHanhChinhViewDTOList(List<DonViHanhChinh> donViHanhChinhs);


    @Mapping(source = "dvcqgtDonViHanhChinhCapTinhDTOS.maSoCapTinh", target = "ma")
    @Mapping(source = "dvcqgtDonViHanhChinhCapTinhDTOS.tenDonViHanhChinh", target = "ten")
    @Mapping(source = "dvcqgtDonViHanhChinhCapTinhDTOS.capDonViHanhChinhId", target = "capDonViHanhChinhId")
    DonViHanhChinhDTO toDVCQGDonViHanhChinhCapTinh(DVCQGTDonViHanhChinhCapTinhDTO dvcqgtDonViHanhChinhCapTinhDTOS);

    List<DonViHanhChinhDTO> toDVCQGDonViHanhChinhCapTinh(List<DVCQGTDonViHanhChinhCapTinhDTO> dvcqgtDonViHanhChinhCapTinhDTOS);

    @Mapping(source = "dvcqgtDonViHanhChinhCapHuyenDTOS.maSoCapQuanHuyenThiXa", target = "ma")
    @Mapping(source = "dvcqgtDonViHanhChinhCapHuyenDTOS.tenDonViHanhChinh", target = "ten")
    @Mapping(source = "dvcqgtDonViHanhChinhCapHuyenDTOS.maSoCapTinh", target = "chaMa")
    @Mapping(source = "dvcqgtDonViHanhChinhCapHuyenDTOS.capDonViHanhChinhId", target = "capDonViHanhChinhId")
    DonViHanhChinhDTO toDVCQGDonViHanhChinhCapHuyen(DVCQGTDonViHanhChinhCapHuyenDTO dvcqgtDonViHanhChinhCapHuyenDTOS);

    List<DonViHanhChinhDTO> toDVCQGDonViHanhChinhCapHuyen(List<DVCQGTDonViHanhChinhCapHuyenDTO> dvcqgtDonViHanhChinhCapHuyenDTOS);

    @Mapping(source = "dvcqgtDonViHanhChinhCapXaDTOS.maSoCapPhuongXa", target = "ma")
    @Mapping(source = "dvcqgtDonViHanhChinhCapXaDTOS.tenDonViHanhChinh", target = "ten")
    @Mapping(source = "dvcqgtDonViHanhChinhCapXaDTOS.maSoCapQuanHuyenThiXa", target = "chaMa")
    @Mapping(source = "dvcqgtDonViHanhChinhCapXaDTOS.capDonViHanhChinhId", target = "capDonViHanhChinhId")
    DonViHanhChinhDTO toDVCQGDonViHanhChinhCapXa(DVCQGTDonViHanhChinhCapXaDTO dvcqgtDonViHanhChinhCapXaDTOS);

    List<DonViHanhChinhDTO> toDVCQGDonViHanhChinhCapXa(List<DVCQGTDonViHanhChinhCapXaDTO> dvcqgtDonViHanhChinhCapXaDTOS);
}
