package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DongBoCongChucDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCongChucDTO;
import sdt.osgi.springboot.model.DongBoCongChuc;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DongBoCongChucMapper {
    DongBoCongChuc toDongBoCongChucDTOToDongBoCongChuc(DongBoCongChucDTO dongBoCongChucDTO);

    DongBoCongChucDTO toDongBoCongChucToDongBoCongChucDTO(DongBoCongChuc dongBoCongChuc);

    List<DongBoCongChucDTO> toDongBoCongChucDTOList(List<DongBoCongChuc> DongBoCongChucs);

    List<DongBoCongChuc> toDongBoCongChucList(List<DongBoCongChucDTO> dongBoCongChucDTOS);

    @Mapping(source = "dvcDongBoCongChucDTO.id",target = "ma")
    @Mapping(source = "dvcDongBoCongChucDTO.hoTen",target = "hoTen")
    @Mapping(source = "dvcDongBoCongChucDTO.dienThoai",target = "dienThoai")
    @Mapping(source = "dvcDongBoCongChucDTO.email",target = "email", qualifiedByName = "convertEmail")
    @Mapping(source = "dvcDongBoCongChucDTO.gioiTinh",target = "gioiTinh")
    @Mapping(source = "dvcDongBoCongChucDTO.ngaySinh",target = "ngaySinh")
    @Mapping(source = "dvcDongBoCongChucDTO.soCmnd",target = "soCmnd")
    @Mapping(source = "dvcDongBoCongChucDTO.ngayCapCmnd",target = "ngayCapCmnd")
    @Mapping(source = "dvcDongBoCongChucDTO.idChucVu",target = "idChucVu")
    @Mapping(source = "dvcDongBoCongChucDTO.chucVu",target = "chucVu")
    @Mapping(source = "dvcDongBoCongChucDTO.idDonVi",target = "idDonVi")
    @Mapping(source = "dvcDongBoCongChucDTO.donVi",target = "donVi")
    @Mapping(source = "dvcDongBoCongChucDTO.idPhongBan",target = "idPhongBan")
    @Mapping(source = "dvcDongBoCongChucDTO.phongBan",target = "phongBan")
    @Mapping(source = "dvcDongBoCongChucDTO.anhThe",target = "anhThe")
    DongBoCongChuc DONG_BO_CONG_CHUC(DVCDongBoCongChucDTO dvcDongBoCongChucDTO);
    List<DongBoCongChuc> DONG_BO_CONG_CHUCS(List<DVCDongBoCongChucDTO> dvcDongBoCongChucDTOS);


}
