package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.TrinhDoChuyenMonKyThuatDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTonGiaoDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTrinhDoChuyenMonDTO;
import sdt.osgi.springboot.model.TonGiao;
import sdt.osgi.springboot.model.TrinhDoChuyenMonKyThuat;
import sdt.osgi.springboot.viewdto.TrinhDoChuyenMonKyThuatViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TrinhDoChuyenMonKyThuatMapper {
    TrinhDoChuyenMonKyThuat trinhDoChuyenMonKyThuatDTOToTrinhDoChuyenMonKyThuat(TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatDTO);

    TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatToTrinhDoChuyenMonKyThuatDTO(TrinhDoChuyenMonKyThuat trinhDoChuyenMonKyThuat);

    List<TrinhDoChuyenMonKyThuatDTO> toTrinhDoChuyenMonKyThuatDTOList(List<TrinhDoChuyenMonKyThuat> trinhDoChuyenMonKyThuats);

    List<TrinhDoChuyenMonKyThuat> toTrinhDoChuyenMonKyThuatList(List<TrinhDoChuyenMonKyThuatDTO> trinhDoChuyenMonKyThuatDTOS);

    List<TrinhDoChuyenMonKyThuatViewDTO> toTrinhDoChuyenMonKyThuatViewDTOList(List<TrinhDoChuyenMonKyThuat> trinhDoChuyenMonKyThuats);

    @Mapping(source = "dvcqgTrinhDoChuyenMonDTO.professionalid", target = "ma")
    @Mapping(source = "dvcqgTrinhDoChuyenMonDTO.qualification", target = "ten")
    TrinhDoChuyenMonKyThuat toDVCQGTrinhDoChuyenMon(DVCQGTrinhDoChuyenMonDTO dvcqgTrinhDoChuyenMonDTO);

    List<TrinhDoChuyenMonKyThuat> toDVCQGTrinhDoChuyenMonList(List<DVCQGTrinhDoChuyenMonDTO> dvcqgTrinhDoChuyenMonDTO);
}
