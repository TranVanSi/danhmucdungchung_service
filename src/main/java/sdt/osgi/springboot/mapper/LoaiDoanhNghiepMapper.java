package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.LoaiDoanhNghiepDTO;
import sdt.osgi.springboot.model.LoaiDoanhNghiep;
import sdt.osgi.springboot.viewdto.LoaiDoanhNghiepViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LoaiDoanhNghiepMapper {
    LoaiDoanhNghiep loaiDoanhNghiepDTOToLoaiDoanhNghiep(LoaiDoanhNghiepDTO loaiDoanhNghiepDTO);

    LoaiDoanhNghiepDTO loaiDoanhNghiepToLoaiDoanhNghiepDTO(LoaiDoanhNghiep loaiDoanhNghiep);

    List<LoaiDoanhNghiepDTO> toLoaiDoanhNghiepDTOList(List<LoaiDoanhNghiep> loaiDoanhNghieps);

    List<LoaiDoanhNghiep> toLoaiDoanhNghiepList(List<LoaiDoanhNghiepDTO> loaiDoanhNghiepDTOS);

    List<LoaiDoanhNghiepViewDTO> toLoaiDoanhNghiepViewDTOList(List<LoaiDoanhNghiep> loaiDoanhNghieps);
}
