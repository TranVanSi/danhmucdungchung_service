package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.QuyTrinhDTO;
import sdt.osgi.springboot.model.QuyTrinh;
import sdt.osgi.springboot.viewdto.QuyTrinhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuyTrinhMapper {
    QuyTrinh quyTrinhDTOToQuyTrinh(QuyTrinhDTO quyTrinhDTO);

    QuyTrinhDTO quyTrinhToQuyTrinhDTO(QuyTrinh quyTrinh);

    List<QuyTrinhDTO> toQuyTrinhDTOList(List<QuyTrinh> quyTrinhs);

    List<QuyTrinh> toQuyTrinhList(List<QuyTrinhDTO> quyTrinhDTOS);

    List<QuyTrinhViewDTO> toQuyTrinhViewDTOList(List<QuyTrinh> quytrinhs);
}
