package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DoanhNghiepDTO;
import sdt.osgi.springboot.model.DoanhNghiep;
import sdt.osgi.springboot.viewdto.DoanhNghiepViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoanhNghiepMapper {
    @Mapping(source = "doanhNghiepDTO.loaiHinhDoanhNghiepId", target = "loaiDoanhNghiep.id")
    @Mapping(source = "doanhNghiepDTO.trangThaiDoanhNghiepId", target = "trangThaiDoanhNghiep.id")
    @Mapping(source = "doanhNghiepDTO.ngayCapCmndNdd", target = "ngayCapCmndNdd", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "doanhNghiepDTO.ngaySinhNdd", target = "ngaySinhNdd", dateFormat = "dd/MM/yyyy")
    DoanhNghiep toDoanhNghiepDTOToDoanhNghiep(DoanhNghiepDTO doanhNghiepDTO);

    @Mapping(source = "doanhNghiep.loaiDoanhNghiep.id", target = "loaiHinhDoanhNghiepId")
    @Mapping(source = "doanhNghiep.trangThaiDoanhNghiep.id", target = "trangThaiDoanhNghiepId")
    @Mapping(source = "doanhNghiep.ngayCapCmndNdd", target = "ngayCapCmndNdd", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "doanhNghiep.ngaySinhNdd", target = "ngaySinhNdd", dateFormat = "yyyy-MM-dd")
    DoanhNghiepDTO toDoanhNghiepToDoanhNghiepDTO(DoanhNghiep doanhNghiep);

    List<DoanhNghiepDTO> toDoanhNghiepDTOList(List<DoanhNghiep> DoanhNghieps);

    List<DoanhNghiep> toDoanhNghiepList(List<DoanhNghiepDTO> doanhNghiepDTOS);

    List<DoanhNghiepViewDTO> toDoanhNghiepViewDTOList(List<DoanhNghiep> doanhNghieps);
}
