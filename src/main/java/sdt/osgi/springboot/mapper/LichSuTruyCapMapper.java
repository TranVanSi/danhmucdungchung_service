package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.LichSuTruyCapDTO;
import sdt.osgi.springboot.model.LichSuTruyCap;
import sdt.osgi.springboot.viewdto.LichSuTruyCapViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LichSuTruyCapMapper {
    LichSuTruyCap toLichSuTruyCap(LichSuTruyCapDTO lichSuTruyCapDTO);

    LichSuTruyCapDTO toLichSuTruyCapDTO(LichSuTruyCap lichSuTruyCap);

    List<LichSuTruyCapDTO> toLichSuTruyCapDTOList(List<LichSuTruyCap> lichSuTruyCaps);

    List<LichSuTruyCap> toLichSuTruyCapList(List<LichSuTruyCapDTO> lichSuTruyCapDTOS);

    List<LichSuTruyCapViewDTO> toLichSuTruyCapViewDTOList(List<LichSuTruyCap> lichSuTruyCaps);
}
