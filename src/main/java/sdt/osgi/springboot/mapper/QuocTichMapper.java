package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.QuocTichDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGDanTocDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGQuocGiaDTO;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.model.QuocTich;
import sdt.osgi.springboot.viewdto.QuocTichViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuocTichMapper {
    QuocTich quocTichDTOToQuocTich(QuocTichDTO quocTichDTO);

    QuocTichDTO quocTichToQuocTichDTO(QuocTich quocTich);

    List<QuocTichDTO> toQuocTichDTOList(List<QuocTich> quocTiches);

    List<QuocTich> toQuocTichList(List<QuocTichDTO> quocTichDTOS);

    List<QuocTichViewDTO> toQuocTichViewDTOList(List<QuocTich> quocTiches);

    @Mapping(source = "dvcqgQuocGiaDTO.maQuocGia", target = "maQuocGia")
    @Mapping(source = "dvcqgQuocGiaDTO.tenQuocGiaDayDuBangTiengViet", target = "tenQuocGiaDayDuBangTiengViet")
    @Mapping(source = "dvcqgQuocGiaDTO.maAlpha_3", target = "maAlpha3")
    @Mapping(source = "dvcqgQuocGiaDTO.tenQuocGiaDayDuBangTiengAnh", target = "tenQuocGiaDayDuBangTiengAnh")
    @Mapping(source = "dvcqgQuocGiaDTO.tenQuocGiaVietGonBangTiengViet", target = "tenQuocGiaVietGonBangTiengViet")
    @Mapping(source = "dvcqgQuocGiaDTO.tenQuocGiaVietGonBangTiengAnh", target = "tenQuocGiaVietGonBangTiengAnh")
    @Mapping(source = "dvcqgQuocGiaDTO.cacNgonNguHanhChinhAlpha_2", target = "cacNgonNguHanhChinhAlpha2")
    @Mapping(source = "dvcqgQuocGiaDTO.cacNgonNguHanhChinhAlpha_3", target = "cacNgonNguHanhChinhAlpha3")
    @Mapping(source = "dvcqgQuocGiaDTO.cacTenDiaPhuongVietGon", target = "cacTenDiaPhuongVietGon")
    @Mapping(source = "dvcqgQuocGiaDTO.nuocDocLap", target = "nuocDocLap")
    @Mapping(source = "dvcqgQuocGiaDTO.qdBanHanhQdSuaDoi", target = "qdBanHanhSuaDoi")
    @Mapping(source = "dvcqgQuocGiaDTO.ngayBanHanhQd", target = "qdNgayBanHanh")
    @Mapping(source = "dvcqgQuocGiaDTO.coQuanBanHanhQd", target = "qdCoQuanBanHanh")
    QuocTich toDVCQGQuocGia(DVCQGQuocGiaDTO dvcqgQuocGiaDTO);

    List<QuocTich> toDVCQGQuocGiaList(List<DVCQGQuocGiaDTO> dvcqgQuocGiaDTOS);
}
