package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.NhomThuTucHanhChinhDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGNhomTTHCChiTietDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGNhomTTHCVoBocDTO;
import sdt.osgi.springboot.model.NhomThuTucHanhChinh;
import sdt.osgi.springboot.viewdto.NhomThuTucHanhChinhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NhomThuTucHanhChinhMapper {
    NhomThuTucHanhChinh nhomThuTucHanhChinhDTOToNhomThuTucHanhChinh(NhomThuTucHanhChinhDTO nhomThuTucHanhChinhDTO);

    NhomThuTucHanhChinhDTO nhomThuTucHanhChinhToNhomThuTucHanhChinhDTO(NhomThuTucHanhChinh nhomThuTucHanhChinh);

    List<NhomThuTucHanhChinhDTO> toNhomThuTucHanhChinhgDTOList(List<NhomThuTucHanhChinh> nhomThuTucHanhChinhs);

    List<NhomThuTucHanhChinh> toNhomThuTucHanhChinhList(List<NhomThuTucHanhChinhDTO> nhomThuTucHanhChinhDTOS);

    List<NhomThuTucHanhChinhViewDTO> toNhomThuTucHanhChinhViewDTOList(List<NhomThuTucHanhChinh> nhomThuTucHanhChinhs);


    @Mapping(source = "dvcqgNhomTTHCVoBocDTO.maNganh",target = "ma")
    @Mapping(source = "dvcqgNhomTTHCVoBocDTO.tenNganh",target = "ten")
    @Mapping(source = "dvcqgNhomTTHCVoBocDTO.loaiNhomTTHC",target = "loaiNhomTTHC")
    NhomThuTucHanhChinh toDVCQGNhomThuTucHanhChinhVoBoc(DVCQGNhomTTHCVoBocDTO dvcqgNhomTTHCVoBocDTO);
    List<NhomThuTucHanhChinh> toDVCQGNhomThuTucHanhChinhVoBocList(List<DVCQGNhomTTHCVoBocDTO> dvcqgNhomTTHCVoBocDTOS);

    @Mapping(source = "dvcqgNhomTTHCChiTietDTO.maLinhVuc",target = "ma")
    @Mapping(source = "dvcqgNhomTTHCChiTietDTO.tenLinhVuc",target = "ten")
    @Mapping(source = "dvcqgNhomTTHCChiTietDTO.maNganh",target = "chaMa")
    @Mapping(source = "dvcqgNhomTTHCChiTietDTO.loaiNhomTTHC",target = "loaiNhomTTHC")
    NhomThuTucHanhChinh toDVCQGNhomThuTucHanhChinhChiTiet(DVCQGNhomTTHCChiTietDTO dvcqgNhomTTHCChiTietDTO);
    List<NhomThuTucHanhChinh> toDVCQGNhomThuTucHanhChinhChiTietList(List<DVCQGNhomTTHCChiTietDTO> dvcqgNhomTTHCChiTietDTOS);
}
