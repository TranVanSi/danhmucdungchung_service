package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.LoaiToChucCaNhanDTO;
import sdt.osgi.springboot.model.LoaiToChucCaNhan;
import sdt.osgi.springboot.viewdto.LoaiToChucCaNhanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LoaiToChucCaNhanMapper {
    LoaiToChucCaNhan loaiToChucCaNhanDTOToLoaiToChucCaNhan(LoaiToChucCaNhanDTO loaiToChucCaNhanDTO);

    LoaiToChucCaNhanDTO loaiToChucCaNhanToLoaiToChucCaNhanDTO(LoaiToChucCaNhan loaiToChucCaNhan);

    List<LoaiToChucCaNhanDTO> toLoaiToChucCaNhanDTOList(List<LoaiToChucCaNhan> loaiToChucCaNhans);

    List<LoaiToChucCaNhan> toLoaiToChucCaNhanList(List<LoaiToChucCaNhanDTO> loaiToChucCaNhanDTOS);

    List<LoaiToChucCaNhanViewDTO> toLoaiToChucCaNhanViewDTOList(List<LoaiToChucCaNhan> loaiToChucCaNhans);
}
