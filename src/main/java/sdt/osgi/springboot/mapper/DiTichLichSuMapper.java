package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DiTichLichSuDTO;
import sdt.osgi.springboot.model.DiTichLichSu;
import sdt.osgi.springboot.viewdto.DiTichLichSuViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DiTichLichSuMapper {
    DiTichLichSu diTichLichSuDTOToDiTichLichSu(DiTichLichSuDTO diTichLichSuDTO);

    DiTichLichSuDTO diTichLichSuToDiTichLichSuDTO(DiTichLichSu diTichLichSu);

    List<DiTichLichSuDTO> toDiTichLichSuDTOList(List<DiTichLichSu> diTichLichSus);

    List<DiTichLichSu> toDiTichLichSuList(List<DiTichLichSuDTO> diTichLichSuDTOS);

    List<DiTichLichSuViewDTO> toDiTichLichSuViewDTOList(List<DiTichLichSu> diTichLichSus);
}
