package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGDanTocDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTDonViHanhChinhCapTinhDTO;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.viewdto.DanTocViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DanTocMapper {
    DanToc danTocDTOToDanToc(DanTocDTO danTocDTO);

    DanTocDTO danTocToDanTocDTO(DanToc danToc);

    List<DanTocDTO> toDanTocDTOList(List<DanToc> danTocs);

    List<DanToc> toDanTocList(List<DanTocDTO> danTocDTOS);

    List<DanTocViewDTO> toDanTocViewDTOList(List<DanToc> danTocs);

    @Mapping(source = "dvcqgDanTocDTO.maDanToc", target = "ma")
    @Mapping(source = "dvcqgDanTocDTO.tenDanToc", target = "ten")
    @Mapping(source = "dvcqgDanTocDTO.qdBanHanhQdSuaDoi", target = "qdBanHanhSuaDoi")
    @Mapping(source = "dvcqgDanTocDTO.ngayBanHanhQd", target = "qdNgayBanHanh")
    @Mapping(source = "dvcqgDanTocDTO.coQuanBanHanhQd", target = "qdCoQuanBanHanh")
    DanToc toDVCQGDanToc(DVCQGDanTocDTO dvcqgDanTocDTO);

    List<DanToc> toDVCQGDanTocDTOList(List<DVCQGDanTocDTO> dvcqgDanTocDTOS);
}
