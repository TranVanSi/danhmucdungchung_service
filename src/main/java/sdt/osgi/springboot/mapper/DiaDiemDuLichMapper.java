package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DiaDiemDuLichDTO;
import sdt.osgi.springboot.model.DiaDiemDuLich;
import sdt.osgi.springboot.viewdto.DiaDiemDuLichViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DiaDiemDuLichMapper {
    DiaDiemDuLich diaDiemDuLichDTOToDiaDiemDuLich(DiaDiemDuLichDTO diaDiemDuLichDTO);

    DiaDiemDuLichDTO diaDiemDuLichToDiaDiemDuLichDTO(DiaDiemDuLich diaDiemDuLich);

    List<DiaDiemDuLichDTO> toDiaDiemDuLichDTOList(List<DiaDiemDuLich> diaDiemDuLichs);

    List<DiaDiemDuLich> toDiaDiemDuLichList(List<DiaDiemDuLichDTO> diaDiemDuLichDTOS);

    List<DiaDiemDuLichViewDTO> toDiaDiemDuLichViewDTOList(List<DiaDiemDuLich> diaDiemDuLichs);

}
