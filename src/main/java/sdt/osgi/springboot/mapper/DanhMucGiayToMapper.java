package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DanhMucGiayToDTO;
import sdt.osgi.springboot.model.DanhMucGiayTo;
import sdt.osgi.springboot.viewdto.DanhMucGiayToViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DanhMucGiayToMapper {
    DanhMucGiayTo danhMucGiayToDTOToDanhMucGiayTo(DanhMucGiayToDTO danhMucGiayToDTO);

    DanhMucGiayToDTO danhMucGiayToToDanhMucGiayToDTO(DanhMucGiayTo danhMucGiayTo);

    List<DanhMucGiayToDTO> toDanhMucGiayTogDTOList(List<DanhMucGiayTo> danhMucGiayTos);

    List<DanhMucGiayTo> toDanhMucGiayToList(List<DanhMucGiayToDTO> danhMucGiayToDTOS);

    List<DanhMucGiayToViewDTO> toDanhMucGiayToViewDTOList(List<DanhMucGiayTo> danhMucGiayTos);
}
