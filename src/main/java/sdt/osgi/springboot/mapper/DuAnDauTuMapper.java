package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DuAnDauTuDTO;
import sdt.osgi.springboot.model.DuAnDauTu;
import sdt.osgi.springboot.viewdto.DuAnDauTuViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DuAnDauTuMapper {
    DuAnDauTu duAnDauTuDTOToDuAnDauTu(DuAnDauTuDTO duAnDauTuDTO);

    DuAnDauTuDTO duAnDauTuToDuAnDauTuDTO(DuAnDauTu duAnDauTu);

    List<DuAnDauTuDTO> toDuAnDauTuDTOList(List<DuAnDauTu> duAnDauTus);

    List<DuAnDauTu> toDuAnDauTuList(List<DuAnDauTuDTO> duAnDauTuDTOS);

    List<DuAnDauTuViewDTO> toDuAnDauTuViewDTOList(List<DuAnDauTu> duAnDauTus);
}
