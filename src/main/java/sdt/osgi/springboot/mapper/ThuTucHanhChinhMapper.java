package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.ThuTucHanhChinhDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGThuTucHanhChinhDTO;
import sdt.osgi.springboot.dvcservice.DichVuCongThuTucHanhChinhCustomService;
import sdt.osgi.springboot.model.ThuTucHanhChinh;
import sdt.osgi.springboot.viewdto.ThuTucHanhChinhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring", uses =  DichVuCongThuTucHanhChinhCustomService.class)
public interface ThuTucHanhChinhMapper {
    @Mapping(source = "thuTucHanhChinhDTO.nhomThuTucHanhChinhId", target = "nhomThuTucHanhChinh.id")
    @Mapping(source = "thuTucHanhChinhDTO.ngayBanHanhQd", target = "ngayBanHanhQd", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "thuTucHanhChinhDTO.ngayApDung", target = "ngayApDung", dateFormat = "dd/MM/yyyy")
    ThuTucHanhChinh thuTucHanhChinhDTOToThuTucHanhChinh(ThuTucHanhChinhDTO thuTucHanhChinhDTO);

    @Mapping(source = "thuTucHanhChinh.nhomThuTucHanhChinh.ten", target = "tenNhomThuTucHanhChinh")
    @Mapping(source = "thuTucHanhChinh.nhomThuTucHanhChinh.id", target = "nhomThuTucHanhChinhId")
    @Mapping(source = "thuTucHanhChinh.ngayBanHanhQd", target = "ngayBanHanhQd", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "thuTucHanhChinh.ngayApDung", target = "ngayApDung", dateFormat = "yyyy-MM-dd")
    ThuTucHanhChinhDTO thuTucHanhChinhToThuTucHanhChinhDTO(ThuTucHanhChinh thuTucHanhChinh);

    List<ThuTucHanhChinhDTO> toThuTucHanhChinhgDTOList(List<ThuTucHanhChinh> thuTucHanhChinhs);

    List<ThuTucHanhChinh> toThuTucHanhChinhList(List<ThuTucHanhChinhDTO> thuTucHanhChinhDTOS);

    List<ThuTucHanhChinhViewDTO> toThuTucHanhChinhViewDTOList(List<ThuTucHanhChinh> thuTucHanhChinhs);



    @Mapping(source = "dvcqgThuTucHanhChinhDTO.maTTHC",target = "ma")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.tenTTHC",target = "ten")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.maTTHC",target = "maNiemYet")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.linhVucThucHien",target = "nhomThuTucHanhChinh.id", qualifiedByName = "getNhomThuTucHanhChinhIdByMa")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.capThucHien",target = "capCoQuanThucHienId", qualifiedByName = "getCapThucHien")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.trangThai",target = "trangThai")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.trinhTuThucHien",target = "trinhTuThucHien", qualifiedByName = "trinhTuThucHien")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.cachThucThucHien",target = "cachThucHien", qualifiedByName = "cachThucThucHien")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.cachThucThucHien",target = "soNgayXuLy", qualifiedByName = "soNgayXuLy")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.thanhPhanHoSo",target = "thanhPhanHoSo", qualifiedByName = "thanhPhanHoSo")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.doiTuongThucHien",target = "doiTuongThucHien", qualifiedByName = "doiTuongThucHien")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.doiTuongThucHien",target = "mucDoDvc", qualifiedByName = "mucDoDvc")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.coQuanThucHien",target = "coQuanThucHien", qualifiedByName = "coQuanThucHien")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.ketQuaThucHien",target = "ketQuaThucHien", qualifiedByName = "ketQuaThucHien")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.canCuPhapLy",target = "canCuPhapLy", qualifiedByName = "canCuPhapLy")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.yeuCau",target = "yeuCauDieuKienThucHien")
    @Mapping(expression = "java(new java.util.Date())",target = "ngayApDung", dateFormat = "dd/MM/yyyy")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.idQuyetDinhCongBo",target = "ngayBanHanhQd", qualifiedByName = "ngayBanHanh")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.idQuyetDinhCongBo",target = "soQdCongBo", qualifiedByName = "soQuyetDinh")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.cachThucThucHien",target = "phi", qualifiedByName = "phiLePhi")
    @Mapping(source = "dvcqgThuTucHanhChinhDTO.cachThucThucHien",target = "lePhi", qualifiedByName = "phiLePhi")
    ThuTucHanhChinh dvcqgThuTucHanhChinhDTOToThuTucHanhChinh(DVCQGThuTucHanhChinhDTO dvcqgThuTucHanhChinhDTO);
    List<ThuTucHanhChinh> dvcqgThuTucHanhChinhDTOToThuTucHanhChinhList(List<DVCQGThuTucHanhChinhDTO> dvcqgThuTucHanhChinhDTOS);

}
