package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.SuKienVanHoaDTO;
import sdt.osgi.springboot.model.SuKienVanHoa;
import sdt.osgi.springboot.viewdto.SuKienVanHoaViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SuKienVanHoaMapper {
    SuKienVanHoa suKienVanHoaDTOToSuKienVanHoa(SuKienVanHoaDTO suKienVanHoaDTO);

    SuKienVanHoaDTO suKienVanHoaToSuKienVanHoaDTO(SuKienVanHoa suKienVanHoa);

    List<SuKienVanHoaDTO> toSuKienVanHoaDTOList(List<SuKienVanHoa> suKienVanHoas);

    List<SuKienVanHoa> toSuKienVanHoaList(List<SuKienVanHoaDTO> suKienVanHoaDTOS);

    List<SuKienVanHoaViewDTO> toSuKienVanHoaViewDTOList(List<SuKienVanHoa> suKienVanHoas);
}
