package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.KhachSanDTO;
import sdt.osgi.springboot.model.KhachSan;
import sdt.osgi.springboot.viewdto.KhachSanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KhachSanMapper {
    KhachSan khachSanDTOToKhachSan(KhachSanDTO khachSanDTO);

    KhachSanDTO khachSanToKhachSanDTO(KhachSan khachSan);

    List<KhachSanDTO> toKhachSanDTOList(List<KhachSan> khachSans);

    List<KhachSan> toKhachSanList(List<KhachSanDTO> khachSanDTOS);

    List<KhachSanViewDTO> toKhachSanViewDTOList(List<KhachSan> khachSans);
}
