package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.SinhTracHocDTO;
import sdt.osgi.springboot.model.SinhTracHoc;
import sdt.osgi.springboot.viewdto.SinhTracHocViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SinhTracHocMapper {
    SinhTracHoc sinhTracHocDTOToSinhTracHoc(SinhTracHocDTO sinhTracHocDTO);

    SinhTracHocDTO sinhTracHocToSinhTracHocDTO(SinhTracHoc sinhTracHoc);

    List<SinhTracHocDTO> toSinhTracHocDTOList(List<SinhTracHoc> sinhTracHocs);

    List<SinhTracHoc> toSinhTracHocList(List<SinhTracHocDTO> sinhTracHocDTOS);

    List<SinhTracHocViewDTO> toSinhTracHocViewDTOList(List<SinhTracHoc> sinhTracHocs);
}
