package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DongBoPhongBanDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoPhongBanDTO;
import sdt.osgi.springboot.model.DongBoPhongBan;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DongBoPhongBanMapper {
    DongBoPhongBan toDongBoPhongBanDTOToDongBoPhongBan(DongBoPhongBanDTO dongBoPhongBanDTO);

    DongBoPhongBanDTO toDongBoPhongBanToDongBoPhongBanDTO(DongBoPhongBan dongBoPhongBan);

    List<DongBoPhongBanDTO> toDongBoPhongBanDTOList(List<DongBoPhongBan> DongBoPhongBans);

    List<DongBoPhongBan> toDongBoPhongBanList(List<DongBoPhongBanDTO> dongBoPhongBanDTOS);

    @Mapping(source = "dvcDongBoPhongBanDTO.id",target = "ma")
    @Mapping(source = "dvcDongBoPhongBanDTO.parentId",target = "parentId")
    @Mapping(source = "dvcDongBoPhongBanDTO.maPhongBan",target = "maPhongBan")
    @Mapping(source = "dvcDongBoPhongBanDTO.tenPhongBan",target = "tenPhongBan")
    DongBoPhongBan DONG_BO_PHONG_BAN(DVCDongBoPhongBanDTO dvcDongBoPhongBanDTO);


}
