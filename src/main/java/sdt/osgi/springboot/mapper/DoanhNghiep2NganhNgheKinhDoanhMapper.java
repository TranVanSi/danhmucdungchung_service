package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DoanhNghiep2NganhNgheKinhDoanhDTO;
import sdt.osgi.springboot.model.DoanhNghiep2NganhNgheKinhDoanh;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoanhNghiep2NganhNgheKinhDoanhMapper {
    DoanhNghiep2NganhNgheKinhDoanh DOANH_NGHIEP_2_NGANH_NGHE_KINH_DOANH(DoanhNghiep2NganhNgheKinhDoanhDTO doanhNghiep2NganhNgheKinhDoanhDTO);
    DoanhNghiep2NganhNgheKinhDoanhDTO DOANH_NGHIEP_2_NGANH_NGHE_KINH_DOANH_DTO(DoanhNghiep2NganhNgheKinhDoanh doanhNghiep2NganhNgheKinhDoanh);
    List<DoanhNghiep2NganhNgheKinhDoanhDTO> DOANH_NGHIEP_2_NGANH_NGHE_KINH_DOANH_DTOS (List<DoanhNghiep2NganhNgheKinhDoanh> doanhNghiep2NganhNgheKinhDoanhs);
}
