package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DongBoDonViDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoDonViDTO;
import sdt.osgi.springboot.model.DongBoDonVi;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DongBoDonViMapper {
    DongBoDonVi toDongBoDonViDTOToDongBoDonVi(DongBoDonViDTO dongBoDonViDTO);

    DongBoDonViDTO toDongBoDonViToDongBoDonViDTO(DongBoDonVi dongBoDonVi);

    List<DongBoDonViDTO> toDongBoDonViDTOList(List<DongBoDonVi> DongBoDonVis);

    List<DongBoDonVi> toDongBoDonViList(List<DongBoDonViDTO> dongBoDonViDTOS);

    @Mapping(source = "dvcDongBoDonViDTO.id",target = "ma")
    @Mapping(source = "dvcDongBoDonViDTO.maDonVi",target = "maDonVi")
    @Mapping(source = "dvcDongBoDonViDTO.tenDonVi",target = "tenDonVi")
    @Mapping(source = "dvcDongBoDonViDTO.capDonVi",target = "capDonVi")
    @Mapping(source = "dvcDongBoDonViDTO.idCoQuanChuQuan",target = "idCoQuanChuQuan")
    @Mapping(source = "dvcDongBoDonViDTO.diaChi",target = "diaChi")
    DongBoDonVi DONG_BO_DON_VI(DVCDongBoDonViDTO dvcDongBoDonViDTO);


}
