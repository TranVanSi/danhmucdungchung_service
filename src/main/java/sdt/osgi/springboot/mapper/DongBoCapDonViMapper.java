package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DongBoCapDonViDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCapDonViDTO;
import sdt.osgi.springboot.model.DongBoCapDonVi;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DongBoCapDonViMapper {
    DongBoCapDonVi toDongBoCapDonViDTOToDongBoCapDonVi(DongBoCapDonViDTO dongBoCapDonViDTO);

    DongBoCapDonViDTO toDongBoCapDonViToDongBoCapDonViDTO(DongBoCapDonVi dongBoCapDonVi);

    List<DongBoCapDonViDTO> toDongBoCapDonViDTOList(List<DongBoCapDonVi> DongBoCapDonVis);

    List<DongBoCapDonVi> toDongBoCapDonViList(List<DongBoCapDonViDTO> dongBoCapDonViDTOS);

    @Mapping(source = "dvcDongBoCapDonViDTO.parentId",target = "parentId")
    @Mapping(source = "dvcDongBoCapDonViDTO.id",target = "ma")
    @Mapping(source = "dvcDongBoCapDonViDTO.ten",target = "ten")
    DongBoCapDonVi DONG_BO_CAP_DON_VI(DVCDongBoCapDonViDTO dvcDongBoCapDonViDTO);


}
