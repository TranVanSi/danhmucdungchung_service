package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.CapCoQuanThucHienDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGCapCoQuanThucHienDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGDanTocDTO;
import sdt.osgi.springboot.model.CapCoQuanThucHien;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.viewdto.CapCoQuanThucHienViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CapCoQuanThucHienMapper {
    CapCoQuanThucHien capCoQuanThucHienDTOToCapCoQuanThucHien(CapCoQuanThucHienDTO capCoQuanThucHienDTO);

    CapCoQuanThucHienDTO capCoQuanThucHienToCapCoQuanThucHienDTO(CapCoQuanThucHien capCoQuanThucHien);

    List<CapCoQuanThucHienDTO> toCapCoQuanThucHiengDTOList(List<CapCoQuanThucHien> capCoQuanThucHiens);

    List<CapCoQuanThucHien> toCapCoQuanThucHienList(List<CapCoQuanThucHienDTO> capCoQuanThucHienDTOS);

    List<CapCoQuanThucHienViewDTO> toCapCoQuanThucHienViewDTOList(List<CapCoQuanThucHien> coQuanThucHiens);


    @Mapping(source = "dvcqgCapCoQuanThucHienDTO.capThucHien",target = "ma")
    @Mapping(source = "dvcqgCapCoQuanThucHienDTO.tenCap",target = "ten")
    CapCoQuanThucHien toDVCQGCapCoQuanThucHien(DVCQGCapCoQuanThucHienDTO dvcqgCapCoQuanThucHienDTO);
    List<CapCoQuanThucHien> toDVCQGCapCoQuanThucHienList(List<DVCQGCapCoQuanThucHienDTO> dvcqgCapCoQuanThucHienDTOS);
}
