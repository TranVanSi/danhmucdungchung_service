package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.SanGolfDTO;
import sdt.osgi.springboot.model.SanGolf;
import sdt.osgi.springboot.viewdto.SanGolfViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SanGolfMapper {
    SanGolf sanGoldDTOToSanGold(SanGolfDTO sanGolfDTO);

    SanGolfDTO sanGoldToSanGoldDTO(SanGolf sanGolf);

    List<SanGolfDTO> toSanGoldDTOList(List<SanGolf> sanGolves);

    List<SanGolf> toSanGoldList(List<SanGolfDTO> sanGolfDTOS);

    List<SanGolfViewDTO> toSanGoldViewDTOList(List<SanGolf> sanGolves);
}
