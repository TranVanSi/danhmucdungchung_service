package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.NhaHangDTO;
import sdt.osgi.springboot.model.NhaHang;
import sdt.osgi.springboot.viewdto.NhaHangViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NhaHangMapper {
    NhaHang nhaHangDTOToNhaHang(NhaHangDTO nhaHangDTO);

    NhaHangDTO nhaHangToNhaHangDTO(NhaHang nhaHang);

    List<NhaHangDTO> toNhaHangDTOList(List<NhaHang> nhaHangs);

    List<NhaHang> toNhaHangList(List<NhaHangDTO> nhaHangDTOS);

    List<NhaHangViewDTO> toNhaHangViewDTOList(List<NhaHang> nhaHangs);
}
