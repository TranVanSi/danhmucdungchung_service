package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.TinhTrangHonNhanDTO;
import sdt.osgi.springboot.dto.TonGiaoDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGQuocGiaDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTinhTrangHonNhanDTO;
import sdt.osgi.springboot.model.QuocTich;
import sdt.osgi.springboot.model.TinhTrangHonNhan;
import sdt.osgi.springboot.model.TonGiao;
import sdt.osgi.springboot.viewdto.TinhTrangHonNhanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TinhTrangHonNhanMapper {
    TinhTrangHonNhan tinhTrangHonNhanDTOToTinhTrangHonNhan(TinhTrangHonNhanDTO tinhTrangHonNhanDTO);

    TinhTrangHonNhanDTO tinhTrangHonNhanToTinhTrangHonNhanDTO(TinhTrangHonNhan tinhTrangHonNhan);

    List<TinhTrangHonNhanDTO> toTinhTrangHonNhanDTOList(List<TinhTrangHonNhan> tinhTrangHonNhans);

    List<TinhTrangHonNhan> toTinhTrangHonNhanList(List<TinhTrangHonNhanDTO> tinhTrangHonNhanDTOS);

    List<TinhTrangHonNhanViewDTO> toTinhTrangHonNhanViewDTOList(List<TinhTrangHonNhan> tinhTrangHonNhans);

    @Mapping(source = "dvcqgTinhTrangHonNhanDTO.maTinhTrangHonNhan", target = "ma")
    @Mapping(source = "dvcqgTinhTrangHonNhanDTO.tinhTrangHonNhan", target = "ten")
    @Mapping(source = "dvcqgTinhTrangHonNhanDTO.ghiChu", target = "ghiChu")
    @Mapping(source = "dvcqgTinhTrangHonNhanDTO.qdBanHanhQdSuaDoi", target = "qdBanHanhSuaDoi")
    @Mapping(source = "dvcqgTinhTrangHonNhanDTO.ngayBanHanhQd", target = "qdNgayBanHanh")
    @Mapping(source = "dvcqgTinhTrangHonNhanDTO.coQuanBanHanhQd", target = "qdCoQuanBanHanh")
    TinhTrangHonNhan toDVCQGTinhTrangHonNhan(DVCQGTinhTrangHonNhanDTO dvcqgTinhTrangHonNhanDTO);

    List<TinhTrangHonNhan> toDVCQGTinhTrangHonNhanList(List<DVCQGTinhTrangHonNhanDTO> dvcqgTinhTrangHonNhanDTOS);
}
