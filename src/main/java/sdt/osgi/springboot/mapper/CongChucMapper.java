package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCongChucDTO;
import sdt.osgi.springboot.dvcservice.CanBoCongChucCustomService;

import java.util.List;

@Mapper(componentModel = "spring", uses =  CanBoCongChucCustomService.class)
public interface CongChucMapper {

    @Mapping(source = "dongBoCongChucDTO.hoTen",target = "hoVaTen")
    @Mapping(source = "dongBoCongChucDTO.dienThoai",target = "dienThoaiDiDong")
    @Mapping(source = "dongBoCongChucDTO.email",target = "email", qualifiedByName = "convertEmail")
    @Mapping(source = "dongBoCongChucDTO.gioiTinh",target = "gioiTinhId", qualifiedByName = "addGioiTinh")
    @Mapping(source = "dongBoCongChucDTO.ngaySinh",target = "ngaySinh")
    @Mapping(source = "dongBoCongChucDTO.soCmnd",target = "soCMND")
    @Mapping(source = "dongBoCongChucDTO.ngayCapCmnd",target = "ngayCapCMND")
    @Mapping(source = "dongBoCongChucDTO.anhThe",target = "anh")
    CongChucDTO dvcDongBoCongChucDTOtoCongChucDTO(DVCDongBoCongChucDTO dongBoCongChucDTO);
    List<CongChucDTO> dvcDongBoCongChucDTOStoCongChucDTOS(List<DVCDongBoCongChucDTO> dongBoCongChucDTOS);

    DVCDongBoCongChucDTO congChucDTOtoDVCDongBoCongChucDTO(CongChucDTO congChucDTO);

}
