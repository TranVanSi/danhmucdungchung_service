package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.LinhVucKinhDoanhDTO;
import sdt.osgi.springboot.model.LinhVucKinhDoanh;
import sdt.osgi.springboot.viewdto.LinhVucKinhDoanhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LinhVucKinhDoanhMapper {
    LinhVucKinhDoanh linhVucKinhDoanhDTOToLinhVucKinhDoanh(LinhVucKinhDoanhDTO linhVucKinhDoanhDTO);

    LinhVucKinhDoanhDTO linhVucKinhDoanhToLinhVucKinhDoanhDTO(LinhVucKinhDoanh linhVucKinhDoanh);

    List<LinhVucKinhDoanhDTO> toLinhVucKinhDoanhgDTOList(List<LinhVucKinhDoanh> linhVucKinhDoanhs);

    List<LinhVucKinhDoanh> toLinhVucKinhDoanhList(List<LinhVucKinhDoanhDTO> linhVucKinhDoanhDTOS);

    List<LinhVucKinhDoanhViewDTO> toLinhVucKinhDoanhViewDTOList(List<LinhVucKinhDoanh> linhVucKinhDoanhs);
}
