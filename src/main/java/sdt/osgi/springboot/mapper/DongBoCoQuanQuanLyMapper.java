package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DongBoCoQuanQuanLyDTO;
import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGCoQuanQuanLyDTO;
import sdt.osgi.springboot.model.DongBoCoQuanQuanLy;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DongBoCoQuanQuanLyMapper {
    DongBoCoQuanQuanLy DVCCoQuanQuanLyDTOToDVCCoQuanQuanLy(DongBoCoQuanQuanLyDTO DongBoCoQuanQuanLyDTO);
    DongBoCoQuanQuanLyDTO DVCCoQuanQuanLyToDVCCoQuanQuanLyDTO(DongBoCoQuanQuanLy DongBoCoQuanQuanLy);
    List<DongBoCoQuanQuanLyDTO> toDVCCoQuanQuanLyDTOList(List<DongBoCoQuanQuanLy> DongBoCoQuanQuanLIES);
    List<DongBoCoQuanQuanLy> toDVCCoQuanQuanLyList(List<DongBoCoQuanQuanLyDTO> DongBoCoQuanQuanLyDTOS);


    @Mapping(source = "dvcqgCoQuanQuanLyDTO.maDonVi",target = "ma")
    @Mapping(source = "dvcqgCoQuanQuanLyDTO.tenDonVi",target = "tenDonVi")
    @Mapping(source = "dvcqgCoQuanQuanLyDTO.capDonVi",target = "capDonVi")
    @Mapping(source = "dvcqgCoQuanQuanLyDTO.loaiDonVi",target = "loaiDonVi")
    DongBoCoQuanQuanLy toDVCCoQuanQuanLy(DVCQGCoQuanQuanLyDTO dvcqgCoQuanQuanLyDTO);
    List<DongBoCoQuanQuanLy> toDVCQGDVCCoQuanQuanLyList(List<DVCQGCoQuanQuanLyDTO> dvcqgCoQuanQuanLyDTOS);
}