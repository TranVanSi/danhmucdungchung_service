package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.LichSuDongBoDTO;
import sdt.osgi.springboot.model.LichSuDongBo;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LichSuDongBoMapper {
    LichSuDongBo toLichSuDongBo(LichSuDongBoDTO lichSuDongBoDTO);
    @Mapping(source = "lichSuDongBo.trangThaiDongBo.id", target = "trangThaiDongBo")
    LichSuDongBoDTO toLichSuDongBoDTO(LichSuDongBo lichSuDongBo);
    List<LichSuDongBoDTO> toLichSuDongBoDTOList(List<LichSuDongBo> lichSuDongBos);
    List<LichSuDongBo> toLichSuDongBoList(List<LichSuDongBoDTO> lichSuDongBoDTOS);

    default TrangThaiDongBo map(int value) {
        return TrangThaiDongBo.getTrangThaiDongBoById(value);
    }

}
