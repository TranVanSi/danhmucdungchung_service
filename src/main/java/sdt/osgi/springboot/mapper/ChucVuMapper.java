package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.dvcqgdto.DongBoChucVuDTO;
import sdt.osgi.springboot.dvcservice.CanBoCongChucCustomService;

import java.util.List;

@Mapper(componentModel = "spring", uses =  CanBoCongChucCustomService.class)
public interface ChucVuMapper {

    @Mapping(source = "dongBoChucVuDTO.heSo",target = "heSo")
    @Mapping(source = "dongBoChucVuDTO.id",target = "ma", qualifiedByName = "addMaById")
    @Mapping(source = "dongBoChucVuDTO.tenChucVu",target = "ten")
    ChucVuDTO CHUC_VU_DTO(DongBoChucVuDTO dongBoChucVuDTO);
    List<ChucVuDTO> CHUC_VU_DTOS(List<DongBoChucVuDTO> dongBoChucVuDTOS);

}
