package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.QuanHeGiaDinhDTO;
import sdt.osgi.springboot.model.QuanHeGiaDinh;
import sdt.osgi.springboot.viewdto.QuanHeGiaDinhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuanHeGiaDinhMapper {
    QuanHeGiaDinh quanHeGiaDinhDTOToQuanHeGiaDinh(QuanHeGiaDinhDTO quanHeGiaDinhDTO);

    QuanHeGiaDinhDTO quanHeGiaDinhToQuanHeGiaDinhDTO(QuanHeGiaDinh quanHeGiaDinh);

    List<QuanHeGiaDinhDTO> toQuanHeGiaDinhDTOList(List<QuanHeGiaDinh> quanHeGiaDinhs);

    List<QuanHeGiaDinh> toQuanHeGiaDinhList(List<QuanHeGiaDinhDTO> quanHeGiaDinhDTOS);

    List<QuanHeGiaDinhViewDTO> toQuanHeGiaDinhViewDTOList(List<QuanHeGiaDinh> quanHeGiaDinhs);
}
