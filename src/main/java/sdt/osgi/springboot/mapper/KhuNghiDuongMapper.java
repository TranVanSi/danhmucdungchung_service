package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.KhuNghiDuongDTO;
import sdt.osgi.springboot.model.KhuNghiDuong;
import sdt.osgi.springboot.viewdto.KhuNghiDuongViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KhuNghiDuongMapper {
    KhuNghiDuong khuNghiDuongDTOToKhuNghiDuong(KhuNghiDuongDTO khuNghiDuongDTO);

    KhuNghiDuongDTO khuNghiDuongToKhuNghiDuongDTO(KhuNghiDuong khuNghiDuong);

    List<KhuNghiDuongDTO> toKhuNghiDuongDTOList(List<KhuNghiDuong> khuNghiDuongs);

    List<KhuNghiDuong> toKhuNghiDuongList(List<KhuNghiDuongDTO> khuNghiDuongDTOS);

    List<KhuNghiDuongViewDTO> toKhuNghiDuongViewDTOList(List<KhuNghiDuong> khuNghiDuongs);
}
