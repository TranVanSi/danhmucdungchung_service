package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.LinhVucVanBanDTO;
import sdt.osgi.springboot.model.LinhVucVanBan;
import sdt.osgi.springboot.model.LinhVucVanBan;
import sdt.osgi.springboot.viewdto.LinhVucVanBanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LinhVucVanBanMapper {
    LinhVucVanBan linhVucVanBanDTOToLinhVucVanBan(LinhVucVanBanDTO linhVucVanBanDTO);

    LinhVucVanBanDTO linhVucVanBanToLinhVucVanBanDTO(LinhVucVanBan linhVucVanBan);

    List<LinhVucVanBanDTO> toLinhVucVanBanDTOList(List<LinhVucVanBan> linhVucVanBans);

    List<LinhVucVanBan> toLinhVucVanBanList(List<LinhVucVanBanDTO> linhVucVanBanDTOS);

    List<LinhVucVanBanViewDTO> toLinhVucVanBanViewDTOList(List<LinhVucVanBan> linhVucVanBans);
}
