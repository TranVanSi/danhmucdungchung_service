package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.CapCoQuanQuanLyDTO;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCapDonViDTO;
import sdt.osgi.springboot.model.CapCoQuanQuanLy;
import sdt.osgi.springboot.viewdto.CapCoQuanQuanLyViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CapCoQuanQuanLyMapper {
    CapCoQuanQuanLy capCoQuanQuanLyDTOToCapCoQuanQuanLy(CapCoQuanQuanLyDTO capCoQuanQuanLyDTO);

    CapCoQuanQuanLyDTO capCoQuanQuanLyToCapCoQuanQuanLyDTO(CapCoQuanQuanLy capCoQuanQuanLy);

    List<CapCoQuanQuanLyDTO> toCapCoQuanQuanLygDTOList(List<CapCoQuanQuanLy> capCoQuanQuanLys);

    List<CapCoQuanQuanLy> toCapCoQuanQuanLyList(List<CapCoQuanQuanLyDTO> capCoQuanQuanLyDTOS);

    List<CapCoQuanQuanLyViewDTO> toCapCoQuanQuanLyViewDTOList(List<CapCoQuanQuanLy> coQuanQuanLys);
}
