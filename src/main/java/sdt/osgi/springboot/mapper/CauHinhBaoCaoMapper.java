package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.CauHinhBaoCaoDTO;
import sdt.osgi.springboot.model.CauHinhBaoCao;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CauHinhBaoCaoMapper {
    CauHinhBaoCao cauHinhBaoCaoDTOToCauHinhBaoCao(CauHinhBaoCaoDTO cauHinhBaoCaoDTO);
    CauHinhBaoCaoDTO cauHinhBaoCaoToCauHinhBaoCaoDTO(CauHinhBaoCao cauHinhBaoCao);
    List<CauHinhBaoCaoDTO> toCauHinhBaoCaoDTOList(Iterable<CauHinhBaoCao> cauHinhBaoCaos);
    List<CauHinhBaoCao> toCauHinhBaoCaoList(List<CauHinhBaoCaoDTO> cauHinhBaoCaoDTOS);
}
