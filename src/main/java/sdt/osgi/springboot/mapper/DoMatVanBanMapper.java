package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DoMatVanBanDTO;
import sdt.osgi.springboot.model.DoMatVanBan;
import sdt.osgi.springboot.viewdto.DoMatVanBanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoMatVanBanMapper {
    DoMatVanBan doMatVanBanDTOToDoMatVanBan(DoMatVanBanDTO doMatVanBanDTO);

    DoMatVanBanDTO doMatVanBanToDoMatVanBanDTO(DoMatVanBan doMatVanBan);

    List<DoMatVanBanDTO> toDoMatVanBanDTOList(List<DoMatVanBan> doMatVanBans);

    List<DoMatVanBan> toDoMatVanBanList(List<DoMatVanBanDTO> doMatVanBanDTOS);

    List<DoMatVanBanViewDTO> toDoMatVanBanViewDTOList(List<DoMatVanBan> doMatVanBans);
}
