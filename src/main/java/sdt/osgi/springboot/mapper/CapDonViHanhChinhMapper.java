package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.CapDonViHanhChinhDTO;
import sdt.osgi.springboot.model.CapDonViHanhChinh;
import sdt.osgi.springboot.viewdto.CapDonViHanhChinhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CapDonViHanhChinhMapper {
    CapDonViHanhChinh toCapDonViHanhChinh(CapDonViHanhChinhDTO capDonViHanhChinhDTO);

    CapDonViHanhChinhDTO toCapDonViHanhChinhDTO(CapDonViHanhChinh capDonViHanhChinh);

    List<CapDonViHanhChinhDTO> toCapDonViHanhChinhDTOList(List<CapDonViHanhChinh> capDonViHanhChinhs);

    List<CapDonViHanhChinh> toCapDonViHanhChinhList(List<CapDonViHanhChinhDTO> capDonViHanhChinhDTOS);

    List<CapDonViHanhChinhViewDTO> toCapDonViHanhChinhViewDTOList(List<CapDonViHanhChinh> capDonViHanhChinhs);
}
