package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.BaoTangDTO;
import sdt.osgi.springboot.model.BaoTang;
import sdt.osgi.springboot.viewdto.BaoTangViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BaoTangMapper {
    BaoTang baoTangDTOToBaoTang(BaoTangDTO baoTangDTO);

    BaoTangDTO baoTangToBaoTangDTO(BaoTang baoTang);

    List<BaoTangDTO> toBaoTangDTOList(List<BaoTang> baoTangs);

    List<BaoTang> toBaoTangList(List<BaoTangDTO> baoTangDTOS);

    List<BaoTangViewDTO> toBaoTangViewDTOList(List<BaoTang> baoTangs);
}
