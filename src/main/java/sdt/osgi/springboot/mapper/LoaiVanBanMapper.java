package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.LoaiVanBanDTO;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.model.LoaiVanBan;
import sdt.osgi.springboot.viewdto.LoaiVanBanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LoaiVanBanMapper {
    LoaiVanBan loaiVanBanDTOToLoaiVanBan(LoaiVanBanDTO loaiVanBanDTO);

    LoaiVanBanDTO loaiVanBanToLoaiVanBanDTO(LoaiVanBan loaiVanBanDTO);

    List<LoaiVanBanDTO> toLoaiVanBanDTOList(List<LoaiVanBan> loaiVanBans);

    List<LoaiVanBan> toLoaiVanBanList(List<LoaiVanBanDTO> loaiVanBanDTOList);

    List<LoaiVanBanViewDTO> toLoaiVanBanViewDTOList(List<LoaiVanBan> loaiVanBans);
}
