package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.TuyenDuongDTO;
import sdt.osgi.springboot.model.TuyenDuong;
import sdt.osgi.springboot.viewdto.TuyenDuongViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TuyenDuongMapper {
    TuyenDuong tuyenDuongDTOToTuyenDuong(TuyenDuongDTO tuyenDuongDTO);

    TuyenDuongDTO tuyenDuongToTuyenDuongDTO(TuyenDuong tuyenDuong);

    List<TuyenDuongDTO> toTuyenDuonggDTOList(List<TuyenDuong> tuyenDuongs);

    List<TuyenDuong> toTuyenDuongList(List<TuyenDuongDTO> tuyenDuongDTOS);

    List<TuyenDuongViewDTO> toTuyenDuongViewDTOList(List<TuyenDuong> tuyenDuongs);
}
