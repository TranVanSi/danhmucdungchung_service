package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import sdt.osgi.springboot.dto.VanBanPhapLyDTO;
import sdt.osgi.springboot.model.VanBanPhapLy;
import sdt.osgi.springboot.viewdto.VanBanPhapLyViewDTO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Mapper(componentModel = "spring")
public interface VanBanPhapLyMapper {

    @Mapping(source = "vanBanPhapLyDTO.ngayBanHanh", target = "ngayBanHanh", qualifiedByName = "stringDefaultToDate")
    @Mapping(source = "vanBanPhapLyDTO.ngayHetHan", target = "ngayHetHan", qualifiedByName = "stringDefaultToDate")
    VanBanPhapLy vanBanPhapLyDTOToVanBanPhapLy(VanBanPhapLyDTO vanBanPhapLyDTO);

    @Mapping(source = "vanBanPhapLy.ngayBanHanh", target = "ngayBanHanh", dateFormat = "yyyy-MM-dd")
    @Mapping(source = "vanBanPhapLy.ngayHetHan", target = "ngayHetHan", dateFormat = "yyyy-MM-dd")
    VanBanPhapLyDTO vanBanPhapLyToVanBanPhapLyDTO(VanBanPhapLy vanBanPhapLy);

    List<VanBanPhapLyDTO> toVanBanPhapLygDTOList(List<VanBanPhapLy> vanBanPhapLys);

    List<VanBanPhapLy> toVanBanPhapLyList(List<VanBanPhapLyDTO> vanBanPhapLyDTOS);

    List<VanBanPhapLyViewDTO> toVanBanPhapLyViewDTOList(List<VanBanPhapLy> suKienVanHoas);

    @Named("stringDefaultToDate")
    public static Date stringDefaultToDate(String date) throws Exception {

        if (date != null && date.length() > 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            return sdf.parse(date);
        }

        return null;
    }
}
