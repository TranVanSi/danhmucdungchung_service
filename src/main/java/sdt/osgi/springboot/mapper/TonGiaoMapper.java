package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.TonGiaoDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTinhTrangHonNhanDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTonGiaoDTO;
import sdt.osgi.springboot.model.TinhTrangHonNhan;
import sdt.osgi.springboot.model.TonGiao;
import sdt.osgi.springboot.viewdto.TonGiaoViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TonGiaoMapper {
    TonGiao tonGiaoDTOToTonGiao(TonGiaoDTO tonGiaoDTO);

    TonGiaoDTO tonGiaoToTonGiaoDTO(TonGiao tonGiao);

    List<TonGiaoDTO> toTonGiaogDTOList(List<TonGiao> tonGiaos);

    List<TonGiao> toTonGiaoList(List<TonGiaoDTO> tonGiaoDTOS);

    List<TonGiaoViewDTO> toTonGiaoViewDTOList(List<TonGiao> suKienVanHoas);

    @Mapping(source = "dvcqgTonGiaoDTO.maTonGiao", target = "ma")
    @Mapping(source = "dvcqgTonGiaoDTO.tonGiao", target = "ten")
    @Mapping(source = "dvcqgTonGiaoDTO.cacToChucTonGiaoChinh", target = "cacToChucTonGiaoChinh")
    @Mapping(source = "dvcqgTonGiaoDTO.qdBanHanhQdSuaDoi", target = "qdBanHanhSuaDoi")
    @Mapping(source = "dvcqgTonGiaoDTO.ngayBanHanhQd", target = "qdNgayBanHanh")
    @Mapping(source = "dvcqgTonGiaoDTO.coQuanBanHanhQd", target = "qdCoQuanBanHanh")
    TonGiao toDVCQGTonGiao(DVCQGTonGiaoDTO dvcqgTonGiaoDTO);

    List<TonGiao> toDVCQGTonGiaoList(List<DVCQGTonGiaoDTO> dvcqgTonGiaoDTOS);
}
