package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.model.NganhNgheKinhDoanh;
import sdt.osgi.springboot.viewdto.NganhNgheKinhDoanhViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NganhNgheKinhDoanhMapper {
    NganhNgheKinhDoanh NGANH_NGHE_KINH_DOANH(NganhNgheKinhDoanhViewDTO nganhNgheKinhDoanhViewDTO);
    NganhNgheKinhDoanhViewDTO NGANH_NGHE_KINH_DOANH_VIEW_DTO(NganhNgheKinhDoanh nganhNgheKinhDoanh);
    List<NganhNgheKinhDoanhViewDTO> NGANH_NGHE_KINH_DOANH_VIEW_DTOS(List<NganhNgheKinhDoanh> nganhNgheKinhDoanhs);
    List<NganhNgheKinhDoanh> NGANH_NGHE_KINH_DOANHS(List<NganhNgheKinhDoanhViewDTO> nganhNgheKinhDoanhViewDTOS);
}
