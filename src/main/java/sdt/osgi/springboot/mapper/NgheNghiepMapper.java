package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import sdt.osgi.springboot.dto.NgheNghiepDTO;
import sdt.osgi.springboot.model.NgheNghiep;
import sdt.osgi.springboot.viewdto.NgheNghiepViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NgheNghiepMapper {
    NgheNghiep ngheNghiepDTOToNgheNghiep(NgheNghiepDTO ngheNghiepDTO);

    NgheNghiepDTO ngheNghiepToNgheNghiepDTO(NgheNghiep ngheNghiep);

    List<NgheNghiepDTO> toNgheNghiepgDTOList(List<NgheNghiep> ngheNghieps);

    List<NgheNghiep> toNgheNghiepList(List<NgheNghiepDTO> ngheNghiepDTOS);

    List<NgheNghiepViewDTO> toNgheNghiepViewDTOList(List<NgheNghiep> ngheNghieps);
}
