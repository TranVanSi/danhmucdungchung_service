package sdt.osgi.springboot.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import sdt.osgi.springboot.dto.DoKhanVanBanDTO;
import sdt.osgi.springboot.dto.LoaiVanBanDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGDoKhanVanBanDTO;
import sdt.osgi.springboot.dvcqgdto.DVCQGTonGiaoDTO;
import sdt.osgi.springboot.model.DoKhanVanBan;
import sdt.osgi.springboot.model.LoaiVanBan;
import sdt.osgi.springboot.model.TonGiao;
import sdt.osgi.springboot.viewdto.DoKhanVanBanViewDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoKhanVanBanMapper {
    DoKhanVanBan doKhanVanBanDTOToDoKhanVanBan(DoKhanVanBanDTO doKhanVanBanDTO);

    DoKhanVanBanDTO doKhanVanBanToKhanVanBanDTO(DoKhanVanBan doKhanVanBan);

    List<DoKhanVanBanDTO> toKhanVanBanDTOList(List<DoKhanVanBan> doKhanVanBans);

    List<DoKhanVanBan> toKhanVanBanList(List<DoKhanVanBanDTO> doKhanVanBanDTOS);

    List<DoKhanVanBanViewDTO> toDoKhanVanBanViewDTOList(List<DoKhanVanBan> doKhanVanBans);

    @Mapping(source = "dvcqgDoKhanVanBanDTO.maLoaiVanBan", target = "ma")
    @Mapping(source = "dvcqgDoKhanVanBanDTO.tenLoaiVanBan", target = "ten")
    @Mapping(source = "dvcqgDoKhanVanBanDTO.qdBanHanhQdSuaDoi", target = "qdBanHanhSuaDoi")
    @Mapping(source = "dvcqgDoKhanVanBanDTO.ngayBanHanhQd", target = "qdNgayBanHanh")
    @Mapping(source = "dvcqgDoKhanVanBanDTO.coQuanBanHanhQd", target = "qdCoQuanBanHanh")
    DoKhanVanBan toDVCQGTonGiao(DVCQGDoKhanVanBanDTO dvcqgDoKhanVanBanDTO);

    List<DoKhanVanBan> toDVCQGTonGiaoList(List<DVCQGDoKhanVanBanDTO> dvcqgDoKhanVanBanDTOS);
}
