package sdt.osgi.springboot.clients;

import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.osgi.springboot.dvcqgdto.*;

import java.util.List;

import sdt.osgi.springboot.dvcqgthutuchanhchinhdto.DVCQGDoiTuongThucHienDTO;

@Service
@RequiredArgsConstructor
public class DichVuCongQuocGiaClient {
    @Value("${dvcqg.maDonVi}")
    private String maDonVi;

    @Value("${dvcqg.userName}")
    private String userName;

    @Value("${dvcqg.password}")
    private String password;

    @Autowired
    @Qualifier("dichVuCongQuocGiaRest")
    public RestTemplate dichVuCongQuocGiaRestTemplate;

    public String getToken() {
        String path = "/mapi/login";

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", userName);
        jsonObject.put("password", password);

        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        DVCQGTokenDTO response = dichVuCongQuocGiaRestTemplate.postForObject(path, request, DVCQGTokenDTO.class);

        if (response != null && response.getError_code() == 0) {
            return response.getSession();
        }

        return null;
    }

    public String getCapCoQuanThucHien() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhMucCapThucHien");


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getNhomVoBoc() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhMucNganh");


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getNhomChiTiet() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhMucLinhVuc");


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getDoiTuongThucHien() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhMucDoiTuong");


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getThuTuc(String maThuTuc, String token) {

        if (token == null) {
            return null;
        }

        String path = "/mapi/g";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayThuTuc");
        jsonObject.put("maTTHC", maThuTuc);


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getDanhSachThuTuc() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhSachTTHC");


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getQuyetDinhCongBo(String quyetDinhCongBoId, String token) {

        if (token == null) {
            return null;
        }

        String path = "/mapi/g";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();


        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayQDCB");
        jsonObject.put("qdcbId", quyetDinhCongBoId);


        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();
    }

    public String getDataDoiTuongThucHien() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhMucDoiTuong");

        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();

    }

    public String getDataCoQuan() {
        String token = getToken();

        if (token == null) {
            return null;
        }

        String path = "/mapi/call";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.set("dstcode", "VN:GOV:000.00.00.G22:vpcpdvcprovider");

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("session", token);
        jsonObject.put("madonvi", maDonVi);
        jsonObject.put("service", "LayDanhMucCoQuan");

        HttpEntity<String> request =
                new HttpEntity<String>(jsonObject.toString(), headers);

        ResponseEntity<String> response = dichVuCongQuocGiaRestTemplate.exchange(path, HttpMethod.POST, request,
                String.class);

        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("result").toString());
        return jsonArray.toString();

    }
}
