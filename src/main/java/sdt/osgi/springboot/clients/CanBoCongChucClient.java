package sdt.osgi.springboot.clients;

import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.osgi.springboot.util.Utils;

@Service
@RequiredArgsConstructor
public class CanBoCongChucClient {
    @Value("${cbcc.option}")
    private String option;

    @Value("${cbcc.controller}")
    private String controller;

    @Value("${cbcc.username}")
    private String userName;

    @Value("${cbcc.password}")
    private String password;

    @Autowired
    @Qualifier("canBoCongChucRest")
    public RestTemplate canBoCongChucRestTemplate;

    public String getDataByTask(String task) {
        Utils.disableSslVerification();
        String restURL = "/index.php?option=" + option
                + "&controller=" + controller
                + "&task=" + task
                + "&username=" + userName
                + "&password=" + password;

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);

        ResponseEntity<String> response = canBoCongChucRestTemplate.exchange(builder.toUriString(), HttpMethod.GET, null,
                String.class);
        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("ketqua").toString());
        return jsonArray.toString();
    }

    public String getDataCongChuc(String task) {
        Utils.disableSslVerification();
        String restURL = "/index.php?option=" + option
                + "&controller=" + controller
                + "&task=" + task
                + "&username=" + userName
                + "&password=" + password;

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);

        ResponseEntity<String> response = canBoCongChucRestTemplate.exchange(builder.toUriString(), HttpMethod.GET, null,
                String.class);
        System.out.println(response.getBody());
        JSONObject json = new JSONObject(response.getBody());
        JSONArray jsonArray = new JSONArray(json.get("ketqua").toString());
        return jsonArray.toString();
    }
}
