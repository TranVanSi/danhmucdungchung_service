package sdt.osgi.springboot.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.osgi.springboot.dto.ChucVuDTO;
import sdt.osgi.springboot.dto.Response;

@Service
@RequiredArgsConstructor
public class ChucVuClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = "/api/v1/chucvu";

    public boolean addChucVu(ChucVuDTO chucVuDTO) {

        String restURL = path + "/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<ChucVuDTO> entity = new HttpEntity<>(chucVuDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public long countAll() {
        String restURL = path + "/countall";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);

        ResponseEntity<Response<Long>> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<Response<Long>>() {
                });
        return responseEntity.getBody().getData();
    }


    public boolean deleteByMa(String ma) {
        String restURL = path + "/deleteByMa/" + ma;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<Response<ChucVuDTO>> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE,
                null, new ParameterizedTypeReference<Response<ChucVuDTO>>() {
                });

        if (response.getBody().getStatus() == 1) {
            return true;
        }

        return false;
    }

}
