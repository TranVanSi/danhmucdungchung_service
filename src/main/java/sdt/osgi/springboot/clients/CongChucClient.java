package sdt.osgi.springboot.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.osgi.springboot.dto.CongChucDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.util.Utils;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CongChucClient {

    @Autowired
    @Qualifier("quanLyNguoiDungRest")
    public RestTemplate restTemplate;

    @Autowired
    @Qualifier("objectMapper")
    public ObjectMapper objectMapper;

    private final String path = "/api/v1/congchuc";

    public boolean addCongChuc(CongChucDTO congChucDTO) {

        String restURL = path + "/create";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<CongChucDTO> entity = new HttpEntity<>(congChucDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateCongChuc(CongChucDTO congChucDTO) {

        String restURL = path + "/dongbo";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<CongChucDTO> entity = new HttpEntity<>(congChucDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, String.class, new Object[0]);

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public boolean updateAllCongChuc(List<CongChucDTO> congChucDTO) {

        String restURL = path + "/dongboall";
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        HttpEntity<List<CongChucDTO>> entity = new HttpEntity<>(congChucDTO, headers);

        HttpEntity<?> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
                entity, new ParameterizedTypeReference<List<CongChucDTO>>() {
                });

        return ((ResponseEntity<?>) response).getStatusCode().is2xxSuccessful();

    }

    public long countAll() {
        String restURL = path + "/countall";
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);

        ResponseEntity<Response<Long>> responseEntity = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<Response<Long>>() {
                });
        return responseEntity.getBody().getData();
    }


    public boolean deleteByMa(String ma) {
        String restURL = path + "/deleteByMa/" + ma;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<Response<CongChucDTO>> response = restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE,
                null, new ParameterizedTypeReference<Response<CongChucDTO>>() {
                });

        if (response.getBody().getStatus() == 1) {
            return true;
        }

        return false;
    }

    public CongChucDTO findByEmail(String email) {
        String restURL = path + Utils.SLASH + "findByEmail" + Utils.SLASH + email;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(restURL);
        ResponseEntity<CongChucDTO> response = restTemplate.exchange(
                builder.toUriString(), HttpMethod.GET, null, new ParameterizedTypeReference<CongChucDTO>() {
                });

        return response.getBody();

    }

}
