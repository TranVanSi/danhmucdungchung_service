package sdt.osgi.springboot.clients;

import lombok.RequiredArgsConstructor;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sdt.osgi.springboot.util.OAuthConnectResponse;
import sdt.osgi.springboot.util.Utils;

@Service
@RequiredArgsConstructor
public class DataClient {

    @Value("${token.url}")
    private String tokenUrl;

    @Value("${token.clientId}")
    private String clientId;

    @Value("${token.clientSecret}")
    private String clientSecret;

    @Autowired
    @Qualifier("danhMucDungChungRest")
    public RestTemplate restTemplate;

    public String getAccessToken() {
        Utils.disableSslVerification();
        try {
            OAuthClientRequest tokenRequest = OAuthClientRequest.tokenLocation(tokenUrl)
                    .setGrantType(GrantType.CLIENT_CREDENTIALS)
                    .setClientId(clientId)
                    .setClientSecret(clientSecret).buildBodyMessage();

            OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());
            OAuthConnectResponse oAuthResponse = oAuthClient.accessToken(tokenRequest, OAuthConnectResponse.class);

            return oAuthResponse.getAccessToken();
        } catch (OAuthSystemException e) {
            e.printStackTrace();
        } catch (OAuthProblemException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getDataFromServer(String path, HttpMethod httpMethod) {

        String accessToken = getAccessToken();
        Utils.disableSslVerification();

        if (accessToken == null) {
            return null;
        }

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);
        String data = null;

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(
                    builder.toUriString(), httpMethod, Utils.getHeaderToken(accessToken), String.class);
            data = Utils.getListDataFromResponseService(responseEntity.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public boolean getDataFromServer(String path, HttpMethod httpMethod, String body) {

        String accessToken = getAccessToken();
        Utils.disableSslVerification();

        if (accessToken == null) {
            return false;
        }

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(path);

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(
                    builder.toUriString(), httpMethod, Utils.getHeaderTokenBody(accessToken, body), String.class);

            String response = responseEntity.getBody();
            JSONObject object = new JSONObject(response);

            if (object.has("REQUEST_STATUS") && object.getString("REQUEST_STATUS").equalsIgnoreCase("SUCCESSFUL")) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
