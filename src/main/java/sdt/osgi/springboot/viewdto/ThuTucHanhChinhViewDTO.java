package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class ThuTucHanhChinhViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String maNiemYet;
    private String tenRutGon;
    private String maQuyTrinh;
    private long nhomThuTucHanhChinhId;
    private int mucDoDvc;
    private String trinhTuThucHien;
    private String cachThucHien;
    private String canCuPhapLy;
    private String yeuCauDieuKienThucHien;
    private long capCoQuanThucHienId;
    private String thanhPhanHoSo;
    private int soBoHoSo;
    private String bieuMauDinhKem;
    private byte trangThai;
    private String soQdCongBo;
    private String ngayBanHanhQd;
    private String ngayApDung;
    private String phi;
    private String lePhi;
    private String thoiHanGiaiQuyet;
    private String coQuanThucHien;
    private int soNgayXuLy;
    private int soNgayNiemYet;
    private String ketQuaThucHien;
    private String doiTuongThucHien;
    private String maGiayToKemTheo;
    private String tenNhomThuTucHanhChinh;



    public ThuTucHanhChinhViewDTO() {
    }
}
