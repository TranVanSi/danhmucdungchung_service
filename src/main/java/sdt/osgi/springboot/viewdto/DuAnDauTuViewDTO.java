package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class DuAnDauTuViewDTO {
    private long id;
    private String ma;
    private String tenDuAn;
    private String tongMucDauTu;
    private String diaDiem;
    private String quyMoDuAn;
    private String vatTuThietBi;
    private String duKienTienDo;
    private String hoanVon;
    private String dienTichChiemDung;
    private String soDienThoai;
    private String phanBoVon;
    private String uuDai;
    private String anh;
    private String url;
    private String diaChiGoogleMap;

}
