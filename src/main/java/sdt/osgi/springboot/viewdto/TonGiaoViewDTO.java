package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class TonGiaoViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String nguoiTao;
    private String nguoiSua;
    private String cacToChucTonGiaoChinh;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;

}
