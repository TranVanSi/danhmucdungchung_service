package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class NganhNgheKinhDoanhViewDTO {
    private Long id;
    private String ma;
    private String ten;
    private String chaMa;
    private byte cap;

}
