package sdt.osgi.springboot.viewdto;


import lombok.Data;

@Data
public class DonViHanhChinhViewDTO {
    private long id;
    private String ma;
    private String ten;
    private Long chaId;
    private String chaMa;
    private long capDonViHanhChinhId;
    private String maBuuCuc;
    private String tenCapDonViHanhChinh;

}
