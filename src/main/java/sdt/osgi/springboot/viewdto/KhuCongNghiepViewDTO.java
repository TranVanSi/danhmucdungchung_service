package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class KhuCongNghiepViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private int soLuongNhaMay;
    private int soLuongKhoBai;
    private String linhVucDauTu;
    private String giaThueDat;
    private String url;
    private String diaChiGoogleMap;

}
