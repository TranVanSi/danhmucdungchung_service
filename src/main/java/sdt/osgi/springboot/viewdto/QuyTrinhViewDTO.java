package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class QuyTrinhViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String maHinhThucVanBan;
    private Long coQuanId;
    private Long capVanBanId;
    private String urlFileMoTa;
    private int  daXoa;
    private String tenCoQuan;
    private String maThuTucHanhChinh;
    private String tenThuTucHanhChinh;

    public QuyTrinhViewDTO() {
    }
}
