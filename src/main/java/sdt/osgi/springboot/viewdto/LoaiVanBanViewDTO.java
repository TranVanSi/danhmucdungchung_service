package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class LoaiVanBanViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String chuVietTat;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;

    public LoaiVanBanViewDTO() {
    }
}
