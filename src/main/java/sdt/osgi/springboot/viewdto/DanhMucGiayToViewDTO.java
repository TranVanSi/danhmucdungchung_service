package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class DanhMucGiayToViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String tenRutGon;
    private int gioiHanDungLuong;
    private String kieuTaiLieu;


    public DanhMucGiayToViewDTO() {
    }
}
