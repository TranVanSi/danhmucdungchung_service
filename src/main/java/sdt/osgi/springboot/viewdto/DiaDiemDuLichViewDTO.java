package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class DiaDiemDuLichViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String tenKhac;
    private String diaChi;
    private String netVanHoa;
    private String anh;
    private String diemNoiBat;
    private int dichVuVanChuyen;
    private String dienTich;
    private String hinhThucDuLich;
    private String diaChiGoogleMap;

}
