package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class NgheNghiepViewDTO {
    private long id;
    private String ma;
    private String ten;
    private int cap;
    private String chaMa;


    public NgheNghiepViewDTO() {
    }
}
