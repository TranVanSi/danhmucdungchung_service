package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class ResponseView<T> {
    private int status;
    private T data;
}
