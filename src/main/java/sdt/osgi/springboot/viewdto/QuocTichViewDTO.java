package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class QuocTichViewDTO {
    private long id;
    private String maQuocGia;
    private String tenQuocGiaDayDuBangTiengViet;
    private String maAlpha3;
    private String tenQuocGiaDayDuBangTiengAnh;
    private String tenQuocGiaVietGonBangTiengViet;
    private String tenQuocGiaVietGonBangTiengAnh;
    private String cacNgonNguHanhChinhAlpha2;
    private String cacNgonNguHanhChinhAlpha3;
    private String cacTenDiaPhuongVietGon;
    private String nuocDocLap;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;



    public QuocTichViewDTO() {
    }
}
