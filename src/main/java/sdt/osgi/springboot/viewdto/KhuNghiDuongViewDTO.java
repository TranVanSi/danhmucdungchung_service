package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class KhuNghiDuongViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String loaiBietThu;
    private int dichVuSpa;
    private int trungTamHoiNghi;
    private String phongCachThietKe;
    private int soLuongPhong;
    private int dichVuVeMayBay;
    private int dichVuTaxi;
    private int dichVuDuLich;
    private int dichVuNhaHang;
    private String giaiThuong;
    private String giaTrungBinh;
    private String url;
    private String diaChiGoogleMap;

}
