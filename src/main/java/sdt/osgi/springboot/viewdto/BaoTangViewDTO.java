package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class BaoTangViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String lichSuHinhThanh;
    private String lichSuPhatTrien;
    private int soLuongCoVat;
    private int soPhong;
    private int soLuongVatPhamTrungBay;
    private String chuDe;
    private int dichVuPhongChieuPhim;
    private String boSuuTap;
    private String thuVienLuuTru;
    private String giaVeThamQuan;
    private String url;
    private String diaChiGoogleMap;

}
