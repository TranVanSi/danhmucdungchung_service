package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class VanBanPhapLyViewDTO {
    private long id;
    private String ma;
    private String tenNguoiKy;
    private String soKyHieu;
    private String ngayBanHanh;
    private String ngayHetHan;
    private String tenCoQuanBanHanh;
    private String tenLinhVuc;
    private String tenLoaiVanBan;
    private String trichYeu;
    private String noiDung;
    private String vanBanDinhKem;

}
