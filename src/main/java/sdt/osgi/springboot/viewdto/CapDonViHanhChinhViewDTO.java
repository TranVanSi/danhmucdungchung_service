package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class CapDonViHanhChinhViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String moTa;
    private int cap;

    public CapDonViHanhChinhViewDTO() {
    }
}
