package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class SanGolfViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private int soLo;
    private String moHinhThietKe;
    private int dichVuNhaHang;
    private int dichVuSanChoiTreEm;
    private int dichVuBeBoi;
    private String url;
    private String diaChiGoogleMap;

}
