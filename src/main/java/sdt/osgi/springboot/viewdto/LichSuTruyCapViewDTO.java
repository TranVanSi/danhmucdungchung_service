package sdt.osgi.springboot.viewdto;

import lombok.Data;

import java.util.Date;

@Data
public class LichSuTruyCapViewDTO {

    private long id;
    private long nguoiDungId;
    private String ten;
    private String diaChiDangNhap;
    private Date thoiGianDangNhap;
    private String mayChu;
    private String tenMien;
    private long coQuanQuanLyId;
    private long congChucId;


    public LichSuTruyCapViewDTO() {
    }
}
