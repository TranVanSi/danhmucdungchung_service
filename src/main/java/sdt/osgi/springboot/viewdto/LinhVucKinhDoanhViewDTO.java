package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class LinhVucKinhDoanhViewDTO {

    private long id;
    private String ma;
    private String ten;
    private long chaId;
    private byte cap;

    public LinhVucKinhDoanhViewDTO() {
    }
}
