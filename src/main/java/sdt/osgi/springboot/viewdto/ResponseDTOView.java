package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTOView {
    private Integer statusCode;
    private String message;


}
