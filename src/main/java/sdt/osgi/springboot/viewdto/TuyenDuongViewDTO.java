package sdt.osgi.springboot.viewdto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class TuyenDuongViewDTO {
    private long id;
    private String ma;
    private String ten;
    private long quanHuyenId;
    private long phuongXaId;
    private String thonPho;
    private Long chaId;
    private String diemDau;
    private String diemCuoi;
    private Float chieuDai;
    private Float chieuRongNen;
    private Float chieuRongMat;

    public TuyenDuongViewDTO() {
    }
}
