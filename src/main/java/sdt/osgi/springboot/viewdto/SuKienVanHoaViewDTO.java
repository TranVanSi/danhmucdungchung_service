package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class SuKienVanHoaViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String anh;
    private String netDacTrung;
    private String diaDiem;
    private String thoiGian;
    private String dichVuKemTheo;
    private String lichSuHinhThanh;
    private String diaChiGoogleMap;

}
