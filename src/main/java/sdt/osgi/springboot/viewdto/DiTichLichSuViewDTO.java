package sdt.osgi.springboot.viewdto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@Accessors(chain = true)
public class DiTichLichSuViewDTO {
    private long id;
    private String ma;
    private String ten;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String lichSuHinhThanh;
    private String quaTrinhPhatTrien;
    private String loTrinh;
    private String ynghiaLichSu;
    private String viTri;
    private String capDiTichLichSu;
    private String url;
    private String diaChiGoogleMap;

}
