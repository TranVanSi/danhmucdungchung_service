package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DongBoPhongBanRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.DongBoPhongBanMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DongBoPhongBanService {
    private final DongBoPhongBanMapper dongBoPhongBanMapper;
    private final DongBoPhongBanRepository dongBoPhongBanRepository;

    public Response findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page / size, size);
        if (search != null && search.length() > 0) {
            return new Response(Status.SUCCESS.value(), dongBoPhongBanMapper.toDongBoPhongBanDTOList(dongBoPhongBanRepository.findByMaPhongBanContainingOrTenPhongBanContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent()));
        }
        return new Response(Status.SUCCESS.value(),dongBoPhongBanMapper.toDongBoPhongBanDTOList(dongBoPhongBanRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent()));
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();
        return jsonObject.put("total", dongBoPhongBanRepository.countByMaPhongBanContainingAndDaXoaOrTenPhongBanContainingAndDaXoa(search, Constants.DAXOA,  search, Constants.DAXOA).longValue()).toString();
    }
}
