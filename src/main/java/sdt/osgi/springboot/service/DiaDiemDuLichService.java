package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DiaDiemDuLichRepository;
import sdt.osgi.springboot.dto.DiaDiemDuLichDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DiaDiemDuLichMapper;
import sdt.osgi.springboot.model.DiaDiemDuLich;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DiaDiemDuLichValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DiaDiemDuLichService {

    private final DiaDiemDuLichRepository diaDiemDuLichRepository;
    private final DiaDiemDuLichMapper diaDiemDuLichMapper;
    private final DiaDiemDuLichValidate diaDiemDuLichValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<DiaDiemDuLichDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DiaDiemDuLichDTO diaDiemDuLichDTO = new DiaDiemDuLichDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            diaDiemDuLichDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            diaDiemDuLichDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            diaDiemDuLichDTO.setTenKhac(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            diaDiemDuLichDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            diaDiemDuLichDTO.setNetVanHoa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            diaDiemDuLichDTO.setDiemNoiBat(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            diaDiemDuLichDTO.setDichVuVanChuyen(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            diaDiemDuLichDTO.setDienTich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            diaDiemDuLichDTO.setHinhThucDuLich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            diaDiemDuLichDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }
                    }

                    if (diaDiemDuLichValidate.validateDiaDiemDuLich(diaDiemDuLichDTO, diaDiemDuLichMapper.diaDiemDuLichToDiaDiemDuLichDTO(diaDiemDuLichRepository.findFirstByMaAndDaXoa(diaDiemDuLichDTO.getMa(), Constants.DAXOA)))) {
                        diaDiemDuLichDTO.setNguoiTao(nguoiTao);
                        diaDiemDuLichDTO.setNguoiSua(nguoiTao);
                        if (diaDiemDuLichDTO.getMa() != null) {
                            diaDiemDuLichRepository.save(diaDiemDuLichMapper.diaDiemDuLichDTOToDiaDiemDuLich(diaDiemDuLichDTO));
                        }
                    } else {
                        maFail.add(diaDiemDuLichDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                diaDiemDuLichMapper.diaDiemDuLichToDiaDiemDuLichDTO
                        (diaDiemDuLichRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                diaDiemDuLichMapper.diaDiemDuLichToDiaDiemDuLichDTO(diaDiemDuLichRepository.findFirstById(id)));
    }

    public Response insert(DiaDiemDuLichDTO diaDiemDuLichDTO) {
        diaDiemDuLichRepository.save(diaDiemDuLichMapper.diaDiemDuLichDTOToDiaDiemDuLich(diaDiemDuLichDTO));

        return new Response(Status.SUCCESS.value(), diaDiemDuLichDTO.getMa());
    }

    public Response update(DiaDiemDuLichDTO diaDiemDuLichDTO) {
        DiaDiemDuLich diaDiemDuLichOrigin = diaDiemDuLichRepository.findById(diaDiemDuLichDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DiaDiemDuLich diaDiemDuLich = DiaDiemDuLich.builder()
                .id(diaDiemDuLichDTO.getId())
                .ma(diaDiemDuLichDTO.getMa())
                .ten(diaDiemDuLichDTO.getTen())
                .diaChi(diaDiemDuLichDTO.getDiaChi())
                .tenKhac(diaDiemDuLichDTO.getTenKhac())
                .netVanHoa(diaDiemDuLichDTO.getNetVanHoa())
                .anh(diaDiemDuLichDTO.getAnh() != null ? diaDiemDuLichDTO.getAnh() : diaDiemDuLichOrigin.getAnh())
                .diemNoiBat(diaDiemDuLichDTO.getDiemNoiBat())
                .dichVuVanChuyen(diaDiemDuLichDTO.getDichVuVanChuyen())
                .dienTich(diaDiemDuLichDTO.getDienTich())
                .hinhThucDuLich(diaDiemDuLichDTO.getHinhThucDuLich())
                .diaChiGoogleMap(diaDiemDuLichDTO.getDiaChiGoogleMap())
                .build();

        diaDiemDuLichRepository.save(diaDiemDuLich);
        return new Response(Status.SUCCESS.value(), diaDiemDuLichDTO.getMa());
    }

    public Response delete(long id) {

        DiaDiemDuLich diaDiemDuLich = diaDiemDuLichRepository.findFirstById(id);
        diaDiemDuLich.setDaXoa(Constants.XOA);
        diaDiemDuLichRepository.save(diaDiemDuLich);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<DiaDiemDuLichDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return diaDiemDuLichMapper.toDiaDiemDuLichDTOList(diaDiemDuLichRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return diaDiemDuLichMapper.toDiaDiemDuLichDTOList(diaDiemDuLichRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", diaDiemDuLichRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                diaDiemDuLichMapper.toDiaDiemDuLichViewDTOList(
                        diaDiemDuLichRepository.findByDaXoa(Constants.DAXOA)));

    }

}
