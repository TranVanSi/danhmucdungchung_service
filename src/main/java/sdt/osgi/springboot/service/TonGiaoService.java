package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.TonGiaoRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.TonGiaoDTO;
import sdt.osgi.springboot.mapper.TonGiaoMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.model.TonGiao;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.TonGiaoValidate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class TonGiaoService {
    private final TonGiaoRepository tonGiaoRepository;
    private final TonGiaoMapper tonGiaoMapper;
    private final TonGiaoValidate tonGiaoValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<TonGiaoDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                TonGiaoDTO tonGiaoDTO = new TonGiaoDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            tonGiaoDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            tonGiaoDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 2) {
                            tonGiaoDTO.setCacToChucTonGiaoChinh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 3) {
                            tonGiaoDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            tonGiaoDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 5) {
                            tonGiaoDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }

                    if (tonGiaoValidate.validateTonGiao(tonGiaoDTO, tonGiaoMapper.tonGiaoToTonGiaoDTO(tonGiaoRepository.findFirstByMaAndDaXoa(tonGiaoDTO.getMa(), Constants.DAXOA)))) {
                        tonGiaoDTO.setNguoiTao(nguoiTao);
                        tonGiaoDTO.setNguoiSua(nguoiTao);
                        if (tonGiaoDTO.getMa() != null) {
                            tonGiaoRepository.save(tonGiaoMapper.tonGiaoDTOToTonGiao(tonGiaoDTO));
                        }
                    } else {
                        maFail.add(tonGiaoDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }


    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                tonGiaoMapper.tonGiaoToTonGiaoDTO
                        (tonGiaoRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                tonGiaoMapper.tonGiaoToTonGiaoDTO(tonGiaoRepository.findFirstById(id)));
    }

    public Response insert(TonGiaoDTO tonGiaoDTO) {
        tonGiaoRepository.save(tonGiaoMapper.tonGiaoDTOToTonGiao(tonGiaoDTO));

        return new Response(Status.SUCCESS.value(), tonGiaoDTO.getMa());
    }

    public Response update(TonGiaoDTO tonGiaoDTO) {
        TonGiao tonGiaoOrigin = tonGiaoRepository.findById(tonGiaoDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        TonGiao tonGiao = TonGiao.builder()
                .id(tonGiaoDTO.getId())
                .ma(tonGiaoDTO.getMa())
                .ten(tonGiaoDTO.getTen())
                .cacToChucTonGiaoChinh(tonGiaoDTO.getCacToChucTonGiaoChinh())
                .qdBanHanhSuaDoi(tonGiaoDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(tonGiaoDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(tonGiaoDTO.getQdCoQuanBanHanh())
                .build();

        tonGiaoRepository.save(tonGiao);
        return new Response(Status.SUCCESS.value(), tonGiaoDTO.getMa());
    }

    public Response delete(long id) {
        TonGiao tonGiao = tonGiaoRepository.findById(id).get();
        tonGiao.setDaXoa(Constants.XOA);
        tonGiaoRepository.save(tonGiao);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                tonGiaoMapper.toTonGiaoViewDTOList(
                        tonGiaoRepository.findByDaXoa(Constants.DAXOA)));

    }
}
