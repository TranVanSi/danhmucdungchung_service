package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.CapCoQuanThucHienRepository;
import sdt.osgi.springboot.dto.CapCoQuanThucHienDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.CapCoQuanThucHienMapper;
import sdt.osgi.springboot.model.CapCoQuanThucHien;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;

import java.util.List;


@Service
@RequiredArgsConstructor
public class CapCoQuanThucHienService {

    private final CapCoQuanThucHienRepository capCoQuanThucHienRepository;
    private final CapCoQuanThucHienMapper capCoQuanThucHienMapper;


    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                capCoQuanThucHienMapper.capCoQuanThucHienToCapCoQuanThucHienDTO
                        (capCoQuanThucHienRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                capCoQuanThucHienMapper.capCoQuanThucHienToCapCoQuanThucHienDTO(capCoQuanThucHienRepository.findFirstById(id)));
    }

    public Response insert(CapCoQuanThucHienDTO quyTrinhDTO) {
        capCoQuanThucHienRepository.save(capCoQuanThucHienMapper.capCoQuanThucHienDTOToCapCoQuanThucHien(quyTrinhDTO));
        return new Response(Status.SUCCESS.value(), quyTrinhDTO.getMa());
    }

    public Response update(CapCoQuanThucHienDTO quyTrinhDTO) {
        CapCoQuanThucHien quyTrinhOrigin = capCoQuanThucHienRepository.findById(quyTrinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        CapCoQuanThucHien quyTrinh = CapCoQuanThucHien.builder()
                .id(quyTrinhDTO.getId())
                .ma(quyTrinhDTO.getMa())
                .ten(quyTrinhDTO.getTen())
                .build();
        capCoQuanThucHienRepository.save(quyTrinh);
        return new Response(Status.SUCCESS.value(), quyTrinhDTO.getMa());
    }

    public Response delete(long id) {

        CapCoQuanThucHien capCoQuanThucHien = capCoQuanThucHienRepository.findFirstById(id);
        capCoQuanThucHien.setDaXoa(Constants.XOA);
        capCoQuanThucHienRepository.save(capCoQuanThucHien);
        return new Response(Status.SUCCESS.value(), null);
    }

    public List<CapCoQuanThucHienDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return capCoQuanThucHienMapper.toCapCoQuanThucHiengDTOList(capCoQuanThucHienRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return capCoQuanThucHienMapper.toCapCoQuanThucHiengDTOList(capCoQuanThucHienRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", capCoQuanThucHienRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                capCoQuanThucHienMapper.toCapCoQuanThucHienViewDTOList(
                        capCoQuanThucHienRepository.findByDaXoa(Constants.DAXOA)));

    }


}
