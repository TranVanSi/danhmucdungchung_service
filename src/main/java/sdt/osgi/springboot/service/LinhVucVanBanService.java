package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.LinhVucVanBanRepository;
import sdt.osgi.springboot.dto.LinhVucVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.LinhVucVanBanMapper;
import sdt.osgi.springboot.model.LinhVucVanBan;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.LinhVucVanBanValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class LinhVucVanBanService {
    private final LinhVucVanBanMapper linhVucVanBanMapper;
    private final LinhVucVanBanRepository linhVucVanBanRepository;
    private final LinhVucVanBanValidate linhVucVanBanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<LinhVucVanBanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                LinhVucVanBanDTO linhVucVanBanDTO = new LinhVucVanBanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            linhVucVanBanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            linhVucVanBanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (linhVucVanBanValidate.validateLinhVucVanBan(linhVucVanBanDTO, linhVucVanBanMapper.linhVucVanBanToLinhVucVanBanDTO(linhVucVanBanRepository.findFirstByMaAndDaXoa(linhVucVanBanDTO.getMa(), Constants.DAXOA)))) {
                        linhVucVanBanDTO.setNguoiTao(nguoiTao);
                        linhVucVanBanDTO.setNguoiSua(nguoiTao);
                        if (linhVucVanBanDTO.getMa() != null) {
                            linhVucVanBanRepository.save(linhVucVanBanMapper.linhVucVanBanDTOToLinhVucVanBan(linhVucVanBanDTO));
                        }
                    } else {
                        maFail.add(linhVucVanBanDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                linhVucVanBanMapper.linhVucVanBanToLinhVucVanBanDTO
                        (linhVucVanBanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                linhVucVanBanMapper.linhVucVanBanToLinhVucVanBanDTO(linhVucVanBanRepository.findFirstById(id)));
    }

    public Response insert(LinhVucVanBanDTO linhVucVanBanDTO) {
        linhVucVanBanRepository.save(linhVucVanBanMapper.linhVucVanBanDTOToLinhVucVanBan(linhVucVanBanDTO));

        return new Response(Status.SUCCESS.value(), linhVucVanBanDTO.getMa()
        );
    }

    public Response update(LinhVucVanBanDTO linhVucVanBanDTO) {
        LinhVucVanBan linhVucVanBanOrigin = linhVucVanBanRepository.findById(linhVucVanBanDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        LinhVucVanBan linhVucVanBan = LinhVucVanBan.builder()
                .id(linhVucVanBanDTO.getId())
                .ma(linhVucVanBanDTO.getMa())
                .ten(linhVucVanBanDTO.getTen())
                .build();

        linhVucVanBanRepository.save(linhVucVanBan);
        return new Response(Status.SUCCESS.value(), linhVucVanBanDTO.getMa());
    }

    public Response delete(long id) {
        LinhVucVanBan linhVucVanBan = linhVucVanBanRepository.findFirstById(id);
        linhVucVanBan.setDaXoa(Constants.XOA);
        linhVucVanBanRepository.save(linhVucVanBan);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                linhVucVanBanMapper.toLinhVucVanBanViewDTOList(
                        linhVucVanBanRepository.findByDaXoa(Constants.DAXOA)));

    }
}
