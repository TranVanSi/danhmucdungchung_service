package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import sdt.osgi.springboot.dao.DoanhNghiep2NganhNgheKinhDoanhRepository;
import sdt.osgi.springboot.dao.DoanhNghiepRepository;
import sdt.osgi.springboot.model.DoanhNghiep;
import sdt.osgi.springboot.model.DoanhNghiep2NganhNgheKinhDoanh;
import sdt.osgi.springboot.model.NganhNgheKinhDoanh;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@Transactional
public class DoanhNghiep2NganhNgheKinhDoanhService {

    private final DoanhNghiep2NganhNgheKinhDoanhRepository doanhNghiep2NganhNgheKinhDoanhRepository;
    private final DoanhNghiepRepository doanhNghiepRepository;

    public void createDoanhNghiep2NganhNgheKinhDOanh (long doanhNghiepId, List<NganhNgheKinhDoanh> nganhNgheKinhDoanhs) {
        List<DoanhNghiep2NganhNgheKinhDoanh> doanhNghiep2NganhNgheKinhDoanhs = new ArrayList<>();
        DoanhNghiep doanhNghiep = doanhNghiepRepository.findById(doanhNghiepId).get();

        if ((nganhNgheKinhDoanhs != null && nganhNgheKinhDoanhs.size() > 0)) {
            for (NganhNgheKinhDoanh nganhNgheKinhDoanh:nganhNgheKinhDoanhs) {
                DoanhNghiep2NganhNgheKinhDoanh doanhNghiep2NganhNgheKinhDoanh = new DoanhNghiep2NganhNgheKinhDoanh();
                doanhNghiep2NganhNgheKinhDoanh.setDoanhNghiep(doanhNghiep);
                doanhNghiep2NganhNgheKinhDoanh.setNganhNgheKinhDoanh(nganhNgheKinhDoanh);
                doanhNghiep2NganhNgheKinhDoanhs.add(doanhNghiep2NganhNgheKinhDoanh);
            }
            doanhNghiep2NganhNgheKinhDoanhRepository.saveAll(doanhNghiep2NganhNgheKinhDoanhs);
        }
    }

    public void deleteByDoanhNghiepId (long doanhNghiepId) {
        doanhNghiep2NganhNgheKinhDoanhRepository.deleteByDoanhNghiep_Id(doanhNghiepId);
    }

    public List<DoanhNghiep2NganhNgheKinhDoanh> findByDoanhNghiepId(long doanhNghiepId) {

        return doanhNghiep2NganhNgheKinhDoanhRepository.findByDoanhNghiep_Id(doanhNghiepId);
    }

}
