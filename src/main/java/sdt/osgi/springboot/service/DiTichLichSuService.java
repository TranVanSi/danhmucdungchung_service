package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DiTichLichSuRepository;
import sdt.osgi.springboot.dto.DiTichLichSuDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DiTichLichSuMapper;
import sdt.osgi.springboot.model.DiTichLichSu;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DiTichLichSuValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DiTichLichSuService {

    private final DiTichLichSuRepository diTichLichSuRepository;
    private final DiTichLichSuMapper diTichLichSuMapper;
    private final DiTichLichSuValidate diTichLichSuValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<DiTichLichSuDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DiTichLichSuDTO diTichLichSuDTO = new DiTichLichSuDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            diTichLichSuDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            diTichLichSuDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            diTichLichSuDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            diTichLichSuDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            diTichLichSuDTO.setLichSuHinhThanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            diTichLichSuDTO.setQuaTrinhPhatTrien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            diTichLichSuDTO.setLoTrinh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            diTichLichSuDTO.setYnghiaLichSu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            diTichLichSuDTO.setViTri(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            diTichLichSuDTO.setCapDiTichLichSu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 10) {
                            diTichLichSuDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 11) {
                            diTichLichSuDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (diTichLichSuValidate.validateDiTichLichSu(diTichLichSuDTO, diTichLichSuMapper.diTichLichSuToDiTichLichSuDTO(diTichLichSuRepository.findFirstByMaAndDaXoa(diTichLichSuDTO.getMa(), Constants.DAXOA)))) {
                        diTichLichSuDTO.setNguoiTao(nguoiTao);
                        diTichLichSuDTO.setNguoiSua(nguoiTao);
                        dtoList.add(diTichLichSuDTO);
                        if (diTichLichSuDTO.getMa() != null) {
                            diTichLichSuRepository.save(diTichLichSuMapper.diTichLichSuDTOToDiTichLichSu(diTichLichSuDTO));
                        }
                    } else {
                        maFail.add(diTichLichSuDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                diTichLichSuMapper.diTichLichSuToDiTichLichSuDTO
                        (diTichLichSuRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                diTichLichSuMapper.diTichLichSuToDiTichLichSuDTO(diTichLichSuRepository.findFirstById(id)));
    }

    public Response insert(DiTichLichSuDTO diTichLichSuDTO) {
        diTichLichSuRepository.save(diTichLichSuMapper.diTichLichSuDTOToDiTichLichSu(diTichLichSuDTO));

        return new Response(Status.SUCCESS.value(), diTichLichSuDTO.getMa());
    }

    public Response update(DiTichLichSuDTO diTichLichSuDTO) {
        DiTichLichSu diTichLichSuOrigin = diTichLichSuRepository.findById(diTichLichSuDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DiTichLichSu diTichLichSu = DiTichLichSu.builder()
                .id(diTichLichSuDTO.getId())
                .ma(diTichLichSuDTO.getMa())
                .ten(diTichLichSuDTO.getTen())
                .diaChi(diTichLichSuDTO.getDiaChi())
                .soDienThoai(diTichLichSuDTO.getSoDienThoai())
                .anh(diTichLichSuDTO.getAnh() != null ? diTichLichSuDTO.getAnh() : diTichLichSuOrigin.getAnh())
                .lichSuHinhThanh(diTichLichSuDTO.getLichSuHinhThanh())
                .quaTrinhPhatTrien(diTichLichSuDTO.getQuaTrinhPhatTrien())
                .loTrinh(diTichLichSuDTO.getLoTrinh())
                .ynghiaLichSu(diTichLichSuDTO.getYnghiaLichSu())
                .viTri(diTichLichSuDTO.getViTri())
                .capDiTichLichSu(diTichLichSuDTO.getCapDiTichLichSu())
                .url(diTichLichSuDTO.getUrl())
                .diaChiGoogleMap(diTichLichSuDTO.getDiaChiGoogleMap())
                .build();

        diTichLichSuRepository.save(diTichLichSu);
        return new Response(Status.SUCCESS.value(), diTichLichSuDTO.getMa());
    }

    public Response delete(long id) {

        DiTichLichSu diTichLichSu = diTichLichSuRepository.findFirstById(id);
        diTichLichSu.setDaXoa(Constants.XOA);
        diTichLichSuRepository.save(diTichLichSu);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<DiTichLichSuDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return diTichLichSuMapper.toDiTichLichSuDTOList(diTichLichSuRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return diTichLichSuMapper.toDiTichLichSuDTOList(diTichLichSuRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", diTichLichSuRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                diTichLichSuMapper.toDiTichLichSuViewDTOList(
                        diTichLichSuRepository.findByDaXoa(Constants.DAXOA)));

    }

}
