package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DanTocsRepository;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.DanTocMapper;
import sdt.osgi.springboot.model.DanToc;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;

import java.util.List;


@Service
@RequiredArgsConstructor
public class DanTocService {
    private final DanTocsRepository danTocsRepository;
    private final DanTocMapper danTocMapper;

    public void saveAllDanToc(List<DanTocDTO> danTocDTOList) {
        danTocsRepository.saveAll(danTocMapper.toDanTocList(danTocDTOList));
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                danTocMapper.danTocToDanTocDTO
                        (danTocsRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                danTocMapper.danTocToDanTocDTO(danTocsRepository.findFirstById(id)));
    }

    public Response insert(DanTocDTO danTocDTO) {
        danTocsRepository.save(danTocMapper.danTocDTOToDanToc(danTocDTO));

        return new Response(Status.SUCCESS.value(), danTocDTO.getMa());
    }

    public Response update(DanTocDTO danTocDTO) {
        DanToc danTocOrigin = danTocsRepository.findById(danTocDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DanToc danToc = DanToc.builder()
                .id(danTocDTO.getId())
                .ma(danTocDTO.getMa())
                .ten(danTocDTO.getTen())
                .tenKhac(danTocDTO.getTenKhac())
                .thieuSo(danTocDTO.getThieuSo())
                .qdBanHanhSuaDoi(danTocDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(danTocDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(danTocDTO.getQdCoQuanBanHanh())
                .build();

        danTocsRepository.save(danToc);
        return new Response(Status.SUCCESS.value(), danTocDTO.getMa());
    }

    public Response delete(long id) {

        DanToc danToc = danTocsRepository.findById(id).get();
        danToc.setDaXoa(Constants.XOA);
        danTocsRepository.save(danToc);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                danTocMapper.toDanTocViewDTOList(
                        danTocsRepository.findByDaXoa(Constants.DAXOA)));

    }
}
