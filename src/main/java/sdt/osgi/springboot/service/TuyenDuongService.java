package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.TuyenDuongRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.TuyenDuongDTO;
import sdt.osgi.springboot.mapper.TuyenDuongMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.model.TuyenDuong;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.TuyenDuongValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class TuyenDuongService {

    private final TuyenDuongRepository tuyenDuongRepository;
    private final TuyenDuongMapper tuyenDuongMapper;
    private final TuyenDuongValidate tuyenDuongValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<TuyenDuongDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                TuyenDuongDTO tuyenDuongDTO = new TuyenDuongDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            tuyenDuongDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            tuyenDuongDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            tuyenDuongDTO.setQuanHuyenId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            tuyenDuongDTO.setPhuongXaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            tuyenDuongDTO.setThonPho(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 5) {
                            tuyenDuongDTO.setChaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 6) {
                            tuyenDuongDTO.setDiemDau(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 7) {
                            tuyenDuongDTO.setDiemCuoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 8) {
                            tuyenDuongDTO.setChieuDai(Float.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 9) {
                            tuyenDuongDTO.setChieuRongNen(Float.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 10) {
                            tuyenDuongDTO.setChieuRongMat(Float.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }

                    if (tuyenDuongValidate.validateTuyenDuong(tuyenDuongDTO, tuyenDuongMapper.tuyenDuongToTuyenDuongDTO(tuyenDuongRepository.findFirstByMaAndDaXoa(tuyenDuongDTO.getMa(), Constants.DAXOA)))) {
                        tuyenDuongDTO.setNguoiTao(nguoiTao);
                        tuyenDuongDTO.setNguoiSua(nguoiTao);
                        if (tuyenDuongDTO.getMa() != null) {
                            tuyenDuongRepository.save(tuyenDuongMapper.tuyenDuongDTOToTuyenDuong(tuyenDuongDTO));
                        }
                    } else {
                        maFail.add(tuyenDuongDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                tuyenDuongMapper.tuyenDuongToTuyenDuongDTO
                        (tuyenDuongRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                tuyenDuongMapper.tuyenDuongToTuyenDuongDTO(tuyenDuongRepository.findFirstById(id)));
    }

    public Response findByChaId(long chaId) {
        return new Response(
                Status.SUCCESS.value(),
                tuyenDuongMapper.toTuyenDuonggDTOList(tuyenDuongRepository.findAllByChaIdAndDaXoa(chaId, Constants.DAXOA)));

    }

    public Response insert(TuyenDuongDTO tuyenDuongDTO) {
        tuyenDuongRepository.save(tuyenDuongMapper.tuyenDuongDTOToTuyenDuong(tuyenDuongDTO));

        return new Response(Status.SUCCESS.value(), tuyenDuongDTO.getMa());
    }

    public Response update(TuyenDuongDTO tuyenDuongDTO) {
        TuyenDuong tuyenDuongOrigin = tuyenDuongRepository.findById(tuyenDuongDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        TuyenDuong tuyenDuong = TuyenDuong.builder()
                .id(tuyenDuongDTO.getId())
                .ma(tuyenDuongDTO.getMa())
                .ten(tuyenDuongDTO.getTen())
                .quanHuyenId(tuyenDuongDTO.getQuanHuyenId())
                .phuongXaId(tuyenDuongDTO.getPhuongXaId())
                .chaId(tuyenDuongDTO.getChaId())
                .diemDau(tuyenDuongDTO.getDiemDau())
                .diemCuoi(tuyenDuongDTO.getDiemCuoi())
                .chieuDai(tuyenDuongDTO.getChieuDai())
                .chieuRongNen(tuyenDuongDTO.getChieuRongNen())
                .chieuRongMat(tuyenDuongDTO.getChieuRongMat())
                .thonPho(tuyenDuongDTO.getThonPho())
                .build();

        tuyenDuongRepository.save(tuyenDuong);
        return new Response(Status.SUCCESS.value(), tuyenDuongDTO.getMa());
    }

    public Response delete(long id) {
        TuyenDuong tuyenDuong = tuyenDuongRepository.findFirstById(id);
        tuyenDuong.setDaXoa(Constants.XOA);
        tuyenDuongRepository.save(tuyenDuong);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                tuyenDuongMapper.toTuyenDuongViewDTOList(
                        tuyenDuongRepository.findByDaXoa(Constants.DAXOA)));

    }

}
