package sdt.osgi.springboot.service;

import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.KhachSanRepository;
import sdt.osgi.springboot.dto.KhachSanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.KhachSanMapper;
import sdt.osgi.springboot.model.KhachSan;
import sdt.osgi.springboot.model.KhuCongNghiep;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.KhachSanValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class KhachSanService {

    private final KhachSanRepository khachSanRepository;
    private final KhachSanMapper khachSanMapper;
    private final KhachSanValidate khachSanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<KhachSanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                KhachSanDTO khachSanDTO = new KhachSanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            khachSanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            khachSanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            khachSanDTO.setDienTich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            khachSanDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            khachSanDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            khachSanDTO.setSoLuongPhong(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            khachSanDTO.setSoTang(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            khachSanDTO.setGioNhanPhong(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            khachSanDTO.setGioTraPhong(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khachSanDTO.setChoDauXe(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 10) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khachSanDTO.setDichVuTaxi(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 11) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khachSanDTO.setHoTroXeLan(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 12) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khachSanDTO.setPhongHutThuoc(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 13) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khachSanDTO.setWifiTrongPhong(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 14) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khachSanDTO.setDichVuDoiTien(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 15) {
                            khachSanDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 16) {
                            khachSanDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (khachSanValidate.validateKhachSan(khachSanDTO, khachSanMapper.khachSanToKhachSanDTO(khachSanRepository.findFirstByMaAndDaXoa(khachSanDTO.getMa(), Constants.DAXOA)))) {
                        khachSanDTO.setNguoiTao(nguoiTao);
                        khachSanDTO.setNguoiSua(nguoiTao);
                        if (khachSanDTO.getMa() != null) {
                            khachSanRepository.save(khachSanMapper.khachSanDTOToKhachSan(khachSanDTO));
                        }
                    } else {
                        maFail.add(khachSanDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                khachSanMapper.khachSanToKhachSanDTO
                        (khachSanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                khachSanMapper.khachSanToKhachSanDTO(khachSanRepository.findFirstById(id)));
    }

    public Response insert(KhachSanDTO khachSanDTO) {
        khachSanRepository.save(khachSanMapper.khachSanDTOToKhachSan(khachSanDTO));

        return new Response(Status.SUCCESS.value(), khachSanDTO.getMa());
    }

    public Response update(KhachSanDTO khachSanDTO) {
        KhachSan khachSanOrigin = khachSanRepository.findById(khachSanDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        KhachSan khachSan = KhachSan.builder()
                .id(khachSanDTO.getId())
                .ma(khachSanDTO.getMa())
                .ten(khachSanDTO.getTen())
                .diaChi(khachSanDTO.getDiaChi())
                .soDienThoai(khachSanDTO.getSoDienThoai())
                .anh(khachSanDTO.getAnh() != null ? khachSanDTO.getAnh() : khachSanOrigin.getAnh())
                .dienTich(khachSanDTO.getDienTich())
                .soLuongPhong(khachSanDTO.getSoLuongPhong())
                .soTang(khachSanDTO.getSoTang())
                .gioNhanPhong(khachSanDTO.getGioNhanPhong())
                .gioTraPhong(khachSanDTO.getGioTraPhong())
                .choDauXe(khachSanDTO.getChoDauXe())
                .dichVuTaxi(khachSanDTO.getDichVuTaxi())
                .dichVuDoiTien(khachSanDTO.getDichVuDoiTien())
                .hoTroXeLan(khachSanDTO.getHoTroXeLan())
                .wifiTrongPhong(khachSanDTO.getWifiTrongPhong())
                .phongHutThuoc(khachSanDTO.getPhongHutThuoc())
                .url(khachSanDTO.getUrl())
                .diaChiGoogleMap(khachSanDTO.getDiaChiGoogleMap())
                .build();

        khachSanRepository.save(khachSan);
        return new Response(Status.SUCCESS.value(), khachSanDTO.getMa());
    }

    public Response delete(long id) {

        KhachSan khachSan = khachSanRepository.findFirstById(id);
        khachSan.setDaXoa(Constants.XOA);
        khachSanRepository.save(khachSan);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<KhachSanDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return khachSanMapper.toKhachSanDTOList(khachSanRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return khachSanMapper.toKhachSanDTOList(khachSanRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", khachSanRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                khachSanMapper.toKhachSanViewDTOList(
                        khachSanRepository.findByDaXoa(Constants.DAXOA)));

    }

}
