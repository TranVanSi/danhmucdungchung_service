package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.TrangThaiDoanhNghiepRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class TrangThaiDoanhNghiepService {

    private final TrangThaiDoanhNghiepRepository trangThaiDoanhNghiepRepository;

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),trangThaiDoanhNghiepRepository.findFirstById(id));
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                        trangThaiDoanhNghiepRepository.findByDaXoa(Constants.DAXOA));

    }
}
