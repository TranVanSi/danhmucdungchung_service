package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.SuKienVanHoaRepository;
import sdt.osgi.springboot.dto.SuKienVanHoaDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.SuKienVanHoaMapper;
import sdt.osgi.springboot.model.SuKienVanHoa;
import sdt.osgi.springboot.model.SuKienVanHoa;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.SuKienVanHoaValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class SuKienVanHoaService {

    private final SuKienVanHoaRepository suKienVanHoaRepository;
    private final SuKienVanHoaMapper suKienVanHoaMapper;
    private final SuKienVanHoaValidate suKienVanHoaValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<SuKienVanHoaDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                SuKienVanHoaDTO suKienVanHoaDTO = new SuKienVanHoaDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            suKienVanHoaDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            suKienVanHoaDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            suKienVanHoaDTO.setNetDacTrung(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            suKienVanHoaDTO.setDiaDiem(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            suKienVanHoaDTO.setThoiGian(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            suKienVanHoaDTO.setDichVuKemTheo(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            suKienVanHoaDTO.setLichSuHinhThanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 7) {
                            suKienVanHoaDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }
                    }

                    if (suKienVanHoaValidate.validateSuKienVanHoa(suKienVanHoaDTO, suKienVanHoaMapper.suKienVanHoaToSuKienVanHoaDTO(suKienVanHoaRepository.findFirstByMaAndDaXoa(suKienVanHoaDTO.getMa(), Constants.DAXOA)))) {
                        suKienVanHoaDTO.setNguoiTao(nguoiTao);
                        suKienVanHoaDTO.setNguoiSua(nguoiTao);
                        if (suKienVanHoaDTO.getMa() != null) {
                            suKienVanHoaRepository.save(suKienVanHoaMapper.suKienVanHoaDTOToSuKienVanHoa(suKienVanHoaDTO));
                        }
                    } else {
                        maFail.add(suKienVanHoaDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                suKienVanHoaMapper.suKienVanHoaToSuKienVanHoaDTO
                        (suKienVanHoaRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                suKienVanHoaMapper.suKienVanHoaToSuKienVanHoaDTO(suKienVanHoaRepository.findFirstById(id)));
    }

    public Response insert(SuKienVanHoaDTO suKienVanHoaDTO) {
        suKienVanHoaRepository.save(suKienVanHoaMapper.suKienVanHoaDTOToSuKienVanHoa(suKienVanHoaDTO));

        return new Response(Status.SUCCESS.value(), suKienVanHoaDTO.getMa());
    }

    public Response update(SuKienVanHoaDTO suKienVanHoaDTO) {
        SuKienVanHoa suKienVanHoaOrigin = suKienVanHoaRepository.findById(suKienVanHoaDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        SuKienVanHoa suKienVanHoa = SuKienVanHoa.builder()
                .id(suKienVanHoaDTO.getId())
                .ma(suKienVanHoaDTO.getMa())
                .ten(suKienVanHoaDTO.getTen())
                .anh(suKienVanHoaDTO.getAnh() != null ? suKienVanHoaDTO.getAnh() : suKienVanHoaOrigin.getAnh())
                .netDacTrung(suKienVanHoaDTO.getNetDacTrung())
                .diaDiem(suKienVanHoaDTO.getDiaDiem())
                .thoiGian(suKienVanHoaDTO.getThoiGian())
                .dichVuKemTheo(suKienVanHoaDTO.getDichVuKemTheo())
                .lichSuHinhThanh(suKienVanHoaDTO.getLichSuHinhThanh())
                .diaChiGoogleMap(suKienVanHoaDTO.getDiaChiGoogleMap())
                .build();

        suKienVanHoaRepository.save(suKienVanHoa);
        return new Response(Status.SUCCESS.value(), suKienVanHoaDTO.getMa());
    }

    public Response delete(long id) {

        SuKienVanHoa suKienVanHoa = suKienVanHoaRepository.findFirstById(id);
        suKienVanHoa.setDaXoa(Constants.XOA);
        suKienVanHoaRepository.save(suKienVanHoa);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<SuKienVanHoaDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return suKienVanHoaMapper.toSuKienVanHoaDTOList(suKienVanHoaRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return suKienVanHoaMapper.toSuKienVanHoaDTOList(suKienVanHoaRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", suKienVanHoaRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                suKienVanHoaMapper.toSuKienVanHoaViewDTOList(
                        suKienVanHoaRepository.findByDaXoa(Constants.DAXOA)));

    }

}
