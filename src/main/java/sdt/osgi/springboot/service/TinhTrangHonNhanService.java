package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.TinhTrangHonNhanRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.TinhTrangHonNhanDTO;
import sdt.osgi.springboot.mapper.TinhTrangHonNhanMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.model.TinhTrangHonNhan;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.TinhTrangHonNhanValidate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class TinhTrangHonNhanService {

    private final TinhTrangHonNhanRepository tinhTrangHonNhanRepository;
    private final TinhTrangHonNhanMapper tinhTrangHonNhanMapper;
    private final TinhTrangHonNhanValidate tinhTrangHonNhanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<TinhTrangHonNhanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                TinhTrangHonNhanDTO tinhTrangHonNhanDTO = new TinhTrangHonNhanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            tinhTrangHonNhanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            tinhTrangHonNhanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            tinhTrangHonNhanDTO.setGhiChu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 3) {
                            tinhTrangHonNhanDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            tinhTrangHonNhanDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 5) {
                            tinhTrangHonNhanDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }

                    if (tinhTrangHonNhanValidate.validateTinhTrangHonNhan(tinhTrangHonNhanDTO, tinhTrangHonNhanMapper.tinhTrangHonNhanToTinhTrangHonNhanDTO(tinhTrangHonNhanRepository.findFirstByMaAndDaXoa(tinhTrangHonNhanDTO.getMa(), Constants.DAXOA)))) {
                        tinhTrangHonNhanDTO.setNguoiTao(nguoiTao);
                        tinhTrangHonNhanDTO.setNguoiSua(nguoiTao);
                        if (tinhTrangHonNhanDTO.getMa() != null) {
                            tinhTrangHonNhanRepository.save(tinhTrangHonNhanMapper.tinhTrangHonNhanDTOToTinhTrangHonNhan(tinhTrangHonNhanDTO));
                        }
                    } else {
                        maFail.add(tinhTrangHonNhanDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                tinhTrangHonNhanMapper.tinhTrangHonNhanToTinhTrangHonNhanDTO
                        (tinhTrangHonNhanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                tinhTrangHonNhanMapper.tinhTrangHonNhanToTinhTrangHonNhanDTO(tinhTrangHonNhanRepository.findFirstById(id)));
    }

    public Response insert(TinhTrangHonNhanDTO tinhTrangHonNhanDTO) {
        tinhTrangHonNhanRepository.save(tinhTrangHonNhanMapper.tinhTrangHonNhanDTOToTinhTrangHonNhan(tinhTrangHonNhanDTO));

        return new Response(Status.SUCCESS.value(), tinhTrangHonNhanDTO.getMa());
    }

    public Response update(TinhTrangHonNhanDTO tinhTrangHonNhanDTO) {
        TinhTrangHonNhan tinhTrangHonNhanOrigin = tinhTrangHonNhanRepository.findById(tinhTrangHonNhanDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        TinhTrangHonNhan tinhTrangHonNhan = TinhTrangHonNhan.builder()
                .id(tinhTrangHonNhanDTO.getId())
                .ma(tinhTrangHonNhanDTO.getMa())
                .ten(tinhTrangHonNhanDTO.getTen())
                .ghiChu(tinhTrangHonNhanDTO.getGhiChu())
                .qdBanHanhSuaDoi(tinhTrangHonNhanDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(tinhTrangHonNhanDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(tinhTrangHonNhanDTO.getQdCoQuanBanHanh())
                .build();

        tinhTrangHonNhanRepository.save(tinhTrangHonNhan);
        return new Response(Status.SUCCESS.value(), tinhTrangHonNhanDTO.getMa());
    }

    public Response delete(long id) {
        TinhTrangHonNhan tinhTrangHonNhan = tinhTrangHonNhanRepository.findFirstById(id);
        tinhTrangHonNhan.setDaXoa(Constants.XOA);
        tinhTrangHonNhanRepository.save(tinhTrangHonNhan);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                tinhTrangHonNhanMapper.toTinhTrangHonNhanViewDTOList(
                        tinhTrangHonNhanRepository.findByDaXoa(Constants.DAXOA)));

    }


}
