package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.BaoTangRepository;
import sdt.osgi.springboot.dto.BaoTangDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.BaoTangMapper;
import sdt.osgi.springboot.model.BaoTang;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.BaoTangValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class BaoTangService {

    private final BaoTangRepository baoTangRepository;
    private final BaoTangMapper baoTangMapper;
    private final BaoTangValidate baoTangValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;


    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<BaoTangDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                BaoTangDTO baoTangDTO = new BaoTangDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            baoTangDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            baoTangDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            baoTangDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            baoTangDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            baoTangDTO.setLichSuHinhThanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            baoTangDTO.setLichSuPhatTrien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            baoTangDTO.setSoLuongCoVat(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            baoTangDTO.setSoPhong(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            baoTangDTO.setSoLuongVatPhamTrungBay(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            baoTangDTO.setChuDe(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 10) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            baoTangDTO.setDichVuPhongChieuPhim(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 11) {
                            baoTangDTO.setBoSuuTap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 12) {
                            baoTangDTO.setThuVienLuuTru(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 13) {
                            baoTangDTO.setGiaVeThamQuan(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 14) {
                            baoTangDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 15) {
                            baoTangDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (baoTangValidate.validateBaoTang(baoTangDTO, baoTangMapper.baoTangToBaoTangDTO(baoTangRepository.findFirstByMaAndDaXoa(baoTangDTO.getMa(), Constants.DAXOA)))) {
                        baoTangDTO.setNguoiTao(nguoiTao);
                        baoTangDTO.setNguoiSua(nguoiTao);
                        if (baoTangDTO.getMa() != null) {
                            baoTangRepository.save(baoTangMapper.baoTangDTOToBaoTang(baoTangDTO));
                        }
                    } else {
                        maFail.add(baoTangDTO.getMa());

                    }
                }

                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);
        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                baoTangMapper.baoTangToBaoTangDTO
                        (baoTangRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                baoTangMapper.baoTangToBaoTangDTO(baoTangRepository.findFirstById(id)));
    }

    public Response insert(BaoTangDTO baoTangDTO) {
        baoTangRepository.save(baoTangMapper.baoTangDTOToBaoTang(baoTangDTO));

        return new Response(Status.SUCCESS.value(), baoTangDTO.getMa());
    }

    public Response update(BaoTangDTO baoTangDTO) {
        BaoTang banTangOrigin = baoTangRepository.findById(baoTangDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        BaoTang baoTang = BaoTang.builder()
                .id(baoTangDTO.getId())
                .ma(baoTangDTO.getMa())
                .ten(baoTangDTO.getTen())
                .diaChi(baoTangDTO.getDiaChi())
                .soDienThoai(baoTangDTO.getSoDienThoai())
                .anh(baoTangDTO.getAnh() != null ? baoTangDTO.getAnh() : banTangOrigin.getAnh())
                .lichSuHinhThanh(baoTangDTO.getLichSuHinhThanh())
                .lichSuPhatTrien(baoTangDTO.getLichSuPhatTrien())
                .soLuongCoVat(baoTangDTO.getSoLuongCoVat())
                .soPhong(baoTangDTO.getSoPhong())
                .soLuongVatPhamTrungBay(baoTangDTO.getSoLuongVatPhamTrungBay())
                .chuDe(baoTangDTO.getChuDe())
                .dichVuPhongChieuPhim(baoTangDTO.getDichVuPhongChieuPhim())
                .boSuuTap(baoTangDTO.getBoSuuTap())
                .thuVienLuuTru(baoTangDTO.getThuVienLuuTru())
                .giaVeThamQuan(baoTangDTO.getGiaVeThamQuan())
                .url(baoTangDTO.getUrl())
                .diaChiGoogleMap(baoTangDTO.getDiaChiGoogleMap())
                .build();

        baoTangRepository.save(baoTang);
        return new Response(Status.SUCCESS.value(), baoTangDTO.getMa());
    }

    public Response delete(long id) {

        BaoTang baoTang = baoTangRepository.findFirstById(id);
        baoTang.setDaXoa(Constants.XOA);
        baoTangRepository.save(baoTang);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<BaoTangDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return baoTangMapper.toBaoTangDTOList(baoTangRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return baoTangMapper.toBaoTangDTOList(baoTangRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", baoTangRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                baoTangMapper.toBaoTangViewDTOList(
                        baoTangRepository.findByDaXoa(Constants.DAXOA)));
    }

}
