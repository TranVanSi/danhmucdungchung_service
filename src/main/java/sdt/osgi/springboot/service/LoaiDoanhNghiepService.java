package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.LoaiDoanhNghiepRepository;
import sdt.osgi.springboot.dto.LoaiDoanhNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.LoaiDoanhNghiepMapper;
import sdt.osgi.springboot.model.LoaiDoanhNghiep;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.LoaiDoanhNghiepValidate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class LoaiDoanhNghiepService {
    private final LoaiDoanhNghiepMapper loaiDoanhNghiepMapper;
    private final LoaiDoanhNghiepRepository loaiDoanhNghiepRepository;
    private final LoaiDoanhNghiepValidate loaiDoanhNghiepValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<LoaiDoanhNghiepDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                LoaiDoanhNghiepDTO loaiDoanhNghiepDTO = new LoaiDoanhNghiepDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            loaiDoanhNghiepDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            loaiDoanhNghiepDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            loaiDoanhNghiepDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 3) {
                            loaiDoanhNghiepDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            loaiDoanhNghiepDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (loaiDoanhNghiepValidate.validateLoaiDoanhNghiep(loaiDoanhNghiepDTO, loaiDoanhNghiepMapper.loaiDoanhNghiepToLoaiDoanhNghiepDTO(loaiDoanhNghiepRepository.findFirstByMaAndDaXoa(loaiDoanhNghiepDTO.getMa(), Constants.DAXOA)))) {
                        loaiDoanhNghiepDTO.setNguoiTao(nguoiTao);
                        loaiDoanhNghiepDTO.setNguoiSua(nguoiTao);
                        if (loaiDoanhNghiepDTO.getMa() != null) {
                            loaiDoanhNghiepRepository.save(loaiDoanhNghiepMapper.loaiDoanhNghiepDTOToLoaiDoanhNghiep(loaiDoanhNghiepDTO));
                        }
                    } else {
                        maFail.add(loaiDoanhNghiepDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                loaiDoanhNghiepMapper.loaiDoanhNghiepToLoaiDoanhNghiepDTO
                        (loaiDoanhNghiepRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                loaiDoanhNghiepMapper.loaiDoanhNghiepToLoaiDoanhNghiepDTO(loaiDoanhNghiepRepository.findFirstById(id)));
    }

    public Response insert(LoaiDoanhNghiepDTO loaiDoanhNghiepDTO) {
        loaiDoanhNghiepRepository.save(loaiDoanhNghiepMapper.loaiDoanhNghiepDTOToLoaiDoanhNghiep(loaiDoanhNghiepDTO));

        return new Response(Status.SUCCESS.value(), loaiDoanhNghiepDTO.getMa());
    }

    public Response update(LoaiDoanhNghiepDTO loaiDoanhNghiepDTO) {
        LoaiDoanhNghiep loaiDoanhNghiepOrigin = loaiDoanhNghiepRepository.findById(loaiDoanhNghiepDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        LoaiDoanhNghiep loaiDoanhNghiep = LoaiDoanhNghiep.builder()
                .id(loaiDoanhNghiepDTO.getId())
                .ma(loaiDoanhNghiepDTO.getMa())
                .ten(loaiDoanhNghiepDTO.getTen())
                .qdBanHanhSuaDoi(loaiDoanhNghiepDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(loaiDoanhNghiepDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(loaiDoanhNghiepDTO.getQdCoQuanBanHanh())
                .build();

        loaiDoanhNghiepRepository.save(loaiDoanhNghiep);
        return new Response(Status.SUCCESS.value(), loaiDoanhNghiepDTO.getMa());
    }

    public Response delete(long id) {
        LoaiDoanhNghiep loaiDoanhNghiep = loaiDoanhNghiepRepository.findFirstById(id);
        loaiDoanhNghiep.setDaXoa(Constants.XOA);
        loaiDoanhNghiepRepository.save(loaiDoanhNghiep);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                loaiDoanhNghiepMapper.toLoaiDoanhNghiepViewDTOList(
                        loaiDoanhNghiepRepository.findByDaXoa(Constants.DAXOA)));

    }
}
