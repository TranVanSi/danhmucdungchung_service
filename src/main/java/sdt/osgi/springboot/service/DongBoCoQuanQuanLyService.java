package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DongBoCoQuanQuanLyRepository;
import sdt.osgi.springboot.dao.DongBoDonViRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.DongBoCoQuanQuanLyMapper;
import sdt.osgi.springboot.mapper.DongBoDonViMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;

import java.net.URLDecoder;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DongBoCoQuanQuanLyService {
    private final DongBoCoQuanQuanLyMapper dongBoCoQuanQuanLyMapper;
    private final DongBoCoQuanQuanLyRepository dongBoCoQuanQuanLyRepository;

    public Response findAll(String search, int page, int size) throws Exception {

        Pageable pageable = PageRequest.of(page / size, size);
        if (search != null && search.length() > 0) {
            search = URLDecoder.decode(search, "UTF-8");
            return new Response(Status.SUCCESS.value(), dongBoCoQuanQuanLyMapper.toDVCCoQuanQuanLyDTOList(dongBoCoQuanQuanLyRepository.findByMaContainingAndDaXoaOrTenDonViContainingAndDaXoa(search, Constants.DAXOA, search, Constants.DAXOA, pageable).getContent()));
        }
        return new Response(Status.SUCCESS.value(),dongBoCoQuanQuanLyMapper.toDVCCoQuanQuanLyDTOList(dongBoCoQuanQuanLyRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent()));
    }

    public Response countByMaOrTen(String search) throws Exception {
        if (search != null && search.length() > 0) {
            search = URLDecoder.decode(search, "UTF-8");
        }
        return new Response(
                Status.SUCCESS.value(), dongBoCoQuanQuanLyRepository.countByMaContainingAndDaXoaOrTenDonViContainingAndDaXoa(search, Constants.DAXOA, search, Constants.DAXOA).longValue());
    }

}
