package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DanhMucGiayToRepository;
import sdt.osgi.springboot.dto.DanhMucGiayToDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DanhMucGiayToMapper;
import sdt.osgi.springboot.model.DanhMucGiayTo;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DanhMucGiayToValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DanhMucGiayToService {


    private final DanhMucGiayToRepository danhMucGiayToRepository;
    private final DanhMucGiayToMapper danhMucGiayToMapper;
    private final DanhMucGiayToValidate danhMucGiayToValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<DanhMucGiayToDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DanhMucGiayToDTO danhMucGiayToDTO = new DanhMucGiayToDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            danhMucGiayToDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            danhMucGiayToDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            danhMucGiayToDTO.setTenRutGon(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            danhMucGiayToDTO.setGioiHanDungLuong(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            danhMucGiayToDTO.setKieuTaiLieu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (danhMucGiayToValidate.validateDanhMucGiayTo(danhMucGiayToDTO, danhMucGiayToMapper.danhMucGiayToToDanhMucGiayToDTO(danhMucGiayToRepository.findFirstByMaAndDaXoa(danhMucGiayToDTO.getMa(), Constants.DAXOA)))) {
                        danhMucGiayToDTO.setNguoiTao(nguoiTao);
                        danhMucGiayToDTO.setNguoiSua(nguoiTao);
                        if (danhMucGiayToDTO.getMa() != null) {
                            danhMucGiayToRepository.save(danhMucGiayToMapper.danhMucGiayToDTOToDanhMucGiayTo(danhMucGiayToDTO));
                        }
                    } else {
                        maFail.add(danhMucGiayToDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                danhMucGiayToMapper.danhMucGiayToToDanhMucGiayToDTO
                        (danhMucGiayToRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                danhMucGiayToMapper.danhMucGiayToToDanhMucGiayToDTO(danhMucGiayToRepository.findFirstById(id)));
    }

    public Response insert(DanhMucGiayToDTO danhMucGiayToDTO) {
        danhMucGiayToRepository.save(danhMucGiayToMapper.danhMucGiayToDTOToDanhMucGiayTo(danhMucGiayToDTO));

        return new Response(Status.SUCCESS.value(), danhMucGiayToDTO.getMa());
    }

    public Response update(DanhMucGiayToDTO danhMucGiayToDTO) {
        DanhMucGiayTo danhMucGiayToOrigin = danhMucGiayToRepository.findById(danhMucGiayToDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DanhMucGiayTo danhMucGiayTo = DanhMucGiayTo.builder()
                .id(danhMucGiayToDTO.getId())
                .ma(danhMucGiayToDTO.getMa())
                .ten(danhMucGiayToDTO.getTen())
                .tenRutGon(danhMucGiayToDTO.getTenRutGon())
                .gioiHanDungLuong(danhMucGiayToDTO.getGioiHanDungLuong())
                .kieuTaiLieu(danhMucGiayToDTO.getKieuTaiLieu())
                .build();

        danhMucGiayToRepository.save(danhMucGiayTo);
        return new Response(Status.SUCCESS.value(), danhMucGiayToDTO.getMa());
    }

    public Response delete(long id) {
        DanhMucGiayTo danhMucGiayTo = danhMucGiayToRepository.findFirstById(id);
        danhMucGiayTo.setDaXoa(Constants.XOA);
        danhMucGiayToRepository.save(danhMucGiayTo);
        return new Response(Status.SUCCESS.value(), null);
    }
    ////return reponse
    public Response findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page / size, size);

        if (search != null && search.length() > 0) {
            return new Response(Status.SUCCESS.value(), danhMucGiayToMapper.toDanhMucGiayTogDTOList(danhMucGiayToRepository.findByMaContainingAndDaXoaOrTenContainingAndDaXoa(search, Constants.DAXOA, search, pageable, Constants.DAXOA)));

        }

        return new Response(Status.SUCCESS.value(), danhMucGiayToMapper.toDanhMucGiayTogDTOList(danhMucGiayToRepository.findAllByDaXoa(Constants.DAXOA, pageable)));
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", danhMucGiayToRepository.countByMaContainingAndDaXoaOrTenContainingAndDaXoa(search, Constants.DAXOA, search, Constants.DAXOA).longValue()).toString();
    }


}
