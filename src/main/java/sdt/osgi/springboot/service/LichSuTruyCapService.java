package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.LichSuTruyCapRepository;
import sdt.osgi.springboot.dto.LichSuTruyCapDTO;
import sdt.osgi.springboot.mapper.LichSuTruyCapMapper;
import sdt.osgi.springboot.model.LichSuTruyCap;
import sdt.osgi.springboot.util.Constants;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static sdt.osgi.springboot.util.Constants.DAXOA;

@Service
@RequiredArgsConstructor
public class LichSuTruyCapService {

    private final LichSuTruyCapRepository lichSuTruyCapRepository;
    private final LichSuTruyCapMapper lichSuTruyCapMapper;

    public LichSuTruyCapDTO findById(long id) {
        Optional<LichSuTruyCap> lichSuTruyCap = lichSuTruyCapRepository.findById(id);

        if ((lichSuTruyCap).isPresent()) {
            return lichSuTruyCapMapper.toLichSuTruyCapDTO((lichSuTruyCap).get());
        }

        return null;
    }

    public void insert(LichSuTruyCapDTO lichSuTruyCapDTO) {
        lichSuTruyCapDTO.setThoiGianDangNhap(new Date());
        lichSuTruyCapRepository.save(lichSuTruyCapMapper.toLichSuTruyCap(lichSuTruyCapDTO));
    }

    public void update(LichSuTruyCapDTO lichSuTruyCapDTO) {
        LichSuTruyCap lichSuTruyCapOrigin = lichSuTruyCapRepository.findById(lichSuTruyCapDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        LichSuTruyCap lichSuTruyCap = LichSuTruyCap.builder()
                .id(lichSuTruyCapDTO.getId())
                .ten(lichSuTruyCapDTO.getTen() != null ? lichSuTruyCapDTO.getTen() : lichSuTruyCapOrigin.getTen())
                .build();

        lichSuTruyCapRepository.save(lichSuTruyCap);
    }

    public void delete(LichSuTruyCapDTO lichSuTruyCapDTO) {
        LichSuTruyCap lichSuTruyCap = lichSuTruyCapRepository.findFirstById(lichSuTruyCapDTO.getId());
        lichSuTruyCap.setDaXoa(Constants.XOA);
        lichSuTruyCapRepository.save(lichSuTruyCap);
    }

    public List<LichSuTruyCapDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page / size, size);

        if (search != null && search.length() > 0) {
            return lichSuTruyCapMapper.toLichSuTruyCapDTOList(lichSuTruyCapRepository.findByTenContainingAndDaXoaOrderByNgayTaoDesc(search, pageable, DAXOA).getContent());
        }

        return lichSuTruyCapMapper.toLichSuTruyCapDTOList(lichSuTruyCapRepository.findAllByDaXoaOrderByNgayTaoDesc(pageable, DAXOA).getContent());
    }

    public List<LichSuTruyCapDTO> findByNguoiDungId(long nguoiDungId) {

        return lichSuTruyCapMapper.toLichSuTruyCapDTOList(lichSuTruyCapRepository.findByNguoiDungIdAndDaXoa(nguoiDungId, DAXOA));
    }

    public String countByTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", lichSuTruyCapRepository.countByTenContainingAndDaXoa(search, DAXOA).longValue()).toString();
    }
}
