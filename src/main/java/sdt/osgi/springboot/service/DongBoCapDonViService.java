package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DongBoCapDonViRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.DongBoCapDonViMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DongBoCapDonViService {
    private final DongBoCapDonViMapper dongBoCapDonViMapper;
    private final DongBoCapDonViRepository dongBoCapDonViRepository;

    public Response findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page / size, size);
        if (search != null && search.length() > 0) {
            return new Response(Status.SUCCESS.value(), dongBoCapDonViMapper.toDongBoCapDonViDTOList(dongBoCapDonViRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent()));
        }
        return new Response(Status.SUCCESS.value(),dongBoCapDonViMapper.toDongBoCapDonViDTOList(dongBoCapDonViRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent()));
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();
        return jsonObject.put("total", dongBoCapDonViRepository.countByMaContainingAndDaXoaOrTenContainingAndDaXoa(search, Constants.DAXOA,  search, Constants.DAXOA).longValue()).toString();
    }
}
