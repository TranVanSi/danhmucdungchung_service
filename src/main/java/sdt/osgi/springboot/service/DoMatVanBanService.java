package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DoMatVanBanRepository;
import sdt.osgi.springboot.dto.DoMatVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DoMatVanBanMapper;
import sdt.osgi.springboot.model.DoMatVanBan;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DoMatVanBanValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DoMatVanBanService {
    private final DoMatVanBanMapper doMatVanBanMapper;
    private final DoMatVanBanRepository doMatVanBanRepository;
    private final DoMatVanBanValidate doMatVanBanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<DoMatVanBanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DoMatVanBanDTO doMatVanBanDTO = new DoMatVanBanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            doMatVanBanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            doMatVanBanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (doMatVanBanValidate.validateDoMatVanBan(doMatVanBanDTO, doMatVanBanMapper.doMatVanBanToDoMatVanBanDTO(doMatVanBanRepository.findFirstByMaAndDaXoa(doMatVanBanDTO.getMa(), Constants.DAXOA)))) {
                        doMatVanBanDTO.setNguoiTao(nguoiTao);
                        doMatVanBanDTO.setNguoiSua(nguoiTao);
                        if (doMatVanBanDTO.getMa() != null) {
                            doMatVanBanRepository.save(doMatVanBanMapper.doMatVanBanDTOToDoMatVanBan(doMatVanBanDTO));
                        }
                    } else {
                        maFail.add(doMatVanBanDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                doMatVanBanMapper.doMatVanBanToDoMatVanBanDTO
                        (doMatVanBanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                doMatVanBanMapper.doMatVanBanToDoMatVanBanDTO(doMatVanBanRepository.findFirstById(id)));
    }

    public Response insert(DoMatVanBanDTO doMatVanBanDTO) {
        doMatVanBanRepository.save(doMatVanBanMapper.doMatVanBanDTOToDoMatVanBan(doMatVanBanDTO));

        return new Response(Status.SUCCESS.value(), doMatVanBanDTO.getMa());
    }

    public Response update(DoMatVanBanDTO doMatVanBanDTO) {
        DoMatVanBan doMatVanBanOrigin = doMatVanBanRepository.findById(doMatVanBanDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DoMatVanBan doMatVanBan = DoMatVanBan.builder()
                .id(doMatVanBanDTO.getId())
                .ma(doMatVanBanDTO.getMa())
                .ten(doMatVanBanDTO.getTen())
                .build();

        doMatVanBanRepository.save(doMatVanBan);
        return new Response(Status.SUCCESS.value(), doMatVanBanDTO.getMa());
    }

    public Response delete(long id) {
        DoMatVanBan doMatVanBan = doMatVanBanRepository.findFirstById(id);
        doMatVanBan.setDaXoa(Constants.XOA);
        doMatVanBanRepository.save(doMatVanBan);
        return new Response(Status.SUCCESS.value(), null);
    }

    public List<DoMatVanBanDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return doMatVanBanMapper.toDoMatVanBanDTOList(doMatVanBanRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return doMatVanBanMapper.toDoMatVanBanDTOList(doMatVanBanRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", doMatVanBanRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                doMatVanBanMapper.toDoMatVanBanViewDTOList(
                        doMatVanBanRepository.findByDaXoa(Constants.DAXOA)));

    }
}
