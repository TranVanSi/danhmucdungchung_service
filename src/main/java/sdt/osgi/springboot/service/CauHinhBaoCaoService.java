package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.CauHinhBaoCaoRepository;
import sdt.osgi.springboot.dto.CauHinhBaoCaoDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.CauHinhBaoCaoMapper;
import sdt.osgi.springboot.model.CauHinhBaoCao;
import sdt.osgi.springboot.model.Status;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Service
@RequiredArgsConstructor
public class CauHinhBaoCaoService {

    private final CauHinhBaoCaoRepository cauHinhBaoCaoRepository;
    private final CauHinhBaoCaoMapper cauHinhBaoCaoMapper;

    @PersistenceContext
    private EntityManager entityManager;


    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                cauHinhBaoCaoMapper.cauHinhBaoCaoToCauHinhBaoCaoDTO(cauHinhBaoCaoRepository.findFirstById(id)));
    }

    public Response insert(CauHinhBaoCaoDTO cauHinhBaoCaoDTO) {
        cauHinhBaoCaoRepository.save(cauHinhBaoCaoMapper.cauHinhBaoCaoDTOToCauHinhBaoCao(cauHinhBaoCaoDTO));

        return new Response(Status.SUCCESS.value(), cauHinhBaoCaoDTO.getTenBang());
    }

    public Response update(CauHinhBaoCaoDTO cauHinhBaoCaoDTO) {
        CauHinhBaoCao cauHinhBaoCao = CauHinhBaoCao.builder()
                .id(cauHinhBaoCaoDTO.getId())
                .tenBang(cauHinhBaoCaoDTO.getTenBang())
                .tenDanhMuc(cauHinhBaoCaoDTO.getTenDanhMuc())
                .tenNguonDongBo(cauHinhBaoCaoDTO.getTenNguonDongBo())
                .tongDuLieu(cauHinhBaoCaoDTO.getTongDuLieu())
                .url(cauHinhBaoCaoDTO.getUrl())
                .build();

        cauHinhBaoCaoRepository.save(cauHinhBaoCao);
        return new Response(Status.SUCCESS.value(), cauHinhBaoCaoDTO.getTenBang());
    }

    public Response delete(long id) {

        cauHinhBaoCaoRepository.deleteById(id);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                cauHinhBaoCaoMapper.toCauHinhBaoCaoDTOList(
                        cauHinhBaoCaoRepository.findAll()));
    }

    public Response findByTenBang(String tenBang) {
        return new Response(
                Status.SUCCESS.value(),
                cauHinhBaoCaoMapper.cauHinhBaoCaoToCauHinhBaoCaoDTO(
                        cauHinhBaoCaoRepository.findFirstByTenBang(tenBang)));
    }

    public Response kiemTraTenBang(String tenBang){
        String kiemTraTonTai = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES " +
                "WHERE (TABLE_SCHEMA = \'DAKLAK_DULIEUDUNGCHUNG\' OR TABLE_SCHEMA = \'DAKLAK_QUANLYNGUOIDUNG\') " +
                "AND TABLE_NAME = " + "'" + tenBang + "'";

        Query kiemTraTonTaiQuery = entityManager.createNativeQuery(kiemTraTonTai);
        if (Long.valueOf(String.valueOf(kiemTraTonTaiQuery.getResultList().get(0))) == 0) {
            return new Response(
                    Status.FAIL.value(),null);

        } else {
            return new Response(
                    Status.SUCCESS.value(), null);
        }

    }

    public Response countAllTable(String tenBang){
        String sql = "";
        String kiemTraTonTaiDanhMucDungChung = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES " +
                "WHERE TABLE_SCHEMA = \'DAKLAK_DULIEUDUNGCHUNG\' " +
                "AND TABLE_NAME = " + "'" + tenBang + "'";
        Query queryKiemTraTonTaiDanhMucDungChung = entityManager.createNativeQuery(kiemTraTonTaiDanhMucDungChung);

        String kiemTraTonTaiQuanLyNguoiDung = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES " +
                "WHERE TABLE_SCHEMA = \'DAKLAK_QUANLYNGUOIDUNG\' " +
                "AND TABLE_NAME = " + "'" + tenBang + "'";
        Query queryKiemTraTonTaiQuanLyNguoiDung = entityManager.createNativeQuery(kiemTraTonTaiQuanLyNguoiDung);

        if (Long.valueOf(String.valueOf(queryKiemTraTonTaiDanhMucDungChung.getResultList().get(0))) != 0) {
            sql = "SELECT COUNT(*) FROM DAKLAK_DULIEUDUNGCHUNG."+ tenBang + " WHERE DA_XOA = 0 ";
        } else if (Long.valueOf(String.valueOf(queryKiemTraTonTaiQuanLyNguoiDung.getResultList().get(0))) != 0) {
            sql = "SELECT COUNT(*) FROM DAKLAK_QUANLYNGUOIDUNG."+ tenBang;
        }
        Query query = entityManager.createNativeQuery(sql);

        return new Response(Status.SUCCESS.value(),
                String.valueOf( query.getResultList().get(0)));

    }
}
