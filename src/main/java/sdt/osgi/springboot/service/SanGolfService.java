package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.SanGolfRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.SanGolfDTO;
import sdt.osgi.springboot.mapper.SanGolfMapper;
import sdt.osgi.springboot.model.SanGolf;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.SanGolfValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class SanGolfService {

    private final SanGolfRepository sanGolfRepository;
    private final SanGolfMapper sanGolfMapper;
    private final SanGolfValidate sanGolfValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<SanGolfDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                SanGolfDTO sanGolfDTO = new SanGolfDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            sanGolfDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            sanGolfDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            sanGolfDTO.setDienTich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            sanGolfDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            sanGolfDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            sanGolfDTO.setSoLo(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            sanGolfDTO.setMoHinhThietKe(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            sanGolfDTO.setDichVuNhaHang(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            sanGolfDTO.setDichVuSanChoiTreEm(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            sanGolfDTO.setDichVuBeBoi(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 10) {
                            sanGolfDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 11) {
                            sanGolfDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (sanGolfValidate.validateSanGold(sanGolfDTO, sanGolfMapper.sanGoldToSanGoldDTO(sanGolfRepository.findFirstByMaAndDaXoa(sanGolfDTO.getMa(), Constants.DAXOA)))) {
                        sanGolfDTO.setNguoiTao(nguoiTao);
                        sanGolfDTO.setNguoiSua(nguoiTao);
                        if (sanGolfDTO.getMa() != null) {
                            sanGolfRepository.save(sanGolfMapper.sanGoldDTOToSanGold(sanGolfDTO));
                        }
                    } else {
                        maFail.add(sanGolfDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                sanGolfMapper.sanGoldToSanGoldDTO
                        (sanGolfRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                sanGolfMapper.sanGoldToSanGoldDTO(sanGolfRepository.findFirstById(id)));
    }

    public Response insert(SanGolfDTO sanGolfDTO) {
        sanGolfRepository.save(sanGolfMapper.sanGoldDTOToSanGold(sanGolfDTO));

        return new Response(Status.SUCCESS.value(), sanGolfDTO.getMa());
    }

    public Response update(SanGolfDTO sanGolfDTO) {
        SanGolf sanGolfOrigin = sanGolfRepository.findById(sanGolfDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        SanGolf sanGolf = SanGolf.builder()
                .id(sanGolfDTO.getId())
                .ma(sanGolfDTO.getMa())
                .ten(sanGolfDTO.getTen())
                .dienTich(sanGolfDTO.getDienTich())
                .diaChi(sanGolfDTO.getDiaChi())
                .soDienThoai(sanGolfDTO.getSoDienThoai())
                .anh(sanGolfDTO.getAnh() != null ? sanGolfDTO.getAnh() : sanGolfOrigin.getAnh())
                .soLo(sanGolfDTO.getSoLo())
                .moHinhThietKe(sanGolfDTO.getMoHinhThietKe())
                .dichVuNhaHang(sanGolfDTO.getDichVuNhaHang())
                .dichVuSanChoiTreEm(sanGolfDTO.getDichVuSanChoiTreEm())
                .dichVuBeBoi(sanGolfDTO.getDichVuBeBoi())
                .url(sanGolfDTO.getUrl())
                .diaChiGoogleMap(sanGolfDTO.getDiaChiGoogleMap())
                .build();

        sanGolfRepository.save(sanGolf);
        return new Response(Status.SUCCESS.value(), sanGolfDTO.getMa());
    }

    public Response delete(long id) {

        SanGolf sanGolf = sanGolfRepository.findFirstById(id);
        sanGolf.setDaXoa(Constants.XOA);
        sanGolfRepository.save(sanGolf);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<SanGolfDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return sanGolfMapper.toSanGoldDTOList(sanGolfRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return sanGolfMapper.toSanGoldDTOList(sanGolfRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", sanGolfRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                sanGolfMapper.toSanGoldViewDTOList(
                        sanGolfRepository.findByDaXoa(Constants.DAXOA)));

    }

}
