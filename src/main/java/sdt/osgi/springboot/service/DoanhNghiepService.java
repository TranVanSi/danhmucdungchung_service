package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DoanhNghiepRepository;
import sdt.osgi.springboot.dao.LoaiDoanhNghiepRepository;
import sdt.osgi.springboot.dao.TrangThaiDoanhNghiepRepository;
import sdt.osgi.springboot.dto.DoanhNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DoanhNghiepMapper;
import sdt.osgi.springboot.mapper.NganhNgheKinhDoanhMapper;
import sdt.osgi.springboot.model.DoanhNghiep;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.util.StringUtils;
import sdt.osgi.springboot.validate.DoanhNghiepValidate;
import sdt.osgi.springboot.viewdto.NganhNgheKinhDoanhViewDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DoanhNghiepService {

    private final DoanhNghiepRepository doanhNghiepRepository;
    private final DoanhNghiepMapper doanhNghiepMapper;
    private final DoanhNghiepValidate doanhNghiepValidate;
    private final TrangThaiDoanhNghiepRepository trangThaiDoanhNghiepRepository;
    private final LoaiDoanhNghiepRepository loaiDoanhNghiepRepository;
    private final NganhNgheKinhDoanhService nganhNgheKinhDoanhService;
    private final NganhNgheKinhDoanhMapper nganhNgheKinhDoanhMapper;
    private final DoanhNghiep2NganhNgheKinhDoanhService doanhNghiep2NganhNgheKinhDoanhService;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;
    @Value("${vn.sdt.export.khongtimthaytainguyen}")
    private String khongTimThayTaiNguyen;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DataFormatter dataFormatter = new DataFormatter();
        String nganhNgheKinhDoanhDuocChon ="";

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DoanhNghiepDTO doanhNghiepDTO = new DoanhNghiepDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            doanhNghiepDTO.setMaDoanhNghiep(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 1) {
                            doanhNghiepDTO.setTenDoanhNghiep(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            doanhNghiepDTO.setDiaChiDoanhNghiep(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 3) {
                            doanhNghiepDTO.setDiaChiDoanhNghiepXaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            doanhNghiepDTO.setDiaChiDoanhNghiepHuyenId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 5) {
                            doanhNghiepDTO.setVonDieuLe(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 6) {
                            doanhNghiepDTO.setTrangThaiDoanhNghiepId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 7) {
                            doanhNghiepDTO.setDienThoaiDoanhNghiep(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 8) {
                            doanhNghiepDTO.setEmail(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 9) {
                            doanhNghiepDTO.setHoTenNdd(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 10) {
                            doanhNghiepDTO.setNgaySinhNdd(dataFormatter.formatCellValue(cell).length() > 0 ?
                                    dateFormat.format(sdf.parse(dataFormatter.formatCellValue(cell))) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 11) {
                            doanhNghiepDTO.setCmndNguoiDaiDien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 12) {
                            doanhNghiepDTO.setNgayCapCmndNdd(dataFormatter.formatCellValue(cell).length() > 0 ?
                                    dateFormat.format(sdf.parse(dataFormatter.formatCellValue(cell))) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 13) {
                            doanhNghiepDTO.setNoiCapCmndNddId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 14) {
                            doanhNghiepDTO.setChuSoHuu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 15) {
                            nganhNgheKinhDoanhDuocChon = String.valueOf(dataFormatter.formatCellValue(cell));
                            continue;
                        }
                        if (cell.getColumnIndex() == 16) {
                            doanhNghiepDTO.setLoaiHinhDoanhNghiepId(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 17) {
                            doanhNghiepDTO.setSoLaoDong(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 18) {
                            doanhNghiepDTO.setDanhSachThanhVienGopVon(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 19) {
                            doanhNghiepDTO.setDanhSachCoDong(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 20) {
                            doanhNghiepDTO.setLoaiDoanhNghiepEnum(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 21) {
                            doanhNghiepDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }
                    if (doanhNghiepValidate.validateDoanhNghiep(doanhNghiepDTO, doanhNghiepMapper.toDoanhNghiepToDoanhNghiepDTO(doanhNghiepRepository.findFirstByMaDoanhNghiepAndDaXoa(doanhNghiepDTO.getMaDoanhNghiep(), Constants.DAXOA)))) {
                        doanhNghiepDTO.setNguoiTao(nguoiTao);
                        doanhNghiepDTO.setNguoiSua(nguoiTao);
                        doanhNghiepDTO.setMaSoThue(doanhNghiepDTO.getMaDoanhNghiep());
                        doanhNghiepDTO.setDiaChiDoanhNghiepTinhId(Constants.MACDINH_TINH_DAKLAK);
                        if (doanhNghiepDTO.getMaDoanhNghiep() != null) {
                            doanhNghiepRepository.save(doanhNghiepMapper.toDoanhNghiepDTOToDoanhNghiep(doanhNghiepDTO));
                            List<Long> nganhNgheKinhDoanhDuocChonIds = StringUtils.convertStringToLongs(nganhNgheKinhDoanhDuocChon);
                            List<NganhNgheKinhDoanhViewDTO> nganhNgheKinhDoanhDTOS = nganhNgheKinhDoanhService.findByNganhNgheKinhDoanhIn(nganhNgheKinhDoanhDuocChonIds);
                            long doanhNghiepId = doanhNghiepRepository.findFirstByMaDoanhNghiepAndDaXoa(doanhNghiepDTO.getMaDoanhNghiep(), Constants.DAXOA).getId();
                            doanhNghiep2NganhNgheKinhDoanhService.createDoanhNghiep2NganhNgheKinhDOanh(doanhNghiepId, nganhNgheKinhDoanhMapper.NGANH_NGHE_KINH_DOANHS(nganhNgheKinhDoanhDTOS));
                        }
                    } else {
                        maFail.add(doanhNghiepDTO.getMaDoanhNghiep());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? "" : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                doanhNghiepMapper.toDoanhNghiepToDoanhNghiepDTO
                        (doanhNghiepRepository.findFirstByMaDoanhNghiepAndDaXoa(ma, Constants.DAXOA)));
    }
    public Response findByMaSoThue(String maSoThue) {
        return new Response(
                Status.SUCCESS.value(),
                doanhNghiepMapper.toDoanhNghiepToDoanhNghiepDTO
                        (doanhNghiepRepository.findFirstByMaSoThueAndDaXoa(maSoThue, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                doanhNghiepMapper.toDoanhNghiepToDoanhNghiepDTO(doanhNghiepRepository.findFirstById(id)));
    }

    public Response insert(DoanhNghiepDTO doanhNghiepDTO) {
        doanhNghiepRepository.save(doanhNghiepMapper.toDoanhNghiepDTOToDoanhNghiep(doanhNghiepDTO));

        return new Response(Status.SUCCESS.value(), doanhNghiepDTO.getMaDoanhNghiep());
    }

    public Response update(DoanhNghiepDTO doanhNghiepDTO) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DoanhNghiep doanhNghiepOrigin = doanhNghiepRepository.findById(doanhNghiepDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, khongTimThayTaiNguyen));
        DoanhNghiep doanhNghiep = DoanhNghiep.builder()
                .id(doanhNghiepDTO.getId())
                .tenDoanhNghiep(doanhNghiepDTO.getTenDoanhNghiep())
                .maDoanhNghiep(doanhNghiepDTO.getMaDoanhNghiep())
                .diaChiDoanhNghiep(doanhNghiepDTO.getDiaChiDoanhNghiep())
                .diaChiDoanhNghiepTinhId(doanhNghiepDTO.getDiaChiDoanhNghiepTinhId())
                .diaChiDoanhNghiepHuyenId(doanhNghiepDTO.getDiaChiDoanhNghiepHuyenId())
                .diaChiDoanhNghiepXaId(doanhNghiepDTO.getDiaChiDoanhNghiepXaId())
                .vonDieuLe(doanhNghiepDTO.getVonDieuLe())
                .trangThaiDoanhNghiep(trangThaiDoanhNghiepRepository.findFirstById(doanhNghiepDTO.getTrangThaiDoanhNghiepId()))
                .dienThoaiDoanhNghiep(doanhNghiepDTO.getDienThoaiDoanhNghiep())
                .email(doanhNghiepDTO.getEmail())
                .hoTenNdd(doanhNghiepDTO.getHoTenNdd())
                .ngaySinhNdd(doanhNghiepDTO.getNgaySinhNdd() != null ? simpleDateFormat.parse(doanhNghiepDTO.getNgaySinhNdd()) : doanhNghiepOrigin.getNgaySinhNdd())
                .cmndNguoiDaiDien(doanhNghiepDTO.getCmndNguoiDaiDien())
                .ngayCapCmndNdd(doanhNghiepDTO.getNgayCapCmndNdd() != null ? simpleDateFormat.parse(doanhNghiepDTO.getNgayCapCmndNdd()) : doanhNghiepOrigin.getNgayCapCmndNdd())
                .noiCapCmndNddId(doanhNghiepDTO.getNoiCapCmndNddId())
                .chuSoHuu(doanhNghiepDTO.getChuSoHuu())
                .loaiDoanhNghiep(loaiDoanhNghiepRepository.findFirstById(doanhNghiepDTO.getLoaiHinhDoanhNghiepId()))
                .soLaoDong(doanhNghiepDTO.getSoLaoDong())
                .danhSachThanhVienGopVon(doanhNghiepDTO.getDanhSachThanhVienGopVon())
                .danhSachCoDong(doanhNghiepDTO.getDanhSachCoDong())
                .loaiDoanhNghiepEnum(doanhNghiepDTO.getLoaiDoanhNghiepEnum())
                .diaChiGoogleMap(doanhNghiepDTO.getDiaChiGoogleMap())
                .build();

        doanhNghiepRepository.save(doanhNghiep);
        return new Response(Status.SUCCESS.value(), doanhNghiepDTO.getMaDoanhNghiep());
    }

    public Response delete(long id) {

        DoanhNghiep doanhNghiep = doanhNghiepRepository.findFirstById(id);
        doanhNghiep.setDaXoa(Constants.XOA);
        doanhNghiepRepository.save(doanhNghiep);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page / size, size);
        if (search != null && search.length() > 0) {
            return new Response(Status.SUCCESS.value(), doanhNghiepMapper.toDoanhNghiepDTOList(doanhNghiepRepository.findByMaDoanhNghiepContainingOrTenDoanhNghiepContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent()));
        }
        return new Response(Status.SUCCESS.value(),doanhNghiepMapper.toDoanhNghiepDTOList(doanhNghiepRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent()));
    }

    public String countByMaOrTen(String search, long chaId) {
        JSONObject jsonObject = new JSONObject();
        return jsonObject.put("total", doanhNghiepRepository.countByMaDoanhNghiepContainingAndDaXoaOrTenDoanhNghiepContainingAndDaXoa(search, Constants.DAXOA,  search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                doanhNghiepMapper.toDoanhNghiepViewDTOList(
                        doanhNghiepRepository.findByDaXoa(Constants.DAXOA)));

    }

}
