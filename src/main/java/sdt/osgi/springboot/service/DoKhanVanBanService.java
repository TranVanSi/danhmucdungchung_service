package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.dao.DoKhanVanBanRepository;
import sdt.osgi.springboot.dto.DoKhanVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DoKhanVanBanMapper;
import sdt.osgi.springboot.model.DoKhanVanBan;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DoKhanVanBanValidate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DoKhanVanBanService {
    private final DoKhanVanBanMapper doKhanVanBanMapper;
    private final DoKhanVanBanRepository doKhanVanBanRepository;
    private final DoKhanVanBanValidate doKhanVanBanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<DoKhanVanBanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DoKhanVanBanDTO doKhanVanBanDTO = new DoKhanVanBanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            doKhanVanBanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            doKhanVanBanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            doKhanVanBanDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            doKhanVanBanDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            doKhanVanBanDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (doKhanVanBanValidate.validateDoKhanVanBan(doKhanVanBanDTO, doKhanVanBanMapper.doKhanVanBanToKhanVanBanDTO(doKhanVanBanRepository.findFirstByMaAndDaXoa(doKhanVanBanDTO.getMa(), Constants.DAXOA)))) {
                        doKhanVanBanDTO.setNguoiTao(nguoiTao);
                        doKhanVanBanDTO.setNguoiSua(nguoiTao);
                        if (doKhanVanBanDTO.getMa() != null) {
                            doKhanVanBanRepository.save(doKhanVanBanMapper.doKhanVanBanDTOToDoKhanVanBan(doKhanVanBanDTO));
                        }

                    } else {
                        maFail.add(doKhanVanBanDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                doKhanVanBanMapper.doKhanVanBanToKhanVanBanDTO
                        (doKhanVanBanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                doKhanVanBanMapper.doKhanVanBanToKhanVanBanDTO(doKhanVanBanRepository.findFirstById(id)));
    }


    public Response insert(DoKhanVanBanDTO doKhanVanBanDTO) {
        doKhanVanBanRepository.save(doKhanVanBanMapper.doKhanVanBanDTOToDoKhanVanBan(doKhanVanBanDTO));

        return new Response(Status.SUCCESS.value(), doKhanVanBanDTO.getMa());
    }

    public Response update(DoKhanVanBanDTO doKhanVanBanDTO) {
        DoKhanVanBan doKhanVanBan = DoKhanVanBan.builder()
                .id(doKhanVanBanDTO.getId())
                .ma(doKhanVanBanDTO.getMa())
                .ten(doKhanVanBanDTO.getTen())
                .qdBanHanhSuaDoi(doKhanVanBanDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(doKhanVanBanDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(doKhanVanBanDTO.getQdCoQuanBanHanh())
                .build();

        doKhanVanBanRepository.save(doKhanVanBan);
        return new Response(Status.SUCCESS.value(), doKhanVanBanDTO.getMa());
    }

    public Response delete(long id) {
        DoKhanVanBan doKhanVanBan = doKhanVanBanRepository.findFirstById(id);
        doKhanVanBan.setDaXoa(Constants.XOA);
        doKhanVanBanRepository.save(doKhanVanBan);
        return new Response(Status.SUCCESS.value(), null);
    }

    public List<DoKhanVanBanDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return doKhanVanBanMapper.toKhanVanBanDTOList(doKhanVanBanRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return doKhanVanBanMapper.toKhanVanBanDTOList(doKhanVanBanRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", doKhanVanBanRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                doKhanVanBanMapper.toDoKhanVanBanViewDTOList(
                        doKhanVanBanRepository.findByDaXoa(Constants.DAXOA)));

    }
}
