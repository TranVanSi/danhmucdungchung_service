package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.SinhTracHocRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.SinhTracHocDTO;
import sdt.osgi.springboot.mapper.SinhTracHocMapper;
import sdt.osgi.springboot.model.SinhTracHoc;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.SinhTracHocValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class SinhTracHocService {
    private final SinhTracHocMapper sinhTracHocMapper;
    private final SinhTracHocRepository sinhTracHocRepository;
    private final SinhTracHocValidate sinhTracHocValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<SinhTracHocDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                SinhTracHocDTO sinhTracHocDTO = new SinhTracHocDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            sinhTracHocDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            sinhTracHocDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (sinhTracHocValidate.validateSinhTracHoc(sinhTracHocDTO, sinhTracHocMapper.sinhTracHocToSinhTracHocDTO(sinhTracHocRepository.findFirstByMaAndDaXoa(sinhTracHocDTO.getMa(), Constants.DAXOA)))) {
                        sinhTracHocDTO.setNguoiTao(nguoiTao);
                        sinhTracHocDTO.setNguoiSua(nguoiTao);
                        if (sinhTracHocDTO.getMa() != null) {
                            sinhTracHocRepository.save(sinhTracHocMapper.sinhTracHocDTOToSinhTracHoc(sinhTracHocDTO));
                        }
                    } else {
                        maFail.add(sinhTracHocDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                sinhTracHocMapper.sinhTracHocToSinhTracHocDTO
                        (sinhTracHocRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                sinhTracHocMapper.sinhTracHocToSinhTracHocDTO(sinhTracHocRepository.findFirstById(id)));
    }

    public Response insert(SinhTracHocDTO sinhTracHocDTO) {
        sinhTracHocRepository.save(sinhTracHocMapper.sinhTracHocDTOToSinhTracHoc(sinhTracHocDTO));

        return new Response(Status.SUCCESS.value(), sinhTracHocDTO.getMa());
    }

    public Response update(SinhTracHocDTO sinhTracHocDTO) {
        SinhTracHoc sinhTracHocOrigin = sinhTracHocRepository.findById(sinhTracHocDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        SinhTracHoc sinhTracHoc = SinhTracHoc.builder()
                .id(sinhTracHocDTO.getId())
                .ma(sinhTracHocDTO.getMa())
                .ten(sinhTracHocDTO.getTen())
                .build();

        sinhTracHocRepository.save(sinhTracHoc);
        return new Response(Status.SUCCESS.value(), sinhTracHocDTO.getMa());
    }

    public Response delete(long id) {
        SinhTracHoc sinhTracHoc = sinhTracHocRepository.findFirstById(id);
        sinhTracHoc.setDaXoa(Constants.XOA);
        sinhTracHocRepository.save(sinhTracHoc);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                sinhTracHocMapper.toSinhTracHocViewDTOList(
                        sinhTracHocRepository.findByDaXoa(Constants.DAXOA)));

    }
}
