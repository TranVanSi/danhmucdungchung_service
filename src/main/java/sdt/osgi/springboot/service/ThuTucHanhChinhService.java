package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.NhomThuTucHanhChinhRepository;
import sdt.osgi.springboot.dao.ThuTucHanhChinhRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.ThuTucHanhChinhDTO;
import sdt.osgi.springboot.mapper.NhomThuTucHanhChinhMapper;
import sdt.osgi.springboot.mapper.ThuTucHanhChinhMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.model.ThuTucHanhChinh;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.ThuTucHanhChinhValidate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class ThuTucHanhChinhService {

    private final ThuTucHanhChinhRepository thuTucHanhChinhRepository;
    private final ThuTucHanhChinhMapper thuTucHanhChinhMapper;
    private final ThuTucHanhChinhValidate thuTucHanhChinhValidate;
    private final NhomThuTucHanhChinhRepository nhomThuTucHanhChinhRepository;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<ThuTucHanhChinhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                ThuTucHanhChinhDTO thuTucHanhChinhDTO = new ThuTucHanhChinhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            thuTucHanhChinhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            thuTucHanhChinhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            thuTucHanhChinhDTO.setMaNiemYet(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            thuTucHanhChinhDTO.setTenRutGon(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            thuTucHanhChinhDTO.setMaQuyTrinh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 5) {
                            thuTucHanhChinhDTO.setNhomThuTucHanhChinhId((Long.valueOf(dataFormatter.formatCellValue(cell))));
                            continue;
                        }

                        if (cell.getColumnIndex() == 6) {
                            thuTucHanhChinhDTO.setMucDoDvc(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 7) {
                            thuTucHanhChinhDTO.setTrinhTuThucHien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 8) {
                            thuTucHanhChinhDTO.setCachThucHien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 9) {
                            thuTucHanhChinhDTO.setCanCuPhapLy(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 10) {
                            thuTucHanhChinhDTO.setYeuCauDieuKienThucHien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 11) {
                            thuTucHanhChinhDTO.setCapCoQuanThucHienId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 12) {
                            thuTucHanhChinhDTO.setThanhPhanHoSo(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 13) {
                            thuTucHanhChinhDTO.setSoBoHoSo(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 14) {
                            thuTucHanhChinhDTO.setBieuMauDinhKem(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 15) {
                            thuTucHanhChinhDTO.setTrangThai(Byte.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 16) {
                            thuTucHanhChinhDTO.setSoQdCongBo(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 17) {
                            thuTucHanhChinhDTO.setNgayBanHanhQd(dataFormatter.formatCellValue(cell).length() > 0 ?
                                    dateFormat.format(sdf.parse(dataFormatter.formatCellValue(cell))) : null);
                            continue;
                        }

                        if (cell.getColumnIndex() == 18) {
                            thuTucHanhChinhDTO.setNgayApDung(dataFormatter.formatCellValue(cell).length() > 0 ?
                                    dateFormat.format(sdf.parse(dataFormatter.formatCellValue(cell))) : null);
                            continue;
                        }

                        if (cell.getColumnIndex() == 19) {
                            thuTucHanhChinhDTO.setPhi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 20) {
                            thuTucHanhChinhDTO.setLePhi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 21) {
                            thuTucHanhChinhDTO.setThoiHanGiaiQuyet(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 22) {
                            thuTucHanhChinhDTO.setCoQuanThucHien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 23) {
                            thuTucHanhChinhDTO.setSoNgayXuLy(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 24) {
                            thuTucHanhChinhDTO.setSoNgayNiemYet(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 25) {
                            thuTucHanhChinhDTO.setKetQuaThucHien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 26) {
                            thuTucHanhChinhDTO.setDoiTuongThucHien(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 27) {
                            thuTucHanhChinhDTO.setMaGiayToKemTheo(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (thuTucHanhChinhValidate.validateThuTucHanhChinh(thuTucHanhChinhDTO, thuTucHanhChinhMapper.thuTucHanhChinhToThuTucHanhChinhDTO(thuTucHanhChinhRepository.findFirstByMaAndDaXoa(thuTucHanhChinhDTO.getMa(), Constants.DAXOA)))) {
                        thuTucHanhChinhDTO.setNguoiTao(nguoiTao);
                        thuTucHanhChinhDTO.setNguoiSua(nguoiTao);
                        if (thuTucHanhChinhDTO.getMa() != null) {
                            thuTucHanhChinhRepository.save(thuTucHanhChinhMapper.thuTucHanhChinhDTOToThuTucHanhChinh(thuTucHanhChinhDTO));
                        }
                    } else {
                        maFail.add(thuTucHanhChinhDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                thuTucHanhChinhMapper.thuTucHanhChinhToThuTucHanhChinhDTO
                        (thuTucHanhChinhRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                thuTucHanhChinhMapper.thuTucHanhChinhToThuTucHanhChinhDTO(thuTucHanhChinhRepository.findFirstById(id)));
    }

    public Response insert(ThuTucHanhChinhDTO thuTucHanhChinhDTO) {
        thuTucHanhChinhRepository.save(thuTucHanhChinhMapper.thuTucHanhChinhDTOToThuTucHanhChinh(thuTucHanhChinhDTO));

        return new Response(Status.SUCCESS.value(), thuTucHanhChinhDTO.getMa()
        );
    }

    public Response update(ThuTucHanhChinhDTO thuTucHanhChinhDTO) throws Exception {
        ThuTucHanhChinh thuTucHanhChinhOrigin = thuTucHanhChinhRepository.findById(thuTucHanhChinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        ThuTucHanhChinh thuTucHanhChinh = ThuTucHanhChinh.builder()
                .id(thuTucHanhChinhDTO.getId())
                .ma(thuTucHanhChinhDTO.getMa() != null ? thuTucHanhChinhDTO.getMa() : thuTucHanhChinhOrigin.getMa())
                .ten(thuTucHanhChinhDTO.getTen() != null ? thuTucHanhChinhDTO.getTen() : thuTucHanhChinhOrigin.getTen())
                .maNiemYet(thuTucHanhChinhDTO.getMaNiemYet() != null ? thuTucHanhChinhDTO.getMaNiemYet() : thuTucHanhChinhOrigin.getMaNiemYet())
                .tenRutGon(thuTucHanhChinhDTO.getTenRutGon() != null ? thuTucHanhChinhDTO.getTenRutGon() : thuTucHanhChinhOrigin.getTenRutGon())
                .maQuyTrinh(thuTucHanhChinhDTO.getMaQuyTrinh() != null ? thuTucHanhChinhDTO.getMaQuyTrinh() : thuTucHanhChinhOrigin.getMaQuyTrinh())
                .nhomThuTucHanhChinh(nhomThuTucHanhChinhRepository.findFirstById(thuTucHanhChinhDTO.getNhomThuTucHanhChinhId()))
                .mucDoDvc(thuTucHanhChinhDTO.getMucDoDvc())
                .trinhTuThucHien(thuTucHanhChinhDTO.getTrinhTuThucHien() != null ? thuTucHanhChinhDTO.getTrinhTuThucHien() : thuTucHanhChinhOrigin.getTrinhTuThucHien())
                .cachThucHien(thuTucHanhChinhDTO.getCachThucHien() != null ? thuTucHanhChinhDTO.getCachThucHien() : thuTucHanhChinhOrigin.getCachThucHien())
                .canCuPhapLy(thuTucHanhChinhDTO.getCanCuPhapLy() != null ? thuTucHanhChinhDTO.getCanCuPhapLy() : thuTucHanhChinhOrigin.getCanCuPhapLy())
                .yeuCauDieuKienThucHien(thuTucHanhChinhDTO.getYeuCauDieuKienThucHien() != null ? thuTucHanhChinhDTO.getYeuCauDieuKienThucHien() : thuTucHanhChinhOrigin.getYeuCauDieuKienThucHien())
                .capCoQuanThucHienId(thuTucHanhChinhDTO.getCapCoQuanThucHienId())
                .thanhPhanHoSo(thuTucHanhChinhDTO.getThanhPhanHoSo() != null ? thuTucHanhChinhDTO.getThanhPhanHoSo() : thuTucHanhChinhOrigin.getThanhPhanHoSo())
                .soBoHoSo(thuTucHanhChinhDTO.getSoBoHoSo())
                .bieuMauDinhKem(thuTucHanhChinhDTO.getBieuMauDinhKem() != null ? thuTucHanhChinhDTO.getBieuMauDinhKem() : thuTucHanhChinhOrigin.getBieuMauDinhKem())
                .trangThai(thuTucHanhChinhDTO.getTrangThai())
                .soQdCongBo(thuTucHanhChinhDTO.getSoQdCongBo() != null ? thuTucHanhChinhDTO.getSoQdCongBo() : thuTucHanhChinhOrigin.getSoQdCongBo())
                .ngayBanHanhQd(thuTucHanhChinhDTO.getNgayBanHanhQd() != null ? sdf.parse(thuTucHanhChinhDTO.getNgayBanHanhQd()) : thuTucHanhChinhOrigin.getNgayBanHanhQd())
                .ngayApDung(thuTucHanhChinhDTO.getNgayApDung() != null ? sdf.parse(thuTucHanhChinhDTO.getNgayApDung()) : thuTucHanhChinhOrigin.getNgayApDung())
                .phi(thuTucHanhChinhDTO.getPhi())
                .lePhi(thuTucHanhChinhDTO.getLePhi())
                .thoiHanGiaiQuyet(thuTucHanhChinhDTO.getThoiHanGiaiQuyet() != null ? thuTucHanhChinhDTO.getThoiHanGiaiQuyet() : thuTucHanhChinhOrigin.getThoiHanGiaiQuyet())
                .coQuanThucHien(thuTucHanhChinhDTO.getCoQuanThucHien() != null ? thuTucHanhChinhDTO.getCoQuanThucHien() : thuTucHanhChinhOrigin.getCoQuanThucHien())
                .soNgayXuLy(thuTucHanhChinhDTO.getSoNgayXuLy())
                .soNgayNiemYet(thuTucHanhChinhDTO.getSoNgayNiemYet())
                .ketQuaThucHien(thuTucHanhChinhDTO.getKetQuaThucHien() != null ? thuTucHanhChinhDTO.getKetQuaThucHien() : thuTucHanhChinhOrigin.getKetQuaThucHien())
                .doiTuongThucHien(thuTucHanhChinhDTO.getDoiTuongThucHien() != null ? thuTucHanhChinhDTO.getDoiTuongThucHien() : thuTucHanhChinhOrigin.getDoiTuongThucHien())
                .maGiayToKemTheo(thuTucHanhChinhDTO.getMaGiayToKemTheo() != null ? thuTucHanhChinhDTO.getMaGiayToKemTheo() : thuTucHanhChinhOrigin.getMaGiayToKemTheo()).build();

        thuTucHanhChinhRepository.save(thuTucHanhChinh);
        return new Response(Status.SUCCESS.value(), thuTucHanhChinhDTO.getMa());
    }

    public Response delete(long id) {
        ThuTucHanhChinh thuTucHanhChinh = thuTucHanhChinhRepository.findById(id).get();
        thuTucHanhChinh.setDaXoa(Constants.XOA);

        thuTucHanhChinhRepository.save(thuTucHanhChinh);

        return new Response(Status.SUCCESS.value(), null);
    }
    //return reponse
    public Response findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page / size, size);

        if (search != null && search.length() > 0) {
            return new Response(Status.SUCCESS.value(), thuTucHanhChinhMapper.toThuTucHanhChinhgDTOList(thuTucHanhChinhRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent()));
        }
        return new Response(Status.SUCCESS.value(),thuTucHanhChinhMapper.toThuTucHanhChinhgDTOList(thuTucHanhChinhRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent()));
    }
    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", thuTucHanhChinhRepository.countByMaContainingAndDaXoaOrTenContainingAndDaXoa(search, Constants.DAXOA, search, Constants.DAXOA).longValue()).toString();
    }

}
