package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.DuAnDauTuRepository;
import sdt.osgi.springboot.dto.DuAnDauTuDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DuAnDauTuMapper;
import sdt.osgi.springboot.model.DuAnDauTu;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DuAnDauTuValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DuAnDauTuService {

    private final DuAnDauTuRepository duAnDauTuRepository;
    private final DuAnDauTuMapper duAnDauTuMapper;
    private final DuAnDauTuValidate duAnDauTuValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<DuAnDauTuDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DuAnDauTuDTO duAnDauTuDTO = new DuAnDauTuDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            duAnDauTuDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            duAnDauTuDTO.setTenDuAn(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            duAnDauTuDTO.setTongMucDauTu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            duAnDauTuDTO.setDiaDiem(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            duAnDauTuDTO.setQuyMoDuAn(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            duAnDauTuDTO.setVatTuThietBi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            duAnDauTuDTO.setDuKienTienDo(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            duAnDauTuDTO.setHoanVon(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            duAnDauTuDTO.setDienTichChiemDung(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            duAnDauTuDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 10) {
                            duAnDauTuDTO.setPhanBoVon(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 11) {
                            duAnDauTuDTO.setUuDai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 12) {
                            duAnDauTuDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 13) {
                            duAnDauTuDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (duAnDauTuValidate.validateDuAnDauTu(duAnDauTuDTO, duAnDauTuMapper.duAnDauTuToDuAnDauTuDTO(duAnDauTuRepository.findFirstByMaAndDaXoa(duAnDauTuDTO.getMa(), Constants.DAXOA)))) {
                        duAnDauTuDTO.setNguoiTao(nguoiTao);
                        duAnDauTuDTO.setNguoiSua(nguoiTao);
                        if (duAnDauTuDTO.getMa() != null) {
                            duAnDauTuRepository.save(duAnDauTuMapper.duAnDauTuDTOToDuAnDauTu(duAnDauTuDTO));
                        }
                    } else {
                        maFail.add(duAnDauTuDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                duAnDauTuMapper.duAnDauTuToDuAnDauTuDTO
                        (duAnDauTuRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                duAnDauTuMapper.duAnDauTuToDuAnDauTuDTO(duAnDauTuRepository.findFirstById(id)));
    }

    public Response insert(DuAnDauTuDTO duAnDauTuDTO) {
        duAnDauTuRepository.save(duAnDauTuMapper.duAnDauTuDTOToDuAnDauTu(duAnDauTuDTO));

        return new Response(Status.SUCCESS.value(), duAnDauTuDTO.getMa());
    }

    public Response update(DuAnDauTuDTO duAnDauTuDTO) {
        DuAnDauTu duAnDauTuOrigin = duAnDauTuRepository.findById(duAnDauTuDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DuAnDauTu duAnDauTu = DuAnDauTu.builder()
                .id(duAnDauTuDTO.getId())
                .ma(duAnDauTuDTO.getMa())
                .tenDuAn(duAnDauTuDTO.getTenDuAn())
                .tongMucDauTu(duAnDauTuDTO.getTongMucDauTu())
                .soDienThoai(duAnDauTuDTO.getSoDienThoai())
                .anh(duAnDauTuDTO.getAnh() != null ? duAnDauTuDTO.getAnh() : duAnDauTuOrigin.getAnh())
                .vatTuThietBi(duAnDauTuDTO.getVatTuThietBi())
                .diaDiem(duAnDauTuDTO.getDiaDiem())
                .quyMoDuAn(duAnDauTuDTO.getQuyMoDuAn())
                .duKienTienDo(duAnDauTuDTO.getDuKienTienDo())
                .hoanVon(duAnDauTuDTO.getHoanVon())
                .dienTichChiemDung(duAnDauTuDTO.getDienTichChiemDung())
                .phanBoVon(duAnDauTuDTO.getPhanBoVon())
                .uuDai(duAnDauTuDTO.getUuDai())
                .url(duAnDauTuDTO.getUrl())
                .diaChiGoogleMap(duAnDauTuDTO.getDiaChiGoogleMap())
                .build();

        duAnDauTuRepository.save(duAnDauTu);
        return new Response(Status.SUCCESS.value(), duAnDauTuDTO.getMa());
    }

    public Response delete(long id) {

        DuAnDauTu duAnDauTu = duAnDauTuRepository.findFirstById(id);
        duAnDauTu.setDaXoa(Constants.XOA);
        duAnDauTuRepository.save(duAnDauTu);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                duAnDauTuMapper.toDuAnDauTuViewDTOList(
                        duAnDauTuRepository.findByDaXoa(Constants.DAXOA)));

    }

}
