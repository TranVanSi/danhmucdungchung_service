package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.KhuNghiDuongRepository;
import sdt.osgi.springboot.dto.KhuNghiDuongDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.KhuNghiDuongMapper;
import sdt.osgi.springboot.model.KhuNghiDuong;
import sdt.osgi.springboot.model.NhaHang;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.KhuNghiDuongValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class KhuNghiDuongService {

    private final KhuNghiDuongRepository khuNghiDuongRepository;
    private final KhuNghiDuongMapper khuNghiDuongMapper;
    private final KhuNghiDuongValidate khuNghiDuongValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<KhuNghiDuongDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                KhuNghiDuongDTO khuNghiDuongDTO = new KhuNghiDuongDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            khuNghiDuongDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            khuNghiDuongDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            khuNghiDuongDTO.setDienTich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            khuNghiDuongDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            khuNghiDuongDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            khuNghiDuongDTO.setLoaiBietThu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khuNghiDuongDTO.setDichVuSpa(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khuNghiDuongDTO.setTrungTamHoiNghi(Integer.valueOf(isCheck));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            khuNghiDuongDTO.setPhongCachThietKe(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 9) {
                            khuNghiDuongDTO.setSoLuongPhong(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 10) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khuNghiDuongDTO.setDichVuVeMayBay(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 11) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khuNghiDuongDTO.setDichVuTaxi(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 12) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khuNghiDuongDTO.setDichVuDuLich(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 13) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            khuNghiDuongDTO.setDichVuNhaHang(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 14) {
                            khuNghiDuongDTO.setGiaiThuong(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 15) {
                            khuNghiDuongDTO.setGiaTrungBinh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 16) {
                            khuNghiDuongDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 17) {
                            khuNghiDuongDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (khuNghiDuongValidate.validateKhuNghiDuong(khuNghiDuongDTO, khuNghiDuongMapper.khuNghiDuongToKhuNghiDuongDTO(khuNghiDuongRepository.findFirstByMaAndDaXoa(khuNghiDuongDTO.getMa(), Constants.DAXOA)))) {
                        khuNghiDuongDTO.setNguoiTao(nguoiTao);
                        khuNghiDuongDTO.setNguoiSua(nguoiTao);
                        if (khuNghiDuongDTO.getMa() != null) {
                            khuNghiDuongRepository.save(khuNghiDuongMapper.khuNghiDuongDTOToKhuNghiDuong(khuNghiDuongDTO));
                        }
                    } else {
                        maFail.add(khuNghiDuongDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                khuNghiDuongMapper.khuNghiDuongToKhuNghiDuongDTO
                        (khuNghiDuongRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                khuNghiDuongMapper.khuNghiDuongToKhuNghiDuongDTO(khuNghiDuongRepository.findFirstById(id)));
    }

    public Response insert(KhuNghiDuongDTO khuNghiDuongDTO) {
        khuNghiDuongRepository.save(khuNghiDuongMapper.khuNghiDuongDTOToKhuNghiDuong(khuNghiDuongDTO));

        return new Response(Status.SUCCESS.value(), khuNghiDuongDTO.getMa());
    }

    public Response update(KhuNghiDuongDTO khuNghiDuongDTO) {
        KhuNghiDuong khuNghiDuongOrigin = khuNghiDuongRepository.findById(khuNghiDuongDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        KhuNghiDuong khuNghiDuong = KhuNghiDuong.builder()
                .id(khuNghiDuongDTO.getId())
                .ma(khuNghiDuongDTO.getMa())
                .ten(khuNghiDuongDTO.getTen())
                .diaChi(khuNghiDuongDTO.getDiaChi())
                .soDienThoai(khuNghiDuongDTO.getSoDienThoai())
                .anh(khuNghiDuongDTO.getAnh() != null ? khuNghiDuongDTO.getAnh() : khuNghiDuongOrigin.getAnh())
                .dienTich(khuNghiDuongDTO.getDienTich())
                .loaiBietThu(khuNghiDuongDTO.getLoaiBietThu())
                .dichVuSpa(khuNghiDuongDTO.getDichVuSpa())
                .trungTamHoiNghi(khuNghiDuongDTO.getTrungTamHoiNghi())
                .phongCachThietKe(khuNghiDuongDTO.getPhongCachThietKe())
                .soLuongPhong(khuNghiDuongDTO.getSoLuongPhong())
                .dichVuVeMayBay(khuNghiDuongDTO.getDichVuVeMayBay())
                .dichVuTaxi(khuNghiDuongDTO.getDichVuTaxi())
                .dichVuDuLich(khuNghiDuongDTO.getDichVuDuLich())
                .dichVuNhaHang(khuNghiDuongDTO.getDichVuNhaHang())
                .giaiThuong(khuNghiDuongDTO.getGiaiThuong())
                .giaTrungBinh(khuNghiDuongDTO.getGiaTrungBinh())
                .url(khuNghiDuongDTO.getUrl())
                .diaChiGoogleMap(khuNghiDuongDTO.getDiaChiGoogleMap())
                .build();

        khuNghiDuongRepository.save(khuNghiDuong);
        return new Response(Status.SUCCESS.value(), khuNghiDuongDTO.getMa());
    }

    public Response delete(long id) {

        KhuNghiDuong khuNghiDuong = khuNghiDuongRepository.findFirstById(id);
        khuNghiDuong.setDaXoa(Constants.XOA);
        khuNghiDuongRepository.save(khuNghiDuong);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<KhuNghiDuongDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return khuNghiDuongMapper.toKhuNghiDuongDTOList(khuNghiDuongRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return khuNghiDuongMapper.toKhuNghiDuongDTOList(khuNghiDuongRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", khuNghiDuongRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                khuNghiDuongMapper.toKhuNghiDuongViewDTOList(
                        khuNghiDuongRepository.findByDaXoa(Constants.DAXOA)));

    }

}
