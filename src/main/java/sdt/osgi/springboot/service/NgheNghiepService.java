package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.NgheNghiepRepository;
import sdt.osgi.springboot.dto.NgheNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.NgheNghiepMapper;
import sdt.osgi.springboot.model.NgheNghiep;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.NgheNghiepValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class NgheNghiepService {

    private final NgheNghiepRepository ngheNghiepRepository;
    private final NgheNghiepMapper ngheNghiepMapper;
    private final NgheNghiepValidate ngheNghiepValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<NgheNghiepDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                NgheNghiepDTO ngheNghiepDTO = new NgheNghiepDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            ngheNghiepDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            ngheNghiepDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            ngheNghiepDTO.setChaMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            NgheNghiepDTO maNgheNghiepDTO = ngheNghiepMapper.ngheNghiepToNgheNghiepDTO(ngheNghiepRepository.findFirstByMaAndDaXoa(ngheNghiepDTO.getChaMa(), Constants.DAXOA));
                            int cap = maNgheNghiepDTO.getCap() + 1;
                            ngheNghiepDTO.setCap((byte) cap);
                            break;
                        }


                    }

                    if (ngheNghiepValidate.validateNgheNghiep(ngheNghiepDTO, ngheNghiepMapper.ngheNghiepToNgheNghiepDTO(ngheNghiepRepository.findFirstByMaAndDaXoa(ngheNghiepDTO.getMa(), Constants.DAXOA)))) {
                        ngheNghiepDTO.setNguoiTao(nguoiTao);
                        ngheNghiepDTO.setNguoiSua(nguoiTao);
                        if (ngheNghiepDTO.getMa() != null) {
                            ngheNghiepRepository.save(ngheNghiepMapper.ngheNghiepDTOToNgheNghiep(ngheNghiepDTO));
                        }
                    } else {
                        maFail.add(ngheNghiepDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                ngheNghiepMapper.ngheNghiepToNgheNghiepDTO
                        (ngheNghiepRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                ngheNghiepMapper.ngheNghiepToNgheNghiepDTO(ngheNghiepRepository.findFirstById(id)));
    }

    public Response findByChaMa(String chaMa) {
        return new Response(
                Status.SUCCESS.value(),
                ngheNghiepMapper.ngheNghiepToNgheNghiepDTO(ngheNghiepRepository.findFirstByChaMaAndDaXoa(chaMa, Constants.DAXOA)));
    }

    public Response findNgheNghiepByChaMa(String chaMa) {
        return new Response(
                Status.SUCCESS.value(),
                ngheNghiepMapper.toNgheNghiepgDTOList(ngheNghiepRepository.findAllByChaMaLikeAndDaXoa(chaMa, Constants.DAXOA)));
    }

    public Response insert(NgheNghiepDTO ngheNghiepDTO) {
        ngheNghiepRepository.save(ngheNghiepMapper.ngheNghiepDTOToNgheNghiep(ngheNghiepDTO));

        return new Response(Status.SUCCESS.value(), ngheNghiepDTO.getMa());
    }

    public Response update(NgheNghiepDTO ngheNghiepDTO) {
        NgheNghiep ngheNghiepOrigin = ngheNghiepRepository.findById(ngheNghiepDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        NgheNghiep ngheNghiep = NgheNghiep.builder()
                .id(ngheNghiepDTO.getId())
                .ma(ngheNghiepDTO.getMa())
                .ten(ngheNghiepDTO.getTen())
                .cap(ngheNghiepDTO.getCap())
                .chaMa(ngheNghiepDTO.getChaMa())
                .build();

        ngheNghiepRepository.save(ngheNghiep);
        return new Response(Status.SUCCESS.value(), ngheNghiepDTO.getMa());
    }

    public Response delete(long id) {
        NgheNghiep ngheNghiep = ngheNghiepRepository.findFirstById(id);
        ngheNghiep.setDaXoa(Constants.XOA);
        ngheNghiepRepository.save(ngheNghiep);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                ngheNghiepMapper.toNgheNghiepViewDTOList(
                        ngheNghiepRepository.findByDaXoa(Constants.DAXOA)));

    }

}
