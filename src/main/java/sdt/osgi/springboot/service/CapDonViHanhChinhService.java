package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.CapDonViHanhChinhsRepository;
import sdt.osgi.springboot.dto.CapDonViHanhChinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.CapDonViHanhChinhMapper;
import sdt.osgi.springboot.model.CapDonViHanhChinh;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.CapDonViHanhChinhValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class CapDonViHanhChinhService {

    private final CapDonViHanhChinhsRepository capDonViHanhChinhRepository;
    private final CapDonViHanhChinhMapper capDonViHanhChinhMapper;
    private final CapDonViHanhChinhValidate capDonViHanhChinhValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO saveAllCapDonViHanhChinh(MultipartFile file, String nguoiTao) throws Exception {
        List<CapDonViHanhChinhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                CapDonViHanhChinhDTO capDonViHanhChinhDTO = new CapDonViHanhChinhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            capDonViHanhChinhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            capDonViHanhChinhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            capDonViHanhChinhDTO.setMoTa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            capDonViHanhChinhDTO.setCap(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }

                    CapDonViHanhChinhDTO itemByMa = capDonViHanhChinhMapper.toCapDonViHanhChinhDTO(capDonViHanhChinhRepository.findFirstByMaAndDaXoa(capDonViHanhChinhDTO.getMa(), Constants.DAXOA));
                    if (capDonViHanhChinhValidate.validateCapDonViHanhChinh(capDonViHanhChinhDTO, itemByMa)) {
                        capDonViHanhChinhDTO.setNguoiTao(nguoiTao);
                        capDonViHanhChinhDTO.setNguoiSua(nguoiTao);
                        if (capDonViHanhChinhDTO.getMa() != null) {
                            capDonViHanhChinhRepository.save(capDonViHanhChinhMapper.toCapDonViHanhChinh(capDonViHanhChinhDTO));
                        }
                    } else {
                        maFail.add(capDonViHanhChinhDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                capDonViHanhChinhMapper.toCapDonViHanhChinhDTO
                        (capDonViHanhChinhRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                capDonViHanhChinhMapper.toCapDonViHanhChinhDTO(capDonViHanhChinhRepository.findFirstById(id)));
    }

    public Response insert(CapDonViHanhChinhDTO capDonViHanhChinhDTO) {
        capDonViHanhChinhRepository.save(capDonViHanhChinhMapper.toCapDonViHanhChinh(capDonViHanhChinhDTO));

        return new Response(Status.SUCCESS.value(), capDonViHanhChinhDTO.getMa());
    }

    public Response update(CapDonViHanhChinhDTO capDonViHanhChinhDTO) {
        CapDonViHanhChinh capDonViHanhChinhOrigin = capDonViHanhChinhRepository.findById(capDonViHanhChinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        CapDonViHanhChinh capDonViHanhChinh = CapDonViHanhChinh.builder()
                .id(capDonViHanhChinhDTO.getId())
                .ma(capDonViHanhChinhDTO.getMa())
                .ten(capDonViHanhChinhDTO.getTen())
                .moTa(capDonViHanhChinhDTO.getMoTa())
                .cap(capDonViHanhChinhDTO.getCap())
                .build();

        capDonViHanhChinhRepository.save(capDonViHanhChinh);
        return new Response(Status.SUCCESS.value(), capDonViHanhChinhDTO.getMa());
    }

    public Response delete(long id) {
        CapDonViHanhChinh capDonViHanhChinh = capDonViHanhChinhRepository.findFirstById(id);
        capDonViHanhChinh.setDaXoa(Constants.XOA);
        capDonViHanhChinhRepository.save(capDonViHanhChinh);
        return new Response(Status.SUCCESS.value(), null);
    }

    public List<CapDonViHanhChinhDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return capDonViHanhChinhMapper.toCapDonViHanhChinhDTOList(capDonViHanhChinhRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return capDonViHanhChinhMapper.toCapDonViHanhChinhDTOList(capDonViHanhChinhRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", capDonViHanhChinhRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                capDonViHanhChinhMapper.toCapDonViHanhChinhViewDTOList(
                        capDonViHanhChinhRepository.findByDaXoa(Constants.DAXOA)));

    }
}
