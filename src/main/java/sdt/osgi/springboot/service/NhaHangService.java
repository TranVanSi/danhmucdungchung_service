package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.NhaHangRepository;
import sdt.osgi.springboot.dto.NhaHangDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.NhaHangMapper;
import sdt.osgi.springboot.model.NhaHang;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.NhaHangValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class NhaHangService {

    private final NhaHangRepository nhaHangRepository;
    private final NhaHangMapper nhaHangMapper;
    private final NhaHangValidate nhaHangValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<NhaHangDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                NhaHangDTO nhaHangDTO = new NhaHangDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            nhaHangDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            nhaHangDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            nhaHangDTO.setDienTich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            nhaHangDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            nhaHangDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            nhaHangDTO.setDacSan(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            nhaHangDTO.setSoLuongBep(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            nhaHangDTO.setSoLuongPhucVu(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            nhaHangDTO.setSoLuongBan(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 9) {
                            nhaHangDTO.setSoLuongGhe(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 10) {
                            nhaHangDTO.setDichVuPhongRieng(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 11) {
                            nhaHangDTO.setSucChuaPhongRieng(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 12) {
                            int isCheck = 0;
                            if (String.valueOf(dataFormatter.formatCellValue(cell)).trim().equals("Có")) {
                                isCheck = 1;
                            }
                            nhaHangDTO.setDichVuTaxi(Integer.valueOf(isCheck));
                            continue;

                        }
                        if (cell.getColumnIndex() == 13) {
                            nhaHangDTO.setPhongCachNhaHang(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 14) {
                            nhaHangDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }
                        if (cell.getColumnIndex() == 15) {
                            nhaHangDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (nhaHangValidate.validateNhaHang(nhaHangDTO, nhaHangMapper.nhaHangToNhaHangDTO(nhaHangRepository.findFirstByMaAndDaXoa(nhaHangDTO.getMa(), Constants.DAXOA)))) {
                        nhaHangDTO.setNguoiTao(nguoiTao);
                        nhaHangDTO.setNguoiSua(nguoiTao);
                        if (nhaHangDTO.getMa() != null) {
                            nhaHangRepository.save(nhaHangMapper.nhaHangDTOToNhaHang(nhaHangDTO));
                        }
                    } else {
                        maFail.add(nhaHangDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                nhaHangMapper.nhaHangToNhaHangDTO
                        (nhaHangRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                nhaHangMapper.nhaHangToNhaHangDTO(nhaHangRepository.findFirstById(id)));
    }

    public Response insert(NhaHangDTO nhaHangDTO) {
        nhaHangRepository.save(nhaHangMapper.nhaHangDTOToNhaHang(nhaHangDTO));

        return new Response(Status.SUCCESS.value(), nhaHangDTO.getMa()
        );
    }

    public Response update(NhaHangDTO nhaHangDTO) {
        NhaHang nhaHangOrigin = nhaHangRepository.findById(nhaHangDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        NhaHang nhaHang = NhaHang.builder()
                .id(nhaHangDTO.getId())
                .ma(nhaHangDTO.getMa())
                .ten(nhaHangDTO.getTen())
                .diaChi(nhaHangDTO.getDiaChi())
                .soDienThoai(nhaHangDTO.getSoDienThoai())
                .anh(nhaHangDTO.getAnh() != null ? nhaHangDTO.getAnh() : nhaHangOrigin.getAnh())
                .dienTich(nhaHangDTO.getDienTich())
                .dacSan(nhaHangDTO.getDacSan())
                .soLuongBep(nhaHangDTO.getSoLuongBep())
                .soLuongPhucVu(nhaHangDTO.getSoLuongPhucVu())
                .soLuongBan(nhaHangDTO.getSoLuongBan())
                .soLuongGhe(nhaHangDTO.getSoLuongGhe())
                .dichVuPhongRieng(nhaHangDTO.getDichVuPhongRieng())
                .sucChuaPhongRieng(nhaHangDTO.getSucChuaPhongRieng())
                .dichVuTaxi(nhaHangDTO.getDichVuTaxi())
                .phongCachNhaHang(nhaHangDTO.getPhongCachNhaHang())
                .url(nhaHangDTO.getUrl())
                .diaChiGoogleMap(nhaHangDTO.getDiaChiGoogleMap())
                .build();

        nhaHangRepository.save(nhaHang);
        return new Response(Status.SUCCESS.value(), nhaHangDTO.getMa());
    }

    public Response delete(long id) {

        NhaHang nhaHang = nhaHangRepository.findFirstById(id);
        nhaHang.setDaXoa(Constants.XOA);
        nhaHangRepository.save(nhaHang);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<NhaHangDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return nhaHangMapper.toNhaHangDTOList(nhaHangRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return nhaHangMapper.toNhaHangDTOList(nhaHangRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", nhaHangRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                nhaHangMapper.toNhaHangViewDTOList(
                        nhaHangRepository.findByDaXoa(Constants.DAXOA)));

    }

}
