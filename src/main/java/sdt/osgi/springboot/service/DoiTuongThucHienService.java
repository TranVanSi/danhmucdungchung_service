package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DoiTuongThucHienRepository;
import sdt.osgi.springboot.dto.DoiTuongThucHienDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.DoiTuongThucHienMapper;
import sdt.osgi.springboot.model.DoiTuongThucHien;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;

import java.util.List;

import static sdt.osgi.springboot.util.Constants.DAXOA;
import static sdt.osgi.springboot.util.Constants.XOA;


@Service
@RequiredArgsConstructor
public class DoiTuongThucHienService {

    private final DoiTuongThucHienRepository doiTuongThucHienRepository;
    private final DoiTuongThucHienMapper doiTuongThucHienMapper;

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                doiTuongThucHienMapper.doiTuongThucHienToDoiTuongThucHienDTO
                        (doiTuongThucHienRepository.findFirstByMaAndDaXoa(ma, DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                doiTuongThucHienMapper.doiTuongThucHienToDoiTuongThucHienDTO(doiTuongThucHienRepository.findFirstById(id)));
    }

    public Response insert(DoiTuongThucHienDTO doiTuongThucHienDTO) {
        doiTuongThucHienRepository.save(doiTuongThucHienMapper.doiTuongThucHienDTOToDoiTuongThucHien(doiTuongThucHienDTO));

        return new Response(Status.SUCCESS.value(), doiTuongThucHienDTO.getMa());
    }

    public Response update(DoiTuongThucHienDTO doiTuongThucHienDTO) {
        DoiTuongThucHien doiTuongThucHien = DoiTuongThucHien.builder()
                .id(doiTuongThucHienDTO.getId())
                .ma(doiTuongThucHienDTO.getMa())
                .ten(doiTuongThucHienDTO.getTen())
                .build();

        doiTuongThucHienRepository.save(doiTuongThucHien);
        return new Response(Status.SUCCESS.value(), doiTuongThucHienDTO.getMa());
    }

    public Response delete(long id ) {

        DoiTuongThucHien doiTuongThucHien = doiTuongThucHienRepository.findFirstById(id);
        doiTuongThucHien.setDaXoa(XOA);
        doiTuongThucHienRepository.save(doiTuongThucHien);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<DoiTuongThucHienDTO> findAll(String search, int page, int size) {

        Pageable pageable =  PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return doiTuongThucHienMapper.toDoiTuongThucHienDTOList(doiTuongThucHienRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, DAXOA).getContent());
        }

        return doiTuongThucHienMapper.toDoiTuongThucHienDTOList(doiTuongThucHienRepository.findAllByDaXoa(pageable, DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", doiTuongThucHienRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                doiTuongThucHienMapper.toDoiTuongThucHienViewDTOList(
                doiTuongThucHienRepository.findByDaXoa(Constants.DAXOA)));

    }

}
