package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.NganhNgheKinhDoanhRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.mapper.NganhNgheKinhDoanhMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.viewdto.NganhNgheKinhDoanhViewDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class NganhNgheKinhDoanhService {

    private final NganhNgheKinhDoanhRepository nganhNgheKinhDoanhRepository;
    private final NganhNgheKinhDoanhMapper nganhNgheKinhDoanhMapper;

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),nganhNgheKinhDoanhRepository.findFirstById(id));
    }

    public Response findByCap(byte cap) {
        return new Response(
                Status.SUCCESS.value(),
                nganhNgheKinhDoanhRepository.findAllByCapAndDaXoa(cap, Constants.DAXOA));
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),nganhNgheKinhDoanhMapper.NGANH_NGHE_KINH_DOANH_VIEW_DTOS(
                nganhNgheKinhDoanhRepository.findByDaXoa(Constants.DAXOA)));
    }

    public List<NganhNgheKinhDoanhViewDTO> findByNganhNgheKinhDoanhIn(List<Long> ids) {
        return nganhNgheKinhDoanhMapper.NGANH_NGHE_KINH_DOANH_VIEW_DTOS(nganhNgheKinhDoanhRepository.findByIdIn(ids)) ;
    }
}
