package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.NhomThuTucHanhChinhRepository;
import sdt.osgi.springboot.dto.NhomThuTucHanhChinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.NhomThuTucHanhChinhMapper;
import sdt.osgi.springboot.model.NhomThuTucHanhChinh;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.NhomThuTucHanhChinhValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class NhomThuTucHanhChinhService {

    private final NhomThuTucHanhChinhRepository nhomThuTucHanhChinhRepository;
    private final NhomThuTucHanhChinhMapper nhomThuTucHanhChinhMapper;
    private final NhomThuTucHanhChinhValidate nhomThuTucHanhChinhValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<NhomThuTucHanhChinhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                NhomThuTucHanhChinhDTO nhomThuTucHanhChinhDTO = new NhomThuTucHanhChinhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            nhomThuTucHanhChinhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            nhomThuTucHanhChinhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            nhomThuTucHanhChinhDTO.setChaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            nhomThuTucHanhChinhDTO.setCapCQQLId(Byte.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            nhomThuTucHanhChinhDTO.setLoaiNhomTTHC(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (nhomThuTucHanhChinhValidate.validateNhomThuTucHanhChinh(nhomThuTucHanhChinhDTO, nhomThuTucHanhChinhMapper.nhomThuTucHanhChinhToNhomThuTucHanhChinhDTO(nhomThuTucHanhChinhRepository.findFirstByMaAndDaXoa(nhomThuTucHanhChinhDTO.getMa(), Constants.DAXOA)))) {
                        nhomThuTucHanhChinhDTO.setNguoiTao(nguoiTao);
                        nhomThuTucHanhChinhDTO.setNguoiSua(nguoiTao);
                        if (nhomThuTucHanhChinhDTO.getMa() != null) {
                            nhomThuTucHanhChinhRepository.save(nhomThuTucHanhChinhMapper.nhomThuTucHanhChinhDTOToNhomThuTucHanhChinh(nhomThuTucHanhChinhDTO));
                        }
                    } else {
                        maFail.add(nhomThuTucHanhChinhDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                nhomThuTucHanhChinhMapper.nhomThuTucHanhChinhToNhomThuTucHanhChinhDTO
                        (nhomThuTucHanhChinhRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                nhomThuTucHanhChinhMapper.nhomThuTucHanhChinhToNhomThuTucHanhChinhDTO(nhomThuTucHanhChinhRepository.findFirstById(id)));
    }

    public Response findByLoaiNhom(int loai) {
        return new Response(
                Status.SUCCESS.value(),
                nhomThuTucHanhChinhMapper.toNhomThuTucHanhChinhgDTOList(nhomThuTucHanhChinhRepository.findByLoaiNhomTTHCAndDaXoa(loai, Constants.DAXOA)));
    }

    public Response findByChaId(long chaId) {
        return new Response(
                Status.SUCCESS.value(),
                nhomThuTucHanhChinhMapper.toNhomThuTucHanhChinhgDTOList(nhomThuTucHanhChinhRepository.findByChaIdAndDaXoa(chaId, Constants.DAXOA)));
    }

    public Response findByChaMa(String chaMa) {
        return new Response(
                Status.SUCCESS.value(),
                nhomThuTucHanhChinhMapper.toNhomThuTucHanhChinhgDTOList(nhomThuTucHanhChinhRepository.findByChaMaAndDaXoa(chaMa, Constants.DAXOA)));
    }

    public Response insert(NhomThuTucHanhChinhDTO nhomThuTucHanhChinhDTO) {
        nhomThuTucHanhChinhRepository.save(nhomThuTucHanhChinhMapper.nhomThuTucHanhChinhDTOToNhomThuTucHanhChinh(nhomThuTucHanhChinhDTO));

        return new Response(Status.SUCCESS.value(), nhomThuTucHanhChinhDTO.getMa());
    }

    public Response update(NhomThuTucHanhChinhDTO nhomThuTucHanhChinhDTO) {
        NhomThuTucHanhChinh nhomThuTucHanhChinhOrigin = nhomThuTucHanhChinhRepository.findById(nhomThuTucHanhChinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        NhomThuTucHanhChinh nhomThuTucHanhChinh = NhomThuTucHanhChinh.builder()
                .id(nhomThuTucHanhChinhDTO.getId())
                .ma(nhomThuTucHanhChinhDTO.getMa())
                .ten(nhomThuTucHanhChinhDTO.getTen())
                .chaId(nhomThuTucHanhChinhDTO.getChaId())
                .capCQQLId(nhomThuTucHanhChinhDTO.getCapCQQLId())
                .loaiNhomTTHC(nhomThuTucHanhChinhDTO.getLoaiNhomTTHC())
                .chaMa(nhomThuTucHanhChinhDTO.getChaMa())
                .build();

        nhomThuTucHanhChinhRepository.save(nhomThuTucHanhChinh);
        return new Response(Status.SUCCESS.value(), nhomThuTucHanhChinhDTO.getMa());
    }

    public Response delete(long id) {
        NhomThuTucHanhChinh nhomThuTucHanhChinh = nhomThuTucHanhChinhRepository.findById(id).get();
        nhomThuTucHanhChinh.setDaXoa(Constants.XOA);
        nhomThuTucHanhChinhRepository.save(nhomThuTucHanhChinh);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<NhomThuTucHanhChinhDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return nhomThuTucHanhChinhMapper.toNhomThuTucHanhChinhgDTOList(nhomThuTucHanhChinhRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return nhomThuTucHanhChinhMapper.toNhomThuTucHanhChinhgDTOList(nhomThuTucHanhChinhRepository.findByDaXoa(Constants.DAXOA));
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", nhomThuTucHanhChinhRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                nhomThuTucHanhChinhMapper.toNhomThuTucHanhChinhViewDTOList(
                        nhomThuTucHanhChinhRepository.findByDaXoa(Constants.DAXOA)));

    }

}
