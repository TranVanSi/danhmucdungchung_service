package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.controller.UploadValidate;
import sdt.osgi.springboot.dao.DanTocsRepository;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DanTocMapper;
import sdt.osgi.springboot.util.Constants;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Component
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class UploadFileService {
    private final DanTocService danTocService;
    private final UploadValidate uploadValidate;
    private final DanTocsRepository danTocsRepository;
    private final DanTocMapper danTocMapper;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;


    public ResponseDTO saveAllDanToc(MultipartFile file, String nguoiTao) throws Exception {

        List<DanTocDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DanTocDTO danTocDTO = new DanTocDTO();
                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            danTocDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            danTocDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            danTocDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 3) {
                            danTocDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            danTocDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }

                    DanTocDTO itemByMa = danTocMapper.danTocToDanTocDTO(danTocsRepository.findFirstByMaAndDaXoa(danTocDTO.getMa(), Constants.DAXOA));
                    if (uploadValidate.validate(danTocDTO, itemByMa)) {
                        danTocDTO.setNguoiTao(nguoiTao);
                        danTocDTO.setNguoiSua(nguoiTao);
                        if (danTocDTO.getMa() != null) {
                            danTocService.insert(danTocDTO);
                        }
                    } else {
                        maFail.add(danTocDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }


}
