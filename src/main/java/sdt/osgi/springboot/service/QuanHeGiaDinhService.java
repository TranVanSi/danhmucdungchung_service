package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.QuanHeGiaDinhRepository;
import sdt.osgi.springboot.dto.QuanHeGiaDinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.QuanHeGiaDinhMapper;
import sdt.osgi.springboot.model.QuanHeGiaDinh;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.QuanHeGiaDinhValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class QuanHeGiaDinhService {
    private final QuanHeGiaDinhMapper quanHeGiaDinhMapper;
    private final QuanHeGiaDinhRepository quanHeGiaDinhRepository;
    private final QuanHeGiaDinhValidate quanHeGiaDinhValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<QuanHeGiaDinhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                QuanHeGiaDinhDTO quanHeGiaDinhDTO = new QuanHeGiaDinhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            quanHeGiaDinhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            quanHeGiaDinhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (quanHeGiaDinhValidate.validateQuanHeGiaDinh(quanHeGiaDinhDTO, quanHeGiaDinhMapper.quanHeGiaDinhToQuanHeGiaDinhDTO(quanHeGiaDinhRepository.findFirstByMaAndDaXoa(quanHeGiaDinhDTO.getMa(), Constants.DAXOA)))) {
                        quanHeGiaDinhDTO.setNguoiTao(nguoiTao);
                        quanHeGiaDinhDTO.setNguoiSua(nguoiTao);
                        if (quanHeGiaDinhDTO.getMa() != null) {
                            quanHeGiaDinhRepository.save(quanHeGiaDinhMapper.quanHeGiaDinhDTOToQuanHeGiaDinh(quanHeGiaDinhDTO));
                        }
                    } else {
                        maFail.add(quanHeGiaDinhDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                quanHeGiaDinhMapper.quanHeGiaDinhToQuanHeGiaDinhDTO
                        (quanHeGiaDinhRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                quanHeGiaDinhMapper.quanHeGiaDinhToQuanHeGiaDinhDTO(quanHeGiaDinhRepository.findFirstById(id)));
    }

    public Response insert(QuanHeGiaDinhDTO quanHeGiaDinhDTO) {
        quanHeGiaDinhRepository.save(quanHeGiaDinhMapper.quanHeGiaDinhDTOToQuanHeGiaDinh(quanHeGiaDinhDTO));

        return new Response(Status.SUCCESS.value(), quanHeGiaDinhDTO.getMa());
    }

    public Response update(QuanHeGiaDinhDTO quanHeGiaDinhDTO) {
        QuanHeGiaDinh quanHeGiaDinhOrigin = quanHeGiaDinhRepository.findById(quanHeGiaDinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        QuanHeGiaDinh quanHeGiaDinh = QuanHeGiaDinh.builder()
                .id(quanHeGiaDinhDTO.getId())
                .ma(quanHeGiaDinhDTO.getMa())
                .ten(quanHeGiaDinhDTO.getTen())
                .build();

        quanHeGiaDinhRepository.save(quanHeGiaDinh);
        return new Response(Status.SUCCESS.value(), quanHeGiaDinhDTO.getMa());
    }

    public Response delete(long id) {
        QuanHeGiaDinh quanHeGiaDinh = quanHeGiaDinhRepository.findFirstById(id);
        quanHeGiaDinh.setDaXoa(Constants.XOA);
        quanHeGiaDinhRepository.save(quanHeGiaDinh);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                quanHeGiaDinhMapper.toQuanHeGiaDinhViewDTOList(
                        quanHeGiaDinhRepository.findByDaXoa(Constants.DAXOA)));

    }
}
