package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.QuocTichRepository;
import sdt.osgi.springboot.dto.QuocTichDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.QuocTichMapper;
import sdt.osgi.springboot.model.QuocTich;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.QuocTichValidate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class QuocTichService {
    private final QuocTichRepository quocTichRepository;
    private final QuocTichMapper quocTichMapper;
    private final QuocTichValidate quocTichValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<QuocTichDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                QuocTichDTO quocTichDTO = new QuocTichDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            quocTichDTO.setMaQuocGia(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            quocTichDTO.setTenQuocGiaDayDuBangTiengViet(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            quocTichDTO.setMaAlpha3(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            quocTichDTO.setTenQuocGiaDayDuBangTiengAnh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            quocTichDTO.setTenQuocGiaVietGonBangTiengViet(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 5) {
                            quocTichDTO.setTenQuocGiaVietGonBangTiengAnh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 6) {
                            quocTichDTO.setCacNgonNguHanhChinhAlpha2(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 7) {
                            quocTichDTO.setCacNgonNguHanhChinhAlpha3(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 8) {
                            quocTichDTO.setCacTenDiaPhuongVietGon(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 9) {
                            quocTichDTO.setNuocDocLap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 10) {
                            quocTichDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 11) {
                            quocTichDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 12) {
                            quocTichDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }


                    }

                    if (quocTichValidate.validateQuocTich(quocTichDTO, quocTichMapper.quocTichToQuocTichDTO(quocTichRepository.findFirstByMaQuocGiaAndDaXoa(quocTichDTO.getMaQuocGia(), Constants.DAXOA)))) {
                        quocTichDTO.setNguoiTao(nguoiTao);
                        quocTichDTO.setNguoiSua(nguoiTao);
                        if (quocTichDTO.getMaQuocGia() != null) {
                            quocTichRepository.save(quocTichMapper.quocTichDTOToQuocTich(quocTichDTO));
                        }

                    } else {
                        maFail.add(quocTichDTO.getMaQuocGia());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                quocTichMapper.quocTichToQuocTichDTO
                        (quocTichRepository.findFirstByMaQuocGiaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                quocTichMapper.quocTichToQuocTichDTO(quocTichRepository.findFirstById(id)));
    }

    public Response insert(QuocTichDTO quocTichDTO) {
        quocTichRepository.save(quocTichMapper.quocTichDTOToQuocTich(quocTichDTO));

        return new Response(Status.SUCCESS.value(), quocTichDTO.getMaQuocGia());
    }

    public Response update(QuocTichDTO quocTichDTO) {
        QuocTich quocTichOrigin = quocTichRepository.findById(quocTichDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        QuocTich quocTich = QuocTich.builder()
                .id(quocTichDTO.getId())
                .maQuocGia(quocTichDTO.getMaQuocGia())
                .tenQuocGiaDayDuBangTiengViet(quocTichDTO.getTenQuocGiaDayDuBangTiengViet())
                .maAlpha3(quocTichDTO.getMaAlpha3())
                .tenQuocGiaDayDuBangTiengAnh(quocTichDTO.getTenQuocGiaDayDuBangTiengAnh())
                .tenQuocGiaVietGonBangTiengViet(quocTichDTO.getTenQuocGiaVietGonBangTiengViet())
                .tenQuocGiaVietGonBangTiengAnh(quocTichDTO.getTenQuocGiaVietGonBangTiengAnh())
                .cacNgonNguHanhChinhAlpha2(quocTichDTO.getCacNgonNguHanhChinhAlpha2())
                .cacNgonNguHanhChinhAlpha3(quocTichDTO.getCacNgonNguHanhChinhAlpha3())
                .cacTenDiaPhuongVietGon(quocTichDTO.getCacTenDiaPhuongVietGon())
                .nuocDocLap(quocTichDTO.getNuocDocLap())
                .qdBanHanhSuaDoi(quocTichDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(quocTichDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(quocTichDTO.getQdCoQuanBanHanh())
                .build();

        quocTichRepository.save(quocTich);
        return new Response(Status.SUCCESS.value(), quocTichDTO.getMaQuocGia());
    }

    public Response delete(long id) {
        QuocTich quocTich = quocTichRepository.findById(id).get();
        quocTich.setDaXoa(Constants.XOA);
        quocTichRepository.save(quocTich);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                quocTichMapper.toQuocTichViewDTOList(
                        quocTichRepository.findByDaXoa(Constants.DAXOA)));

    }
}
