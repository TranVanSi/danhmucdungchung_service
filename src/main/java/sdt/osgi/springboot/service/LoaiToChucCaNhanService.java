package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.LoaiToChucCaNhanRepository;
import sdt.osgi.springboot.dto.LoaiToChucCaNhanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.LoaiToChucCaNhanMapper;
import sdt.osgi.springboot.model.LoaiToChucCaNhan;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.LoaiToChucCaNhanValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class LoaiToChucCaNhanService {
    private final LoaiToChucCaNhanMapper loaiToChucCaNhanMapper;
    private final LoaiToChucCaNhanRepository loaiToChucCaNhanRepository;
    private final LoaiToChucCaNhanValidate loaiToChucCaNhanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<LoaiToChucCaNhanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                LoaiToChucCaNhanDTO loaiToChucCaNhanDTO = new LoaiToChucCaNhanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            loaiToChucCaNhanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            loaiToChucCaNhanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (loaiToChucCaNhanValidate.validateLoaiToChucCaNhan(loaiToChucCaNhanDTO, loaiToChucCaNhanMapper.loaiToChucCaNhanToLoaiToChucCaNhanDTO(loaiToChucCaNhanRepository.findFirstByMaAndDaXoa(loaiToChucCaNhanDTO.getMa(), Constants.DAXOA)))) {
                        loaiToChucCaNhanDTO.setNguoiTao(nguoiTao);
                        loaiToChucCaNhanDTO.setNguoiSua(nguoiTao);
                        if (loaiToChucCaNhanDTO.getMa() != null) {
                            loaiToChucCaNhanRepository.save(loaiToChucCaNhanMapper.loaiToChucCaNhanDTOToLoaiToChucCaNhan(loaiToChucCaNhanDTO));
                        }
                    } else {
                        maFail.add(loaiToChucCaNhanDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                loaiToChucCaNhanMapper.loaiToChucCaNhanToLoaiToChucCaNhanDTO
                        (loaiToChucCaNhanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                loaiToChucCaNhanMapper.loaiToChucCaNhanToLoaiToChucCaNhanDTO(loaiToChucCaNhanRepository.findFirstById(id)));
    }

    public Response insert(LoaiToChucCaNhanDTO loaiToChucCaNhanDTO) {
        loaiToChucCaNhanRepository.save(loaiToChucCaNhanMapper.loaiToChucCaNhanDTOToLoaiToChucCaNhan(loaiToChucCaNhanDTO));

        return new Response(Status.SUCCESS.value(), loaiToChucCaNhanDTO.getMa());
    }

    public Response update(LoaiToChucCaNhanDTO loaiToChucCaNhanDTO) {
        LoaiToChucCaNhan loaiToChucCaNhanOrigin = loaiToChucCaNhanRepository.findById(loaiToChucCaNhanDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        LoaiToChucCaNhan loaiToChucCaNhan = LoaiToChucCaNhan.builder()
                .id(loaiToChucCaNhanDTO.getId())
                .ma(loaiToChucCaNhanDTO.getMa())
                .ten(loaiToChucCaNhanDTO.getTen())
                .build();

        loaiToChucCaNhanRepository.save(loaiToChucCaNhan);
        return new Response(Status.SUCCESS.value(), loaiToChucCaNhanDTO.getMa());
    }

    public Response delete(long id) {
        LoaiToChucCaNhan loaiToChucCaNhan = loaiToChucCaNhanRepository.findFirstById(id);
        loaiToChucCaNhan.setDaXoa(Constants.XOA);
        loaiToChucCaNhanRepository.save(loaiToChucCaNhan);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                loaiToChucCaNhanMapper.toLoaiToChucCaNhanViewDTOList(
                        loaiToChucCaNhanRepository.findByDaXoa(Constants.DAXOA)));

    }
}
