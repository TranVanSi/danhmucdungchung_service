package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.CapDonViHanhChinhsRepository;
import sdt.osgi.springboot.dao.DonViHanhChinhsRepository;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.DonViHanhChinhMapper;
import sdt.osgi.springboot.model.DonViHanhChinh;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.DonViHanhChinhValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class DonViHanhChinhService {

    private final DonViHanhChinhsRepository donViHanhChinhsRepository;
    private final DonViHanhChinhMapper donViHanhChinhMapper;
    private final DonViHanhChinhValidate donViHanhChinhValidate;
    private final CapDonViHanhChinhsRepository capDonViHanhChinhsRepository;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO saveAllDonViHanhChinh(MultipartFile file, String nguoiTao) throws Exception {
        List<DonViHanhChinhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                DonViHanhChinhDTO donViHanhChinhDTO = new DonViHanhChinhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            donViHanhChinhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            donViHanhChinhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            donViHanhChinhDTO.setChaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            donViHanhChinhDTO.setCapDonViHanhChinhId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            donViHanhChinhDTO.setMaBuuCuc(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }
                    DonViHanhChinhDTO itemByMa = donViHanhChinhMapper.toDonViHanhChinhDTO(donViHanhChinhsRepository.findFirstByMaAndDaXoa(donViHanhChinhDTO.getMa(), Constants.DAXOA));
                    if (donViHanhChinhValidate.validateDonViHanhChinh(donViHanhChinhDTO, itemByMa)) {
                        donViHanhChinhDTO.setNguoiTao(nguoiTao);
                        donViHanhChinhDTO.setNguoiSua(nguoiTao);
                        if (donViHanhChinhDTO.getMa() != null) {
                            donViHanhChinhsRepository.save(donViHanhChinhMapper.toDonViHanhChinh(donViHanhChinhDTO));
                        }
                    } else {
                        maFail.add(donViHanhChinhDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                donViHanhChinhMapper.toDonViHanhChinhDTO
                        (donViHanhChinhsRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                donViHanhChinhMapper.toDonViHanhChinhDTO(donViHanhChinhsRepository.findFirstById(id)));
    }

    public Response findByChaId(long chaId) {
        return new Response(
                Status.SUCCESS.value(),
                donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findByChaIdAndDaXoa(chaId, Constants.DAXOA)));

    }

    public Response findByCapCoQuanId(long capId) {
        return new Response(
                Status.SUCCESS.value(),
                donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findByCapDonViHanhChinh_IdAndDaXoa(capId, Constants.DAXOA)));

    }

    public Response insert(DonViHanhChinhDTO donViHanhChinhDTO) {
        donViHanhChinhsRepository.save(donViHanhChinhMapper.toDonViHanhChinh(donViHanhChinhDTO));

        return new Response(Status.SUCCESS.value(), donViHanhChinhDTO.getMa());
    }

    public Response update(DonViHanhChinhDTO donViHanhChinhDTO) {
        DonViHanhChinh donViHanhChinhOrigin = donViHanhChinhsRepository.findById(donViHanhChinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        DonViHanhChinh donViHanhChinh = DonViHanhChinh.builder()
                .id(donViHanhChinhDTO.getId())
                .ma(donViHanhChinhDTO.getMa())
                .ten(donViHanhChinhDTO.getTen())
                .maBuuCuc(donViHanhChinhDTO.getMaBuuCuc())
                .chaId(donViHanhChinhDTO.getChaId())
                .capDonViHanhChinh(donViHanhChinhDTO.getCapDonViHanhChinhId() > 0 ? capDonViHanhChinhsRepository.findFirstById(donViHanhChinhDTO.getCapDonViHanhChinhId()) : donViHanhChinhOrigin.getCapDonViHanhChinh())
                .build();

        donViHanhChinhsRepository.save(donViHanhChinh);
        return new Response(Status.SUCCESS.value(), donViHanhChinhDTO.getMa());
    }

    public Response delete(long id) {
        DonViHanhChinh donViHanhChinh = donViHanhChinhsRepository.findFirstById(id);
        donViHanhChinh.setDaXoa(Constants.XOA);
        donViHanhChinhsRepository.save(donViHanhChinh);
        return new Response(Status.SUCCESS.value(), null);
    }
    //return reponse
    public Response findAll(String search, long chaId, int page, int size) {
        if (page == -1 && size == -1){
            return new Response(Status.SUCCESS.value(),donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findAllByDaXoa(Constants.DAXOA)));
        }
        Pageable pageable = PageRequest.of(page / size, size);

        if (search != null && search.length() > 0) {
            if (chaId > 0) {
                return new Response(Status.SUCCESS.value(),donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findByMaContainingAndChaIdAndDaXoaOrTenContainingAndChaIdAndDaXoa(search, chaId, Constants.DAXOA, search, chaId, Constants.DAXOA, pageable).getContent()));
            }
            return new Response(Status.SUCCESS.value(),donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent()));
        } else if (chaId > 0) {
            return new Response(Status.SUCCESS.value(), donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findByMaContainingAndChaIdAndDaXoaOrTenContainingAndChaIdAndDaXoa(search, chaId, Constants.DAXOA, search, chaId, Constants.DAXOA, pageable).getContent()));
        }

        return new Response(Status.SUCCESS.value(),donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent()));
    }

    public String countByMaOrTen(String search, long chaId) {
        JSONObject jsonObject = new JSONObject();
        if (chaId > 0) {
            return jsonObject.put("total", donViHanhChinhsRepository.countByMaContainingAndChaIdAndDaXoaOrTenContainingAndChaIdAndDaXoa(search, chaId, Constants.DAXOA, search, chaId, Constants.DAXOA).longValue()).toString();
        }
        return jsonObject.put("total", donViHanhChinhsRepository.countByMaContainingAndDaXoaOrTenContainingAndDaXoa(search, Constants.DAXOA, search, Constants.DAXOA).longValue()).toString();
    }

    public Response getListDonViHanhChinh() {
        return new Response(
                Status.SUCCESS.value(),
                donViHanhChinhMapper.toDonViHanhChinhViewDTOList(
                        donViHanhChinhsRepository.findAllByDaXoa(Constants.DAXOA)));

    }

    public Response findByChaMa(String chaMa) {
        return new Response(
                Status.SUCCESS.value(),
                donViHanhChinhMapper.toDonViHanhChinhDTOList(donViHanhChinhsRepository.findByChaMaAndDaXoa(chaMa, Constants.DAXOA)));

    }
}
