package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.KhuCongNghiepRepository;
import sdt.osgi.springboot.dto.KhuCongNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.KhuCongNghiepMapper;
import sdt.osgi.springboot.model.KhuCongNghiep;
import sdt.osgi.springboot.model.KhuNghiDuong;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.KhuCongNghiepValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class KhuCongNghiepService {

    private final KhuCongNghiepRepository khuCongNghiepRepository;
    private final KhuCongNghiepMapper khuCongNghiepMapper;
    private final KhuCongNghiepValidate khuCongNghiepValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<KhuCongNghiepDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                KhuCongNghiepDTO khuCongNghiepDTO = new KhuCongNghiepDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            khuCongNghiepDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            khuCongNghiepDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            khuCongNghiepDTO.setDienTich(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 3) {
                            khuCongNghiepDTO.setDiaChi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 4) {
                            khuCongNghiepDTO.setSoDienThoai(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 5) {
                            khuCongNghiepDTO.setSoLuongNhaMay(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 6) {
                            khuCongNghiepDTO.setSoLuongKhoBai(Integer.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 7) {
                            khuCongNghiepDTO.setLinhVucDauTu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 8) {
                            khuCongNghiepDTO.setGiaThueDat(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 9) {
                            khuCongNghiepDTO.setUrl(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;

                        }

                        if (cell.getColumnIndex() == 10) {
                            khuCongNghiepDTO.setDiaChiGoogleMap(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;

                        }

                    }

                    if (khuCongNghiepValidate.validateKhuCongNghiep(khuCongNghiepDTO, khuCongNghiepMapper.khuCongNghiepToKhuCongNghiepDTO(khuCongNghiepRepository.findFirstByMaAndDaXoa(khuCongNghiepDTO.getMa(), Constants.DAXOA)))) {
                        khuCongNghiepDTO.setNguoiTao(nguoiTao);
                        khuCongNghiepDTO.setNguoiSua(nguoiTao);
                        if (khuCongNghiepDTO.getMa() != null) {
                            khuCongNghiepRepository.save(khuCongNghiepMapper.khuCongNghiepDTOToKhuCongNghiep(khuCongNghiepDTO));
                        }
                    } else {
                        maFail.add(khuCongNghiepDTO.getMa());
                    }
                }
                i++;
            }


            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                khuCongNghiepMapper.khuCongNghiepToKhuCongNghiepDTO
                        (khuCongNghiepRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                khuCongNghiepMapper.khuCongNghiepToKhuCongNghiepDTO(khuCongNghiepRepository.findFirstById(id)));
    }

    public Response insert(KhuCongNghiepDTO khuCongNghiepDTO) {
        khuCongNghiepRepository.save(khuCongNghiepMapper.khuCongNghiepDTOToKhuCongNghiep(khuCongNghiepDTO));

        return new Response(Status.SUCCESS.value(), khuCongNghiepDTO.getMa());
    }

    public Response update(KhuCongNghiepDTO khuCongNghiepDTO) {
        KhuCongNghiep khuCongNghiepOrigin = khuCongNghiepRepository.findById(khuCongNghiepDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay tai nguyen!!!"));
        KhuCongNghiep khuCongNghiep = KhuCongNghiep.builder()
                .id(khuCongNghiepDTO.getId())
                .ma(khuCongNghiepDTO.getMa())
                .ten(khuCongNghiepDTO.getTen())
                .diaChi(khuCongNghiepDTO.getDiaChi())
                .soDienThoai(khuCongNghiepDTO.getSoDienThoai())
                .anh(khuCongNghiepDTO.getAnh() != null ? khuCongNghiepDTO.getAnh() : khuCongNghiepOrigin.getAnh())
                .dienTich(khuCongNghiepDTO.getDienTich())
                .soLuongNhaMay(khuCongNghiepDTO.getSoLuongNhaMay())
                .soLuongKhoBai(khuCongNghiepDTO.getSoLuongKhoBai())
                .linhVucDauTu(khuCongNghiepDTO.getLinhVucDauTu())
                .giaThueDat(khuCongNghiepDTO.getGiaThueDat())
                .url(khuCongNghiepDTO.getUrl())
                .diaChiGoogleMap(khuCongNghiepDTO.getDiaChiGoogleMap())
                .build();

        khuCongNghiepRepository.save(khuCongNghiep);
        return new Response(Status.SUCCESS.value(), khuCongNghiepDTO.getMa());
    }

    public Response delete(long id) {

        KhuCongNghiep khuCongNghiep = khuCongNghiepRepository.findFirstById(id);
        khuCongNghiep.setDaXoa(Constants.XOA);
        khuCongNghiepRepository.save(khuCongNghiep);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<KhuCongNghiepDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return khuCongNghiepMapper.toKhuCongNghiepDTOList(khuCongNghiepRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return khuCongNghiepMapper.toKhuCongNghiepDTOList(khuCongNghiepRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", khuCongNghiepRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                khuCongNghiepMapper.toKhuCongNghiepViewDTOList(
                        khuCongNghiepRepository.findByDaXoa(Constants.DAXOA)));

    }

}
