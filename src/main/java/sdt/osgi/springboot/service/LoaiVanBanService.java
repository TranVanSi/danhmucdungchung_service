package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.LoaiVanBanRepository;
import sdt.osgi.springboot.dto.LoaiVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.LoaiVanBanMapper;
import sdt.osgi.springboot.model.LoaiVanBan;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.LoaiVanBanValidate;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class LoaiVanBanService {
    private final LoaiVanBanMapper loaiVanBanMapper;
    private final LoaiVanBanRepository loaiVanBanRepository;
    private final LoaiVanBanValidate loaiVanBanValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;


    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<LoaiVanBanDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                LoaiVanBanDTO loaiVanBanDTO = new LoaiVanBanDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            loaiVanBanDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            loaiVanBanDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            loaiVanBanDTO.setChuVietTat(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 3) {
                            loaiVanBanDTO.setQdBanHanhSuaDoi(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }
                        if (cell.getColumnIndex() == 4) {
                            loaiVanBanDTO.setQdNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ? Date.valueOf(dataFormatter.formatCellValue(cell)) : null);
                            continue;
                        }
                        if (cell.getColumnIndex() == 5) {
                            loaiVanBanDTO.setQdCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }
                    }

                    if (loaiVanBanValidate.validateLoaiVanBan(loaiVanBanDTO, loaiVanBanMapper.loaiVanBanToLoaiVanBanDTO(loaiVanBanRepository.findFirstByMaAndDaXoa(loaiVanBanDTO.getMa(), Constants.DAXOA)))) {
                        loaiVanBanDTO.setNguoiTao(nguoiTao);
                        loaiVanBanDTO.setNguoiSua(nguoiTao);
                        if (loaiVanBanDTO.getMa() != null) {
                            loaiVanBanRepository.save(loaiVanBanMapper.loaiVanBanDTOToLoaiVanBan(loaiVanBanDTO));
                        }

                    } else {
                        maFail.add(loaiVanBanDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                loaiVanBanMapper.loaiVanBanToLoaiVanBanDTO
                        (loaiVanBanRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                loaiVanBanMapper.loaiVanBanToLoaiVanBanDTO(loaiVanBanRepository.findFirstById(id)));
    }

    public Response insert(LoaiVanBanDTO loaiVanBanDTO) {
        loaiVanBanRepository.save(loaiVanBanMapper.loaiVanBanDTOToLoaiVanBan(loaiVanBanDTO));

        return new Response(Status.SUCCESS.value(), loaiVanBanDTO.getMa());
    }

    public Response update(LoaiVanBanDTO loaiVanBanDTO) {
        LoaiVanBan loaiVanBanOrigin = loaiVanBanRepository.findById(loaiVanBanDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        LoaiVanBan loaiVanBan = LoaiVanBan.builder()
                .id(loaiVanBanDTO.getId())
                .ma(loaiVanBanDTO.getMa())
                .ten(loaiVanBanDTO.getTen())
                .chuVietTat(loaiVanBanDTO.getChuVietTat())
                .qdBanHanhSuaDoi(loaiVanBanDTO.getQdBanHanhSuaDoi())
                .qdNgayBanHanh(loaiVanBanDTO.getQdNgayBanHanh())
                .qdCoQuanBanHanh(loaiVanBanDTO.getQdCoQuanBanHanh())
                .build();

        loaiVanBanRepository.save(loaiVanBan);
        return new Response(Status.SUCCESS.value(), loaiVanBanDTO.getMa());
    }

    public Response delete(long id) {
        LoaiVanBan loaiVanBan = loaiVanBanRepository.findFirstById(id);
        loaiVanBan.setDaXoa(Constants.XOA);
        loaiVanBanRepository.save(loaiVanBan);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                loaiVanBanMapper.toLoaiVanBanViewDTOList(
                        loaiVanBanRepository.findByDaXoa(Constants.DAXOA)));

    }
}
