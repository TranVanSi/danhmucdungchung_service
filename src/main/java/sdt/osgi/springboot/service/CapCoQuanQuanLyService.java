package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.dao.CapCoQuanQuanLyRepository;
import sdt.osgi.springboot.dto.CapCoQuanQuanLyDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.CapCoQuanQuanLyMapper;
import sdt.osgi.springboot.model.CapCoQuanQuanLy;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.CapCoQuanQuanLyValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class CapCoQuanQuanLyService {

    private final CapCoQuanQuanLyRepository capCoQuanQuanLyRepository;

    private final CapCoQuanQuanLyMapper capCoQuanQuanLyMapper;
    private final CapCoQuanQuanLyValidate capCoQuanQuanLyValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<CapCoQuanQuanLyDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                CapCoQuanQuanLyDTO capCoQuanQuanLyDTO = new CapCoQuanQuanLyDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            capCoQuanQuanLyDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            capCoQuanQuanLyDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            capCoQuanQuanLyDTO.setChaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            CapCoQuanQuanLyDTO chaId = capCoQuanQuanLyMapper.capCoQuanQuanLyToCapCoQuanQuanLyDTO(capCoQuanQuanLyRepository.findFirstById(capCoQuanQuanLyDTO.getChaId()));
                            int cap = chaId.getCap() + 1;
                            capCoQuanQuanLyDTO.setCap((byte) cap);
                            break;
                        }

                    }

                    if (capCoQuanQuanLyValidate.validateCapCoQuanQuanLy(capCoQuanQuanLyDTO, capCoQuanQuanLyMapper.capCoQuanQuanLyToCapCoQuanQuanLyDTO(capCoQuanQuanLyRepository.findFirstByMaAndDaXoa(capCoQuanQuanLyDTO.getMa(), Constants.DAXOA)))) {
                        capCoQuanQuanLyDTO.setNguoiTao(nguoiTao);
                        capCoQuanQuanLyDTO.setNguoiSua(nguoiTao);
                        if (capCoQuanQuanLyDTO.getMa() != null){
                            capCoQuanQuanLyRepository.save(capCoQuanQuanLyMapper.capCoQuanQuanLyDTOToCapCoQuanQuanLy(capCoQuanQuanLyDTO));
                        }
                    } else {
                        maFail.add(capCoQuanQuanLyDTO.getMa());
                    }
                }
                i++;
            }

            String message = maFail.size() == 0 ? Constants.SPACE : cacMa +Constants.SPACE+ String.join(", ", maFail)+ Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                capCoQuanQuanLyMapper.capCoQuanQuanLyToCapCoQuanQuanLyDTO
                        (capCoQuanQuanLyRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }
    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                capCoQuanQuanLyMapper.capCoQuanQuanLyToCapCoQuanQuanLyDTO(capCoQuanQuanLyRepository.findFirstById(id)));
    }

    public Response insert(CapCoQuanQuanLyDTO capCoQuanQuanLyDTO) {
        capCoQuanQuanLyRepository.save(capCoQuanQuanLyMapper.capCoQuanQuanLyDTOToCapCoQuanQuanLy(capCoQuanQuanLyDTO));

        return new Response(Status.SUCCESS.value(), capCoQuanQuanLyDTO.getMa());
    }

    public Response update(CapCoQuanQuanLyDTO capCoQuanQuanLyDTO) {
        CapCoQuanQuanLy quyTrinh = CapCoQuanQuanLy.builder()
                .id(capCoQuanQuanLyDTO.getId())
                .ma(capCoQuanQuanLyDTO.getMa())
                .ten(capCoQuanQuanLyDTO.getTen())
                .chaId(capCoQuanQuanLyDTO.getChaId())
                .cap(capCoQuanQuanLyDTO.getCap())
                .build();

        capCoQuanQuanLyRepository.save(quyTrinh);
        return new Response(Status.SUCCESS.value(), capCoQuanQuanLyDTO.getMa());
    }

    public Response delete(long id ) {

        CapCoQuanQuanLy capCoQuanQuanLy = capCoQuanQuanLyRepository.findFirstById(id);
        capCoQuanQuanLy.setDaXoa(Constants.XOA);
        capCoQuanQuanLyRepository.save(capCoQuanQuanLy);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<CapCoQuanQuanLyDTO> findAll(String search, int page, int size) {

        Pageable pageable =  PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return capCoQuanQuanLyMapper.toCapCoQuanQuanLygDTOList(capCoQuanQuanLyRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return capCoQuanQuanLyMapper.toCapCoQuanQuanLygDTOList(capCoQuanQuanLyRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", capCoQuanQuanLyRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                capCoQuanQuanLyMapper.toCapCoQuanQuanLyViewDTOList(
                        capCoQuanQuanLyRepository.findByDaXoa(Constants.DAXOA)));

    }

}
