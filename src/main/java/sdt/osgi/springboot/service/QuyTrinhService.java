package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.QuyTrinhRepository;
import sdt.osgi.springboot.dto.QuyTrinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.QuyTrinhMapper;
import sdt.osgi.springboot.model.QuyTrinh;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.QuyTrinhValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class QuyTrinhService {

    private final QuyTrinhRepository quyTrinhRepository;
    private final QuyTrinhMapper quyTrinhMapper;
    private final QuyTrinhValidate quyTrinhValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<QuyTrinhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                QuyTrinhDTO quyTrinhDTO = new QuyTrinhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            quyTrinhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            quyTrinhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            quyTrinhDTO.setMaHinhThucVanBan(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            quyTrinhDTO.setCoQuanId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            quyTrinhDTO.setCapVanBanId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 5) {
                            quyTrinhDTO.setUrlFileMoTa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 6) {
                            quyTrinhDTO.setTenCoQuan(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 7) {
                            quyTrinhDTO.setMaThuTucHanhChinh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 8) {
                            quyTrinhDTO.setTenThuTucHanhChinh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (quyTrinhValidate.validateQuyTrinh(quyTrinhDTO, quyTrinhMapper.quyTrinhToQuyTrinhDTO(quyTrinhRepository.findFirstByMaAndDaXoa(quyTrinhDTO.getMa(), Constants.DAXOA)))) {
                        quyTrinhDTO.setNguoiTao(nguoiTao);
                        quyTrinhDTO.setNguoiSua(nguoiTao);
                        if (quyTrinhDTO.getMa() != null) {
                            quyTrinhRepository.save(quyTrinhMapper.quyTrinhDTOToQuyTrinh(quyTrinhDTO));
                        }
                    } else {
                        maFail.add(quyTrinhDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                quyTrinhMapper.quyTrinhToQuyTrinhDTO
                        (quyTrinhRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                quyTrinhMapper.quyTrinhToQuyTrinhDTO(quyTrinhRepository.findFirstById(id)));
    }

    public Response insert(QuyTrinhDTO quyTrinhDTO) {
        quyTrinhRepository.save(quyTrinhMapper.quyTrinhDTOToQuyTrinh(quyTrinhDTO));

        return new Response(Status.SUCCESS.value(), quyTrinhDTO.getMa());
    }

    public Response update(QuyTrinhDTO quyTrinhDTO) {
        QuyTrinh quyTrinhOrigin = quyTrinhRepository.findById(quyTrinhDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        QuyTrinh quyTrinh = QuyTrinh.builder()
                .id(quyTrinhDTO.getId())
                .ma(quyTrinhDTO.getMa())
                .ten(quyTrinhDTO.getTen())
                .maHinhThucVanBan(quyTrinhDTO.getMaHinhThucVanBan())
                .coQuanId(quyTrinhDTO.getCoQuanId())
                .capVanBanId(quyTrinhDTO.getCapVanBanId())
                .tenCoQuan(quyTrinhDTO.getTenCoQuan())
                .maThuTucHanhChinh(quyTrinhDTO.getMaThuTucHanhChinh())
                .tenThuTucHanhChinh(quyTrinhDTO.getTenThuTucHanhChinh())
                .urlFileMoTa(quyTrinhDTO.getUrlFileMoTa())
                .build();

        quyTrinhRepository.save(quyTrinh);
        return new Response(Status.SUCCESS.value(), quyTrinhDTO.getMa());
    }

    public Response delete(long id) {
        QuyTrinh quyTrinh = quyTrinhRepository.findFirstById(id);
        quyTrinh.setDaXoa(Constants.XOA);
        quyTrinhRepository.save(quyTrinh);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<QuyTrinhDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return quyTrinhMapper.toQuyTrinhDTOList(quyTrinhRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return quyTrinhMapper.toQuyTrinhDTOList(quyTrinhRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", quyTrinhRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                quyTrinhMapper.toQuyTrinhViewDTOList(
                        quyTrinhRepository.findByDaXoa(Constants.DAXOA)));

    }


}
