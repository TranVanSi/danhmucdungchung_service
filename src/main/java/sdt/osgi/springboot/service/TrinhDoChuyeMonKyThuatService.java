package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.TrinhDoChuyenMonKyThuatRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.TrinhDoChuyenMonKyThuatDTO;
import sdt.osgi.springboot.mapper.TrinhDoChuyenMonKyThuatMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.model.TrinhDoChuyenMonKyThuat;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.TrinhDoChuyenMonKyThuatValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class TrinhDoChuyeMonKyThuatService {

    private final TrinhDoChuyenMonKyThuatRepository trinhDoChuyenMonKyThuatRepository;
    private final TrinhDoChuyenMonKyThuatMapper trinhDoChuyenMonKyThuatMapper;
    private final TrinhDoChuyenMonKyThuatValidate trinhDoChuyenMonKyThuatValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<TrinhDoChuyenMonKyThuatDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatDTO = new TrinhDoChuyenMonKyThuatDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            trinhDoChuyenMonKyThuatDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            trinhDoChuyenMonKyThuatDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (trinhDoChuyenMonKyThuatValidate.validateTrinhDoChuyenMonKyThuat(trinhDoChuyenMonKyThuatDTO,
                            trinhDoChuyenMonKyThuatMapper.trinhDoChuyenMonKyThuatToTrinhDoChuyenMonKyThuatDTO
                                    (trinhDoChuyenMonKyThuatRepository.findFirstByMaAndDaXoa(trinhDoChuyenMonKyThuatDTO.getMa(), Constants.DAXOA)))) {
                        trinhDoChuyenMonKyThuatDTO.setNguoiTao(nguoiTao);
                        trinhDoChuyenMonKyThuatDTO.setNguoiSua(nguoiTao);
                        if (trinhDoChuyenMonKyThuatDTO.getMa() != null) {
                            trinhDoChuyenMonKyThuatRepository.save(trinhDoChuyenMonKyThuatMapper.trinhDoChuyenMonKyThuatDTOToTrinhDoChuyenMonKyThuat(trinhDoChuyenMonKyThuatDTO));
                        }
                    } else {
                        maFail.add(trinhDoChuyenMonKyThuatDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                trinhDoChuyenMonKyThuatMapper.trinhDoChuyenMonKyThuatToTrinhDoChuyenMonKyThuatDTO
                        (trinhDoChuyenMonKyThuatRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                trinhDoChuyenMonKyThuatMapper.trinhDoChuyenMonKyThuatToTrinhDoChuyenMonKyThuatDTO(trinhDoChuyenMonKyThuatRepository.findFirstById(id)));
    }

    public Response insert(TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatDTO) {
        trinhDoChuyenMonKyThuatRepository.save(trinhDoChuyenMonKyThuatMapper.trinhDoChuyenMonKyThuatDTOToTrinhDoChuyenMonKyThuat(trinhDoChuyenMonKyThuatDTO));

        return new Response(Status.SUCCESS.value(), trinhDoChuyenMonKyThuatDTO.getMa());
    }

    public Response update(TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatDTO) {
        TrinhDoChuyenMonKyThuat trinhDoChuyenMonKyThuatOrigin = trinhDoChuyenMonKyThuatRepository.findById(trinhDoChuyenMonKyThuatDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        TrinhDoChuyenMonKyThuat trinhDoChuyenMonKyThuat = TrinhDoChuyenMonKyThuat.builder()
                .id(trinhDoChuyenMonKyThuatDTO.getId())
                .ma(trinhDoChuyenMonKyThuatDTO.getMa())
                .ten(trinhDoChuyenMonKyThuatDTO.getTen())
                .build();

        trinhDoChuyenMonKyThuatRepository.save(trinhDoChuyenMonKyThuat);
        return new Response(Status.SUCCESS.value(), trinhDoChuyenMonKyThuatDTO.getMa());
    }

    public Response delete(long id) {
        TrinhDoChuyenMonKyThuat trinhDoChuyenMonKyThuat = trinhDoChuyenMonKyThuatRepository.findFirstById(id);
        trinhDoChuyenMonKyThuat.setDaXoa(Constants.XOA);
        trinhDoChuyenMonKyThuatRepository.save(trinhDoChuyenMonKyThuat);
        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                trinhDoChuyenMonKyThuatMapper.toTrinhDoChuyenMonKyThuatViewDTOList(
                        trinhDoChuyenMonKyThuatRepository.findByDaXoa(Constants.DAXOA)));

    }


}
