package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import sdt.osgi.springboot.dao.VanBanPhapLyRepository;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.dto.VanBanPhapLyDTO;
import sdt.osgi.springboot.mapper.VanBanPhapLyMapper;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.model.VanBanPhapLy;
import sdt.osgi.springboot.model.VanBanPhapLy;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.VanBanPhapLyValidate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static sdt.osgi.springboot.util.Constants.*;

@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class VanBanPhapLyService {

    private final VanBanPhapLyRepository vanBanPhapLyRepository;
    private final VanBanPhapLyMapper vanBanPhapLyMapper;
    private final VanBanPhapLyValidate vanBanPhapLyValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;


    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<VanBanPhapLyDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                VanBanPhapLyDTO vanBanPhapLyDTO = new VanBanPhapLyDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            vanBanPhapLyDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            vanBanPhapLyDTO.setTenNguoiKy(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            vanBanPhapLyDTO.setSoKyHieu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 3) {
                            vanBanPhapLyDTO.setNgayBanHanh(dataFormatter.formatCellValue(cell).length() > 0 ?
                                    dateFormat.format(sdf.parse(dataFormatter.formatCellValue(cell))) : null);
                            continue;
                        }

                        if (cell.getColumnIndex() == 4) {
                            vanBanPhapLyDTO.setNgayHetHan(dataFormatter.formatCellValue(cell).length() > 0 ?
                                    dateFormat.format(sdf.parse(dataFormatter.formatCellValue(cell))) : null);
                            continue;
                        }

                        if (cell.getColumnIndex() == 5) {
                            vanBanPhapLyDTO.setTenCoQuanBanHanh(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 6) {
                            vanBanPhapLyDTO.setTenLinhVuc(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 7) {
                            vanBanPhapLyDTO.setTenLoaiVanBan(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 8) {
                            vanBanPhapLyDTO.setTrichYeu(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 9) {
                            vanBanPhapLyDTO.setNoiDung(String.valueOf(dataFormatter.formatCellValue(cell)));
                            break;
                        }

                    }

                    if (vanBanPhapLyValidate.validateVanBanPhapLy(vanBanPhapLyDTO, vanBanPhapLyMapper.vanBanPhapLyToVanBanPhapLyDTO(vanBanPhapLyRepository.findFirstByMaAndDaXoa(vanBanPhapLyDTO.getMa(), DAXOA)))) {
                        vanBanPhapLyDTO.setNguoiTao(nguoiTao);
                        vanBanPhapLyDTO.setNguoiSua(nguoiTao);
                        if (vanBanPhapLyDTO.getMa() != null) {
                            vanBanPhapLyRepository.save(vanBanPhapLyMapper.vanBanPhapLyDTOToVanBanPhapLy(vanBanPhapLyDTO));
                        }
                    } else {
                        maFail.add(vanBanPhapLyDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                vanBanPhapLyMapper.vanBanPhapLyToVanBanPhapLyDTO
                        (vanBanPhapLyRepository.findFirstByMaAndDaXoa(ma, DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                vanBanPhapLyMapper.vanBanPhapLyToVanBanPhapLyDTO(vanBanPhapLyRepository.findFirstById(id)));
    }

    public Response insert(VanBanPhapLyDTO vanBanPhapLyDTO) {
        vanBanPhapLyRepository.save(vanBanPhapLyMapper.vanBanPhapLyDTOToVanBanPhapLy(vanBanPhapLyDTO));

        return new Response(Status.SUCCESS.value(), vanBanPhapLyDTO.getMa());
    }

    public Response update(VanBanPhapLyDTO vanBanPhapLyDTO) throws Exception {
        VanBanPhapLy vanBanPhapLyOrigin = vanBanPhapLyRepository.findById(vanBanPhapLyDTO.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Khong tim thay!!!"));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        VanBanPhapLy vanBanPhapLy = VanBanPhapLy.builder()
                .id(vanBanPhapLyDTO.getId())
                .ma(vanBanPhapLyDTO.getMa())
                .tenNguoiKy(vanBanPhapLyDTO.getTenNguoiKy())
                .soKyHieu(vanBanPhapLyDTO.getSoKyHieu())
                .ngayBanHanh((vanBanPhapLyDTO.getNgayBanHanh() != null && vanBanPhapLyDTO.getNgayBanHanh().length() > 0) ? sdf.parse(vanBanPhapLyDTO.getNgayBanHanh()) : vanBanPhapLyOrigin.getNgayBanHanh())
                .ngayHetHan((vanBanPhapLyDTO.getNgayHetHan() != null && vanBanPhapLyDTO.getNgayHetHan().length() > 0) ? sdf.parse(vanBanPhapLyDTO.getNgayHetHan()) : vanBanPhapLyOrigin.getNgayHetHan())
                .tenCoQuanBanHanh(vanBanPhapLyDTO.getTenCoQuanBanHanh())
                .tenLinhVuc(vanBanPhapLyDTO.getTenLinhVuc())
                .tenLoaiVanBan(vanBanPhapLyDTO.getTenLoaiVanBan())
                .trichYeu(vanBanPhapLyDTO.getTrichYeu())
                .noiDung(vanBanPhapLyDTO.getNoiDung())
                .vanBanDinhKem(vanBanPhapLyDTO.getVanBanDinhKem() != null ? vanBanPhapLyDTO.getVanBanDinhKem() : vanBanPhapLyOrigin.getVanBanDinhKem())
                .build();

        vanBanPhapLyRepository.save(vanBanPhapLy);
        return new Response(Status.SUCCESS.value(), vanBanPhapLyDTO.getMa());
    }

    public Response delete(long id) {

        VanBanPhapLy vanBanPhapLy = vanBanPhapLyRepository.findFirstById(id);
        vanBanPhapLy.setDaXoa(XOA);
        vanBanPhapLyRepository.save(vanBanPhapLy);

        return new Response(Status.SUCCESS.value(), null);
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                vanBanPhapLyMapper.toVanBanPhapLyViewDTOList(
                        vanBanPhapLyRepository.findByDaXoa(Constants.DAXOA)));

    }

}
