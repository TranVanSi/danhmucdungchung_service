package sdt.osgi.springboot.service;

import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.dao.LinhVucKinhDoanhRepository;
import sdt.osgi.springboot.dto.LinhVucKinhDoanhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.mapper.LinhVucKinhDoanhMapper;
import sdt.osgi.springboot.model.LinhVucKinhDoanh;
import sdt.osgi.springboot.model.Status;
import sdt.osgi.springboot.util.Constants;
import sdt.osgi.springboot.validate.LinhVucKinhDoanhValidate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service
@RequiredArgsConstructor
@PropertySource("classpath:language.properties")
public class LinhVucKinhDoanhService {

    private final LinhVucKinhDoanhRepository linhVucKinhDoanhRepository;
    private final LinhVucKinhDoanhMapper linhVucKinhDoanhMapper;
    private final LinhVucKinhDoanhValidate linhVucKinhDoanhValidate;
    @Value("${vn.sdt.export.cacMa}")
    private String cacMa;
    @Value("${vn.sdt.export.thuchien}")
    private String thucHien;
    @Value("${vn.sdt.export.importthanhcong}")
    private String importThanhCong;

    public ResponseDTO uploadExcelFile(MultipartFile file, String nguoiTao) throws Exception {
        List<LinhVucKinhDoanhDTO> dtoList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.rowIterator();
        DataFormatter dataFormatter = new DataFormatter();

        try {
            List<String> maFail = new ArrayList<>();
            int i = 0;
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                LinhVucKinhDoanhDTO linhVucKinhDoanhDTO = new LinhVucKinhDoanhDTO();

                if (i != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            linhVucKinhDoanhDTO.setMa(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 1) {
                            linhVucKinhDoanhDTO.setTen(String.valueOf(dataFormatter.formatCellValue(cell)));
                            continue;
                        }

                        if (cell.getColumnIndex() == 2) {
                            linhVucKinhDoanhDTO.setChaId(Long.valueOf(dataFormatter.formatCellValue(cell)));
                            LinhVucKinhDoanhDTO chaId = linhVucKinhDoanhMapper.linhVucKinhDoanhToLinhVucKinhDoanhDTO(linhVucKinhDoanhRepository.findFirstById(linhVucKinhDoanhDTO.getChaId()));
                            int cap = chaId.getCap() + 1;
                            linhVucKinhDoanhDTO.setCap((byte) cap);
                            break;
                        }

                    }

                    if (linhVucKinhDoanhValidate.validateLinhVucKinhDoanh(linhVucKinhDoanhDTO, linhVucKinhDoanhMapper.linhVucKinhDoanhToLinhVucKinhDoanhDTO(linhVucKinhDoanhRepository.findFirstByMaAndDaXoa(linhVucKinhDoanhDTO.getMa(), Constants.DAXOA)))) {
                        linhVucKinhDoanhDTO.setNguoiTao(nguoiTao);
                        linhVucKinhDoanhDTO.setNguoiSua(nguoiTao);
                        if (linhVucKinhDoanhDTO.getMa() != null) {
                            linhVucKinhDoanhRepository.save(linhVucKinhDoanhMapper.linhVucKinhDoanhDTOToLinhVucKinhDoanh(linhVucKinhDoanhDTO));
                        }
                    } else {
                        maFail.add(linhVucKinhDoanhDTO.getMa());
                    }
                }
                i++;
            }
            String message = maFail.size() == 0 ? Constants.SPACE : cacMa + Constants.SPACE + String.join(", ", maFail) + Constants.SPACE + thucHien;
            return new ResponseDTO(HttpStatus.OK.value(), message);

        } catch (Exception e) {
            return new ResponseDTO(HttpStatus.BAD_REQUEST.value(), importThanhCong);
        }
    }

    public Response findByMa(String ma) {
        return new Response(
                Status.SUCCESS.value(),
                linhVucKinhDoanhMapper.linhVucKinhDoanhToLinhVucKinhDoanhDTO
                        (linhVucKinhDoanhRepository.findFirstByMaAndDaXoa(ma, Constants.DAXOA)));
    }

    public Response findById(long id) {
        return new Response(
                Status.SUCCESS.value(),
                linhVucKinhDoanhMapper.linhVucKinhDoanhToLinhVucKinhDoanhDTO(linhVucKinhDoanhRepository.findFirstById(id)));
    }

    public Response insert(LinhVucKinhDoanhDTO linhVucKinhDoanhDTO) {
        linhVucKinhDoanhRepository.save(linhVucKinhDoanhMapper.linhVucKinhDoanhDTOToLinhVucKinhDoanh(linhVucKinhDoanhDTO));

        return new Response(Status.SUCCESS.value(), linhVucKinhDoanhDTO.getMa());
    }

    public Response update(LinhVucKinhDoanhDTO linhVucKinhDoanhDTO) {
        LinhVucKinhDoanh linhVucKinhDoanh = LinhVucKinhDoanh.builder()
                .id(linhVucKinhDoanhDTO.getId())
                .ma(linhVucKinhDoanhDTO.getMa())
                .ten(linhVucKinhDoanhDTO.getTen())
                .chaId(linhVucKinhDoanhDTO.getChaId())
                .cap(linhVucKinhDoanhDTO.getCap())
                .build();

        linhVucKinhDoanhRepository.save(linhVucKinhDoanh);
        return new Response(Status.SUCCESS.value(), linhVucKinhDoanhDTO.getMa());
    }

    public Response delete(long id) {

        LinhVucKinhDoanh linhVucKinhDoanh = linhVucKinhDoanhRepository.findFirstById(id);
        linhVucKinhDoanh.setDaXoa(Constants.XOA);
        linhVucKinhDoanhRepository.save(linhVucKinhDoanh);

        return new Response(Status.SUCCESS.value(), null);
    }

    public List<LinhVucKinhDoanhDTO> findAll(String search, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);

        if (search != null && search.length() > 0) {
            return linhVucKinhDoanhMapper.toLinhVucKinhDoanhgDTOList(linhVucKinhDoanhRepository.findByMaContainingOrTenContainingAndDaXoa(search, search, pageable, Constants.DAXOA).getContent());
        }

        return linhVucKinhDoanhMapper.toLinhVucKinhDoanhgDTOList(linhVucKinhDoanhRepository.findAllByDaXoa(pageable, Constants.DAXOA).getContent());
    }

    public String countByMaOrTen(String search) {
        JSONObject jsonObject = new JSONObject();

        return jsonObject.put("total", linhVucKinhDoanhRepository.countByMaContainingOrTenContainingAndDaXoa(search, search, Constants.DAXOA).longValue()).toString();
    }

    public Response findAll() {
        return new Response(
                Status.SUCCESS.value(),
                linhVucKinhDoanhMapper.toLinhVucKinhDoanhViewDTOList(
                        linhVucKinhDoanhRepository.findByDaXoa(Constants.DAXOA)));

    }

}
