package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.NganhNgheKinhDoanh;

import java.util.List;

@Repository
public interface NganhNgheKinhDoanhRepository extends CrudRepository<NganhNgheKinhDoanh, Long> {
    NganhNgheKinhDoanh findFirstById(long id);
    List<NganhNgheKinhDoanh> findAllByCapAndDaXoa(byte cap, int daXoa);
    List<NganhNgheKinhDoanh> findByDaXoa(int daXoa);
    List<NganhNgheKinhDoanh> findByIdIn(List<Long> ids);
}

