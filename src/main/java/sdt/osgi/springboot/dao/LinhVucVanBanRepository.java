package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LinhVucVanBan;

import java.util.List;

@Repository
public interface LinhVucVanBanRepository extends CrudRepository<LinhVucVanBan, Long> {
    public LinhVucVanBan findFirstByMaAndDaXoa(String ma, int daXoa);

    public LinhVucVanBan findFirstByTen(String ten);

    LinhVucVanBan findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    LinhVucVanBan findFirstByMa(String ma);

    LinhVucVanBan findFirstById(long id);

    List<LinhVucVanBan> findByDaXoa(int daXoa);

}
