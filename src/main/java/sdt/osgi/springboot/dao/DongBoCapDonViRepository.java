package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DongBoCapDonVi;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DongBoCapDonViRepository extends CrudRepository<DongBoCapDonVi, Long> {
    DongBoCapDonVi findFirstById(long id);
    Page<DongBoCapDonVi> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);
    Page<DongBoCapDonVi> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaContainingAndDaXoaOrTenContainingAndDaXoa(String ma, int da, String ten, int daXoa);
    List<DongBoCapDonVi> findByDaXoa(int daXoa);

}

