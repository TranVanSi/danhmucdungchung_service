package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.NhaHang;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface NhaHangRepository extends CrudRepository<NhaHang, Long> {
    public NhaHang findFirstByMaAndDaXoa(String ma, int daXoa);

    public NhaHang findFirstById(long id);

    Page<NhaHang> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<NhaHang> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<NhaHang> findByDaXoa(int daXoa);
}
