package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TinhTrangHonNhan;

import java.util.List;

@Repository
public interface TinhTrangHonNhanRepository extends CrudRepository<TinhTrangHonNhan, Long> {
    public TinhTrangHonNhan findFirstByIdAndDaXoa(long id, int daXoa);

    public TinhTrangHonNhan findFirstByMaAndDaXoa(String ma, int daXoa);

    public TinhTrangHonNhan findFirstByTen(String ten);

    TinhTrangHonNhan findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    TinhTrangHonNhan findFirstByMa(String ma);

    TinhTrangHonNhan findFirstById(long id);

    List<TinhTrangHonNhan> findByDaXoa(int daXoa);

}
