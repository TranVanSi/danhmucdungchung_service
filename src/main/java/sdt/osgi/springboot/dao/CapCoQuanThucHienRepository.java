package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CapCoQuanThucHien;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface CapCoQuanThucHienRepository extends CrudRepository<CapCoQuanThucHien, Long> {
    public CapCoQuanThucHien findFirstByMaAndDaXoa(String ma, int daXoa);

    public CapCoQuanThucHien findFirstById(long id);

    Page<CapCoQuanThucHien> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<CapCoQuanThucHien> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<CapCoQuanThucHien> findByDaXoa(int daXoa);
}
