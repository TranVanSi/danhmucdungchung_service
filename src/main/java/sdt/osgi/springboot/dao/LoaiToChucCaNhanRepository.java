package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LoaiToChucCaNhan;

import java.util.List;

@Repository
public interface LoaiToChucCaNhanRepository extends CrudRepository<LoaiToChucCaNhan, Long> {
    public LoaiToChucCaNhan findFirstByMaAndDaXoa(String ma, int daXoa);

    public LoaiToChucCaNhan findFirstByTen(String ten);

    LoaiToChucCaNhan findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    LoaiToChucCaNhan findFirstByMa(String ma);

    LoaiToChucCaNhan findFirstById(long id);

    List<LoaiToChucCaNhan> findByDaXoa(int daXoa);
}
