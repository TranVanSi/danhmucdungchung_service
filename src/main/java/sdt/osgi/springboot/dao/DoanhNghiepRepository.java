package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DoanhNghiep;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DoanhNghiepRepository extends CrudRepository<DoanhNghiep, Long> {
    DoanhNghiep findFirstByMaDoanhNghiepAndDaXoa(String ma, int daXoa);
    DoanhNghiep findFirstByMaSoThueAndDaXoa(String ma, int daXoa);
    DoanhNghiep findFirstById(long id);
    Page<DoanhNghiep> findByMaDoanhNghiepContainingOrTenDoanhNghiepContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);
    Page<DoanhNghiep> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaDoanhNghiepContainingAndDaXoaOrTenDoanhNghiepContainingAndDaXoa(String ma, int da, String ten, int daXoa);
    List<DoanhNghiep> findByDaXoa(int daXoa);
}

