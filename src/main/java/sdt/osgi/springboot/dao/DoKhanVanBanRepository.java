package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DoKhanVanBan;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DoKhanVanBanRepository extends CrudRepository<DoKhanVanBan, Long> {
    public DoKhanVanBan findFirstByMaAndDaXoa(String ma, int daXoa);

    public DoKhanVanBan findFirstById(long id);

    Page<DoKhanVanBan> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<DoKhanVanBan> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<DoKhanVanBan> findByDaXoa(int daXoa);
}
