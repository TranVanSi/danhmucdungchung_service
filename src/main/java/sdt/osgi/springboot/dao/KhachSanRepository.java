package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.KhachSan;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface KhachSanRepository extends CrudRepository<KhachSan, Long> {
    public KhachSan findFirstByMaAndDaXoa(String ma, int daXoa);

    public KhachSan findFirstById(long id);

    Page<KhachSan> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<KhachSan> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<KhachSan> findByDaXoa(int daXoa);
}
