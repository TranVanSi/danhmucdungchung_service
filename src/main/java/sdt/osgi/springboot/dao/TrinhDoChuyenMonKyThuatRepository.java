package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TrinhDoChuyenMonKyThuat;

import java.util.List;

@Repository
public interface TrinhDoChuyenMonKyThuatRepository extends CrudRepository<TrinhDoChuyenMonKyThuat, Long> {
    public TrinhDoChuyenMonKyThuat findFirstByIdAndDaXoa(long id, int daXoa);

    public TrinhDoChuyenMonKyThuat findFirstByMaAndDaXoa(String ma, int daXoa);

    public TrinhDoChuyenMonKyThuat findFirstByTen(String ten);

    TrinhDoChuyenMonKyThuat findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    TrinhDoChuyenMonKyThuat findFirstByMa(String ma);

    TrinhDoChuyenMonKyThuat findFirstById(long id);

    List<TrinhDoChuyenMonKyThuat> findByDaXoa(int daXoa);
}
