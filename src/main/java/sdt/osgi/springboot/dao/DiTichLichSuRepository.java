package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DiTichLichSu;
import sdt.osgi.springboot.model.DiaDiemDuLich;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DiTichLichSuRepository extends CrudRepository<DiTichLichSu, Long> {
    public DiTichLichSu findFirstByMaAndDaXoa(String ma, int daXoa);

    public DiTichLichSu findFirstById(long id);

    Page<DiTichLichSu> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<DiTichLichSu> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<DiTichLichSu> findByDaXoa(int daXoa);
}
