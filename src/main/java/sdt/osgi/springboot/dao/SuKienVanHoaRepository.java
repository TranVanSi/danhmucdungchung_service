package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.SuKienVanHoa;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface SuKienVanHoaRepository extends CrudRepository<SuKienVanHoa, Long> {
    public SuKienVanHoa findFirstByMaAndDaXoa(String ma, int daXoa);

    public SuKienVanHoa findFirstById(long id);

    Page<SuKienVanHoa> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<SuKienVanHoa> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<SuKienVanHoa> findByDaXoa(int daXoa);
}
