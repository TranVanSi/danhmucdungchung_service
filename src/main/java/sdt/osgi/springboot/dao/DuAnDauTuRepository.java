package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DiTichLichSu;
import sdt.osgi.springboot.model.DuAnDauTu;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Repository
public interface DuAnDauTuRepository extends CrudRepository<DuAnDauTu, Long> {
    public DuAnDauTu findFirstByMaAndDaXoa(String ma, int daXoa);

    public DuAnDauTu findFirstById(long id);

    Page<DuAnDauTu> findAllByDaXoa(Pageable pageable, int daXoa);

    List<DuAnDauTu> findByDaXoa(int daXoa);
}
