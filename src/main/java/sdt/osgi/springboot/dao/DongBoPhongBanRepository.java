package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DongBoPhongBan;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DongBoPhongBanRepository extends CrudRepository<DongBoPhongBan, Long> {
    DongBoPhongBan findFirstById(long id);
    Page<DongBoPhongBan> findByMaPhongBanContainingOrTenPhongBanContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);
    Page<DongBoPhongBan> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaPhongBanContainingAndDaXoaOrTenPhongBanContainingAndDaXoa(String ma, int da, String ten, int daXoa);
    List<DongBoPhongBan> findByDaXoa(int daXoa);

}

