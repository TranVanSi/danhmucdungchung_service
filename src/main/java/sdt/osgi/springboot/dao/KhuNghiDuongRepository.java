package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.KhuNghiDuong;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface KhuNghiDuongRepository extends CrudRepository<KhuNghiDuong, Long> {
    public KhuNghiDuong findFirstByMaAndDaXoa(String ma, int daXoa);

    public KhuNghiDuong findFirstById(long id);

    Page<KhuNghiDuong> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<KhuNghiDuong> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<KhuNghiDuong> findByDaXoa(int daXoa);
}
