package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CauHinhBaoCao;

@Repository
public interface CauHinhBaoCaoRepository extends CrudRepository<CauHinhBaoCao, Long> {
    public CauHinhBaoCao findFirstById(long id);
    CauHinhBaoCao findFirstByTenBang(String tenBang);

}
