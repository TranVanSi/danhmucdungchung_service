package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.BaoTang;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface BaoTangRepository extends CrudRepository<BaoTang, Long> {
    public BaoTang findFirstByMaAndDaXoa(String ma, int daXoa);

    public BaoTang findFirstById(long id);

    Page<BaoTang> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<BaoTang> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<BaoTang> findByDaXoa(int daXoa);
}
