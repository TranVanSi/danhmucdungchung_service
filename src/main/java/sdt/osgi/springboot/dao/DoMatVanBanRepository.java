package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DoMatVanBan;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DoMatVanBanRepository extends CrudRepository<DoMatVanBan, Long> {
    public DoMatVanBan findFirstByMaAndDaXoa(String ma, int daXoa);

    public DoMatVanBan findFirstById(long id);

    Page<DoMatVanBan> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<DoMatVanBan> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<DoMatVanBan> findByDaXoa(int daXoa);
}
