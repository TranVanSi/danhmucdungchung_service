package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LichSuDongBo;

@Repository
public interface LichSuDongBoRepository extends CrudRepository<LichSuDongBo, Long> {
    Page<LichSuDongBo> findAll(Pageable pageable);

}
