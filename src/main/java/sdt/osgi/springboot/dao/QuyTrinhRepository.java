package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.QuyTrinh;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface QuyTrinhRepository extends CrudRepository<QuyTrinh, Long> {
    public QuyTrinh findFirstByMaAndDaXoa(String ma, int daXoa);

    Page<QuyTrinh> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<QuyTrinh> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<QuyTrinh> findByDaXoa(int daXoa);

    QuyTrinh findFirstById(long id);
}
