package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LichSuTruyCap;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface LichSuTruyCapRepository extends CrudRepository<LichSuTruyCap, Long> {
    public LichSuTruyCap findFirstByTenAndDaXoa(String ten, int daXoa);

    public LichSuTruyCap findFirstById(long id);

    Page<LichSuTruyCap> findByTenContainingAndDaXoaOrderByNgayTaoDesc(String ten, Pageable pageable, int daXoa);

    Page<LichSuTruyCap> findAllByDaXoaOrderByNgayTaoDesc(Pageable pageable, int daXoa);

    BigInteger countByTenContainingAndDaXoa(String ten, int daXoa);

    List<LichSuTruyCap> findByNguoiDungIdAndDaXoa(long nguoiDungId, int daXoa);
}
