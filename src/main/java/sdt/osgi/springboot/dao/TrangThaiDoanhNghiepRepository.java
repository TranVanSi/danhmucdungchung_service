package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TrangThaiDoanhNghiep;

import java.util.List;

@Repository
public interface TrangThaiDoanhNghiepRepository extends CrudRepository<TrangThaiDoanhNghiep, Long> {
    TrangThaiDoanhNghiep findFirstById(long id);
    List<TrangThaiDoanhNghiep> findByDaXoa(int daXoa);
}
