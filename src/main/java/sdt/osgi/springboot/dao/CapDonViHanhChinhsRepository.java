package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CapDonViHanhChinh;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface CapDonViHanhChinhsRepository extends CrudRepository<CapDonViHanhChinh, Long> {
    public CapDonViHanhChinh findFirstByMaAndDaXoa(String ma, int daXoa);

    public CapDonViHanhChinh findFirstById(long id);

    Page<CapDonViHanhChinh> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<CapDonViHanhChinh> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<CapDonViHanhChinh> findByDaXoa(int daXoa);
}
