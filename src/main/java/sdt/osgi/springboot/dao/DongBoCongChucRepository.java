package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DongBoCongChuc;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DongBoCongChucRepository extends CrudRepository<DongBoCongChuc, Long> {
    DongBoCongChuc findFirstById(long id);
    Page<DongBoCongChuc> findByMaContainingOrHoTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);
    Page<DongBoCongChuc> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaContainingAndDaXoaOrHoTenContainingAndDaXoa(String ma, int da, String ten, int daXoa);
    List<DongBoCongChuc> findByDaXoa(int daXoa);

}

