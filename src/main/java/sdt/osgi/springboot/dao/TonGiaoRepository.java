package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TonGiao;

import java.util.List;

@Repository
public interface TonGiaoRepository extends CrudRepository<TonGiao, Long> {
    public TonGiao findFirstByMaAndDaXoa(String ma, int daXoa);

    public TonGiao findFirstById(long id);

    public TonGiao findFirstByTen(String ten);

    TonGiao findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    TonGiao findFirstByMa(String ma);

    List<TonGiao> findByDaXoa(int daXoa);

}
