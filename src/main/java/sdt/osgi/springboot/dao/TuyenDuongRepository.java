package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.TuyenDuong;

import java.util.List;

@Repository
public interface TuyenDuongRepository extends CrudRepository<TuyenDuong, Long> {
    public TuyenDuong findFirstByIdAndDaXoa(long id, int daXoa);

    public TuyenDuong findFirstByMaAndDaXoa(String ma, int daXoa);

    public TuyenDuong findFirstByTen(String ten);

    TuyenDuong findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    TuyenDuong findFirstByMa(String ma);

    TuyenDuong findFirstById(long id);

    List<TuyenDuong> findByDaXoa(int daXoa);

    List<TuyenDuong> findAllByChaIdAndDaXoa(long chaId, int daXoa);
}
