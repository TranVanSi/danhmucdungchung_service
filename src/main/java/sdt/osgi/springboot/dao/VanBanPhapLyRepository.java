package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.SuKienVanHoa;
import sdt.osgi.springboot.model.VanBanPhapLy;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface VanBanPhapLyRepository extends CrudRepository<VanBanPhapLy, Long> {
    public VanBanPhapLy findFirstByMaAndDaXoa(String ma, int daXoa);

    public VanBanPhapLy findFirstById(long id);

    Page<VanBanPhapLy> findAllByDaXoa(Pageable pageable, int daXoa);

    List<VanBanPhapLy> findByDaXoa(int daXoa);
}
