package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.ThuTucHanhChinh;

import java.math.BigInteger;

@Repository
public interface ThuTucHanhChinhRepository extends CrudRepository<ThuTucHanhChinh, Long> {
    public ThuTucHanhChinh findFirstByMaAndDaXoa(String ma, int daXoa);

    public ThuTucHanhChinh findFirstById(long id);

    Page<ThuTucHanhChinh> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<ThuTucHanhChinh> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingAndDaXoaOrTenContainingAndDaXoa(String ma, int daXoaMa, String ten, int daXoa);
}
