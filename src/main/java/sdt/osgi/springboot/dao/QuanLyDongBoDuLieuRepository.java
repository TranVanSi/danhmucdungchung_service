package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.BaoTang;
import sdt.osgi.springboot.model.QuanLyDongBoDuLieu;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface QuanLyDongBoDuLieuRepository extends CrudRepository<QuanLyDongBoDuLieu, Long> {
}
