package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.KhuCongNghiep;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface KhuCongNghiepRepository extends CrudRepository<KhuCongNghiep, Long> {
    public KhuCongNghiep findFirstByMaAndDaXoa(String ma, int daXoa);

    public KhuCongNghiep findFirstById(long id);

    Page<KhuCongNghiep> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<KhuCongNghiep> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<KhuCongNghiep> findByDaXoa(int daXoa);
}
