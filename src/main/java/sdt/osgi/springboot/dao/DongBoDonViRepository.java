package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DongBoCapDonVi;
import sdt.osgi.springboot.model.DongBoDonVi;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DongBoDonViRepository extends CrudRepository<DongBoDonVi, Long> {
    DongBoDonVi findFirstById(long id);
    Page<DongBoDonVi> findByMaDonViContainingOrTenDonViContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);
    Page<DongBoDonVi> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaDonViContainingAndDaXoaOrTenDonViContainingAndDaXoa(String ma, int da, String ten, int daXoa);
    List<DongBoDonVi> findByDaXoa(int daXoa);

}

