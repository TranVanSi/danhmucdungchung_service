package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sdt.osgi.springboot.model.DonViHanhChinh;

import java.math.BigInteger;
import java.util.List;

@Repository
@Transactional
public interface DonViHanhChinhsRepository extends CrudRepository<DonViHanhChinh, Long> {
    public DonViHanhChinh findFirstByMaAndDaXoa(String ma, int daXoa);

    public DonViHanhChinh findFirstById(long id);

    List<DonViHanhChinh> findAllByDaXoa(int daXoa);

    List<DonViHanhChinh> findByChaIdAndDaXoa(long chaId, int daXoa);

    Page<DonViHanhChinh> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<DonViHanhChinh> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingAndDaXoaOrTenContainingAndDaXoa(String ma, int daXoaMa, String ten, int daXoa);

    Page<DonViHanhChinh> findByMaContainingAndChaIdAndDaXoaOrTenContainingAndChaIdAndDaXoa(String ma, long chaIdMa, int daXoaMa, String ten, long chaIdTen, int daXoaTen, Pageable pageable);

    BigInteger countByMaContainingAndChaIdAndDaXoaOrTenContainingAndChaIdAndDaXoa(String ma, long chaIdMa, int daXoaMa, String ten, long chaIdTen, int daXoaTen);

    List<DonViHanhChinh> findByCapDonViHanhChinh_IdAndDaXoa(long capId, int daXoa);

    Integer countByCapDonViHanhChinh_Id(long capId);

    void deleteDonViHanhChinhsByCapDonViHanhChinh_Id(long capId);
    List<DonViHanhChinh> findByChaMaAndDaXoa(String chaMa, int daXoa);

}
