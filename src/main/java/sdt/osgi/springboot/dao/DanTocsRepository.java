package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DanToc;

import java.util.List;

@Repository
public interface DanTocsRepository extends CrudRepository<DanToc, Long> {
    public DanToc findFirstByMaAndDaXoa(String ma, int daXoa);

    public DanToc findFirstByTen(String ten);

    public DanToc findFirstById(long id);

    DanToc findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    DanToc findFirstByMa(String ma);

    List<DanToc> findByDaXoa(int daXoa);
}
