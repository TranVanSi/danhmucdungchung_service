package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LoaiVanBan;

import java.util.List;

@Repository
public interface LoaiVanBanRepository extends CrudRepository<LoaiVanBan, Long> {
    public LoaiVanBan findFirstByMaAndDaXoa(String ma, int daXoa);

    public LoaiVanBan findFirstByTen(String ten);

    LoaiVanBan findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    LoaiVanBan findFirstByMa(String ma);

    LoaiVanBan findFirstById(long id);

    List<LoaiVanBan> findByDaXoa(int daXoa);
}
