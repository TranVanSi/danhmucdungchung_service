package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.QuanHeGiaDinh;

import java.util.List;

@Repository
public interface QuanHeGiaDinhRepository extends CrudRepository<QuanHeGiaDinh, Long> {
    public QuanHeGiaDinh findFirstByIdAndDaXoa(long id, int daXoa);

    public QuanHeGiaDinh findFirstByMaAndDaXoa(String ma, int daXoa);

    public QuanHeGiaDinh findFirstByTen(String ten);

    QuanHeGiaDinh findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    QuanHeGiaDinh findFirstByMa(String ma);

    QuanHeGiaDinh findFirstById(long id);

    List<QuanHeGiaDinh> findByDaXoa(int daXoa);
}
