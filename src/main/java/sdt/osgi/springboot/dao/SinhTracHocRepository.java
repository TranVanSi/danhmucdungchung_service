package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.SinhTracHoc;

import java.util.List;

@Repository
public interface SinhTracHocRepository extends CrudRepository<SinhTracHoc, Long> {
    public SinhTracHoc findFirstByIdAndDaXoa(long id, int daXoa);

    public SinhTracHoc findFirstByMaAndDaXoa(String ma, int daXoa);

    public SinhTracHoc findFirstByTen(String ten);

    SinhTracHoc findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    SinhTracHoc findFirstByMa(String ma);

    SinhTracHoc findFirstById(long id);

    List<SinhTracHoc> findByDaXoa(int daXoa);
}
