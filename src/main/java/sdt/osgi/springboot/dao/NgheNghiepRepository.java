package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.NgheNghiep;

import java.util.List;

@Repository
public interface NgheNghiepRepository extends CrudRepository<NgheNghiep, Long> {
    public NgheNghiep findFirstByIdAndDaXoa(long id, int daXoa);

    public NgheNghiep findFirstByMaAndDaXoa(String ma, int daXoa);

    public NgheNghiep findFirstByTen(String ten);

    NgheNghiep findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    NgheNghiep findFirstByMa(String ma);

    NgheNghiep findFirstById(long id);

    List<NgheNghiep> findByDaXoa(int daXoa);

    NgheNghiep findFirstByChaMaAndDaXoa(String chaMa, int daXoa);

    List<NgheNghiep> findAllByChaMaLikeAndDaXoa(String chaMa, int daXoa);
}
