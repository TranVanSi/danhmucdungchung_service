package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DongBoCoQuanQuanLy;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DongBoCoQuanQuanLyRepository extends CrudRepository<DongBoCoQuanQuanLy, Long> {
    public DongBoCoQuanQuanLy findFirstByMaAndDaXoa(String ma, int daXoa);
    public DongBoCoQuanQuanLy findFirstById(long id);
    Page<DongBoCoQuanQuanLy> findByMaContainingAndDaXoaOrTenDonViContainingAndDaXoa(String ma, int daXoaMa, String ten, int daXoaTen, Pageable pageable);
    Page<DongBoCoQuanQuanLy> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaContainingAndDaXoaOrTenDonViContainingAndDaXoa(String ma, int daXoaMa, String ten, int daXoaTen);
    List<DongBoCoQuanQuanLy> findByDaXoa(int daXoa);
}
