package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.QuocTich;

import java.util.List;

@Repository
public interface QuocTichRepository extends CrudRepository<QuocTich, Long> {
    public QuocTich findFirstByMaQuocGia(String ma);

    public QuocTich findFirstByMaQuocGiaAndDaXoa(String ma, int daXoa);

    public QuocTich findFirstByTenQuocGiaDayDuBangTiengViet(String ten);

    QuocTich findByIdOrMaQuocGiaAndDaXoa(long id, String ma, int daXoa);

    List<QuocTich> findByDaXoa(int daXoa);

    public QuocTich findFirstById(long id);

}
