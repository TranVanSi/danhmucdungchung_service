package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DanhMucGiayTo;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DanhMucGiayToRepository extends CrudRepository<DanhMucGiayTo, Long> {
    public DanhMucGiayTo findFirstByMaAndDaXoa(String ma, int daXoa);

    public DanhMucGiayTo findFirstById(long id);

    List<DanhMucGiayTo> findByMaContainingAndDaXoaOrTenContainingAndDaXoa(String ma, int daXoaMa, String ten, Pageable pageable, int daXoa);

    List<DanhMucGiayTo> findAllByDaXoa(int daXoa, Pageable pageable);

    BigInteger countByMaContainingAndDaXoaOrTenContainingAndDaXoa(String ma, int daXoaMa, String ten, int daXoa);
}
