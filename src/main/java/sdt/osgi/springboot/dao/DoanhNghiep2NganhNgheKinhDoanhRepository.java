package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DoanhNghiep2NganhNgheKinhDoanh;

import java.util.List;

@Repository
public interface DoanhNghiep2NganhNgheKinhDoanhRepository extends CrudRepository<DoanhNghiep2NganhNgheKinhDoanh, Long> {
    List<DoanhNghiep2NganhNgheKinhDoanh> findByDoanhNghiep_Id(long doanhNghiepId);
    void deleteByDoanhNghiep_Id(long doanhNghiepId);

}
