package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.SanGolf;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface SanGolfRepository extends CrudRepository<SanGolf, Long> {
    public SanGolf findFirstByMaAndDaXoa(String ma, int daXoa);

    public SanGolf findFirstById(long id);

    Page<SanGolf> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<SanGolf> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<SanGolf> findByDaXoa(int daXoa);
}
