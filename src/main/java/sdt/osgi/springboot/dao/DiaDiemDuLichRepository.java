package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DiaDiemDuLich;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DiaDiemDuLichRepository extends CrudRepository<DiaDiemDuLich, Long> {
    public DiaDiemDuLich findFirstByMaAndDaXoa(String ma, int daXoa);

    public DiaDiemDuLich findFirstById(long id);

    Page<DiaDiemDuLich> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<DiaDiemDuLich> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<DiaDiemDuLich> findByDaXoa(int daXoa);
}
