package sdt.osgi.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.LoaiDoanhNghiep;

import java.util.List;

@Repository
public interface LoaiDoanhNghiepRepository extends CrudRepository<LoaiDoanhNghiep, Long> {
    public LoaiDoanhNghiep findFirstByMaAndDaXoa(String ma, int daXoa);

    public LoaiDoanhNghiep findFirstByTen(String ten);

    LoaiDoanhNghiep findByIdOrMaAndDaXoa(long id, String ma, int daXoa);

    LoaiDoanhNghiep findFirstByMa(String ma);

    LoaiDoanhNghiep findFirstById(long id);

    List<LoaiDoanhNghiep> findByDaXoa(int daXoa);
}
