package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.CapCoQuanQuanLy;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface CapCoQuanQuanLyRepository extends CrudRepository<CapCoQuanQuanLy, Long> {
    public CapCoQuanQuanLy findFirstByMaAndDaXoa(String ma, int daXoa);

    public CapCoQuanQuanLy findFirstById(long id);

    Page<CapCoQuanQuanLy> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    Page<CapCoQuanQuanLy> findAllByDaXoa(Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<CapCoQuanQuanLy> findByDaXoa(int daXoa);
}
