package sdt.osgi.springboot.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sdt.osgi.springboot.model.DoiTuongThucHien;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface DoiTuongThucHienRepository extends CrudRepository<DoiTuongThucHien, Long> {
    public DoiTuongThucHien findFirstByMaAndDaXoa(String ma, int daXoa);
    public DoiTuongThucHien findFirstById(long id);
    Page<DoiTuongThucHien> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);
    Page<DoiTuongThucHien> findAllByDaXoa(Pageable pageable, int daXoa);
    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);
    List<DoiTuongThucHien> findByDaXoa(int daXoa);
}
