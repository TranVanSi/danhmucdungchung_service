package sdt.osgi.springboot.dao;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sdt.osgi.springboot.model.NhomThuTucHanhChinh;

import java.math.BigInteger;
import java.util.List;

@Repository
@Transactional
public interface NhomThuTucHanhChinhRepository extends CrudRepository<NhomThuTucHanhChinh, Long> {

    public NhomThuTucHanhChinh findFirstByMaAndDaXoa(String ma, int daXoa);

    public NhomThuTucHanhChinh findFirstById(long id);

    List<NhomThuTucHanhChinh> findByChaIdAndDaXoa(long chaId, int daXoa);
    List<NhomThuTucHanhChinh> findByChaMaAndDaXoa(String chaMa, int daXoa);

    List<NhomThuTucHanhChinh> findByLoaiNhomTTHCAndDaXoa(int loaiId, int daXoa);

    Page<NhomThuTucHanhChinh> findAllByDaXoa(Pageable pageable, int daXoa);

    Page<NhomThuTucHanhChinh> findByMaContainingOrTenContainingAndDaXoa(String ma, String ten, Pageable pageable, int daXoa);

    BigInteger countByMaContainingOrTenContainingAndDaXoa(String ma, String ten, int daXoa);

    List<NhomThuTucHanhChinh> findByDaXoa(int daXoa);
    Integer countByLoaiNhomTTHC(int loaiNhom);
    void deleteNhomThuTucHanhChinhsByLoaiNhomTTHC(int loaiNhom);

}
