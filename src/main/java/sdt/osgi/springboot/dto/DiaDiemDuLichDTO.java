package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DiaDiemDuLichDTO {
    private long id;
    private String ma;
    private String ten;
    private String tenKhac;
    private String diaChi;
    private String netVanHoa;
    private String anh;
    private String diemNoiBat;
    private int dichVuVanChuyen;
    private String dienTich;
    private String hinhThucDuLich;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
