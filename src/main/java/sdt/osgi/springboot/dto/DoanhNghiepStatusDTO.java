package sdt.osgi.springboot.dto;

public enum DoanhNghiepStatusDTO {
    TN(0),
    FDI(1);


    private final Integer status;

    private DoanhNghiepStatusDTO(int value) {
        this.status = value;
    }

    public int value() {
        return this.status;
    }
    @Override
    public String toString() {
        return status.toString();
    }

}
