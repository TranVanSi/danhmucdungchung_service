package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DuAnDauTuDTO {
    private long id;
    private String ma;
    private String tenDuAn;
    private String tongMucDauTu;
    private String diaDiem;
    private String quyMoDuAn;
    private String vatTuThietBi;
    private String duKienTienDo;
    private String hoanVon;
    private String dienTichChiemDung;
    private String soDienThoai;
    private String phanBoVon;
    private String uuDai;
    private String anh;
    private String url;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
