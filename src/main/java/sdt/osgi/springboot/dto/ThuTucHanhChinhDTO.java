package sdt.osgi.springboot.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.NhomThuTucHanhChinh;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ThuTucHanhChinhDTO {
    private long id;
    private String ma;
    private String ten;
    private String maNiemYet;
    private String tenRutGon;
    private String maQuyTrinh;
    private long nhomThuTucHanhChinhId;
    private int mucDoDvc;
    private String trinhTuThucHien;
    private String cachThucHien;
    private String canCuPhapLy;
    private String yeuCauDieuKienThucHien;
    private long capCoQuanThucHienId;
    private String thanhPhanHoSo;
    private int soBoHoSo;
    private String bieuMauDinhKem;
    private byte trangThai;
    private String soQdCongBo;
    private String ngayBanHanhQd;
    private String ngayApDung;
    private String phi;
    private String lePhi;
    private String thoiHanGiaiQuyet;
    private String coQuanThucHien;
    private int soNgayXuLy;
    private int soNgayNiemYet;
    private String ketQuaThucHien;
    private String doiTuongThucHien;
    private String maGiayToKemTheo;
    private String tenNhomThuTucHanhChinh;

    private String nguoiTao;
    private String nguoiSua;


}
