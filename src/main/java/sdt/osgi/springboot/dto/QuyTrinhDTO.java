package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class QuyTrinhDTO {
    private long id;
    private String ma;
    private String ten;
    private String maHinhThucVanBan;
    private Long coQuanId;
    private Long capVanBanId;
    private String urlFileMoTa;
    private int daXoa;
    private String tenCoQuan;
    private String maThuTucHanhChinh;
    private String tenThuTucHanhChinh;
    private String nguoiTao;
    private String nguoiSua;


}
