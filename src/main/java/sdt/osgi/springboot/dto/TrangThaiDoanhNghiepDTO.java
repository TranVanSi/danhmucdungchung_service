package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sdt.osgi.springboot.model.Auditable;
import sdt.osgi.springboot.model.DoanhNghiep;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TrangThaiDoanhNghiepDTO extends Auditable<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String ma;

    private String nguoiTao;
    private String nguoiSua;
    private String daXoa;

}
