package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class KhuNghiDuongDTO {
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String loaiBietThu;
    private int dichVuSpa;
    private int trungTamHoiNghi;
    private String phongCachThietKe;
    private int soLuongPhong;
    private int dichVuVeMayBay;
    private int dichVuTaxi;
    private int dichVuDuLich;
    private int dichVuNhaHang;
    private String giaiThuong;
    private String giaTrungBinh;
    private String url;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
