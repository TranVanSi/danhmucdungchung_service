package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sdt.osgi.springboot.util.TrangThaiDongBo;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LichSuDongBoDTO {

    private Long id;
    private String lopDongBo;
    private Date thoiGianXoaDuLieuCu;
    private Date thoiGianBatDauDongBo;
    private Date thoiGianKetThucDongBo;
    private int trangThaiDongBo;
    private int soLuongDongBo;
    private int soLuongTruocDongBo;

}
