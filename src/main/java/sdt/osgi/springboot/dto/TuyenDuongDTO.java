package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TuyenDuongDTO {
    private long id;
    private String ma;
    private String ten;
    private long quanHuyenId;
    private long phuongXaId;
    private String thonPho;
    private Long chaId;
    private String diemDau;
    private String diemCuoi;
    private Float chieuDai;
    private Float chieuRongNen;
    private Float chieuRongMat;
    private String nguoiTao;
    private String nguoiSua;


}
