package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DoanhNghiepDTO {
    private long id;
    private String tenDoanhNghiep;
    private String maDoanhNghiep;
    private String maSoThue;
    private String diaChiDoanhNghiep;
    private Long diaChiDoanhNghiepTinhId;
    private Long diaChiDoanhNghiepHuyenId;
    private Long diaChiDoanhNghiepXaId;
    private String vonDieuLe;
    private long trangThaiDoanhNghiepId;
    private String dienThoaiDoanhNghiep;
    private String email;
    private String hoTenNdd;
    private String ngaySinhNdd;
    private String cmndNguoiDaiDien;
    private String ngayCapCmndNdd;
    private Long noiCapCmndNddId;
    private String chuSoHuu;
    private long loaiHinhDoanhNghiepId;
    private Integer soLaoDong;
    private String danhSachThanhVienGopVon;
    private String danhSachCoDong;
    private int loaiDoanhNghiepEnum;
    private String diaChiGoogleMap;
    private int daXoa;
    private String nguoiTao;
    private String nguoiSua;
}
