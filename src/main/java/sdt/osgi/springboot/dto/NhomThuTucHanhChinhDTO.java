package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class NhomThuTucHanhChinhDTO {
    private long id;
    private String ma;
    private String ten;
    private long chaId;
    private byte capCQQLId;
    private int loaiNhomTTHC;
    private String chaMa;

    private String nguoiTao;
    private String nguoiSua;


}
