package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DongBoCoQuanQuanLyDTO {
    private long id;
    private String ma;
    private String tenDonVi;
    private int capDonVi;
    private int loaiDonVi;
}
