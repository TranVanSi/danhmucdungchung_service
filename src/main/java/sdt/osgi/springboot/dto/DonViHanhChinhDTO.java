package sdt.osgi.springboot.dto;


import lombok.Data;

@Data
public class DonViHanhChinhDTO {
    private long id;
    private String ma;
    private String ten;
    private Long chaId;
    private String chaMa;
    private long capDonViHanhChinhId;
    private String maBuuCuc;
    private String tenCapDonViHanhChinh;

    private String nguoiTao;
    private String nguoiSua;
}
