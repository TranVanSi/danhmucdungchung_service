package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Column;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DiTichLichSuDTO {
    private long id;
    private String ma;
    private String ten;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String lichSuHinhThanh;
    private String quaTrinhPhatTrien;
    private String loTrinh;
    private String ynghiaLichSu;
    private String viTri;
    private String capDiTichLichSu;
    private String url;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
