package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DanTocDTO {
    private long id;
    private String ma;
    private String ten;
    private int thieuSo;
    private String tenKhac;

    private String nguoiTao;
    private String nguoiSua;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;


}
