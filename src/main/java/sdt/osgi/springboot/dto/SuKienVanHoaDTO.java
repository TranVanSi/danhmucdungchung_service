package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SuKienVanHoaDTO {
    private long id;
    private String ma;
    private String ten;
    private String anh;
    private String netDacTrung;
    private String diaDiem;
    private String thoiGian;
    private String dichVuKemTheo;
    private String lichSuHinhThanh;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
