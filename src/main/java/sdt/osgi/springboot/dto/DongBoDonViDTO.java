package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DongBoDonViDTO {
    private long id;
    private String ma;
    private String maDonVi;
    private String tenDonVi;
    private String capDonVi;
    private String idCoQuanChuQuan;
    private String diaChi;
    int daXoa;
}
