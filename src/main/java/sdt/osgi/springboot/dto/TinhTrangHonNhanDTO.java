package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TinhTrangHonNhanDTO {
    private long id;
    private String ma;
    private String ten;
    private String ghiChu;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;
    private String nguoiTao;
    private String nguoiSua;


}
