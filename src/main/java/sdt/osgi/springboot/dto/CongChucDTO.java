package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CongChucDTO {
    private long id;
    private String ma;
    private String hoVaTen;
    private String ngaySinh;
    private String soCMND;
    private String ngayCapCMND;
    private long noiCapId;
    private long gioiTinhId;
    private String anh;
    private String dienThoaiDiDong;
    private String email;
    private int daXoa;
}
