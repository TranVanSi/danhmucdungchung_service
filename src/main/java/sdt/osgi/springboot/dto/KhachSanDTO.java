package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class KhachSanDTO {
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private int soLuongPhong;
    private int soTang;
    private String gioNhanPhong;
    private String gioTraPhong;
    private int choDauXe;
    private int dichVuTaxi;
    private int hoTroXeLan;
    private int phongHutThuoc;
    private int wifiTrongPhong;
    private int dichVuDoiTien;
    private String url;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
