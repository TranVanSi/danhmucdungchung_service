package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class NhaHangDTO {
    private long id;
    private String ma;
    private String ten;
    private String dienTich;
    private String diaChi;
    private String soDienThoai;
    private String anh;
    private String dacSan;
    private int soLuongBep;
    private int soLuongPhucVu;
    private int soLuongBan;
    private int soLuongGhe;
    private String dichVuPhongRieng;
    private int sucChuaPhongRieng;
    private int dichVuTaxi;
    private String phongCachNhaHang;
    private String url;
    private String diaChiGoogleMap;
    private int daXoa;

    private String nguoiTao;
    private String nguoiSua;



}
