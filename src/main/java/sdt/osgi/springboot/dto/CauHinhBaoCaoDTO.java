package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CauHinhBaoCaoDTO {


    private long id;
    private String tenDanhMuc;
    private String tenBang;
    private String tenNguonDongBo;
    private Long tongDuLieu;
    private String url;

    private String nguoiTao;
    private String nguoiSua;

}
