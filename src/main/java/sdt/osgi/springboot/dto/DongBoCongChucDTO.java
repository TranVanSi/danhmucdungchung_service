package sdt.osgi.springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DongBoCongChucDTO {
    private long id;
    private String ma;
    private String hoTen;
    private String dienThoai;
    private String email;
    private String gioiTinh;
    private String ngaySinh;
    private String soCmnd;
    private String ngayCapCmnd;
    private int idChucVu;
    private String chucVu;
    private int idDonVi;
    private String donVi;
    private int idPhongBan;
    private String phongBan;
    private String url;
    int daXoa;
}
