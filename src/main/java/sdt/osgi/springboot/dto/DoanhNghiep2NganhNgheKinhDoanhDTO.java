package sdt.osgi.springboot.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import sdt.osgi.springboot.model.DoanhNghiep;
import sdt.osgi.springboot.model.NganhNgheKinhDoanh;

@Data
@Builder
@Accessors(chain = true)
public class DoanhNghiep2NganhNgheKinhDoanhDTO {

    private Long id;
    private DoanhNghiep doanhNghiep;
    private NganhNgheKinhDoanh nganhNgheKinhDoanh;
}
