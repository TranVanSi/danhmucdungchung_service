package sdt.osgi.springboot.dto;

import lombok.Data;

import java.util.Date;

@Data
public class LichSuTruyCapDTO {

    private long id;
    private long nguoiDungId;
    private String ten;
    private String diaChiDangNhap;
    private Date thoiGianDangNhap;
    private String mayChu;
    private String tenMien;
    private long coQuanQuanLyId;
    private long congChucId;
    private String nguoiTao;
    private String nguoiSua;

    public LichSuTruyCapDTO() {
    }
}
