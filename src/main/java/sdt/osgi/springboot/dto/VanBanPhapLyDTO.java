package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class VanBanPhapLyDTO {
    private long id;
    private String ma;
    private String tenNguoiKy;
    private String soKyHieu;
    private String ngayBanHanh;
    private String ngayHetHan;
    private String tenCoQuanBanHanh;
    private String tenLinhVuc;
    private String tenLoaiVanBan;
    private String trichYeu;
    private String noiDung;
    private String vanBanDinhKem;

    private String nguoiTao;
    private String nguoiSua;
}
