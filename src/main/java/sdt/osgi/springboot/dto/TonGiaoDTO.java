package sdt.osgi.springboot.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TonGiaoDTO {
    private long id;
    private String ma;
    private String ten;
    private String nguoiTao;
    private String nguoiSua;
    private String cacToChucTonGiaoChinh;
    private String qdBanHanhSuaDoi;
    private Date qdNgayBanHanh;
    private String qdCoQuanBanHanh;


}
