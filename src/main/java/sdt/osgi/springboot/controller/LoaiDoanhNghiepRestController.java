package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LoaiDoanhNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.LoaiDoanhNghiepService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/loaidoanhnghiep")
@RequiredArgsConstructor
public class LoaiDoanhNghiepRestController {

    private final LoaiDoanhNghiepService loaiDoanhNghiepService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return loaiDoanhNghiepService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return loaiDoanhNghiepService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return loaiDoanhNghiepService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody LoaiDoanhNghiepDTO loaiDoanhNghiepDTO) throws Exception {
        return loaiDoanhNghiepService.insert(loaiDoanhNghiepDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody LoaiDoanhNghiepDTO loaiDoanhNghiepDTO) throws Exception {
        return loaiDoanhNghiepService.update(loaiDoanhNghiepDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return loaiDoanhNghiepService.delete(id);
    }
}
