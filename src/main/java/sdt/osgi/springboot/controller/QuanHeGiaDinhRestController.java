package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.QuanHeGiaDinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.QuanHeGiaDinhService;

@RestController
@RequestMapping("/api/v1/quanhegiadinh")
@RequiredArgsConstructor
public class QuanHeGiaDinhRestController {

    private final QuanHeGiaDinhService quanHeGiaDinhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return quanHeGiaDinhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return quanHeGiaDinhService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return quanHeGiaDinhService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody QuanHeGiaDinhDTO quanHeGiaDinhDTO) throws Exception {
        return quanHeGiaDinhService.insert(quanHeGiaDinhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody QuanHeGiaDinhDTO quanHeGiaDinhDTO) throws Exception {
        return quanHeGiaDinhService.update(quanHeGiaDinhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return quanHeGiaDinhService.delete(id);
    }
}
