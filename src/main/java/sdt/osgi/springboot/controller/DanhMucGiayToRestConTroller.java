package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DanhMucGiayToDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DanhMucGiayToService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/danhmucgiayto")
@RequiredArgsConstructor
public class DanhMucGiayToRestConTroller {

    private final DanhMucGiayToService danhMucGiayToService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return danhMucGiayToService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return danhMucGiayToService.findByMa(ma);
    }

    @GetMapping(value = {"/search", "/", ""})
    private Response findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                                           @RequestParam(value = "offset", defaultValue = "0") int offset,
                                           @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return danhMucGiayToService.findAll(ma, offset, limit);
    }

    @GetMapping(value = {"/count"})
    private String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search) {
        return danhMucGiayToService.countByMaOrTen(search);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DanhMucGiayToDTO danhMucGiayToDTO) throws Exception {
        return danhMucGiayToService.insert(danhMucGiayToDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DanhMucGiayToDTO danhMucGiayToDTO) throws Exception {
        return danhMucGiayToService.update(danhMucGiayToDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return danhMucGiayToService.delete(id);
    }
}
