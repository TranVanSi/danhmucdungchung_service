package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DongBoCapDonViService;

@RestController
@RequestMapping("/api/v1/capdonvi")
@RequiredArgsConstructor
public class DongBoCapDonViConTroller {

    private final DongBoCapDonViService dongBoCapDonViService;

    @GetMapping(value = {"/search", "/", ""})
    public Response findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                            @RequestParam(value = "offset", defaultValue = "0") int offset,
                            @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return dongBoCapDonViService.findAll(ma, offset, limit);
    }

    @GetMapping(value = {"/count"})
    public String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search) {
        return dongBoCapDonViService.countByMaOrTen(search);
    }

}
