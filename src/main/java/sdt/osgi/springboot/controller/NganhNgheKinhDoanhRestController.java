package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.NganhNgheKinhDoanhService;
import sdt.osgi.springboot.util.StringUtils;
import sdt.osgi.springboot.viewdto.NganhNgheKinhDoanhViewDTO;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/nganhnghekinhdoanh")
@RequiredArgsConstructor
public class NganhNgheKinhDoanhRestController {

    private final NganhNgheKinhDoanhService nganhNgheKinhDoanhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return nganhNgheKinhDoanhService.findById(id);
    }

    @GetMapping(value = "/cap/{cap}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCap(@PathVariable(value = "cap") byte cap) {
        return nganhNgheKinhDoanhService.findByCap(cap);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return nganhNgheKinhDoanhService.findAll();
    }

    @GetMapping("/findByIdIn")
    public List<NganhNgheKinhDoanhViewDTO> findByIdIn(@RequestParam(value = "nganhNgheKinhDoanhIds", defaultValue = "") String nganhNgheKinhDoanhIds){

        if (nganhNgheKinhDoanhIds.length() > 0) {
            List<Long> ids = StringUtils.convertStringToLongs(nganhNgheKinhDoanhIds);
            return nganhNgheKinhDoanhService.findByNganhNgheKinhDoanhIn(ids);
        }
        return new ArrayList<>();
    }


}
