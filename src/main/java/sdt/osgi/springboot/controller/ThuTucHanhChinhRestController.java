package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.ThuTucHanhChinhDTO;
import sdt.osgi.springboot.service.ThuTucHanhChinhService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/tthc")
@RequiredArgsConstructor
public class ThuTucHanhChinhRestController {

    private final ThuTucHanhChinhService thuTucHanhChinhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return thuTucHanhChinhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return thuTucHanhChinhService.findByMa(ma);
    }

    @GetMapping(value = {"/search", "/", ""}, produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                             @RequestParam(value = "offset", defaultValue = "0") int offset,
                             @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return thuTucHanhChinhService.findAll(ma, offset, limit);
    }

    @GetMapping(value = {"/count"}, produces = MediaType.APPLICATION_JSON_VALUE)
    private String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search) {
        return thuTucHanhChinhService.countByMaOrTen(search);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody ThuTucHanhChinhDTO thuTucHanhChinhDTO) throws Exception {
        return thuTucHanhChinhService.insert(thuTucHanhChinhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody ThuTucHanhChinhDTO thuTucHanhChinhDTO) throws Exception {
        return thuTucHanhChinhService.update(thuTucHanhChinhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return thuTucHanhChinhService.delete(id);
    }
}
