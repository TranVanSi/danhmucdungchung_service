package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sdt.osgi.springboot.dto.ResponseDTO;
import sdt.osgi.springboot.service.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/upload")
public class UploadFileRestController {
    private final UploadFileService uploadFileService;
    private final TinhTrangHonNhanService tinhTrangHonNhanService;
    private final TonGiaoService tonGiaoService;
    private final QuocTichService quocTichService;
    private final LoaiVanBanService loaiVanBanService;
    private final DoKhanVanBanService doKhanVanBanService;
    private final DoMatVanBanService doMatVanBanService;
    private final LinhVucVanBanService linhVucVanBanService;
    private final LoaiDoanhNghiepService loaiDoanhNghiepService;
    private final SinhTracHocService sinhTracHocService;
    private final QuanHeGiaDinhService quanHeGiaDinhService;
    private final TuyenDuongService tuyenDuongService;
    private final TrinhDoChuyeMonKyThuatService trinhDoChuyeMonKyThuatService;
    private final NgheNghiepService ngheNghiepService;
    private final CapCoQuanQuanLyService capCoQuanQuanLyService;
    private final QuyTrinhService quyTrinhService;
    private final DanhMucGiayToService danhMucGiayToService;
    private final NhomThuTucHanhChinhService nhomThuTucHanhChinhService;
    private final LoaiToChucCaNhanService loaiToChucCaNhanService;
    private final ThuTucHanhChinhService thuTucHanhChinhService;
    private final CapDonViHanhChinhService capDonViHanhChinhService;
    private final DonViHanhChinhService donViHanhChinhService;
    private final LinhVucKinhDoanhService linhVucKinhDoanhService;
    private final SanGolfService sanGolfService;
    private final BaoTangService baoTangService;
    private final NhaHangService nhaHangService;
    private final KhuNghiDuongService khuNghiDuongService;
    private final KhuCongNghiepService khuCongNghiepService;
    private final KhachSanService khachSanService;
    private final DuAnDauTuService duAnDauTuService;
    private final DiaDiemDuLichService diaDiemDuLichService;
    private final DiTichLichSuService diTichLichSuService;
    private final SuKienVanHoaService suKienVanHoaService;
    private final VanBanPhapLyService vanBanPhapLyService;
    private final DoanhNghiepService doanhNghiepService;
    @RequestMapping(value = "/tthc", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileThuTucHanhChinh(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return thuTucHanhChinhService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/loaitochuccanhan", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileLoaiToChucCaNhan(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return loaiToChucCaNhanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/nhomtthc", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileNhomTTHC(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return nhomThuTucHanhChinhService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/danhmucgiayto", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileDanhMucGiayTo(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return danhMucGiayToService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/quytrinh", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileQuyTrinh(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return quyTrinhService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/capcqql", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileCapCQQL(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return capCoQuanQuanLyService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/dantoc", method = RequestMethod.POST)
    public ResponseDTO uploadDanTocExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return uploadFileService.saveAllDanToc(file, nguoiTao);
    }

    @RequestMapping(value = "/tinhtranghonnhan", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileTinhTrangHonNhan(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return tinhTrangHonNhanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/tongiao", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileTonGiao(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return tonGiaoService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/quoctich", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileQuocTich(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return quocTichService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/loaivanban", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileLoaiVanBan(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return loaiVanBanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/dokhan", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileDoKhanVanBan(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return doKhanVanBanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/domat", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileDoMatVanBan(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return doMatVanBanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/linhvucvanban", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileLinhVucVanBan(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return linhVucVanBanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/loaidoanhnghiep", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileLoaiDoanhNghiep(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return loaiDoanhNghiepService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/sinhtrachoc", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileSinhTracHoc(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return sinhTracHocService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/quanhegiadinh", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileQuanHeGiaDinh(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return quanHeGiaDinhService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/tuyenduong", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileTuyenDuong(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return tuyenDuongService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/trinhdochuyenmonkythuat", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileTrinhDoCMKT(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return trinhDoChuyeMonKyThuatService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/nghenghiep", method = RequestMethod.POST)
    public ResponseDTO uploadExcelFileNgheNghiep(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return ngheNghiepService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/capdvhc", method = RequestMethod.POST)
    public ResponseDTO importDataCapDVHCExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return capDonViHanhChinhService.saveAllCapDonViHanhChinh(file, nguoiTao);
    }

    @RequestMapping(value = "/dvhc", method = RequestMethod.POST)
    public ResponseDTO importDataDVHCExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return donViHanhChinhService.saveAllDonViHanhChinh(file, nguoiTao);
    }

    @RequestMapping(value = "/linhvuckinhdoanh", method = RequestMethod.POST)
    public ResponseDTO importDataLinhVucKinhDOanhExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return linhVucKinhDoanhService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/sangolf", method = RequestMethod.POST)
    public ResponseDTO importDataSanGolfExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return sanGolfService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/baotang", method = RequestMethod.POST)
    public ResponseDTO importDataBaoTangExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return baoTangService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/nhahang", method = RequestMethod.POST)
    public ResponseDTO importDataNhaHangExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return nhaHangService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/khunghiduong", method = RequestMethod.POST)
    public ResponseDTO importDataKhuNghiDuongExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return khuNghiDuongService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/khucongnghiep", method = RequestMethod.POST)
    public ResponseDTO importDataKhuCongNghiepExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return khuCongNghiepService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/khachsan", method = RequestMethod.POST)
    public ResponseDTO importDataKhachSanExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return khachSanService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/duandautu", method = RequestMethod.POST)
    public ResponseDTO importDataDuAnDauTuExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return duAnDauTuService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/diadiemdulich", method = RequestMethod.POST)
    public ResponseDTO importDataDiaDiemDuLichExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return diaDiemDuLichService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/ditichlichsu", method = RequestMethod.POST)
    public ResponseDTO importDataDiTichLichSuExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return diTichLichSuService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/sukienvanhoa", method = RequestMethod.POST)
    public ResponseDTO importDataSuKienVanHoaExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return suKienVanHoaService.uploadExcelFile(file, nguoiTao);
    }

    @RequestMapping(value = "/vanbanphaply", method = RequestMethod.POST)
    public ResponseDTO importDataVanBanPhapLyExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return vanBanPhapLyService.uploadExcelFile(file, nguoiTao);
    }
    @RequestMapping(value = "/doanhnghiep", method = RequestMethod.POST)
    public ResponseDTO importDataDoanhNghiepExcelFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("nguoiTao") String nguoiTao) throws Exception {
        return doanhNghiepService.uploadExcelFile(file, nguoiTao);
    }


}
