package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DoMatVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DoMatVanBanService;

@RestController
@RequestMapping("/api/v1/domat")
@RequiredArgsConstructor
public class DoMatVanBanRestConTroller {

    private final DoMatVanBanService doMatVanBanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return doMatVanBanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return doMatVanBanService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DoMatVanBanDTO doMatVanBanDTO) throws Exception {
        return doMatVanBanService.insert(doMatVanBanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DoMatVanBanDTO doMatVanBanDTO) throws Exception {
        return doMatVanBanService.update(doMatVanBanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return doMatVanBanService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return doMatVanBanService.findAll();
    }
}
