package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DiaDiemDuLichDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DiaDiemDuLichService;

@RestController
@RequestMapping("/api/v1/diadiemdulich")
@RequiredArgsConstructor
public class DiaDiemDuLichRestConTroller {

    private final DiaDiemDuLichService diaDiemDuLichService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return diaDiemDuLichService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return diaDiemDuLichService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DiaDiemDuLichDTO diaDiemDuLichDTO) {
        return diaDiemDuLichService.insert(diaDiemDuLichDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DiaDiemDuLichDTO diaDiemDuLichDTO) {
        return diaDiemDuLichService.update(diaDiemDuLichDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return diaDiemDuLichService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return diaDiemDuLichService.findAll();
    }
}
