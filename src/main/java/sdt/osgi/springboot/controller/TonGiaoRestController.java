package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.TonGiaoDTO;
import sdt.osgi.springboot.service.TonGiaoService;

@RestController
@RequestMapping("/api/v1/tongiao")
@RequiredArgsConstructor
public class TonGiaoRestController {

    private final TonGiaoService tonGiaoService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return tonGiaoService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return tonGiaoService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return tonGiaoService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody TonGiaoDTO tonGiaoDTO) throws Exception {
        return tonGiaoService.insert(tonGiaoDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody TonGiaoDTO tonGiaoDTO) throws Exception {
        return tonGiaoService.update(tonGiaoDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return tonGiaoService.delete(id);
    }
}
