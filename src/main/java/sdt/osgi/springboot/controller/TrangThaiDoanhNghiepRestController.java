package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.TrangThaiDoanhNghiepService;

@RestController
@RequestMapping("/api/v1/trangthaidoanhnghiep")
@RequiredArgsConstructor
public class TrangThaiDoanhNghiepRestController {

    private final TrangThaiDoanhNghiepService trangThaiDoanhNghiepService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return trangThaiDoanhNghiepService.findById(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return trangThaiDoanhNghiepService.findAll();
    }

}
