package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.NhomThuTucHanhChinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.NhomThuTucHanhChinhService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/nhomtthc")
@RequiredArgsConstructor
public class NhomThuTucHanhChinhRestController {

    private final NhomThuTucHanhChinhService nhomThuTucHanhChinhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return nhomThuTucHanhChinhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return nhomThuTucHanhChinhService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return nhomThuTucHanhChinhService.findAll();
    }


    @GetMapping(value = "/child/{chaId}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaId(@PathVariable(value = "chaId") long chaId) {
        return nhomThuTucHanhChinhService.findByChaId(chaId);
    }

    @GetMapping(value = "/chaMa/{chaMa}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaMa(@PathVariable(value = "chaMa") String chaMa) {
        return nhomThuTucHanhChinhService.findByChaMa(chaMa);
    }

    @GetMapping(value = "/type/{loai}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaId(@PathVariable(value = "loai") int loai) {
        return nhomThuTucHanhChinhService.findByLoaiNhom(loai);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody NhomThuTucHanhChinhDTO nhomThuTucHanhChinhDTO) throws Exception {
        return nhomThuTucHanhChinhService.insert(nhomThuTucHanhChinhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody NhomThuTucHanhChinhDTO nhomThuTucHanhChinhDTO) throws Exception {
        return nhomThuTucHanhChinhService.update(nhomThuTucHanhChinhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return nhomThuTucHanhChinhService.delete(id);
    }
}
