package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.QuyTrinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.QuyTrinhService;
import java.util.List;

@RestController
@RequestMapping("/api/v1/quytrinh")
@RequiredArgsConstructor
public class QuyTrinhRestConTroller {

    private final QuyTrinhService quyTrinhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return quyTrinhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return quyTrinhService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return quyTrinhService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody QuyTrinhDTO quyTrinhDTO) throws Exception {
        return quyTrinhService.insert(quyTrinhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody QuyTrinhDTO quyTrinhDTO) throws Exception {
        return quyTrinhService.update(quyTrinhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return quyTrinhService.delete(id);
    }
}
