package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dvcqgdto.DVCDongBoCapDonViDTO;
import sdt.osgi.springboot.dvcservice.*;
import sdt.osgi.springboot.model.Status;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/dongbo")
public class DongBoRestController {
    private final DichVuCongQuocGiaService dichVuCongQuocGiaService;
    private final DichVuCongDanTocService dichVuCongDanTocService;
    private final DichVuCongTonGiaoService dichVuCongTonGiaoService;
    private final DichVuCongTinhTrangHonNhanService dichVuCongTinhTrangHonNhanService;
    private final DichVuCongDonViHanhChinhService dichVuCongDonViHanhChinhService;
    private final DichVuCongTrinhDoChuyenMonService dichVuCongTrinhDoChuyenMonService;
    private final DichVuCongDoKhanVanBanService dichVuCongDoKhanVanBanService;
    private final DichVuCongCapCoQuanThucHienService dichVuCongCapCoQuanThucHienService;
    private final DichVuCongNhomTTHCService dichVuCongNhomTTHCService;
    private final DichVuCongDoiTuongThucHienService dichVuCongDoiTuongThucHienService;
    private final DichVuCongThuTucHanhChinhService dichVuCongThuTucHanhChinhService;
    private final DVCDongBoCapDonViService dvcDongBoCapDonViService;
    private final DVCDongBoDonViService dvcDongBoDonViService;
    private final DVCDongBoPhongBanService dvcDongBoPhongBanService;
    private final CanBoCongChucService canBoCongChucService;
    private final DVCDongBoCongChucService dvcDongBoCongChucService;
    private final DichVuCongCoQuanQuanLyService dichVuCongCoQuanQuanLyService;

    @RequestMapping(value = "/tinhtranghonnhan", method = RequestMethod.POST)
    public Response dongBoTinhTrangHonNhan() throws Exception {
        dichVuCongTinhTrangHonNhanService.dongBoDuLieuTinhTrangHonNhan();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/tongiao", method = RequestMethod.POST)
    public Response dongBoTonGiao() throws Exception {
        dichVuCongTonGiaoService.dongBoDuLieuTonGiao();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/dantoc", method = RequestMethod.POST)
    public Response dongBoDanToc() throws Exception {
        dichVuCongDanTocService.dongBoDuLieuDanToc();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/quoctich", method = RequestMethod.POST)
    public Response dongBoQuocTich() throws Exception {
        dichVuCongQuocGiaService.dongBoDuLieuQuocGia();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/dokhan", method = RequestMethod.POST)
    public Response dongBoDoKhan() throws Exception {
        dichVuCongDoKhanVanBanService.dongBoDuLieuDoKhanVanBan();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/trinhdochuyenmonkythuat", method = RequestMethod.POST)
    public Response dongBoTrinhDoChuyenMonKyThuat() throws Exception {
        dichVuCongTrinhDoChuyenMonService.dongBoDuLieuTrinhDoChuyenMonKyThuat();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/tthc", method = RequestMethod.POST)
    public Response dongBoThuTucHanhChinh() throws Exception {
        dichVuCongDoiTuongThucHienService.dongBoDuLieuDoiTuongThucHien();
        dichVuCongCapCoQuanThucHienService.dongBoDuLieuCapCoQuanThucHien();
        dichVuCongNhomTTHCService.dongBoDuLieuNhomTTHCVoBoc();
        dichVuCongNhomTTHCService.dongBoDuLieuNhomTTHCChiTiet();
        dichVuCongThuTucHanhChinhService.dongBoDuLieuThuTucHanhChinh();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/donvihanhchinh", method = RequestMethod.POST)
    public Response dongBoDonViHanhChinh() throws Exception {
        dichVuCongDonViHanhChinhService.dongBoDuLieuDonViHanhChinhCapTinh();
        dichVuCongDonViHanhChinhService.dongBoDuLieuDonViHanhChinhCapHuyen();
        dichVuCongDonViHanhChinhService.dongBoDuLieuDonViHanhChinhCapXa();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/capdonvi", method = RequestMethod.POST)
    public Response dongBoCapDonVi() throws Exception {
        dvcDongBoCapDonViService.dongBoDuLieuCapDonVi();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/donvi", method = RequestMethod.POST)
    public Response dongBoDonVi() throws Exception {
        dvcDongBoDonViService.dongBoDuLieuDonVi();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/phongban", method = RequestMethod.POST)
    public Response dongBoPhongBan() throws Exception {
        dvcDongBoPhongBanService.dongBoDuLieuPhongBan();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/chucvu", method = RequestMethod.POST)
    public Response dongBoChucVu() throws Exception {
        canBoCongChucService.dongBoDuLieuChucVu();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/congchuc", method = RequestMethod.POST)
    public Response dongBoCongChuc() throws Exception {
        dvcDongBoCongChucService.dongBoDuLieuCongChuc();
        return new Response(Status.SUCCESS.value(), null);
    }

    @RequestMapping(value = "/coquanquanly", method = RequestMethod.POST)
    public Response dongBoCoQuanQuanLy() throws Exception {
        dichVuCongCoQuanQuanLyService.dongBoDuLieuCoQuanQuanLy();
        return new Response(Status.SUCCESS.value(), null);
    }
}
