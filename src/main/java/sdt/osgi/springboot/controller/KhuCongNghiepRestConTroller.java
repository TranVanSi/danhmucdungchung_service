package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.KhuCongNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.KhuCongNghiepService;

@RestController
@RequestMapping("/api/v1/khucongnghiep")
@RequiredArgsConstructor
public class KhuCongNghiepRestConTroller {

    private final KhuCongNghiepService khuCongNghiepService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return khuCongNghiepService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return khuCongNghiepService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody KhuCongNghiepDTO khuCongNghiepDTO) {
        return khuCongNghiepService.insert(khuCongNghiepDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody KhuCongNghiepDTO khuCongNghiepDTO) {
        return khuCongNghiepService.update(khuCongNghiepDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return khuCongNghiepService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return khuCongNghiepService.findAll();
    }
}
