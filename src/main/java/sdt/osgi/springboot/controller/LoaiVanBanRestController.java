package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LoaiVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.LoaiVanBanService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/loaivanban")
@RequiredArgsConstructor
public class LoaiVanBanRestController {

    private final LoaiVanBanService loaiVanBanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return loaiVanBanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return loaiVanBanService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return loaiVanBanService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody LoaiVanBanDTO loaiVanBanDTO) throws Exception {
        return loaiVanBanService.insert(loaiVanBanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody LoaiVanBanDTO loaiVanBanDTO) throws Exception {
        return loaiVanBanService.update(loaiVanBanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return loaiVanBanService.delete(id);
    }
}
