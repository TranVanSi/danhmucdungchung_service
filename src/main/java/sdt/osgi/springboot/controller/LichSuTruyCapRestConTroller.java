package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LichSuTruyCapDTO;
import sdt.osgi.springboot.service.LichSuTruyCapService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lichsutruycap")
@RequiredArgsConstructor
public class LichSuTruyCapRestConTroller {

    private final LichSuTruyCapService lichSuTruyCapService;

    @GetMapping("/id")
    private LichSuTruyCapDTO findById(@RequestParam(value = "id", defaultValue = "0") long id) {
        return lichSuTruyCapService.findById(id);
    }

    @GetMapping("/user")
    private List<LichSuTruyCapDTO> findByNguoiDungId(@RequestParam(value = "nguoiDungId", defaultValue = "0") long nguoiDungId) {
        return lichSuTruyCapService.findByNguoiDungId(nguoiDungId);
    }

    @GetMapping(value = {"/search", "/", ""})
    private List<LichSuTruyCapDTO> findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                                           @RequestParam(value = "offset", defaultValue = "0") int offset,
                                           @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return lichSuTruyCapService.findAll(ma, offset, limit);
    }

    @GetMapping(value = {"/count"})
    private String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search) {

        return lichSuTruyCapService.countByTen(search);
    }

    @PostMapping("/insert")
    private void insert(@RequestBody String jsonData) throws Exception {

        JSONObject jsonObject = new JSONObject(jsonData);
        JSONObject value = jsonObject.getJSONObject("data");
        LichSuTruyCapDTO lichSuTruyCapDTO = new ObjectMapper().readValue(value.toString(), LichSuTruyCapDTO.class);

        lichSuTruyCapService.insert(lichSuTruyCapDTO);
    }

    @PostMapping("/update")
    private void update(@RequestBody String jsonData) throws Exception {

        JSONObject jsonObject = new JSONObject(jsonData);
        JSONObject value = jsonObject.getJSONObject("data");
        LichSuTruyCapDTO lichSuTruyCapDTO = new ObjectMapper().readValue(value.toString(), LichSuTruyCapDTO.class);

        lichSuTruyCapService.update(lichSuTruyCapDTO);
    }

    @PostMapping("/delete")
    private void delete(@RequestBody String jsonData) throws Exception {

        JSONObject jsonObject = new JSONObject(jsonData);
        JSONObject value = jsonObject.getJSONObject("data");
        LichSuTruyCapDTO lichSuTruyCapDTO = new ObjectMapper().readValue(value.toString(), LichSuTruyCapDTO.class);

        lichSuTruyCapService.delete(lichSuTruyCapDTO);
    }
}
