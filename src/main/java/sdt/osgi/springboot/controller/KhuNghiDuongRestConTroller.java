package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.KhuNghiDuongDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.KhuNghiDuongService;

@RestController
@RequestMapping("/api/v1/khunghiduong")
@RequiredArgsConstructor
public class KhuNghiDuongRestConTroller {

    private final KhuNghiDuongService khuNghiDuongService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return khuNghiDuongService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return khuNghiDuongService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody KhuNghiDuongDTO khuNghiDuongDTO) {
        return khuNghiDuongService.insert(khuNghiDuongDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody KhuNghiDuongDTO khuNghiDuongDTO) {
        return khuNghiDuongService.update(khuNghiDuongDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return khuNghiDuongService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return khuNghiDuongService.findAll();
    }
}
