package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DuAnDauTuDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DuAnDauTuService;

@RestController
@RequestMapping("/api/v1/duandautu")
@RequiredArgsConstructor
public class DuAnDauTuRestConTroller {

    private final DuAnDauTuService duAnDauTuService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return duAnDauTuService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return duAnDauTuService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DuAnDauTuDTO duAnDauTuDTO) {
        return duAnDauTuService.insert(duAnDauTuDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DuAnDauTuDTO duAnDauTuDTO) {
        return duAnDauTuService.update(duAnDauTuDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return duAnDauTuService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return duAnDauTuService.findAll();
    }
}
