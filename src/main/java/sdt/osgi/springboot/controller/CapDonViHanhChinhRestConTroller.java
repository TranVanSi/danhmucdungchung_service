package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.CapDonViHanhChinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.CapDonViHanhChinhService;

@RestController
@RequestMapping("/api/v1/capdvhc")
@RequiredArgsConstructor
public class CapDonViHanhChinhRestConTroller {

    private final CapDonViHanhChinhService capDonViHanhChinhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return capDonViHanhChinhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return capDonViHanhChinhService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody CapDonViHanhChinhDTO capDonViHanhChinhDTO) throws Exception {
        return capDonViHanhChinhService.insert(capDonViHanhChinhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody CapDonViHanhChinhDTO capDonViHanhChinhDTO) throws Exception {
        return capDonViHanhChinhService.update(capDonViHanhChinhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return capDonViHanhChinhService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return capDonViHanhChinhService.findAll();
    }
}
