package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.SinhTracHocDTO;
import sdt.osgi.springboot.service.SinhTracHocService;

@RestController
@RequestMapping("/api/v1/sinhtrachoc")
@RequiredArgsConstructor
public class SinhTracHocRestController {

    private final SinhTracHocService sinhTracHocService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return sinhTracHocService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return sinhTracHocService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return sinhTracHocService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody SinhTracHocDTO sinhTracHocDTO) throws Exception {
        return sinhTracHocService.insert(sinhTracHocDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody SinhTracHocDTO sinhTracHocDTO) throws Exception {
        return sinhTracHocService.update(sinhTracHocDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return sinhTracHocService.delete(id);
    }
}
