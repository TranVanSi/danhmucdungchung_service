package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.TrinhDoChuyenMonKyThuatDTO;
import sdt.osgi.springboot.service.TrinhDoChuyeMonKyThuatService;

@RestController
@RequestMapping("/api/v1/trinhdochuyenmonkythuat")
@RequiredArgsConstructor
public class TrinhDoChuyenMonKyThuatRestController {

    private final TrinhDoChuyeMonKyThuatService trinhDoChuyenMonKyThuatService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return trinhDoChuyenMonKyThuatService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return trinhDoChuyenMonKyThuatService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return trinhDoChuyenMonKyThuatService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatDTO) throws Exception {
        return trinhDoChuyenMonKyThuatService.insert(trinhDoChuyenMonKyThuatDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody TrinhDoChuyenMonKyThuatDTO trinhDoChuyenMonKyThuatDTO) throws Exception {
        return trinhDoChuyenMonKyThuatService.update(trinhDoChuyenMonKyThuatDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return trinhDoChuyenMonKyThuatService.delete(id);
    }
}
