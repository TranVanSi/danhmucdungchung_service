package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.CapCoQuanThucHienDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.CapCoQuanThucHienService;

@RestController
@RequestMapping("/api/v1/capcqth")
@RequiredArgsConstructor
public class CapCoQuanThucHienRestConTroller {

    private final CapCoQuanThucHienService capCoQuanThucHienService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return capCoQuanThucHienService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return capCoQuanThucHienService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody CapCoQuanThucHienDTO capCoQuanThucHienDTO) throws Exception {
        return capCoQuanThucHienService.insert(capCoQuanThucHienDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody CapCoQuanThucHienDTO capCoQuanThucHienDTO) throws Exception {
        return capCoQuanThucHienService.update(capCoQuanThucHienDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return capCoQuanThucHienService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return capCoQuanThucHienService.findAll();
    }
}
