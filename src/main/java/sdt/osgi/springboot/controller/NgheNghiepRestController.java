package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.NgheNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.NgheNghiepService;

@RestController
@RequestMapping("/api/v1/nghenghiep")
@RequiredArgsConstructor
public class NgheNghiepRestController {

    private final NgheNghiepService ngheNghiepService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return ngheNghiepService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return ngheNghiepService.findByMa(ma);
    }


    @GetMapping(value = "/chaMa/{chaMa}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaMa(@PathVariable(value = "chaMa") String chaMa) {
        return ngheNghiepService.findByChaMa(chaMa);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return ngheNghiepService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody NgheNghiepDTO ngheNghiepDTO) throws Exception {
        return ngheNghiepService.insert(ngheNghiepDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody NgheNghiepDTO ngheNghiepDTO) throws Exception {
        return ngheNghiepService.update(ngheNghiepDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return ngheNghiepService.delete(id);
    }

    @GetMapping(value = "/listByChaMa/{chaMa}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findNgheNghiepByChaMa(@PathVariable(value = "chaMa") String chaMa) {
        return ngheNghiepService.findNgheNghiepByChaMa(chaMa);
    }

}
