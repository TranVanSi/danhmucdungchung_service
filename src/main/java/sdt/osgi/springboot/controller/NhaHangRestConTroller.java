package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.NhaHangDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.NhaHangService;

@RestController
@RequestMapping("/api/v1/nhahang")
@RequiredArgsConstructor
public class NhaHangRestConTroller {

    private final NhaHangService nhaHangService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return nhaHangService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return nhaHangService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody NhaHangDTO nhaHangDTO) {
        return nhaHangService.insert(nhaHangDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody NhaHangDTO nhaHangDTO) {
        return nhaHangService.update(nhaHangDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return nhaHangService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return nhaHangService.findAll();
    }
}
