package sdt.osgi.springboot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LinhVucVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.LinhVucVanBanService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/linhvucvanban")
@RequiredArgsConstructor
public class LinhVucVanBanRestController {

    private final LinhVucVanBanService linhVucVanBanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return linhVucVanBanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return linhVucVanBanService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return linhVucVanBanService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody LinhVucVanBanDTO linhVucVanBanDTO) throws Exception {
        return linhVucVanBanService.insert(linhVucVanBanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody LinhVucVanBanDTO linhVucVanBanDTO) throws Exception {
        return linhVucVanBanService.update(linhVucVanBanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return linhVucVanBanService.delete(id);
    }
}
