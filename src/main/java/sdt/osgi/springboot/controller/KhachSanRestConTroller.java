package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.KhachSanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.KhachSanService;

@RestController
@RequestMapping("/api/v1/khachsan")
@RequiredArgsConstructor
public class KhachSanRestConTroller {

    private final KhachSanService khachSanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return khachSanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return khachSanService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody KhachSanDTO khachSanDTO) {
        return khachSanService.insert(khachSanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody KhachSanDTO khachSanDTO) {
        return khachSanService.update(khachSanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return khachSanService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return khachSanService.findAll();
    }
}
