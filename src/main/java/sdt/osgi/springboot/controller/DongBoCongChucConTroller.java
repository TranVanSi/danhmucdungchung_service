package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DongBoCongChucService;

@RestController
@RequestMapping("/api/v1/congchuc")
@RequiredArgsConstructor
public class DongBoCongChucConTroller {

    private final DongBoCongChucService dongBoCongChucService;

    @GetMapping(value = {"/search", "/", ""})
    public Response findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                            @RequestParam(value = "offset", defaultValue = "0") int offset,
                            @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return dongBoCongChucService.findAll(ma, offset, limit);
    }

    @GetMapping(value = {"/count"})
    public String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search) {
        return dongBoCongChucService.countByMaOrTen(search);
    }

}
