package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.CauHinhBaoCaoDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.CauHinhBaoCaoService;

@RestController
@RequestMapping("/api/v1/baocao")
@RequiredArgsConstructor
public class CauHinhBaoCaoRestController {

    private final CauHinhBaoCaoService cauHinhBaoCaoService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return cauHinhBaoCaoService.findById(id);
    }

    @GetMapping(value = "/tenbang/{tenbang}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByTenBang(@PathVariable(value = "tenbang") String tenBang) {
        return cauHinhBaoCaoService.findByTenBang(tenBang);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody CauHinhBaoCaoDTO cauHinhBaoCaoDTO) {
        return cauHinhBaoCaoService.insert(cauHinhBaoCaoDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody CauHinhBaoCaoDTO cauHinhBaoCaoDTO) {
        return cauHinhBaoCaoService.update(cauHinhBaoCaoDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return cauHinhBaoCaoService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return cauHinhBaoCaoService.findAll();
    }

    @GetMapping(value = {"/countall"}, produces = MediaType.APPLICATION_JSON_VALUE)
    private Response countall(@RequestParam(value = "tenBang", defaultValue = "") String tenBang) {
        return cauHinhBaoCaoService.countAllTable(tenBang);
    }

    @GetMapping(value = {"/kiemtratenbang"}, produces = MediaType.APPLICATION_JSON_VALUE)
    private Response kiemTraTenBang(@RequestParam(value = "tenBang", defaultValue = "") String tenBang) {
        return cauHinhBaoCaoService.kiemTraTenBang(tenBang);
    }
}
