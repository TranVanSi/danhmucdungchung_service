package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DanTocService;

@RestController
@RequestMapping("/api/v1/dantoc")
@RequiredArgsConstructor
public class DanTocRestController {

    private final DanTocService danTocService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return danTocService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByMa(@PathVariable(value = "ma") String ma) {
        return danTocService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response getListAll() {
        return danTocService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DanTocDTO danTocDTO) throws Exception {
        return danTocService.insert(danTocDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DanTocDTO danTocDTO) throws Exception {
        return danTocService.update(danTocDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return danTocService.delete(id);
    }
}
