package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.VanBanPhapLyDTO;
import sdt.osgi.springboot.service.VanBanPhapLyService;

@RestController
@RequestMapping("/api/v1/vanbanphaply")
@RequiredArgsConstructor
public class VanBanPhapLyRestConTroller {

    private final VanBanPhapLyService vanBanPhapLyService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return vanBanPhapLyService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return vanBanPhapLyService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody VanBanPhapLyDTO vanBanPhapLyDTO) {
        return vanBanPhapLyService.insert(vanBanPhapLyDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody VanBanPhapLyDTO vanBanPhapLyDTO) throws Exception {
        return vanBanPhapLyService.update(vanBanPhapLyDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return vanBanPhapLyService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return vanBanPhapLyService.findAll();
    }
}
