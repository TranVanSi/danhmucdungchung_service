package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.QuocTichDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.QuocTichService;

@RestController
@RequestMapping("/api/v1/quoctich")
@RequiredArgsConstructor
public class QuocTichRestController {

    private final QuocTichService quocTichService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return quocTichService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return quocTichService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return quocTichService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody QuocTichDTO quocTichDTO) throws Exception {
        return quocTichService.insert(quocTichDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody QuocTichDTO quocTichDTO) throws Exception {
        return quocTichService.update(quocTichDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return quocTichService.delete(id);
    }
}
