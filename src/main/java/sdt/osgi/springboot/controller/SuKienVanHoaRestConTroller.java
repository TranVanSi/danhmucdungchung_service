package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.SuKienVanHoaDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.SuKienVanHoaService;

@RestController
@RequestMapping("/api/v1/sukienvanhoa")
@RequiredArgsConstructor
public class SuKienVanHoaRestConTroller {

    private final SuKienVanHoaService suKienVanHoaService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return suKienVanHoaService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return suKienVanHoaService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody SuKienVanHoaDTO suKienVanHoaDTO) {
        return suKienVanHoaService.insert(suKienVanHoaDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody SuKienVanHoaDTO suKienVanHoaDTO) {
        return suKienVanHoaService.update(suKienVanHoaDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return suKienVanHoaService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return suKienVanHoaService.findAll();
    }
}
