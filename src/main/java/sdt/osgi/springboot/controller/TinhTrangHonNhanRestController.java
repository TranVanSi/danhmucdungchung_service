package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.TinhTrangHonNhanDTO;
import sdt.osgi.springboot.service.TinhTrangHonNhanService;

@RestController
@RequestMapping("/api/v1/tinhtranghonnhan")
@RequiredArgsConstructor
public class TinhTrangHonNhanRestController {

    private final TinhTrangHonNhanService tinhTrangHonNhanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return tinhTrangHonNhanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return tinhTrangHonNhanService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return tinhTrangHonNhanService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody TinhTrangHonNhanDTO tinhTrangHonNhanDTO) throws Exception {
        return tinhTrangHonNhanService.insert(tinhTrangHonNhanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody TinhTrangHonNhanDTO tinhTrangHonNhanDTO) throws Exception {
        return tinhTrangHonNhanService.update(tinhTrangHonNhanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return tinhTrangHonNhanService.delete(id);
    }
}
