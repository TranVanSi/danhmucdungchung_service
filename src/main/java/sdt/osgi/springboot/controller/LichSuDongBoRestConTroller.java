package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dvcservice.LichSuDongBoService;

@RestController
@RequestMapping("/api/v1/lichsudongbo")
@RequiredArgsConstructor
public class LichSuDongBoRestConTroller {

    private final LichSuDongBoService lichSuDongBoService;

    @GetMapping(value = {"/search"})
    private Response findAll(@RequestParam(value = "offset", defaultValue = "0") int offset,
                                          @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return lichSuDongBoService.findAll(offset, limit);
    }

    @GetMapping(value = {"/count"})
    private Response count() {
        return lichSuDongBoService.count();
    }
}
