package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LoaiToChucCaNhanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.LoaiToChucCaNhanService;

@RestController
@RequestMapping("/api/v1/loaitochuccanhan")
@RequiredArgsConstructor
public class LoaiToChucCaNhanRestController {

    private final LoaiToChucCaNhanService loaiToChucCaNhanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return loaiToChucCaNhanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return loaiToChucCaNhanService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return loaiToChucCaNhanService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody LoaiToChucCaNhanDTO loaiToChucCaNhanDTO) throws Exception {
        return loaiToChucCaNhanService.insert(loaiToChucCaNhanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody LoaiToChucCaNhanDTO loaiToChucCaNhanDTO) throws Exception {
        return loaiToChucCaNhanService.update(loaiToChucCaNhanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return loaiToChucCaNhanService.delete(id);
    }
}
