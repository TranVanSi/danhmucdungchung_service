package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.model.DoanhNghiep2NganhNgheKinhDoanh;
import sdt.osgi.springboot.model.NganhNgheKinhDoanh;
import sdt.osgi.springboot.service.DoanhNghiep2NganhNgheKinhDoanhService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/doanhnghiep2nganhnghekinhdoanh")
@RequiredArgsConstructor
public class DoanhNghiep2NganhNgheKinhDoanhRestConTroller {

    private final DoanhNghiep2NganhNgheKinhDoanhService doanhNghiep2NganhNgheKinhDoanhService;

    @PostMapping("/createDoanhNghiepAndNganhNgheKinhDoanh/{doanhNghiepId}")
    public boolean createByDoanhNghiepAndNganhNgheKinhDoanh(@RequestBody List<NganhNgheKinhDoanh> nganhNgheKinhDoanhs,
                                           @PathVariable(value = "doanhNghiepId") long doanhNghiepId) {
        doanhNghiep2NganhNgheKinhDoanhService.createDoanhNghiep2NganhNgheKinhDOanh(doanhNghiepId, nganhNgheKinhDoanhs);
        return true;
    }

    @DeleteMapping("/deleteByDoanhNghiep/{doanhNghiepId}")
    public boolean deleteByDoanhNghiep(@PathVariable(value = "doanhNghiepId") long doanhNghiepId) {
        doanhNghiep2NganhNgheKinhDoanhService.deleteByDoanhNghiepId(doanhNghiepId);
        return true;
    }

    @GetMapping("/findByDoanhNghiepId/{doanhNghiepId}")
    public List<DoanhNghiep2NganhNgheKinhDoanh> findByDoanhNghiepId(@PathVariable(value = "doanhNghiepId") long doanhNghiepId) {
        return doanhNghiep2NganhNgheKinhDoanhService.findByDoanhNghiepId(doanhNghiepId);
    }
}
