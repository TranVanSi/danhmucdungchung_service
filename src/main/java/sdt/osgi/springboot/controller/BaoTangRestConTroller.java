package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.BaoTangDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.BaoTangService;

@RestController
@RequestMapping("/api/v1/baotang")
@RequiredArgsConstructor
public class BaoTangRestConTroller {

    private final BaoTangService baoTangService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return baoTangService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return baoTangService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody BaoTangDTO baoTangDTO) {
        return baoTangService.insert(baoTangDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody BaoTangDTO baoTangDTO) {
        return baoTangService.update(baoTangDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return baoTangService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return baoTangService.findAll();
    }
}
