package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.SanGolfDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.SanGolfService;

@RestController
@RequestMapping("/api/v1/sangolf")
@RequiredArgsConstructor
public class SanGolfRestConTroller {

    private final SanGolfService sanGolfService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return sanGolfService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return sanGolfService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody SanGolfDTO sanGolfDTO) {
        return sanGolfService.insert(sanGolfDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody SanGolfDTO sanGolfDTO) {
        return sanGolfService.update(sanGolfDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return sanGolfService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return sanGolfService.findAll();
    }
}
