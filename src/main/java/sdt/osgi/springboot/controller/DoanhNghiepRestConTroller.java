package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DoanhNghiepDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DoanhNghiepService;

@RestController
@RequestMapping("/api/v1/doanhnghiep")
@RequiredArgsConstructor
public class DoanhNghiepRestConTroller {

    private final DoanhNghiepService doanhNghiepService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response findById(@PathVariable(value = "id") long id) {
        return doanhNghiepService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response findByCode(@PathVariable(value = "ma") String ma) {
        return doanhNghiepService.findByMa(ma);
    }

    @GetMapping(value = "/maSoThue/{maSoThue}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response findByMaSoThue(@PathVariable(value = "maSoThue") String maSoThue) {
        return doanhNghiepService.findByMaSoThue(maSoThue);
    }

    @GetMapping(value = {"/search", "/", ""})
    public Response findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                             @RequestParam(value = "offset", defaultValue = "0") int offset,
                             @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return doanhNghiepService.findAll(ma, offset, limit);
    }

    @GetMapping(value = {"/count"})
    public String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search,
                                 @RequestParam(value = "chaId", defaultValue = "0") long chaId) {
        return doanhNghiepService.countByMaOrTen(search, chaId);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Response insert(@RequestBody DoanhNghiepDTO doanhNghiepDTO) throws Exception {
        return doanhNghiepService.insert(doanhNghiepDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Response update(@RequestBody DoanhNghiepDTO doanhNghiepDTO) throws Exception {
        return doanhNghiepService.update(doanhNghiepDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response delete(@PathVariable long id) throws Exception {
        return doanhNghiepService.delete(id);
    }
}
