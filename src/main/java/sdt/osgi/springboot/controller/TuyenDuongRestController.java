package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.dto.TuyenDuongDTO;
import sdt.osgi.springboot.service.TuyenDuongService;

@RestController
@RequestMapping("/api/v1/tuyenduong")
@RequiredArgsConstructor
public class TuyenDuongRestController {

    private final TuyenDuongService tuyenDuongService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return tuyenDuongService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return tuyenDuongService.findByMa(ma);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return tuyenDuongService.findAll();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody TuyenDuongDTO tuyenDuongDTO) throws Exception {
        return tuyenDuongService.insert(tuyenDuongDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody TuyenDuongDTO tuyenDuongDTO) throws Exception {
        return tuyenDuongService.update(tuyenDuongDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return tuyenDuongService.delete(id);
    }

    @GetMapping(value = "/chaId/{chaId}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaId(@PathVariable(value = "chaId") long chaId) {
        return tuyenDuongService.findByChaId(chaId);
    }
}
