package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DonViHanhChinhService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/dvhc")
@RequiredArgsConstructor
public class DonViHanhChinhRestConTroller {

    private final DonViHanhChinhService donViHanhChinhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return donViHanhChinhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return donViHanhChinhService.findByMa(ma);
    }

    @GetMapping(value = {"/search", "/", ""})
    private Response findAll(@RequestParam(value = "ma", defaultValue = "") String ma,
                                            @RequestParam(value = "chaId", defaultValue = "0") long chaId,
                                            @RequestParam(value = "offset", defaultValue = "0") int offset,
                                            @RequestParam(value = "limit", defaultValue = "10") int limit) {
        return donViHanhChinhService.findAll(ma, chaId, offset, limit);
    }

    @GetMapping(value = "/child/{chaId}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaId(@PathVariable(value = "chaId") long chaId) {
        return donViHanhChinhService.findByChaId(chaId);
    }

    @GetMapping(value = "/chaMa/{chaMa}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByChaMa(@PathVariable(value = "chaMa") String chaMa) {
        return donViHanhChinhService.findByChaMa(chaMa);
    }

    @GetMapping(value = "/capCoQuanQuanLyId/{capId}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCapCoQuanId(@PathVariable long capId) {
        return donViHanhChinhService.findByCapCoQuanId(capId);
    }

    @GetMapping(value = {"/count"})
    private String countBySearch(@RequestParam(value = "ma", defaultValue = "") String search,
                                 @RequestParam(value = "chaId", defaultValue = "0") long chaId) {

        return donViHanhChinhService.countByMaOrTen(search, chaId);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DonViHanhChinhDTO donViHanhChinhDTO) throws Exception {
        return donViHanhChinhService.insert(donViHanhChinhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DonViHanhChinhDTO donViHanhChinhDTO) throws Exception {
        return donViHanhChinhService.update(donViHanhChinhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return donViHanhChinhService.delete(id);
    }
    @GetMapping("/listAll")
    public Response getListAllChucVu() throws Exception {

        return donViHanhChinhService.getListDonViHanhChinh();
    }

}
