package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DoKhanVanBanDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DoKhanVanBanService;

@RestController
@RequestMapping("/api/v1/dokhan")
@RequiredArgsConstructor
public class DoKhanVanBanRestConTroller {

    private final DoKhanVanBanService doKhanVanBanService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return doKhanVanBanService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return doKhanVanBanService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DoKhanVanBanDTO doKhanVanBanDTO) throws Exception {
        return doKhanVanBanService.insert(doKhanVanBanDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DoKhanVanBanDTO doKhanVanBanDTO) throws Exception {
        return doKhanVanBanService.update(doKhanVanBanDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return doKhanVanBanService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return doKhanVanBanService.findAll();
    }
}
