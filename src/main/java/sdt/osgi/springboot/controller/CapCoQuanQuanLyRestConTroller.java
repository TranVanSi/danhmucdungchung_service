package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.CapCoQuanQuanLyDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.CapCoQuanQuanLyService;

@RestController
@RequestMapping("/api/v1/capcqql")
@RequiredArgsConstructor
public class CapCoQuanQuanLyRestConTroller {

    private final CapCoQuanQuanLyService capCoQuanQuanLyService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return capCoQuanQuanLyService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return capCoQuanQuanLyService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody CapCoQuanQuanLyDTO capCoQuanQuanLyDTO) throws Exception {
        return capCoQuanQuanLyService.insert(capCoQuanQuanLyDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody CapCoQuanQuanLyDTO capCoQuanQuanLyDTO) throws Exception {
        return capCoQuanQuanLyService.update(capCoQuanQuanLyDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) throws Exception {
        return capCoQuanQuanLyService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return capCoQuanQuanLyService.findAll();
    }
}
