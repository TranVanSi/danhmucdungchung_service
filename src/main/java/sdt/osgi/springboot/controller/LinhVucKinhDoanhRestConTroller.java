package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.LinhVucKinhDoanhDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.LinhVucKinhDoanhService;

@RestController
@RequestMapping("/api/v1/linhvuckinhdoanh")
@RequiredArgsConstructor
public class LinhVucKinhDoanhRestConTroller {

    private final LinhVucKinhDoanhService linhVucKinhDoanhService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return linhVucKinhDoanhService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return linhVucKinhDoanhService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody LinhVucKinhDoanhDTO linhVucKinhDoanhDTO) {
        return linhVucKinhDoanhService.insert(linhVucKinhDoanhDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody LinhVucKinhDoanhDTO linhVucKinhDoanhDTO) {
        return linhVucKinhDoanhService.update(linhVucKinhDoanhDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return linhVucKinhDoanhService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return linhVucKinhDoanhService.findAll();
    }
}
