package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sdt.osgi.springboot.dao.DanTocsRepository;
import sdt.osgi.springboot.dto.CapDonViHanhChinhDTO;
import sdt.osgi.springboot.dto.DanTocDTO;
import sdt.osgi.springboot.dto.DonViHanhChinhDTO;
import sdt.osgi.springboot.mapper.DanTocMapper;
import sdt.osgi.springboot.service.CapDonViHanhChinhService;
import sdt.osgi.springboot.service.DanTocService;
import sdt.osgi.springboot.service.LoaiVanBanService;

@Service
@RequiredArgsConstructor
public class UploadValidate {
    private final DanTocService danTocService;
    private final DanTocsRepository danTocsRepository;
    private final DanTocMapper danTocMapper;
    private final CapDonViHanhChinhService capDonViHanhChinhService;

    public boolean validate(DanTocDTO itemDraft, DanTocDTO itemByMa) throws Exception {

        boolean valid = true;
        long danTocId = itemDraft.getId();

        if ((danTocId == 0 && itemByMa != null && itemDraft.getMa().equals(itemByMa.getMa())) ||
                (danTocId > 0 && itemByMa != null && danTocId != itemByMa.getId())) {
            valid = false;
        }

        return valid;
    }

}
