package sdt.osgi.springboot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sdt.osgi.springboot.dto.DiTichLichSuDTO;
import sdt.osgi.springboot.dto.Response;
import sdt.osgi.springboot.service.DiTichLichSuService;

@RestController
@RequestMapping("/api/v1/ditichlichsu")
@RequiredArgsConstructor
public class DiTichLichSuRestConTroller {

    private final DiTichLichSuService diTichLichSuService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findById(@PathVariable(value = "id") long id) {
        return diTichLichSuService.findById(id);
    }

    @GetMapping(value = "/ma/{ma}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findByCode(@PathVariable(value = "ma") String ma) {
        return diTichLichSuService.findByMa(ma);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response insert(@RequestBody DiTichLichSuDTO diTichLichSuDTO) {
        return diTichLichSuService.insert(diTichLichSuDTO);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response update(@RequestBody DiTichLichSuDTO diTichLichSuDTO) {
        return diTichLichSuService.update(diTichLichSuDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    private Response delete(@PathVariable long id) {
        return diTichLichSuService.delete(id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private Response findAll() {
        return diTichLichSuService.findAll();
    }
}
